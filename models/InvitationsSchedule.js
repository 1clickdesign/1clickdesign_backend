import { Sequelize } from "sequelize";
import db from "../config/Database.js";

const { DataTypes } = Sequelize;

const InvitationsSchedule = db.define(
  "invitations_schedule",
  {
    id: {
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4,
      primaryKey: true,
      unique: true,
      allowNull: false,
    },
    passcode: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    available_showrooms: {
      type: DataTypes.STRING,
      allowNull: true,
    },
    invitation_showrooms: {
      type: DataTypes.JSON,
      allowNull: true,
    },
    invitation_participant: {
      type: DataTypes.JSON,
      allowNull: true,
    },
    can_share: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: false,
    },
    access_expiration_date: {
      type: DataTypes.DATE,
      allowNull: false,
      // defaultValue: DataTypes.NOW,
    },
    schedule_send_date: {
      type: DataTypes.DATE,
      allowNull: true,
    },

    //virtual_experience_id
    //client_id
    //customer_id
    //user_id
    //sent_email

    created_by: {
      type: DataTypes.JSON,
      defaultValue: {
        id: null,
        name: "System",
        role: null,
      },
      allowNull: false,
    },
    updated_by: {
      type: DataTypes.JSON,
      allowNull: true,
    },
    created_at: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: DataTypes.NOW,
    },
    updated_at: {
      type: DataTypes.DATE,
      allowNull: true,
    },
  },
  {
    freezeTableName: true,
    timestamps: true,
    createdAt: "created_at",
    updatedAt: "updated_at",
  }
);

export default InvitationsSchedule;
