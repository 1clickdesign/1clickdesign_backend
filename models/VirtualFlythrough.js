import { Sequelize } from "sequelize";
import db from "../config/Database.js";

const { DataTypes } = Sequelize;

const VirtualFlythrough = db.define("virtual_flythrough", {
  id: {
    type: DataTypes.UUID,
    defaultValue: DataTypes.UUIDV4,
    primaryKey: true,
    unique: true,
    allowNull: false,
  },
  name: {
    type: DataTypes.STRING,
    allowNull: false,
    primaryKey: false,
    unique: true,
  },
  showroom_url: {
    type: DataTypes.TEXT,
    allowNull: true,
  },
  thumbnail_url: {
    type: DataTypes.TEXT,
    allowNull: false,
  },
  description: {
    type: DataTypes.TEXT,
    allowNull: false,
  },

  fly_through_video_urls: {
    type: DataTypes.JSON,
    allowNull: false,
  },

  schedule_send_date: {
    type: DataTypes.DATE,
    allowNull: true
  },
  //type_id
  //client_id

  status: {
    type: DataTypes.ENUM,
    values: ["Pending", "Active", "Inactive"],
    defaultValue: "Pending",
    allowNull: false,
  },
  created_by: {
    type: DataTypes.JSON,
    defaultValue: {
      id: null,
      name: "System",
      role: null,
    },
    allowNull: false,
  },
  updated_by: {
    type: DataTypes.JSON,
    allowNull: true,
  },
  created_at: {
    type: DataTypes.DATE,
    allowNull: false,
    defaultValue: DataTypes.NOW,
  },
  updated_at: {
    type: DataTypes.DATE,
    allowNull: true,
  },
});

export default VirtualFlythrough;
