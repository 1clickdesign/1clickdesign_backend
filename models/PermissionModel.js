import { Sequelize } from "sequelize";
import db from "../config/Database.js";

const { DataTypes } = Sequelize;

const Permissions = db.define(
  "permissions",
  {
    id: {
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4,
      primaryKey: true,
      unique: true,
      allowNull: false,
    },
    name: {
      type: DataTypes.STRING,
      allowNull: false,
      primaryKey: false,
      unique: true,
    },
    description: {
      type: DataTypes.TEXT,
      allowNull: true,
    },
    status: {
      type: DataTypes.ENUM,
      defaultValue: "Pending",
      values: ["Active", "Inactive", "Pending"],
      allowNull: false,
    },
    category: {
      type: DataTypes.ENUM,
      defaultValue: "1CD",
      values: ["1CD", "Developer", "Client"],
      allowNull: false,
    },
    created_by: {
      type: DataTypes.JSON,
      defaultValue: {
        id: null,
        name: "System",
        role: null,
      },
      allowNull: false,
    },
    updated_by: {
      type: DataTypes.JSON,
      allowNull: true,
    },
    created_at: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: DataTypes.NOW,
    },
    updated_at: {
      type: DataTypes.DATE,
      allowNull: true,
    },
  },
  {
    hooks: {
      afterCreate: async (permission, options) => {
        db.models.notifications.create({
          title: "Permission Created",
          content: `Permission named ${permission.dataValues.name} was successfully created`,
          user_id: permission.created_by.id,
          created_by: permission.created_by
        })
      },
    },
    freezeTableName: true,
    timestamps: true,
    createdAt: "created_at",
    updatedAt: "updated_at",
  }
);

export default Permissions;
