import { Sequelize } from "sequelize";
import db from "../config/Database.js";

const { DataTypes } = Sequelize;

const ProductNodes = db.define(
  "product_nodes",
  {
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true,
      unique: true,
      allowNull: false,
    },
    name: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    model: {
      type: DataTypes.STRING,
      allowNull: true
    },
    showroom_link: {
      type: DataTypes.STRING,
      allowNull: true
    },
    pitch: {
      type: DataTypes.DECIMAL(34, 30),
      defaultValue: 0,
      allowNull: false,
    },
    x: {
      type: DataTypes.DECIMAL(34, 30),
      defaultValue: 0,
      allowNull: false,
    },
    y: {
      type: DataTypes.DECIMAL(34, 30),
      defaultValue: 0,
      allowNull: false,
    },
    yaw: {
      type: DataTypes.DECIMAL(34, 30),
      defaultValue: 0,
      allowNull: false,
    },
    z: {
      type: DataTypes.DECIMAL(34, 30),
      defaultValue: 0,
      allowNull: false,
    },

    //product_id
    
    status: {
      type: DataTypes.ENUM,
      defaultValue: "Pending",
      values: ["Active", "Pending", "Inactive"],
      allowNull: false,
    },
    created_by: {
      type: DataTypes.JSON,
      defaultValue: {
        id: null,
        name: "System",
        role: null,
      },
      allowNull: false,
    },
    updated_by: {
      type: DataTypes.JSON,
      allowNull: true,
    },
    created_at: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: DataTypes.NOW,
    },
    updated_at: {
      type: DataTypes.DATE,
      allowNull: true,
    },
  }, {
  freezeTableName: true,
  timestamps: true,
  createdAt: "created_at",
  updatedAt: "updated_at"
}
)

export default ProductNodes;