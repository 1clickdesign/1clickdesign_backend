import { Sequelize } from "sequelize";
import db from "../config/Database.js";

const { DataTypes } = Sequelize;

const InvitationFlythrough = db.define(
  "invitation_flythrough",
  {
    id: {
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4,
      primaryKey: true,
      unique: true,
      allowNull: false,
    },
    available_showrooms: {
      type: DataTypes.STRING,
      allowNull: true,
    },
    invitation_showrooms: {
      type: DataTypes.JSON,
      allowNull: true,
    },
    invitation_participant: {
      type: DataTypes.JSON,
      allowNull: true,
    },
    can_share: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: false,
    },
    access_expiration_date: {
      type: DataTypes.DATE,
      allowNull: false,
      // defaultValue: DataTypes.NOW,
    },
    schedule_send_date: {
      type: DataTypes.DATE,
      allowNull: true,
    },
    showroom_url: {
      type: DataTypes.STRING,
      allowNull: true,
    },
    is_viewed: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: false,
    },

    //virtual_experience_id
    //client_id
    //customer_id
    //user_id
    //sent_email

    created_by: {
      type: DataTypes.JSON,
      defaultValue: {
        id: null,
        name: "System",
        role: null,
      },
      allowNull: false,
    },
    created_at: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: DataTypes.NOW,
    },
  },
  {
    freezeTableName: true,
    timestamps: true,
    createdAt: "created_at",
    updatedAt: "updated_at",
  }
);

export default InvitationFlythrough;
