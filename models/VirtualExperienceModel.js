import { Sequelize } from "sequelize";
import db from "../config/Database.js";

const { DataTypes } = Sequelize;

const VirtualExperiences = db.define(
  "virtual_experiences",
  {
    id: {
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4,
      primaryKey: true,
      unique: true,
      allowNull: false,
    },
    name: {
      type: DataTypes.STRING,
      allowNull: false,
      primaryKey: false,
      unique: true,
    },
    showroom_url: {
      type: DataTypes.TEXT,
      allowNull: false,
    },
    thumbnail_url: {
      type: DataTypes.TEXT,
      allowNull: false,
    },
    photo_urls: {
      type: DataTypes.TEXT,
      allowNull: false,
    },
    description: {
      type: DataTypes.TEXT,
      allowNull: false,
    },
    show_hotspot: {
      type: DataTypes.BOOLEAN,
      defaultValue: false,
      allowNull: false,
    },
    is_third_party_space: {
      type: DataTypes.BOOLEAN,
      defaultValue: false,
      allowNull: false,
    },
    access_expiration_date: {
      type: DataTypes.DATE,
      allowNull: true,
    },
    link_name: {
      type: DataTypes.STRING,
      primaryKey: false,
      unique: true,
      allowNull: false
    },
    fly_through_video_urls: {
      type: DataTypes.JSON,
      allowNull: false,
    },
    dealer_locator_url: {
      type: DataTypes.STRING,
      allowNull: false
    },

    //background_music_id
    //type_id
    //business_type_id
    //access_type_id
    //client_id
    //category_id
    //sales_id

    status: {
      type: DataTypes.ENUM,
      values: ["Pending","Active","Inactive"],
      defaultValue: "Pending",
      allowNull: false,
    },
    created_by: {
      type: DataTypes.JSON,
      defaultValue: {
        id: null,
        name: "System",
        role: null,
      },
      allowNull: false,
    },
    updated_by: {
      type: DataTypes.JSON,
      allowNull: true,
    },
    created_at: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: DataTypes.NOW,
    },
    updated_at: {
      type: DataTypes.DATE,
      allowNull: true,
    },
  }, {
    hooks: {
      afterCreate: async (virtualExperiences, options) => {
        db.models.notifications.create({
          title: "Virtual Experience Created",
          content: `Virtual Experience named ${virtualExperiences.dataValues.name} with ID ${virtualExperiences.dataValues.id} was successfully created`,
          user_id: virtualExperiences.created_by.id,
          created_by: virtualExperiences.created_by,
        });
      },
    },
  freezeTableName: true,
  timestamps: true,
  createdAt: "created_at",
  updatedAt: "updated_at"
}
)

export default VirtualExperiences;