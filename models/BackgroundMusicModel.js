import { Sequelize } from "sequelize";
import db from "../config/Database.js";
import VirtualExperiences from "./VirtualExperienceModel.js";

const { DataTypes } = Sequelize;

const BackgroundMusics = db.define(
  "background_musics",
  {
    id: {
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4,
      primaryKey: true,
      unique: true,
      allowNull: false,
    },
    name: {
      type: DataTypes.STRING,
      allowNull: false,
      primaryKey: false,
      unique: true,
    },
    music_url: {
      type: DataTypes.TEXT,
      allowNull: false,
    },
    status: {
      type: DataTypes.ENUM,
      defaultValue: "Pending",
      values: ["Active", "Pending", "Inactive"],
      allowNull: false,
    },
    created_by: {
      type: DataTypes.JSON,
      defaultValue: {
        id: null,
        name: "System",
        role: null,
      },
      allowNull: false,
    },
    updated_by: {
      type: DataTypes.JSON,
      allowNull: true,
    },
    created_at: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: DataTypes.NOW,
    },
    updated_at: {
      type: DataTypes.DATE,
      allowNull: true,
    },
  },
  {
    hooks: {
      afterCreate: async (music, options) => {
        db.models.notifications.create({
          title: "Background Music Created",
          content: `Background Music named ${music.dataValues.name} with url ${music.dataValues.music_url} was successfully created`,
          user_id: music.created_by.id,
          created_by: music.created_by
        })
      },
    },
    freezeTableName: true,
    timestamps: true,
    createdAt: "created_at",
    updatedAt: "updated_at",
  }
);

export default BackgroundMusics;
