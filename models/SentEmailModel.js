import { Sequelize } from "sequelize";
import db from "../config/Database.js";

const { DataTypes } = Sequelize;

const SentEmails = db.define(
  "sent_emails",
  {
    id: {
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4,
      primaryKey: true,
      unique: true,
      allowNull: false,
    },
    to: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    sender_name: {
      type: DataTypes.STRING,
      defaultValue: "1 Click Design",
      allowNull: false,
    },
    from: {
      type: DataTypes.TEXT,
      allowNull: false,
    },
    subject: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    bcc: {
      type: DataTypes.TEXT,
      allowNull: false,
    },
    cc: {
      type: DataTypes.TEXT,
      allowNull: false,
    },
    reply_to: {
      type: DataTypes.TEXT,
      allowNull: false,
    },
    headers: {
      type: DataTypes.JSON,
      allowNull: true,
    },
    attachments: {
      type: DataTypes.TEXT,
      allowNull: true,
    },
    tags: {
      type: DataTypes.TEXT,
      allowNull: true,
    },
    html: {
      type: DataTypes.TEXT,
      allowNull: false,
    },
    text: {
      type: DataTypes.TEXT,
      allowNull: false,
    },
    content: {
      type: DataTypes.TEXT,
      allowNull: false,
    },
    greeting: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    regards: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    response: {
      type: DataTypes.INTEGER,
      defaultValue: 200,
      allowNull: true,
    },
    is_viewed: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: false,
    },
    email_id: {
      type: DataTypes.STRING,
      allowNull: true,
    },
    type: {
      type: DataTypes.STRING,
      allowNull: true,
      defaultValue: "Invitation"
    },
    status: {
      type: DataTypes.ENUM,
      values: ["Delivered","Sent","Delivered Delay","Complained","Bounced","Opened","Clicked"],
      defaultValue: "Sent",
      allowNull: true,
    },
    
    //user_id
    //customer_id

    created_by: {
      type: DataTypes.JSON,
      defaultValue: {
        id: null,
        name: "System",
        role: null,
      },
      allowNull: false,
    },
    updated_by: {
      type: DataTypes.JSON,
      allowNull: true,
    },
    created_at: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: DataTypes.NOW,
    },
    updated_at: {
      type: DataTypes.DATE,
      allowNull: true,
    },
  }, {
  freezeTableName: true,
  timestamps: true,
  createdAt: "created_at",
  updatedAt: "updated_at"
}
)

export default SentEmails;