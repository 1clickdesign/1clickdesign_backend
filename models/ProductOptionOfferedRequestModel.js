import { Sequelize } from "sequelize";
import db from "../config/Database.js";
import ProductOptionOffered from "./ProductOptionOfferedModel.js";

const { DataTypes } = Sequelize;

const ProductOptionOfferedRequests = db.define(
  "product_option_offered_requests",
  {
    id: {
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4,
      primaryKey: true,
      unique: true,
      allowNull: false,
    },
    
    //product_option_offered_id
    //sent_email_id
    //customer_id
    //assigned_user_id
    //virtual_experience_id
    //client_id

    created_by: {
      type: DataTypes.JSON,
      defaultValue: {
        id: null,
        name: "System",
        role: null,
      },
      allowNull: false,
    },
    updated_by: {
      type: DataTypes.JSON,
      allowNull: true,
    },
    created_at: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: DataTypes.NOW,
    },
    updated_at: {
      type: DataTypes.DATE,
      allowNull: true,
    },
  }, {
  freezeTableName: true,
  timestamps: true,
  createdAt: "created_at",
  updatedAt: "updated_at"
}
)

export default ProductOptionOfferedRequests;