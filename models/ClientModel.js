import { Sequelize } from "sequelize";
import db from "../config/Database.js";

const { DataTypes } = Sequelize;

const Clients = db.define(
  "clients",
  {
    id: {
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4,
      primaryKey: true,
      unique: true,
      allowNull: false,
    },
    name: {
      type: DataTypes.STRING,
      allowNull: false,
      primaryKey: false,
      unique: true,
    },
    email: {
      type: DataTypes.STRING,
      allowNull: false,
      primaryKey: false,
      unique: true,
    },
    phone_number: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    country: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    city: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    state: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    region: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    zip_code: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    place_id: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    latitude: {
      type: DataTypes.FLOAT(14, 10),
      allowNull: false,
      defaultValue: 0,
    },
    longitude: {
      type: DataTypes.FLOAT(14, 10),
      allowNull: false,
      defaultValue: 0,
    },
    logo_url: {
      type: DataTypes.TEXT,
      allowNull: false,
    },
    website_url: {
      type: DataTypes.TEXT,
      allowNull: false,
    },
    description: {
      type: DataTypes.TEXT,
      allowNull: false,
    },
    user_limit: {
      type: DataTypes.INTEGER,
      defaultValue: 1,
      allowNull: false
    },
    additional_user_charge: {
      type: DataTypes.FLOAT,
      defaultValue: 0,
      allowNull: false
    },
    additional_minutes_charge_every: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0,
    },
    additional_minutes_charge: {
      type: DataTypes.FLOAT,
      allowNull: false,
      defaultValue: 0,
    },
    free_video_conference_minutes: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0,
    },
    additional_email_charge_every: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0,
    },
    additional_email_charge: {
      type: DataTypes.FLOAT,
      allowNull: false,
      defaultValue: 0,
    },
    free_emails: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0,
    },
    status: {
      type: DataTypes.ENUM,
      defaultValue: "Pending",
      values: ["Active", "Pending", "Inactive"],
      allowNull: false,
    },
    automatic_approved_requests: {
      type: DataTypes.BOOLEAN,
      defaultValue: true,
      allowNull: false,
    },



    //account_holder_id
    //package_id
    //client_category_id

    created_by: {
      type: DataTypes.JSON,
      defaultValue: {
        id: null,
        name: "System",
        role: null,
      },
      allowNull: false,
    },
    updated_by: {
      type: DataTypes.JSON,
      allowNull: true,
    },
    created_at: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: DataTypes.NOW,
    },
    updated_at: {
      type: DataTypes.DATE,
      allowNull: true,
    },
  }, {
  freezeTableName: true,
  timestamps: true,
  createdAt: "created_at",
  updatedAt: "updated_at"
}
)

export default Clients;