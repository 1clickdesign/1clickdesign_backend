import { Sequelize } from "sequelize";
import db from "../config/Database.js";

const { DataTypes } = Sequelize;

const Products = db.define(
  "products",
  {
    id: {
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4,
      primaryKey: true,
      unique: true,
      allowNull: false,
    },
    name: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    sku: {
      type: DataTypes.STRING
    },
    price: {
      type: DataTypes.FLOAT,
      allowNull: false,
    },
    discount: {
      type: DataTypes.FLOAT,
      allowNull: false,
      defaultValue: 0
    },
    model: {
      type: DataTypes.STRING,
      primaryKey: false,
      unique: true,
      allowNull: false,
    },
    description: {
      type: DataTypes.TEXT,
      allowNull: true,
    },
    nft_link: {
      type: DataTypes.TEXT,
      allowNull: false,
    },
    ecommerce_website_url: {
      type: DataTypes.TEXT,
      allowNull: false,
    },
    anchor: {
      type: DataTypes.ENUM,
      defaultValue: "Floor",
      values: ["Floor", "Wall"],
      allowNull: false,
    },
    product_stock_api: {
      type: DataTypes.TEXT,
      allowNull: true,
    },
    dimension_height: {
      type: DataTypes.FLOAT,
      defaultValue: 0,
      allowNull: false,
    },
    dimension_width: {
      type: DataTypes.FLOAT,
      defaultValue: 0,
      allowNull: false,
    },
    dimension_length: {
      type: DataTypes.FLOAT,
      defaultValue: 0,
      allowNull: false,
    },
    dimension_depth: {
      type: DataTypes.FLOAT,
      defaultValue: 0,
      allowNull: false,
    },
    dimension_diameter: {
      type: DataTypes.FLOAT,
      defaultValue: 0,
      allowNull: false,
    },
    weight: {
      type: DataTypes.FLOAT,
      defaultValue: 0,
      allowNull: false,
    },
    dimension_unit: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    weight_unit: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    finish: {
      type: DataTypes.STRING
    },
    materials: {
      type: DataTypes.STRING
    },
    variations: {
      type: DataTypes.STRING
    },
    color: {
      type: DataTypes.STRING
    },
    category: {
      type: DataTypes.STRING
    },
    brand: {
      type: DataTypes.STRING
    },
    application: {
      type: DataTypes.STRING
    },
    manufacturer: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    product_3d_glb_url: {
      type: DataTypes.TEXT,
      allowNull: false,
    },
    product_3d_image_urls: {
      type: DataTypes.TEXT,
      allowNull: false,
    },
    image_urls: {
      type: DataTypes.TEXT,
      allowNull: false,
    },
    in_stock: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: true
    },
    show_price: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: true
    },
    status: {
      type: DataTypes.ENUM,
      defaultValue: "Pending",
      values: ["Active", "Pending", "Inactive"],
      allowNull: false,
    },
    photo_video_url: {
      type: DataTypes.TEXT,
      allowNull: false,
    },

    //real_estate_id
    //virtual_experience_id
    //client_id

    created_by: {
      type: DataTypes.JSON,
      defaultValue: {
        id: null,
        name: "System",
        role: null,
      },
      allowNull: false,
    },
    updated_by: {
      type: DataTypes.JSON,
      allowNull: true,
    },
    created_at: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: DataTypes.NOW,
    },
    updated_at: {
      type: DataTypes.DATE,
      allowNull: true,
    },
  }, {
  freezeTableName: true,
  timestamps: true,
  createdAt: "created_at",
  updatedAt: "updated_at"
}
)

export default Products;