import { Sequelize } from "sequelize"
import db from "../config/Database.js"

const { DataTypes } = Sequelize

const AnalyticsVirtualFlythrough = db.define(
  "analytics_virtual_flythrough",
  {
    id: {
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4,
      primaryKey: true,
      unique: true,
      allowNull: false,
    },
    video_duration: {
      type: DataTypes.FLOAT,
      allowNull: false,
    },
    watch_duration: {
      type: DataTypes.FLOAT,
      allowNull: false,
    },
    isFinish: {
      type: DataTypes.BOOLEAN,
      defaultValue: false,
      allowNull: true,
    },

    //customer_id
    //virtual_flythrough
    //invitation_flythrough
    created_by: {
      type: DataTypes.JSON,
      defaultValue: {
        id: null,
        name: "System",
        role: null,
      },
      allowNull: false,
    },
    updated_by: {
      type: DataTypes.JSON,
      allowNull: true,
    },
    created_at: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: DataTypes.NOW,
    },
    updated_at: {
      type: DataTypes.DATE,
      allowNull: true,
    },
  },
  {
    freezeTableName: true,
    timestamps: true,
    createdAt: "created_at",
    updatedAt: "updated_at",
  },
)

export default AnalyticsVirtualFlythrough
