import { Sequelize } from "sequelize";
import db from "../config/Database.js";

const { DataTypes } = Sequelize;

const RealEstates = db.define(
  "real_estates",
  {
    id: {
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4,
      primaryKey: true,
      unique: true,
      allowNull: false,
    },
    name: {
      type: DataTypes.STRING,
      defaultValue: "",
      allowNull: false,
    },
    amenities: {
      type: DataTypes.TEXT,
      allowNull: false,
      defaultValue: "[]"
    },
    unit_amenities: {
      type: DataTypes.TEXT,
      allowNull: false,
      defaultValue: "[]"
    },
    price: {
      type: DataTypes.FLOAT,
      allowNull: true,
    },
    unit_description: {
      type: DataTypes.TEXT,
      allowNull: true,
      defaultValue: "[]"
    },
    type: {
      type: DataTypes.ENUM,
      defaultValue: "For Rent",
      values: ["For Rent", "For Lease", "For Sale"],
      allowNull: false,
    },
    description: {
      type: DataTypes.TEXT,
      allowNull: false,
      defaultValue: "[]"
    },
    terms: {
      type: DataTypes.TEXT,
      allowNull: true,
      defaultValue: "[]"
    },
    location: {
      type: DataTypes.TEXT,
      allowNull: false,
    },
    floor_plan_url: {
      type: DataTypes.TEXT,
      allowNull: false,
    },
    contact_property: {
      type: DataTypes.JSON,
      allowNull: false,
    },

    //virtual_experience_id
    //client_id


    created_by: {
      type: DataTypes.JSON,
      defaultValue: {
        id: null,
        name: "System",
        role: null,
      },
      allowNull: false,
    },
    updated_by: {
      type: DataTypes.JSON,
      allowNull: true,
    },
    created_at: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: DataTypes.NOW,
    },
    updated_at: {
      type: DataTypes.DATE,
      allowNull: true,
    },
  }, {
  freezeTableName: true,
  timestamps: true,
  createdAt: "created_at",
  updatedAt: "updated_at"
}
)

export default RealEstates;