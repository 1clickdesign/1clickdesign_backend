import express from "express"
import multer from "multer"
import multerS3 from "multer-s3"
import { S3Client, S3 } from "@aws-sdk/client-s3"
import {
  check_if_permission_unique_name,
  create_permission,
  delete_permission,
  edit_permission,
  get_all_permissions,
  get_all_permissions_option,
  get_permission,
} from "../controllers/AdminControllers/Permission.js"
import {
  check_if_role_unique_name,
  create_role,
  delete_role,
  edit_role,
  get_all_roles,
  get_all_roles_option,
  get_role,
} from "../controllers/AdminControllers/Role.js"
import {
  check_if_access_type_unique_name,
  create_access_type,
  delete_access_type,
  edit_access_type,
  get_access_type,
  get_all_access_types,
  get_all_access_types_option,
} from "../controllers/AdminControllers/VirtualExperienceAccessType.js"
import {
  check_if_business_type_unique_name,
  create_business_type,
  delete_business_type,
  edit_business_type,
  get_all_business_types,
  get_all_business_types_option,
  get_business_type,
} from "../controllers/AdminControllers/VirtualExperienceBusinessType.js"
import {
  check_if_type_unique_name,
  create_type,
  delete_type,
  edit_type,
  get_all_types,
  get_all_types_option,
  get_type,
} from "../controllers/AdminControllers/VirtualExperienceType.js"
import {
  check_if_client_unique_name,
  create_client,
  delete_client,
  edit_client,
  edit_client_with_file,
  get_all_clients,
  get_all_clients_option,
  get_client,
  get_client_account_holder,
} from "../controllers/AdminControllers/Client.js"
import {
  change_password,
  check_if_user_unique_name,
  create_user,
  delete_user,
  edit_user,
  edit_user_with_file,
  get_all_pending_users,
  get_all_users,
  get_all_users_location,
  get_all_users_option,
  get_my_backup_users,
  get_user,
  get_user_by_email,
  update_user_online,
  update_user_status,
} from "../controllers/AdminControllers/User.js"
import {
  create_customer_group,
  delete_customer_group,
  get_all_customer_groups,
  get_all_customer_groups_option,
  get_customer_group,
} from "../controllers/AdminControllers/CustomerGroup.js"
import {
  get_dashboard_information,
  get_sales_dashboard,
} from "../controllers/AdminControllers/Dashboard.js"
import {
  create_virtual_experience,
  create_virtual_experience_v2,
  delete_virtual_experience,
  edit_virtual_experience,
  edit_virtual_experience_v2,
  get_all_virtual_experience_option,
  get_all_virtual_experiences,
  get_virtual_experience,
} from "../controllers/AdminControllers/VirtualExperience.js"
import {
  check_if_product_unique_name,
  create_product,
  create_product_v2,
  delete_product,
  edit_product,
  get_all_product_option,
  get_all_products,
  get_all_product_names,
  get_product,
  get_product_show_price,
  update_product_show_price,
  uploadProducts,
} from "../controllers/AdminControllers/Product.js"
import {
  create_option_offered,
  get_all_option_offered,
  get_all_option_offered_option,
  get_option_offered,
} from "../controllers/AdminControllers/OptionOffered.js"
import {
  check_if_background_music_unique_name,
  create_background_music,
  delete_background_music,
  edit_background_music,
  edit_background_music_with_file,
  get_all_background_musics,
  get_background_music,
  get_background_music_option,
} from "../controllers/AdminControllers/BackgroundMusic.js"
import {
  assign_customer,
  create_customer,
  delete_customer,
  edit_customer,
  edit_customer_status,
  enterNotification,
  get_all_customer_pendings,
  get_all_customers,
  get_all_customers_option,
  get_customer,
  get_customer_pending,
  uploadCustomer,
} from "../controllers/AdminControllers/Customer.js"
import {
  check_if_product_node_unique_name,
  create_product_node,
  delete_product_nodes,
  edit_product_node,
  get_all_product_nodes,
  get_product_node,
  get_product_nodes,
  get_product_nodes_showroom_links,
} from "../controllers/AdminControllers/ProductNode.js"
import {
  check_if_package_unique_name,
  create_package,
  delete_package,
  edit_package,
  get_all_package_option,
  get_all_packages,
  get_package,
} from "../controllers/AdminControllers/Package.js"
import {
  all_client_map_analytics,
  all_customer_map_analytics,
  all_emails_analytics,
  all_option_offered_analytics,
  all_product_preview_analytics,
  all_product_quantity_analytics,
  all_video_conference_duration_analytics,
  all_virtual_flythrough_analytics,
  all_visit_duration_analytics,
  all_visitor_analytics,
} from "../controllers/AdminControllers/Analytics.js"
import {
  check_if_category_unique_name,
  create_category,
  delete_category,
  edit_category,
  get_all_categories,
  get_all_categories_option,
  get_category,
} from "../controllers/AdminControllers/ProductCategory.js"
import {
  create_video_conference,
  edit_video_conference_status,
  get_all_video_conference,
  get_all_video_conference_requests,
  get_video_conference_room,
} from "../controllers/AdminControllers/VideoConference.js"
import { create_qr_code_image } from "../controllers/AdminControllers/QrCodeImage.js"
import {
  create_sent_email,
  create_sent_email_invitation,
  create_sent_email_invitation_flythrough,
  create_sent_email_invitation_flythrough_create,
  delete_flythrough_invitation,
  delete_invitation,
  delete_sent_email,
  get_all_emails,
  get_sent_email,
  update_flythrough_is_viewed,
  update_sent_email_is_viewed,
  update_sent_email_status,
} from "../controllers/AdminControllers/SentEmail.js"
import {
  check_if_invitation_passcode_unique,
  get_all_invitation,
  get_invitation,
  update_invitation_is_viewed,
} from "../controllers/AdminControllers/Invitation.js"
import {
  create_real_estate,
  delete_real_estate,
  edit_real_estate_with_file,
  get_all_real_estates,
  get_real_estate,
} from "../controllers/AdminControllers/RealEstate.js"
import {
  delete_expression_of_interest,
  get_all_expression_of_interest,
  get_expression_of_interest,
} from "../controllers/PublicControllers/ExpressionOfInterest.js"
import deleteMultipleFiles from "../controllers/AdminControllers/DeleteBucketObject.js"
import {
  check_if_client_category_unique_name,
  create_client_category,
  delete_client_category,
  edit_client_category,
  get_all_client_categories,
  get_all_client_categories_option,
  get_client_category,
} from "../controllers/AdminControllers/ClientCategory.js"
import {
  get_all_sales,
  get_all_sales_customers,
  get_all_sales_emails,
  get_all_sales_invitation,
  get_all_sales_option,
  get_all_sales_products,
  get_all_sales_users,
  get_all_sales_virtual_experiences,
  get_sales,
} from "../controllers/AdminControllers/Sales.js"
import {
  check_if_ve_category_unique_name,
  create_ve_category,
  delete_ve_category,
  edit_ve_category,
  get_all_ve_categories,
  get_all_ve_categories_option,
  get_ve_category,
} from "../controllers/AdminControllers/VirtualExperienceCategory.js"
import {
  create_virtual_flythrough,
  delete_virtual_flythrough,
  edit_virtual_flythrough,
  get_all_virtual_flythroughs,
  get_virtual_flythrough,
} from "../controllers/AdminControllers/VirtualFlythrough.js"
import {
  create_client_flythrough,
  get_client_flythrough,
  get_client_flythroughs,
} from "../controllers/AdminControllers/ClientFlythrough.js"
import { get_invitation_flythrough } from "../controllers/AdminControllers/FlythroughInvitation.js"

const accessKeyId = process.env.AWS_ACCESS_KEY_ID
const secretAccessKey = process.env.AWS_SECRET_ACCESS_KEY
const region = process.env.S3_REGION
const Bucket = process.env.S3_BUCKET

const s3 = new S3Client({
  region: region,
  credentials: {
    accessKeyId: accessKeyId,
    secretAccessKey: secretAccessKey,
  },
})

const upload_client_logo = multer({
  storage: multerS3({
    s3: s3,
    acl: "public-read",
    bucket: Bucket,
    contentType: multerS3.AUTO_CONTENT_TYPE,
    key: function (req, file, cb) {
      cb(null, `client/${file.originalname}`) //use Date.now() for unique file keys
    },
  }),
})

const upload_user_picture = multer({
  storage: multerS3({
    s3: s3,
    acl: "public-read",
    bucket: Bucket,
    contentType: multerS3.AUTO_CONTENT_TYPE,
    key: function (req, file, cb) {
      cb(null, `user/${file.originalname}`) //use Date.now() for unique file keys
    },
  }),
})

const upload_ve_images = multer({
  storage: multerS3({
    s3: s3,
    acl: "public-read",
    bucket: Bucket,
    contentType: multerS3.AUTO_CONTENT_TYPE,
    key: function (req, file, cb) {
      cb(null, `virtual_experience/${file.originalname}`) //use Date.now() for unique file keys
    },
  }),
})

const upload_real_estate = multer({
  storage: multerS3({
    s3: s3,
    acl: "public-read",
    bucket: Bucket,
    contentType: multerS3.AUTO_CONTENT_TYPE,
    key: function (req, file, cb) {
      cb(null, `virtual_experience/real_estate/${file.originalname}`) //use Date.now() for unique file keys
    },
  }),
})

const upload_product_files = multer({
  storage: multerS3({
    s3: s3,
    acl: "public-read",
    bucket: Bucket,
    contentType: multerS3.AUTO_CONTENT_TYPE,
    key: function (req, file, cb) {
      cb(null, `product/${file.originalname}`) //use Date.now() for unique file keys
    },
  }),
})

const upload_music = multer({
  storage: multerS3({
    s3: s3,
    acl: "public-read",
    bucket: Bucket,
    contentType: multerS3.AUTO_CONTENT_TYPE,
    key: function (req, file, cb) {
      cb(null, `background_music/${file.originalname}`) //use Date.now() for unique file keys
    },
  }),
})

const upload_customer_picture = multer({
  storage: multerS3({
    s3: s3,
    acl: "public-read",
    bucket: Bucket,
    contentType: multerS3.AUTO_CONTENT_TYPE,
    key: function (req, file, cb) {
      cb(null, `customer/${file.originalname}`) //use Date.now() for unique file keys
    },
  }),
})

const upload_qr_code = multer({
  storage: multerS3({
    s3: s3,
    acl: "public-read",
    bucket: Bucket,
    contentType: multerS3.AUTO_CONTENT_TYPE,
    key: function (req, file, cb) {
      cb(null, `qr-code/${file.originalname}`) //use Date.now() for unique file keys
    },
  }),
})

const upload_virtual_flythrough = multer({
  storage: multerS3({
    s3: s3,
    acl: "public-read",
    bucket: Bucket,
    contentType: multerS3.AUTO_CONTENT_TYPE,
    key: function (req, file, cb) {
      let folder = "other"
      if (file.fieldname === "flythrough_thumbnail") {
        folder = "thumbnails"
      } else if (file.fieldname === "flythrough_video") {
        folder = "videos"
      }
      cb(
        null,
        `virtual-flythroughs/${folder}/${Date.now()}-${file.originalname}`,
      )
    },
  }),
})

const adminRouter = express.Router()

adminRouter.post(
  "/1cd/api/v1/admin/qr_code",
  upload_qr_code.array("qr"),
  create_qr_code_image,
)

adminRouter.get("/1cd/api/v1/admin/dashboard", get_dashboard_information)
adminRouter.get("/1cd/api/v1/admin/sales/dashboard", get_sales_dashboard)

adminRouter.get("/1cd/api/v1/admin/client", get_all_clients)
adminRouter.get("/1cd/api/v1/admin/client/option", get_all_clients_option)
adminRouter.get("/1cd/api/v1/admin/client/check", check_if_client_unique_name)
adminRouter.get("/1cd/api/v1/admin/client/:id", get_client)
adminRouter.get(
  "/1cd/api/v1/admin/client/:id/account_holder",
  get_client_account_holder,
)
adminRouter.post(
  "/1cd/api/v1/admin/client/create",
  upload_client_logo.array("files"),
  create_client,
)
adminRouter.put(
  "/1cd/api/v1/admin/client/:id/edit/with_file",
  upload_client_logo.array("files"),
  edit_client_with_file,
)
adminRouter.put("/1cd/api/v1/admin/client/:id/edit", edit_client)
adminRouter.delete("/1cd/api/v1/admin/client/delete/:id", delete_client)

adminRouter.get("/1cd/api/v1/admin/customer_group", get_all_customer_groups)
adminRouter.get(
  "/1cd/api/v1/admin/customer_group/option",
  get_all_customer_groups_option,
)
adminRouter.get("/1cd/api/v1/admin/customer_group/:id", get_customer_group)
adminRouter.post(
  "/1cd/api/v1/admin/customer_group/create",
  create_customer_group,
)
adminRouter.delete(
  "/1cd/api/v1/admin/customer_group/delete/:id",
  delete_customer_group,
)

adminRouter.get("/1cd/api/v1/admin/user", get_all_users)
adminRouter.get("/1cd/api/v1/admin/user/email", get_user_by_email)
adminRouter.get("/1cd/api/v1/admin/user/option", get_all_users_option)
adminRouter.get("/1cd/api/v1/admin/user/check", check_if_user_unique_name)
adminRouter.get("/1cd/api/v1/admin/user/location", get_all_users_location)
adminRouter.get("/1cd/api/v1/admin/user/pending", get_all_pending_users)
adminRouter.get(
  "/1cd/api/v1/admin/user/:id/my_backup_user",
  get_my_backup_users,
)
adminRouter.get("/1cd/api/v1/admin/user/:id", get_user)
adminRouter.post(
  "/1cd/api/v1/admin/user/create",
  upload_user_picture.array("files"),
  create_user,
)
adminRouter.put("/1cd/api/v1/admin/user/status", update_user_status)
adminRouter.put("/1cd/api/v1/admin/user/online", update_user_online)
adminRouter.put(
  "/1cd/api/v1/admin/user/:id/edit/with_file",
  upload_user_picture.array("files"),
  edit_user_with_file,
)
adminRouter.put("/1cd/api/v1/admin/user/:id/edit", edit_user)

adminRouter.delete("/1cd/api/v1/admin/user/delete/:id", delete_user)

adminRouter.post("/1cd/api/v1/admin/product/upload", uploadProducts)
adminRouter.get("/1cd/api/v1/admin/product", get_all_products)
adminRouter.get("/1cd/api/v1/admin/product_names", get_all_product_names)
adminRouter.get("/1cd/api/v1/admin/product/option", get_all_product_option)
adminRouter.get("/1cd/api/v1/admin/product/show_price", get_product_show_price)
adminRouter.get("/1cd/api/v1/admin/product/check", check_if_product_unique_name)
adminRouter.get("/1cd/api/v1/admin/product/:id", get_product)
adminRouter.post(
  "/1cd/api/v1/admin/product/create",
  upload_product_files.fields([
    { name: "glb" },
    { name: "photo_video" },
    { name: "three_d_images" },
    { name: "images" },
  ]),
  create_product,
)

adminRouter.post("/1cd/api/v1/admin/product/create_v2", create_product_v2)

adminRouter.put(
  "/1cd/api/v1/admin/product/show_price/update",
  update_product_show_price,
)
adminRouter.put(
  "/1cd/api/v1/admin/product/:id/edit",
  upload_product_files.fields([
    { name: "glb" },
    { name: "photo_video" },
    { name: "three_d_images" },
    { name: "images" },
  ]),
  edit_product,
)
adminRouter.delete("/1cd/api/v1/admin/product/delete/:id", delete_product)

adminRouter.get("/1cd/api/v1/admin/product_node", get_all_product_nodes)
adminRouter.get("/1cd/api/v1/admin/product_nodes", get_product_nodes)
adminRouter.get(
  "/1cd/api/v1/admin/product_nodes_showroom_link",
  get_product_nodes_showroom_links,
)
adminRouter.get(
  "/1cd/api/v1/admin/product_node/check",
  check_if_product_node_unique_name,
)
adminRouter.get("/1cd/api/v1/admin/product_node/:id", get_product_node)
adminRouter.post("/1cd/api/v1/admin/product_node/create", create_product_node)
adminRouter.put("/1cd/api/v1/admin/product_node/:id/edit", edit_product_node)
adminRouter.delete(
  "/1cd/api/v1/admin/product_node/delete/:id",
  delete_product_nodes,
)

adminRouter.get("/1cd/api/v1/admin/option_offered", get_all_option_offered)
adminRouter.get(
  "/1cd/api/v1/admin/option_offered/option",
  get_all_option_offered_option,
)
adminRouter.get("/1cd/api/v1/admin/option_offered/:id", get_option_offered)
adminRouter.post(
  "/1cd/api/v1/admin/option_offered/create",
  create_option_offered,
)

adminRouter.get("/1cd/api/v1/admin/permission", get_all_permissions)
adminRouter.get(
  "/1cd/api/v1/admin/permission/option",
  get_all_permissions_option,
)
adminRouter.get(
  "/1cd/api/v1/admin/permission/check",
  check_if_permission_unique_name,
)
adminRouter.get("/1cd/api/v1/admin/permission/:id", get_permission)
adminRouter.post("/1cd/api/v1/admin/permission/create", create_permission)
adminRouter.put("/1cd/api/v1/admin/permission/:id/edit", edit_permission)
adminRouter.delete("/1cd/api/v1/admin/permission/delete/:id", delete_permission)

adminRouter.get("/1cd/api/v1/admin/client_category", get_all_client_categories)
adminRouter.get(
  "/1cd/api/v1/admin/client_category/option",
  get_all_client_categories_option,
)
adminRouter.get(
  "/1cd/api/v1/admin/client_category/check",
  check_if_client_category_unique_name,
)
adminRouter.get("/1cd/api/v1/admin/client_category/:id", get_client_category)
adminRouter.post(
  "/1cd/api/v1/admin/client_category/create",
  create_client_category,
)
adminRouter.put(
  "/1cd/api/v1/admin/client_category/:id/edit",
  edit_client_category,
)
adminRouter.delete(
  "/1cd/api/v1/admin/client_category/delete/:id",
  delete_client_category,
)

adminRouter.get("/1cd/api/v1/admin/role", get_all_roles)
adminRouter.get("/1cd/api/v1/admin/role/option", get_all_roles_option)
adminRouter.get("/1cd/api/v1/admin/role/check", check_if_role_unique_name)
adminRouter.get("/1cd/api/v1/admin/role/:id", get_role)
adminRouter.post("/1cd/api/v1/admin/role/create", create_role)
adminRouter.put("/1cd/api/v1/admin/role/:id/edit", edit_role)
adminRouter.delete("/1cd/api/v1/admin/role/delete/:id", delete_role)

adminRouter.get("/1cd/api/v1/admin/access_type", get_all_access_types)
adminRouter.get(
  "/1cd/api/v1/admin/access_type/option",
  get_all_access_types_option,
)
adminRouter.get(
  "/1cd/api/v1/admin/access_type/check",
  check_if_access_type_unique_name,
)
adminRouter.get("/1cd/api/v1/admin/access_type/:id", get_access_type)
adminRouter.post("/1cd/api/v1/admin/access_type/create", create_access_type)
adminRouter.put("/1cd/api/v1/admin/access_type/:id/edit", edit_access_type)
adminRouter.delete(
  "/1cd/api/v1/admin/access_type/delete/:id",
  delete_access_type,
)

adminRouter.get("/1cd/api/v1/admin/business_type", get_all_business_types)
adminRouter.get(
  "/1cd/api/v1/admin/business_type/option",
  get_all_business_types_option,
)
adminRouter.get(
  "/1cd/api/v1/admin/business_type/check",
  check_if_business_type_unique_name,
)
adminRouter.get("/1cd/api/v1/admin/business_type/:id", get_business_type)
adminRouter.post("/1cd/api/v1/admin/business_type/create", create_business_type)
adminRouter.put("/1cd/api/v1/admin/business_type/:id/edit", edit_business_type)
adminRouter.delete(
  "/1cd/api/v1/admin/business_type/delete/:id",
  delete_business_type,
)

adminRouter.get("/1cd/api/v1/admin/type", get_all_types)
adminRouter.get("/1cd/api/v1/admin/type/option", get_all_types_option)
adminRouter.get("/1cd/api/v1/admin/type/check", check_if_type_unique_name)
adminRouter.get("/1cd/api/v1/admin/type/:id", get_type)
adminRouter.post("/1cd/api/v1/admin/type/create", create_type)
adminRouter.put("/1cd/api/v1/admin/type/:id/edit", edit_type)
adminRouter.delete("/1cd/api/v1/admin/type/delete/:id", delete_type)

adminRouter.get("/1cd/api/v1/admin/category", get_all_categories)
adminRouter.get("/1cd/api/v1/admin/category/option", get_all_categories_option)
adminRouter.get(
  "/1cd/api/v1/admin/category/check",
  check_if_category_unique_name,
)
adminRouter.get("/1cd/api/v1/admin/category/:id", get_category)
adminRouter.post("/1cd/api/v1/admin/category/create", create_category)
adminRouter.put("/1cd/api/v1/admin/category/:id/edit", edit_category)
adminRouter.delete("/1cd/api/v1/admin/category/delete/:id", delete_category)

adminRouter.get("/1cd/api/v1/admin/ve_category", get_all_ve_categories)
adminRouter.get(
  "/1cd/api/v1/admin/ve_category/option",
  get_all_ve_categories_option,
)
adminRouter.get(
  "/1cd/api/v1/admin/ve_category/check",
  check_if_ve_category_unique_name,
)
adminRouter.get("/1cd/api/v1/admin/ve_category/:id", get_ve_category)
adminRouter.post("/1cd/api/v1/admin/ve_category/create", create_ve_category)
adminRouter.put("/1cd/api/v1/admin/ve_category/:id/edit", edit_ve_category)
adminRouter.delete(
  "/1cd/api/v1/admin/ve_category/delete/:id",
  delete_ve_category,
)

adminRouter.get(
  "/1cd/api/v1/admin/virtual_experience",
  get_all_virtual_experiences,
)
adminRouter.get(
  "/1cd/api/v1/admin/virtual_experience/option",
  get_all_virtual_experience_option,
)
adminRouter.get(
  "/1cd/api/v1/admin/virtual_experience/:id",
  get_virtual_experience,
)
adminRouter.post(
  "/1cd/api/v1/admin/virtual_experience/create",
  upload_ve_images.fields([
    { name: "internal_flythrough" },
    { name: "photos" },
    { name: "thumbnail" },
  ]),
  create_virtual_experience,
)

adminRouter.post(
  "/1cd/api/v1/admin/virtual_experience/create_v2",
  create_virtual_experience_v2,
)

adminRouter.put(
  "/1cd/api/v1/admin/virtual_experience/:id",
  edit_virtual_experience_v2,
)

adminRouter.put(
  "/1cd/api/v1/admin/virtual_experience/:id/edit",
  upload_ve_images.fields([
    { name: "internal_flythrough" },
    { name: "external_flythrough" },
    { name: "photos" },
    { name: "thumbnail" },
  ]),
  edit_virtual_experience,
)

adminRouter.post(
  "/1cd/api/v1/admin/virtual_experience/multiple_flythrough/create",
  upload_ve_images.fields([
    { name: "internal_flythrough" },
    { name: "external_flythrough" },
    { name: "photos" },
    { name: "thumbnail" },
  ]),
  create_virtual_experience,
)

adminRouter.delete(
  "/1cd/api/v1/admin/virtual_experience/delete/:id",
  delete_virtual_experience,
)

// VIRTUAL FLYTHROUGH
adminRouter.post(
  "/1cd/api/v1/admin/virtual_flythrough/create",
  upload_virtual_flythrough.fields([
    { name: "flythrough_video" },
    { name: "flythrough_thumbnail" },
  ]),
  create_virtual_flythrough,
)

adminRouter.get(
  "/1cd/api/v1/admin/virtual_flythrough",
  get_all_virtual_flythroughs,
)
adminRouter.get(
  "/1cd/api/v1/admin/virtual_flythrough/:id",
  get_virtual_flythrough,
)
adminRouter.put(
  "/1cd/api/v1/admin/virtual_flythrough/:id",
  upload_virtual_flythrough.fields([
    { name: "flythrough_video" },
    { name: "flythrough_thumbnail" },
  ]),
  edit_virtual_flythrough,
)
adminRouter.delete(
  "/1cd/api/v1/admin/virtual_flythrough/:id",
  delete_virtual_flythrough,
)

adminRouter.get(
  "/1cd/api/v1/admin/client_flythrough/:id",
  get_client_flythrough,
)
adminRouter.get("/1cd/api/v1/admin/client_flythrough", get_client_flythroughs)
adminRouter.post(
  "/1cd/api/v1/admin/client_flythrough/create",
  create_client_flythrough,
)

adminRouter.get("/1cd/api/v1/admin/background_music", get_all_background_musics)
adminRouter.get(
  "/1cd/api/v1/admin/background_music/option",
  get_background_music_option,
)
adminRouter.get(
  "/1cd/api/v1/admin/background_music/check",
  check_if_background_music_unique_name,
)
adminRouter.get("/1cd/api/v1/admin/background_music/:id", get_background_music)
adminRouter.post(
  "/1cd/api/v1/admin/background_music/create",
  upload_music.array("files"),
  create_background_music,
)
adminRouter.put(
  "/1cd/api/v1/admin/background_music/:id/edit",
  edit_background_music,
)
adminRouter.put(
  "/1cd/api/v1/admin/background_music/:id/edit/with_file",
  upload_music.array("files"),
  edit_background_music_with_file,
)

adminRouter.delete(
  "/1cd/api/v1/admin/background_music/delete/:id",
  delete_background_music,
)

adminRouter.post("/1cd/api/v1/admin/customer/upload", uploadCustomer)
adminRouter.get("/1cd/api/v1/admin/customer", get_all_customers)

adminRouter.get("/1cd/api/v1/admin/customer/option", get_all_customers_option)
adminRouter.get("/1cd/api/v1/admin/customer/:id", get_customer)
adminRouter.post(
  "/1cd/api/v1/admin/customer/enterNotification",
  enterNotification,
)
adminRouter.post("/1cd/api/v1/admin/customer/assign", assign_customer)
adminRouter.post(
  "/1cd/api/v1/admin/customer/create",
  upload_customer_picture.array("files"),
  create_customer,
)
adminRouter.put("/1cd/api/v1/admin/customer/status", edit_customer_status)
adminRouter.put(
  "/1cd/api/v1/admin/customer/:id/edit",
  upload_customer_picture.fields([{ name: "files" }]),
  edit_customer,
)
adminRouter.delete("/1cd/api/v1/admin/customer/delete/:id", delete_customer)

adminRouter.get("/1cd/api/v1/admin/customer_pending", get_all_customer_pendings)
adminRouter.get("/1cd/api/v1/admin/customer_pending/:id", get_customer_pending)

adminRouter.get("/1cd/api/v1/admin/package", get_all_packages)
adminRouter.get("/1cd/api/v1/admin/package/option", get_all_package_option)
adminRouter.get("/1cd/api/v1/admin/package/check", check_if_package_unique_name)
adminRouter.get("/1cd/api/v1/admin/package/:id", get_package)
adminRouter.post("/1cd/api/v1/admin/package/create", create_package)
adminRouter.put("/1cd/api/v1/admin/package/:id/edit", edit_package)
adminRouter.delete("/1cd/api/v1/admin/package/delete/:id", delete_package)

//video conference
adminRouter.get("/1cd/api/v1/admin/video_conference", get_all_video_conference)
adminRouter.get(
  "/1cd/api/v1/admin/video_conference/requests",
  get_all_video_conference_requests,
)
adminRouter.get(
  "/1cd/api/v1/admin/video_conference/room/:room",
  get_video_conference_room,
)
adminRouter.post(
  "/1cd/api/v1/admin/video_conference/create",
  create_video_conference,
)
adminRouter.put(
  "/1cd/api/v1/admin/video_conference/status/:id",
  edit_video_conference_status,
)

//invitations
adminRouter.get("/1cd/api/v1/admin/invitation", get_all_invitation)
adminRouter.get(
  "/1cd/api/v1/admin/invitation/check",
  check_if_invitation_passcode_unique,
)
adminRouter.get("/1cd/api/v1/admin/invitation/:id", get_invitation)
adminRouter.get(
  "/1cd/api/v1/admin/invitation/email_opened/:id",
  update_invitation_is_viewed,
)
adminRouter.get(
  "/1cd/api/v1/admin/invitation/flythrough/:id",
  get_invitation_flythrough,
)

//sent emails
adminRouter.get("/1cd/api/v1/admin/sent_email", get_all_emails)
adminRouter.get("/1cd/api/v1/admin/sent_email/:id", get_sent_email)
adminRouter.get(
  "/1cd/api/v1/admin/sent_email/email_opened/:id",
  update_sent_email_is_viewed,
)

adminRouter.post(
  "/1cd/api/v1/admin/sent_email/invitation",
  create_sent_email_invitation,
)
adminRouter.post(
  "/1cd/api/v1/admin/sent_email/invitation_flythrough",
  create_sent_email_invitation_flythrough,
)
adminRouter.post(
  "/1cd/api/v1/admin/sent_email/invitation_flythrough_create",
  create_sent_email_invitation_flythrough_create,
)
adminRouter.get(
  "/1cd/api/v1/admin/flythrough/email_opened/:id",
  update_flythrough_is_viewed,
)

adminRouter.post("/1cd/api/v1/admin/sent_email/create", create_sent_email)
adminRouter.delete("/1cd/api/v1/admin/sent_email/delete/:id", delete_sent_email)

adminRouter.delete(
  "/1cd/api/v1/admin/sent_email/walkthrough/:id",
  delete_invitation,
)
adminRouter.delete(
  "/1cd/api/v1/admin/sent_email/flythrough/:id",
  delete_flythrough_invitation,
)
//real_estate
adminRouter.get("/1cd/api/v1/admin/real_estate", get_all_real_estates)
adminRouter.get("/1cd/api/v1/admin/real_estate/:id", get_real_estate)
adminRouter.post(
  "/1cd/api/v1/admin/real_estate/create",
  upload_real_estate.fields([{ name: "logo" }, { name: "floor_plan" }]),
  create_real_estate,
)
adminRouter.put(
  "/1cd/api/v1/admin/real_estate/:id/edit",
  upload_real_estate.fields([{ name: "logo" }, { name: "floor_plan" }]),
  edit_real_estate_with_file,
)
adminRouter.delete("/1cd/api/v1/admin/real_estate/:id", delete_real_estate)

//analytics
adminRouter.get(
  "/1cd/api/v1/admin/analytics/client/map",
  all_client_map_analytics,
)
adminRouter.get(
  "/1cd/api/v1/admin/analytics/customer/map",
  all_customer_map_analytics,
)
adminRouter.get("/1cd/api/v1/admin/analytics/sent_email", all_emails_analytics)
adminRouter.get(
  "/1cd/api/v1/admin/analytics/product_requests",
  all_option_offered_analytics,
)
adminRouter.get(
  "/1cd/api/v1/admin/analytics/visit_duration",
  all_visit_duration_analytics,
)
adminRouter.get(
  "/1cd/api/v1/admin/analytics/video_conference_duration",
  all_video_conference_duration_analytics,
)
adminRouter.get(
  "/1cd/api/v1/admin/analytics/virtual_experience_visitor",
  all_visitor_analytics,
)
adminRouter.get(
  "/1cd/api/v1/admin/analytics/product_preview",
  all_product_preview_analytics,
)
adminRouter.get(
  "/1cd/api/v1/admin/analytics/product_quantity",
  all_product_quantity_analytics,
)
adminRouter.get(
  "/1cd/api/v1/admin/analytics/virtual_flythrough",
  all_virtual_flythrough_analytics,
)

adminRouter.post(
  "/1cd/api/v1/admin/analytics/sent_email/status/:id/:status",
  update_sent_email_status,
)

//interest
adminRouter.get(
  "/1cd/api/v1/admin/expression_of_interest",
  get_all_expression_of_interest,
)
adminRouter.get(
  "/1cd/api/v1/admin/expression_of_interest/:id",
  get_expression_of_interest,
)
adminRouter.delete(
  "/1cd/api/v1/admin/expression_of_interest/delete/:id",
  delete_expression_of_interest,
)

adminRouter.post("/1cd/api/v1/admin/change_password", change_password)

// adminRouter.delete(
//   "/1cd/admin/v1/admin/s3_bucket_objects_delete",
//   deleteMultipleFiles
// );

//sales
adminRouter.get("/1cd/api/v1/admin/sales", get_all_sales)
adminRouter.get("/1cd/api/v1/admin/sales/option", get_all_sales_option)
adminRouter.get(
  "/1cd/api/v1/admin/sales/virtual_experience",
  get_all_sales_virtual_experiences,
)
adminRouter.get("/1cd/api/v1/admin/sales/invitation", get_all_sales_invitation)
adminRouter.get("/1cd/api/v1/admin/sales/sent_email", get_all_sales_emails)
adminRouter.get("/1cd/api/v1/admin/sales/user", get_all_sales_users)
adminRouter.get("/1cd/api/v1/admin/sales/product", get_all_sales_products)
adminRouter.get("/1cd/api/v1/admin/sales/customer", get_all_sales_customers)

adminRouter.get("/1cd/api/v1/admin/sales/:id", get_sales)

export default adminRouter
