import express from "express"
import multer from "multer"
import multerS3 from "multer-s3"
import { S3Client, S3 } from "@aws-sdk/client-s3"
import { login } from "../controllers/PublicControllers/Login.js"
import {
  checkIfInvited,
  checkShowroom,
  checkShowroomInvitation,
  getShowroomInfo,
  getShowroomNode,
  getShowroomProductInfo,
  showroom_customer_visit_details,
} from "../controllers/PublicControllers/Showroom.js"
import {
  check_agent_status_online,
  get_nearest_agents,
} from "../controllers/AdminControllers/User.js"
import { get_real_estate_info } from "../controllers/AdminControllers/RealEstate.js"
import {
  create_virtual_flythrough_analytics,
  productStayDuration,
  productViewedCount,
  shapeSparkStayDuration,
} from "../controllers/PublicControllers/Analytics.js"
import {
  get_product_option_offered,
  product_other_option,
  request_product_option_offered,
} from "../controllers/PublicControllers/OptionOffered.js"
import { create_expression_of_interest } from "../controllers/PublicControllers/ExpressionOfInterest.js"
import {
  check_customer_invited,
  check_flythrough_invitation_expiration,
  check_invitation,
  check_invitation_expiration,
  get_flythrough_invitation,
  get_invitations,
  update_email_id,
  update_email_status,
} from "../controllers/PublicControllers/Invitation.js"
import {
  create_customer_registered,
  get_customer_data,
  sendEmail,
} from "../controllers/PublicControllers/Customer.js"
import {
  addVideoConferenceMinutes,
  create_video_conference,
} from "../controllers/PublicControllers/VideoCall.js"
import {
  check_video_conference_invitation_respond,
  video_conference_invitation_respond,
} from "../controllers/AdminControllers/VideoConference.js"
import {
  bulk_create_product_nodes,
  create_product_node,
} from "../controllers/PublicControllers/ProductNodes.js"

const accessKeyId = process.env.AWS_ACCESS_KEY_ID
const secretAccessKey = process.env.AWS_SECRET_ACCESS_KEY
const region = process.env.S3_REGION
const Bucket = process.env.S3_BUCKET

const s3 = new S3Client({
  region: region,
  credentials: {
    accessKeyId: accessKeyId,
    secretAccessKey: secretAccessKey,
  },
})

const upload_client_logo = multer({
  storage: multerS3({
    s3: s3,
    acl: "public-read",
    bucket: Bucket,
    key: function (req, file, cb) {
      cb(null, `${file.originalname}`) //use Date.now() for unique file keys
    },
  }),
})

const upload_user_picture = multer({
  storage: multerS3({
    s3: s3,
    acl: "public-read",
    bucket: Bucket,
    key: function (req, file, cb) {
      cb(null, `${file.originalname}`) //use Date.now() for unique file keys
    },
  }),
})

const publicRouter = express.Router()

publicRouter.get("/", (req, res) => {
  return res.status(200).json()
})

publicRouter.post("/1cd/api/v1/public/email", sendEmail)
publicRouter.post("/1cd/api/v1/public/login", login)

publicRouter.post("/1cd/api/v1/public/sent_email/status", update_email_status)
publicRouter.post("/1cd/api/v1/public/sent_email/id", update_email_id)

publicRouter.get("/1cd/api/v1/public/invitation/check", check_invitation)
publicRouter.get(
  "/1cd/api/v1/public/invitation/check/expiration",
  check_invitation_expiration,
)
publicRouter.get(
  "/1cd/api/v1/public/flythrough_invitation/check/expiration",
  check_flythrough_invitation_expiration,
)
publicRouter.get("/1cd/api/v1/public/user/nearest", get_nearest_agents)

publicRouter.get("/1cd/api/v1/public/customer/id", get_customer_data)
publicRouter.get("/1cd/api/v1/public/customer/invited", check_customer_invited)

publicRouter.post(
  "/1cd/api/v1/public/customer/registered/create",
  create_customer_registered,
)

publicRouter.get("/1cd/api/v1/public/showroom/check", checkShowroom)
publicRouter.get("/1cd/api/v1/public/showroom/invitation", checkIfInvited)
publicRouter.post("/1cd/api/v1/public/showroom/product_nodes", getShowroomNode)
publicRouter.post(
  "/1cd/api/v1/public/user/agent/online",
  check_agent_status_online,
)
publicRouter.get("/1cd/api/v1/public/showroom/info", getShowroomInfo)
publicRouter.get(
  "/1cd/api/v1/public/showroom/product/:id",
  getShowroomProductInfo,
)
publicRouter.post(
  "/1cd/api/v1/public/showroom/actions",
  showroom_customer_visit_details,
)

publicRouter.get("/1cd/api/v1/public/real_estate/info", get_real_estate_info)

publicRouter.get(
  "/1cd/api/v1/public/option_offered",
  get_product_option_offered,
)
publicRouter.post(
  "/1cd/api/v1/public/option_offered/request/create",
  request_product_option_offered,
)

publicRouter.post(
  "/1cd/api/v1/public/other_option/create",
  product_other_option,
)

publicRouter.post(
  "/1cd/api/v1/public/analytics/shapespark_duration",
  shapeSparkStayDuration,
)
publicRouter.post(
  "/1cd/api/v1/public/analytics/shapespark_product_duration",
  productStayDuration,
)
publicRouter.post(
  "/1cd/api/v1/public/analytics/shapespark_product_count",
  productViewedCount,
)

publicRouter.post(
  "/1cd/api/v1/public/expression_of_interest/create",
  create_expression_of_interest,
)

publicRouter.get(
  "/1cd/api/v1/public/video_conference/check_invitation_respond/:id",
  check_video_conference_invitation_respond,
)
publicRouter.get(
  "/1cd/api/v1/public/flythrough_invitation",
  get_flythrough_invitation,
)
publicRouter.get("/1cd/api/v1/public/invitation", get_invitations)
publicRouter.post(
  "/1cd/api/v1/public/video_conference/duration",
  addVideoConferenceMinutes,
)
publicRouter.post(
  "/1cd/api/v1/public/video_conference/request/create",
  create_video_conference,
)
publicRouter.put(
  "/1cd/api/v1/public/video_conference/invitation_respond",
  video_conference_invitation_respond,
)

publicRouter.post("/1cd/api/v1/public/product_node/create", create_product_node)
publicRouter.post(
  "/1cd/api/v1/public/product_node/bulk_create",
  bulk_create_product_nodes,
)
publicRouter.post(
  "/1cd/api/v1/public/analytics/virtual_flythrough",
  create_virtual_flythrough_analytics,
)

export default publicRouter
