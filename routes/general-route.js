import express from "express";
import { get_account_information } from "../controllers/GeneralControllers/Account.js";
import { create_notification, delete_notification, get_all_notifications, get_all_notifications_unread, get_notification, update_all_notifications_unread } from "../controllers/GeneralControllers/Notification.js";
import { get_sales_dashboard } from "../controllers/AdminControllers/Dashboard.js";

const generalRouter = express.Router();


generalRouter.get('/1cd/api/v1/general/account_information', get_account_information);

generalRouter.get('/1cd/api/v1/general/notification', get_all_notifications);
generalRouter.get('/1cd/api/v1/general/notification/unread', get_all_notifications_unread);
generalRouter.get('/1cd/api/v1/general/notification/:id', get_notification);
generalRouter.post('/1cd/api/v1/general/notification/create', create_notification);
generalRouter.put('/1cd/api/v1/general/notification/unread/update', update_all_notifications_unread)
generalRouter.delete('/1cd/api/v1/general/notification/delete/:id', delete_notification);

export default generalRouter;