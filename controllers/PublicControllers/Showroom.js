import AnalyticsFrameDuration from "../../models/AnalyticsFrameDurationModel.js";
import AnalyticsViewedProducts from "../../models/AnalyticsViewedProductModel.js";
import AnalyticsVisitDuration from "../../models/AnalyticsVisitDurationModel.js";
import Clients from "../../models/ClientModel.js";
import Customers from "../../models/CustomerModel.js";
import Products from "../../models/ProductModel.js";
import ProductNodes from "../../models/ProductNodeModel.js";
import ProductOptionOffered from "../../models/ProductOptionOfferedModel.js";
import Users from "../../models/UserModel.js";
import VirtualExperienceAccessTypes from "../../models/VirtualExperienceAccessTypeModel.js";
import VirtualExperienceBusinessTypes from "../../models/VirtualExperienceBusinessTypeModel.js";
import VirtualExperienceCategories from "../../models/VirtualExperienceCategoryModel.js";
import VirtualExperiences from "../../models/VirtualExperienceModel.js";
import VirtualExperienceTypes from "../../models/VirtualExperienceTypeModel.js";
import VirtualExperienceUsers from "../../models/VirtualExperienceUserModel.js";
import { Op } from "sequelize";

import got from "got";
import OptionOffered from "../../models/OptionOfferedModel.js";
import RealEstates from "../../models/RealEstateModel.js";
import Invitations from "../../models/InvitationModel.js";
import AnalyticsProductPreview from "../../models/AnalyticsProductPreviewModel.js";
import ProductOptionOfferedRequests from "../../models/ProductOptionOfferedRequestModel.js";
import AnalyticsProductQuantity from "../../models/AnalyticsProductQuantityModel.js";
import ProductCategories from "../../models/ProductCategoryModel.js";

export const checkShowroom = async (req, res) => {
  let virtual_experience;

  if (req.query.id) {
    virtual_experience = await VirtualExperiences.findOne({
      where: {
        id: req.query.id,
        status: "Active",
      },
      include: [
        {
          model: Clients,
          attributes: ["id", "name", "logo_url", "website_url"],
        },
        {
          model: Users,
          as: "virtual_experience_of_user",
          include: [
            {
              model: Users,
              as: "backup_user",
            },
          ],
        },
        {
          model: VirtualExperienceBusinessTypes,
          attributes: ["id", "name"],
        },
        {
          model: VirtualExperienceTypes,
          attributes: ["id", "name"],
        },
        {
          model: RealEstates,
          required: false,
        },
      ],
    });
  } else {
    virtual_experience = await VirtualExperiences.findOne({
      where: {
        link_name: req.query.name,
      },
      include: [
        {
          model: Clients,
          attributes: ["id", "name", "logo_url", "website_url"],
        },
        {
          model: Users,
          as: "virtual_experience_of_user",
          include: [
            {
              model: Users,
              as: "backup_user",
            },
          ],
        },
        {
          model: VirtualExperienceBusinessTypes,
          attributes: ["id", "name"],
        },
        {
          model: VirtualExperienceTypes,
          attributes: ["id", "name"],
        },
        {
          model: RealEstates,
          required: false,
        },
      ],
    });
  }

  console.log(virtual_experience);

  if (!virtual_experience) {
    return res.json({ status: 204, message: "No Showroom" });
  }

  return res.json({
    status: 200,
    message: "Has Showroom",
    showroom: virtual_experience,
  });
};

export const checkIfInvited = async (req, res) => {
  const { code, link_name } = req.query;
  try {
    const is_invited = await Invitations.findOne({
      nest: true,
      where: {
        passcode: code,
      },
      include: [
        {
          model: VirtualExperiences,
          where: {
            link_name: link_name,
          },
        },
        {
          model: Customers,
          attributes: ["id", "full_name", "email"],
        },
      ],
    });

    if (!is_invited) {
      return res.status(201).json();
    }

    if (is_invited.dataValues.access_expiration_date < new Date()) {
      return res.status(204).json();
    }

    return res.status(200).json({
      invitation: {
        pin_code: is_invited.dataValues.passcode,
        invitation: is_invited.dataValues.id,
      },
      customer: is_invited.dataValues.customer,
    });
  } catch (error) {}
};

export const getShowroomInfo = async (req, res) => {
  const { name } = req.query;

  const virtual_experience = await VirtualExperiences.findOne({
    where: {
      link_name: name,
    },
    include: [
      {
        model: Clients,
        attributes: ["id", "name", "logo_url"],
      },
      {
        model: Users,
        attributes: [
          "id",
          "first_name",
          "last_name",
          "middle_name",
          "email",
          "phone_number",
          "is_online",
          "image_url",
        ],
        as: "virtual_experience_of_user",
      },
      {
        model: Products,
        attributes: ["id", "image_urls"],
        include: [
          {
            model: ProductNodes,
          },
        ],
      },
      {
        model: VirtualExperienceAccessTypes,
        attributes: ["id", "name"],
      },
      {
        model: VirtualExperienceBusinessTypes,
        attributes: ["id", "name"],
      },
      {
        model: VirtualExperienceTypes,
        attributes: ["id", "name"],
      },
    ],
  });

  console.log(virtual_experience);

  if (!virtual_experience) {
    return res.json({ status: 204, message: "No Showroom" });
  }

  return res.json({
    status: 200,
    message: "Has Showroom",
    showroom: virtual_experience,
  });
};

export const getShowroomProductInfo = async (req, res) => {
  const { id } = req.params;

  const product = await Products.findOne({
    where: {
      id: id,
    },
    include: [
      {
        model: ProductNodes,
      },
      {
        model: ProductCategories,
        attributes: ["id", "name"],
      }
    ],
  });

  console.log(product);

  if (!product) {
    return res.json({ status: 204, message: "No Showroom" });
  }

  return res.json({ status: 200, product: product });
};

export const getShowroomNode = async (req, res) => {
  let { name, showroom_link } = req.body;

  try {
    const product_nodes = await ProductNodes.findOne({
      where: {
        name: name,
        showroom_link: showroom_link,
      },
    });

    if (!product_nodes) {
      return res.status(400).json({ message: "Something went wrong" });
    }

    return res.status(200).json(product_nodes);
  } catch (error) {
    console.log(error);
  }
};

export const checkShowroomInvitation = async (req, res) => {
  const {
    email,
    fullName,
    latitude,
    longitude,
    client_id,
    invitation,
    showroom,
    code,
  } = req.body;

  console.log(showroom);

  try {
    const virtual_experience = await VirtualExperiences.findOne({
      where: {
        link_name: showroom,
      },
      attributes: ["id"],
    });

    console.log(virtual_experience);

    if (!virtual_experience) {
      return res.json({ status: 204, message: "No Showroom" });
    }

    // let assigned_user_id;

    // const customer = await Customers.findOne({
    //   where: {
    //     email: email,
    //   },
    // });

    // if (!customer) {
    //   const unregistered = await UnregisteredCustomers.create({
    //     email: email,
    //     name: fullName,
    //     latitude: latitude,
    //     longitude: longitude,
    //     assigned_user_id: assigned_user_id,
    //   });
    // }

    // if (customer.dataValues.is_email_verified) {
    // } else {
    // }
  } catch (error) {
    console.log(error);
  }
};

export const showroom_customer_visit_details = async (req, res) => {
  const {
    customer_id,
    virtual_experience_id,
    virtual_experience_type,
    client_id,
    shapespark_duration,
    assigned_users,
    enter_time,
    exit_time,
  } = req.body;
  console.log("BODY: ", req.body);

  const customer = await Customers.findOne({
    where: {
      id: customer_id,
    },
    nest: true,
  });

  const virtual_experience = await VirtualExperiences.findOne({
    where: {
      id: virtual_experience_id,
    },
    attributes: ["name"],
    nest: true,
  });

  const client = await Clients.findOne({
    where: {
      id: client_id,
    },
    attributes: ["name"],
    nest: true,
  });

  const thank_you_note = await got
    .post("https://platform.1clickdesign.com/api/notification/thank_you_note", {
      headers: {
        "Access-Control-Allow-Credentials": "true",
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Methods": "POST",
      },
      json: {
        virtual_experience_type_name: virtual_experience_type,
        user:
          assigned_users[0]?.first_name + " " + assigned_users[0]?.last_name,
        customer: customer?.dataValues?.full_name,
        customer_email: customer?.dataValues?.email,
        user_position: assigned_users[0]?.position,
        company: assigned_users[0]?.client?.name,
        user_email: assigned_users[0]?.email,
        user_number: assigned_users[0]?.phone_number,
        user_id: assigned_users[0]?.id,
        customer_id: customer_id,
        client_id: client_id,
        virtual_experience_id: virtual_experience_id,
        client: client,
        product_list: req.body.products
      },
    })
    .json();

  console.log("Thank You: ", thank_you_note);

  const duration = await AnalyticsVisitDuration.create({
    duration: parseFloat(shapespark_duration),
    virtual_experience_id: virtual_experience_id,
    customer_id: customer_id,
    created_by: {
      id: null,
      name: "System",
      role: null,
    },
  });

  let frame_duration_info = [];
  let product_count_info = [];
  let product_quantity_info = [];
  let option_offered_info = [];
  let product_preview_buttons_info = [];
  if (Object.entries(req.body.products).length !== 0) {
    Object.entries(req.body.products).forEach(([key, value]) => {
      console.log(`${key}: ${value}`);
      frame_duration_info.push({
        duration: parseFloat(value.duration),
        product_id: key,
        customer_id: customer_id,
        created_by: {
          id: null,
          name: "System",
          role: null,
        },
      });
      product_count_info.push({
        count: value.click_count,
        product_id: key,
        virtual_experience_id: virtual_experience_id,
        customer_id: customer_id,
        created_by: {
          id: null,
          name: "System",
          role: null,
        },
      });
      product_quantity_info.push({
        count: value.quantity,
        product_id: key,
        virtual_experience_id: virtual_experience_id,
        customer_id: customer_id,
        created_by: {
          id: null,
          name: "System",
          role: null,
        },
      });
      if (value.requests && value.requests.length > 0) {
        for (let index = 0; index < value.requests.length; index++) {
          const element = value.requests[index];
          option_offered_info.push({
            opt_offer_id: element,
            product_id: key,
            sent_email_id: null,
            customer_id: customer_id,
            virtual_exp_id: virtual_experience_id,
            client_id: client_id,
            assigned_user_id: assigned_users[0]?.id,
            created_by: { id: null, name: "System", role: null },
          });
        }
      }
      Object.entries(value.product_preview_button_clicks).forEach(
        ([keys, val]) => {
          if (val === true) {
            if (keys === "sceneImages") {
              product_preview_buttons_info.push({
                client_id: client_id,
                customer_id: customer_id,
                product_id: key,
                virtual_experience_id: virtual_experience_id,
                name: "scene_images",
                created_by: { id: null, name: "System", role: null },
              });
            } else {
              product_preview_buttons_info.push({
                client_id: client_id,
                customer_id: customer_id,
                product_id: key,
                virtual_experience_id: virtual_experience_id,
                name: keys.toString(),
                created_by: { id: null, name: "System", role: null },
              });
            }
          }
        }
      );
    });
  }

  await AnalyticsFrameDuration.bulkCreate(frame_duration_info);
  await AnalyticsProductQuantity.bulkCreate(product_quantity_info);
  await ProductOptionOfferedRequests.bulkCreate(option_offered_info);
  await AnalyticsProductPreview.bulkCreate(product_preview_buttons_info);

  if (assigned_users[0]?.receive_email === true) {
    const customer_leaves = await got
      .post(
        "https://platform.1clickdesign.com/api/notification/customer_leave",
        {
          headers: {
            "Access-Control-Allow-Credentials": "true",
            "Access-Control-Allow-Origin": "*",
            "Access-Control-Allow-Methods": "POST",
          },

          json: {
            virtual_experience_type_name: virtual_experience_type,
            user:
              assigned_users[0]?.first_name +
              " " +
              assigned_users[0]?.last_name,
            user_email: assigned_users[0].email,
            customer: customer.dataValues.full_name,
            company: assigned_users[0]?.client?.name,
            user_email: assigned_users[0]?.email,
            user_id: assigned_users[0]?.id,
            customer_id: customer_id,
            client_id: client_id,
            virtual_experience_id: virtual_experience_id,
            product_count: Object.keys(req.body.products).length ?? 0,
            product_list: req.body.products,
            enter_time: enter_time,
            exit_time: exit_time,
          },
        }
      )
      .json();

      console.log(customer_leaves, "Customer Left")
  }

  if (assigned_users[1]?.receive_email === true) {
    const customer_leaves = await got
      .post(
        "https://platform.1clickdesign.com/api/notification/customer_leave",
        {
          headers: {
            "Access-Control-Allow-Credentials": "true",
            "Access-Control-Allow-Origin": "*",
            "Access-Control-Allow-Methods": "POST",
          },

          json: {
            virtual_experience_type_name: virtual_experience_type,
            user:
              assigned_users[1]?.first_name +
              " " +
              assigned_users[1]?.last_name,
            user_email: assigned_users[1].email,
            customer: customer.dataValues.full_name,
            company: assigned_users[1]?.client?.name,
            user_email: assigned_users[1]?.email,
            user_id: assigned_users[1]?.id,
            customer_id: customer_id,
            client_id: client_id,
            virtual_experience_id: virtual_experience_id,
            product_count: Object.keys(req.body.products).length ?? 0,
            product_list: req.body.products,
            enter_time: enter_time,
            exit_time: exit_time,
          },
        }
      )
      .json();

      console.log(customer_leaves, "Customer Left backup")
  }

  let product_ids = Object.keys(req.body.products) || [];
  const request_info = await OptionOffered.findAll({
    where: {
      status: "Active",
    },
    attributes: ["id", "name"],
  });

  const product_list = await Products.findAll({
    where: {
      id: {
        [Op.in]: product_ids,
      },
    },
    attributes: ["id", "name", "sku"],
  });

  if (
    (virtual_experience_type === "Virtual Showroom" || virtual_experience_type === "Virtual Art Gallery") &&
    Object.keys(req.body.products).length !== 0 &&
    assigned_users[0]?.receive_email === true
  ) {
    const customer_activity = await got
      .post(
        "https://platform.1clickdesign.com/api/notification/customer_activity",
        {
          headers: {
            "Access-Control-Allow-Credentials": "true",
            "Access-Control-Allow-Origin": "*",
            "Access-Control-Allow-Methods": "POST",
          },
          json: {
            virtual_experience_type_name: virtual_experience_type,
            user:
              assigned_users[0]?.first_name +
              " " +
              assigned_users[0]?.last_name,
            customer: customer.dataValues.full_name,
            email: customer.dataValues.email,
            phone_number: customer.dataValues.phone_number,
            company: assigned_users[0]?.client?.name,
            user_email: assigned_users[0]?.email,
            user_id: assigned_users[0]?.id,
            customer_id: customer_id,
            client_id: client_id,
            virtual_experience_id: virtual_experience_id,
            product_count: Object.keys(req.body.products).length ?? 0,
            product_list: product_list,
            products: req.body.products,
            enter_time: enter_time,
            exit_time: exit_time,
            request_info: request_info,
          },
        }
      )
      .json();
    console.log("Activity: ", customer_activity);

    // const customer_summary = 
  } else if (
    virtual_experience_type === "Real Estates" &&
    assigned_users[0]?.receive_email === true
  ) {
    const customer_activity = await got
      .post(
        "https://platform.1clickdesign.com/api/notification/real_estate_activity",
        {
          headers: {
            "Access-Control-Allow-Credentials": "true",
            "Access-Control-Allow-Origin": "*",
            "Access-Control-Allow-Methods": "POST",
          },
          json: {
            virtual_experience_type_name: virtual_experience_type,
            user:
              assigned_users[0]?.first_name +
              " " +
              assigned_users[0]?.last_name,
            customer: customer.dataValues.full_name,
            email: customer.dataValues.email,
            phone_number: customer.dataValues.phone_number,
            company: assigned_users[0]?.client.name,
            user_email: assigned_users[0]?.email,
            user_id: assigned_users[0]?.id,
            customer_id: customer_id,
            client_id: client_id,
            virtual_experience_id: virtual_experience_id,
            virtual_experience_name: virtual_experience.dataValues.name,
            enter_time: enter_time,
            exit_time: exit_time,
            expression_message: req.body.expression_message,
            request_to_apply: req.body.request_to_apply,
          },
        }
      )
      .json();
    console.log("Activity: ", customer_activity);
  }

  return res.json({ status: 204, message: "Showroom actions" });
};
