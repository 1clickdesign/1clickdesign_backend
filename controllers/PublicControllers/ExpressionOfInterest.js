import Clients from "../../models/ClientModel.js";
import ExpressionOfInterests from "../../models/ExpressionOfInterestModel.js";
import RealEstates from "../../models/RealEstateModel.js";
import VirtualExperiences from "../../models/VirtualExperienceModel.js";
import got from "got";

import {Op} from "sequelize";

export const get_all_expression_of_interest = async (req, res) => {
  let query = req.query;

  let interests;

  try {
    if (query && query.search !== "null") {
      if(req.query.id !== "null") {
        interests = await ExpressionOfInterests.findAndCountAll({
          offset: parseInt(query.offset),
          limit: parseInt(query.limit),
          nest: true,
          where: {
            [Op.or]: {
              name: {
                [Op.substring]: query.search,
              },
              email: {
                [Op.substring]: query.search,
              },
            }
          },
          include: [
            {
              model: RealEstates,
              attributes: ["virtual_experience_id"],
              include: [
                {
                  model: VirtualExperiences,
                  attributes: ["id", "name"],
                  where: {
                    client_id: query.id
                  }
                }
              ],
            }
          ],
          order: [["created_at", "DESC"]]
        });
      } else {
        interests = await ExpressionOfInterests.findAndCountAll({
          offset: parseInt(query.offset),
          limit: parseInt(query.limit),
          nest: true,
          where: {
            [Op.or]: {
              name: {
                [Op.substring]: query.search,
              },
              email: {
                [Op.substring]: query.search,
              },
            }
          },
          include: [
            {
              model: RealEstates,
              attributes: ["virtual_experience_id"],
              include: [
                {
                  model: VirtualExperiences,
                  attributes: ["id", "name"],
                }
              ],
            }
          ],
          order: [["created_at", "DESC"]]
        });
      }
    } else {

      if(query.id !== "null") {
        interests = await ExpressionOfInterests.findAndCountAll({
          offset: parseInt(query.offset),
          limit: parseInt(query.limit),
          nest: true,
          include: [
            {
              model: RealEstates,
              attributes: ["virtual_experience_id"],
              include: [
                {
                  model: VirtualExperiences,
                  attributes: ["id", "name"],
                  where: {
                    client_id: query.id
                  }
                }
              ],
            }
          ],
          order: [["created_at", "DESC"]]
        });
      } else {
        interests = await ExpressionOfInterests.findAndCountAll({
          offset: parseInt(query.offset),
          limit: parseInt(query.limit),
          nest: true,
          include: [
            {
              model: RealEstates,
              attributes: ["virtual_experience_id"],
              include: [
                {
                  model: VirtualExperiences,
                  attributes: ["id", "name"]
                }
              ],
            }
          ],
          order: [["created_at", "DESC"]]
        });
      }
      
    }

    if (!interests) {
      return res.status(204).json({ message: "No Expression of Interests found" });
    }

    return res.status(200).json({ expression_of_interests: interests });
  } catch (error) {
    console.log(error);
    return res.status(400).json();
  }
};

export const get_expression_of_interest = async (req, res) => {
  let { id } = req.params;

  let expression_of_interest;

  try {
    expression_of_interest = await ExpressionOfInterests.findOne({
      where: {
        id: id,
      },
      plain: true,
      nest: true,
      include: [
        {
          model: RealEstates,
          attributes: ["virtual_experience_id"],
          include: [
            {
              model: VirtualExperiences,
              attributes: ["id", "name"],
              include: [
                {
                  model: Clients,
                  attributes: ["id", "name"]
                }
              ],
            },
          ],
        }
      ],
    });

    if (!expression_of_interest) {
      return res.status(400).json({ message: "No Expression of Interest found" });
    }

    return res.status(200).json(expression_of_interest);
  } catch (error) {
    console.log(error);
  }
};


export const create_expression_of_interest = async (req, res) => {

  const {name, email, sendTo, message, real_estate_id, real_estate_name} = req.body;

  try {
    const customer_activity = await got
    .post(
      "https://platform.1clickdesign.com/api/expression_of_interest",
      // "http://localhost:443/api/expression_of_interest",
      {
        headers: {
          "Access-Control-Allow-Credentials": "true",
          "Access-Control-Allow-Origin": "*",
          "Access-Control-Allow-Methods": "POST",
        },
        json: {
          sendTo: sendTo,
          customer: name,
          real_estate: real_estate_name,
          message: message
        }
      }
    )

    const interest = await ExpressionOfInterests.create({
      name: name,
      message: message,
      email: email,
      real_estate_id: real_estate_id
    })

    if(!interest) {
      return res.status(400).json({message: "Something went wrong"})
    }

    return res.status(200).json({message: "Successfully Created"})
  } catch (error) {
    console.log(error)
  }
}

export const delete_expression_of_interest = async (req, res) => {

  const {id} = req.params;

  try {
    await ExpressionOfInterests.destroy({
      where: {
        id: id,
      }
    })
    return res.status(200).json({message: "Expression of Interest was successfully deleted"})
  } catch (error) {
    console.log(error)
    return res.status(400).json();
  }
}