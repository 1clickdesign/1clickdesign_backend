import ClientCustomers from "../../models/ClientCustomerModel.js"
import Customers from "../../models/CustomerModel.js"
import { Op } from "sequelize"
import { Resend } from "resend"

export const create_customer = async (req, res) => {
  const { email, full_name, latitude, longitude } = req.body

  try {
    const [customer, created] = await Customers.findOrCreate({
      where: {
        email: email,
      },
      defaults: {
        full_name: full_name,
        latitude: latitude,
        longitude: longitude,
      },
    })

    if (!customer) {
      return res.status(400).json()
    }

    return res.status(200).json(created, customer)
  } catch (error) {
    console.log(error)
  }
}

export const create_customer_registered = async (req, res) => {
  const {
    full_name,
    email,
    image_url,
    gender,
    shipping_address,
    latitude,
    longitude,
    place_id,
    phone_number,
    email_verification_status,
    status,
    token,
    city,
    state,
    region,
    country,
    zip_code,
    client_id,
  } = req.body

  const existed = await Customers.findOne({
    where: {
      [Op.or]: {
        full_name: full_name,
        email: email,
      },
    },
  })

  if (existed) {
    return res
      .status(200)
      .json({ id: existed.dataValues.id, name: existed.dataValues.full_name })
  }
  try {
    const customer = await Customers.create({
      full_name: full_name,
      email: email,
      image_url: "",
      gender: gender,
      latitude: latitude,
      longitude: longitude,
      place_id: place_id,
      country: country,
      state: state,
      city: city,
      region: region,
      zip_code: zip_code,
      shipping_address: shipping_address,
      phone_number: phone_number,
      email_verification_status: email_verification_status,
      token: null,
      status: status,
    })

    if (!customer) {
      return res
        .status(400)
        .json({ message: "Customer was not successfully created" })
    }

    if (client_id) {
      const client = await ClientCustomers.create({
        client_id: client_id,
        customer_id: customer.dataValues.id,
        user_id: null,
      })
    }

    return res.status(201).json({
      id: customer.dataValues.id,
      name: customer.dataValues.full_name,
    })
  } catch (error) {
    console.log(error)
  }
}

export const get_customer_data = async (req, res) => {
  let { id } = req.params

  try {
    const customer = await Customers.findOne({
      where: {
        id: id,
      },
      attributes: ["id", "full_name"],
    })

    if (!customer) {
    }

    return res.status(200).json(customer)
  } catch (error) {
    console.log(error)
  }
}

export const sendEmail = async (req, res) => {
  // TODO: Change words base on type

  const { data, type } = req.body
  console.log(data)
  const resend = new Resend("re_ZDjQoiZK_FV8BiJTwz338diygw5Brqf8v")
  const content1CD =
    type == "message"
      ? `
    Dear 1 Click Design Sales Team, <br/>
    This is an automated notification to inform you that ${data.email} from ${
          data.company
        } has requested a demo in our ${
          data.showroomType == "art_gallery"
            ? "Virtual Art Gallery"
            : data.showroomType == "showroom"
            ? "Virtual Showroom"
            : data.showroomType == "real_estate"
            ? "Virtual Real Estate"
            : ""
        } on ${
          data.date
        }. Please reach out to them at your earliest convenience to arrange the demonstration.<br/>
    Thank you.
  `
      : type == "schedule-live"
      ? `This is an automated notification to inform you that ${data.name} has requested to schedule a live session.

      Details:
      Name: ${data.name}
      Email: ${data.email}
      Message: ${data.message}
      
      Please reach out to them at your earliest convenience to confirm and arrange the live session.`
      : type == "contact"
      ? `This is an automated notification to inform you that a new contact form submission has been received.

      Details:
      Name: ${data.name}
      Email: ${data.email}
      Message: ${data.message}
      
      Please reach out to the customer at your earliest convenience to address their inquiry.`
      : type == "schedule-meeting"
      ? `This is an automated notification to inform you that ${data.name} has requested to schedule a meeting.<br/>
  <br/>
      Details<br/>
      Name: ${data.name}<br/>
      Email: ${data.email}<br/>
      Meeting Date: ${data.date}<br/>
      Message: ${data.message}<br/>
      <br/><br/>
      Please reach out to them at your earliest convenience to confirm and arrange the meeting.`
      : ""

  const contentCustomer =
    type == "message"
      ? `
      Dear ${data.name}, <br/><br>

      Thank you for your interest in our Virtual Demo! We have received your request and will get back to you shortly to arrange the demonstration.<br/><br>
      
      Here are the details we received:<br>
      Email: ${data.email}<br>
      Company: ${data.company}<br><br>

      We look forward to connecting with you soon!<br/><br>

      Best regards,<br>
      1 Click Design`
      : type == "schedule-live"
      ? `Dear ${data.name},<br><br>

      Thank you for your interest in scheduling a live session with us! We have received your request and will get back to you shortly to confirm the details.<br><br>
      
      Here are the details we received:<br>
      Email: ${data.email}<br>
      Message: ${data.message}<br><br>
      
      We look forward to connecting with you soon!<br><br>
      
      Best regards,<br>
      1 Click Design`
      : type == "contact"
      ? `Dear ${data.name},<br/><br/>

      Thank you for reaching out to us! We have received your message and will get back to you as soon as possible.<br><br>
      
      Here are the details we received:<br>
      Email: ${data.email}<br>
      Message: ${data.message}<br><br>
      
      If you have any additional information or questions, feel free to reply to this email.<br/>
      
      Best regards,<br/>
      1 Click Design`
      : type == "schedule-meeting"
      ? `Dear ${data.name},<br><br>
  
      Thank you for your interest in scheduling a meeting with us! We have received your request and will get back to you shortly to confirm the details.<br><br>
      
      Here are the details we received:<br>
      Email: ${data.email}<br>
      Meeting Date: ${data.date}<br>
      Message: ${data.message}<br><br>
      
      We look forward to connecting with you soon!<br><br>
      
      Best regards,<br>
      1 Click Design`
      : ""

  const resendRes = await resend.emails.send({
    from: "1 Click Design <info@1clickdesign.us>",
    to: "marketing@1clickdesign.com",
    subject:
      type == "message"
        ? "Requesting Demo"
        : type == "contact"
        ? "1ClickDesign | Contact Us"
        : "Scheduled Live Showing",
    html: content1CD || "",
    text: content1CD,
    headers: {
      "List-Unsubscribe": "<https://emailer.1clickdesign.com/unsubscribe>",
    },
  })

  const sendEmailCustomerRes = await resend.emails.send({
    from: "1 Click Design LLC <info@1clickdesign.us>",
    to: [data?.email],
    subject:
      type == "message"
        ? "Requesting Demo"
        : type == "contact"
        ? "1ClickDesign | Contact Us"
        : "Scheduled Live Showing",
    html: contentCustomer || "",
    text: contentCustomer,
    headers: {
      "List-Unsubscribe": "<https://emailer.1clickdesign.com/unsubscribe>",
    },
  })

  console.log("resendred", resendRes)
  console.log("resendRes", sendEmailCustomerRes)

  return res.json({ message: "success" })
}
