import Products from "../../models/ProductModel.js";
import ProductNodes from "../../models/ProductNodeModel.js";

export const bulk_create_product_nodes = async (req, res) => {
  const{nodes} = req.body;

  if(nodes) {
    for (let index = 0; index < nodes.length; index++) {
      const element = nodes[index];
      try {
        const product = await Products.findOne({
          where: {
            model: element.model,
          },
        });

        if(!product) {
          const search_node = await ProductNodes.findOne({
            where: {
              model: element.model,
              name: element.component_name
            }
          })

          if(!search_node) {
            const create_node = await ProductNodes.create({
              name: element.component_name,
              model: element.model,
              showroom_link: element.showroom_link,
              x: element.x,
              y: element.y,
              yaw: element.yaw,
              z: element.z,
              pitch: element.pitch,
              status: element.status,
              product_id: null,
              created_by: element.created_by,
            });
          } else {
            const update_node = await ProductNodes.update({
              name: element.component_name,
              model: element.model,
              showroom_link: element.showroom_link,
              x: element.x,
              y: element.y,
              yaw: element.yaw,
              z: element.z,
              pitch: element.pitch,
              status: element.status,
              product_id: null,
              updated_by: JSON.stringify(element.created_by),
            }, {
              where: {
                id: search_node.dataValues.id
              }
            });
          }
          
        } else {
          const product_node_exists = await ProductNodes.findOne({
            where: {
              name: element.component_name,
              product_id: product.dataValues.id,
            },
            // include: [
            //   {
            //     model: Products,
            //     where: {
            //       name: element.component_name
            //     },
            //   },
            // ],
          });
    
      
          if (!product_node_exists) {
            const create_node = await ProductNodes.create({
              name: element.component_name,
              model: element.model,
              showroom_link: element.showroom_link,
              x: element.x,
              y: element.y,
              yaw: element.yaw,
              z: element.z,
              pitch: element.pitch,
              status: element.status,
              product_id: product.dataValues.id,
              created_by: element.created_by,
            });
  
          } else {
            const update_node = await ProductNodes.update(
              {
                name: element.component_name,
                model: element.model,
                showroom_link: element.showroom_link,
                x: element.x,
                y: element.y,
                yaw: element.yaw,
                z: element.z,
                pitch: element.pitch,
                status: element.status,
                product_id: product.dataValues.id,
                updated_by: JSON.stringify(element.created_by),
              },
              {
                where: {
                  id: product_node_exists.dataValues.id,
                },
              }
            );
        
          }
        }
      } catch (error) {
        console.log(error);
      }
    }
    return res.status(200).json();
  }
}

export const create_product_node = async (req, res) => {
  const { model, showroom_link, component_name, pitch, x, y, yaw, z, status, created_by } =
    req.body;

  try {
    const product = await Products.findOne({
      where: {
        model: model,
      },
    });


    if(!product) {
      console.log("product doesnt exist")
      const search_node = await ProductNodes.findOne({
        where: {
          name: component_name,
          showroom_link: showroom_link
        }
      })

      if(!search_node) {
        const create_node = await ProductNodes.create({
          name: component_name,
          model: model,
          showroom_link: showroom_link,
          x: x,
          y: y,
          yaw: yaw,
          z: z,
          pitch: pitch,
          status: status,
          product_id: null,
          created_by: created_by,
        });

        return res.status(200).json({ message: "Created product node successfully" });
      } else {
        const update_node = await ProductNodes.update({
          name: component_name,
          model: model,
          showroom_link: showroom_link,
          x: x,
          y: y,
          yaw: yaw,
          z: z,
          pitch: pitch,
          status: status,
          product_id: null,
          updated_by: created_by,
        }, {
          where: {
            id: search_node.dataValues.id
          }
        });

        return res.status(200).json({ message: "Updated product node successfully" });
      }
    }

    const product_node_exists = await ProductNodes.findOne({
      where: {
        name: component_name,
        product_id: product.dataValues.id,
      },
      // include: [
      //   {
      //     model: Products,
      //     where: {
      //       model: product.dataValues.model,
      //       name: component_name
      //     },
      //   },
      // ],
    });

    if (!product_node_exists) {
      const create_node = await ProductNodes.create({
        name: component_name,
        model: model,
        showroom_link: showroom_link,
        x: x,
        y: y,
        yaw: yaw,
        z: z,
        pitch: pitch,
        status: status,
        product_id: product.dataValues.id,
        created_by: created_by,
      });

      return res.status(200).json({ message: "Created product node for product successfully" });
    }

    const update_node = await ProductNodes.update(
      {
        name: component_name,
        model: model,
        showroom_link: showroom_link,
        x: x,
        y: y,
        yaw: yaw,
        z: z,
        pitch: pitch,
        status: status,
        product_id: product.dataValues.id,
        updated_by: created_by,
      },
      {
        where: {
          id: product_node_exists.dataValues.id,
        },
      }
    );

    if (!update_node) {
      return res.status(400).json();
    }

    return res.status(200).json({ message: "Updated product node for product successfully" });
  } catch (error) {
    console.log(error);
  }
};
