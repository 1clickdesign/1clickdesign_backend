import AnalyticsVideoMinutes from "../../models/AnalyticsVideoMinuteModel.js";
import ClientCustomers from "../../models/ClientCustomerModel.js";
import Customers from "../../models/CustomerModel.js";
import VideoConferences from "../../models/VideoConferenceModel.js";
import VideoConferenceParticipants from "../../models/VideoConferenceParticipantModel.js";
import VideoConferenceShowrooms from "../../models/VideoConferenceShowroomModel.js";

export const scheduleMeeting = async (req, res) => {
  const {
    start_date,
    end_date,
    host_id,
    backup_host_id,
    showrooms,
    participants,
    created_by,
  } = req.body;

  try {
    const schedule = await VideoConferences.create({
      status: "Pending",
      start_date: start_date,
      end_date: end_date,
      host_id: host_id,
      backup_host_id: backup_host_id,
    });

    if (!schedule) {
    }

    let virtual_experiences = [];

    for (let index = 0; index < showrooms.length; index++) {
      const element = showrooms[index];
      virtual_experiences.push({
        virtual_experience_id: element,
        video_conference_id: schedule.dataValues.id,
        created_by: created_by,
      });
    }

    const showrooms = await VideoConferenceShowrooms.bulkCreate(
      virtual_experiences
    );

    if (!showrooms) {
    }

    const video_conference_participants = [];

    for (let index = 0; index < participants.length; index++) {
      const element = participants[index];
      if (element.type === "Customer") {
        video_conference_participants.push({
          video_conference_id: schedule.dataValues.id,
          customer_id: element.id,
          unregistered_customer_id: null,
          created_by: created_by,
        });
      } else {
        video_conference_participants.push({
          video_conference_id: schedule.dataValues.id,
          customer_id: null,
          unregistered_customer_id: element.id,
          created_by: created_by,
        });
      }
    }

    const participants = await VideoConferenceParticipants.bulkCreate(
      video_conference_participants
    );
  } catch (error) {}
};

export const getRoomDetails = async (req, res) => {
  let { id } = req.params;

  try {
    const room = VideoConferences.findOne({
      where: {
        id: id,
      },
      include: [
        {
          model: VideoConferenceParticipants,
        },
        {
          model: VideoConferenceShowrooms,
        },
      ],
    });
  } catch (error) {}
};

export const create_video_conference = async (req, res) => {
  const {
    title,
    status,
    host_id,
    backup_host_id,
    start_time,
    end_time,
    customer_id,
    virtual_exp_id,
    customer
  } = req.body;

  console.log(req.body);

  let customer_create;
  if(!customer_id) {
    customer_create = await Customers.create(customer)
  }

  let assign_customer = await ClientCustomers.findOne({
    where: {
      client_id: req.body.client_id,
      customer_id: customer_id ?? customer_create.dataValues.id,
      user_id: host_id,
    }
  })

  if(!assign_customer) {
    const assign_customer_user = await ClientCustomers.create({
      client_id: req.body.client_id,
      customer_id: customer_id ?? customer_create.dataValues.id,
      user_id: host_id,
    })
  }

  try {
    const conference = await VideoConferences.create({
      title: title,
      status: status,
      start_time: start_time,
      end_time: end_time,
      host_id: host_id,
      backup_host_id: backup_host_id,
      customer_id: customer_id ?? customer_create.dataValues.id,
    });

    if (!conference) {
      return res
        .status(202)
        .json({ message: "Video Conference was not successfully created" });
    }

    await VideoConferenceParticipants.create({
      customer_id: customer_id ?? customer_create.dataValues.id,
      video_con_id: conference.dataValues.id,
    });

    await VideoConferenceShowrooms.create({
      virtual_exp_id: virtual_exp_id,
      video_con_id: conference.dataValues.id,
    });

    return res
      .status(200)
      .json({ message: "Package was successfully created" });
  } catch (error) {
    console.log(error);
    return res.status(400).json();
  }
};

export const addVideoConferenceMinutes = async (req, res) => {
  const { customer_id, user_id, video_conference_id, duration } = req.body;

  try {
    const video_minutes = await AnalyticsVideoMinutes.create({
      customer_id: customer_id,
      user_id: user_id,
      video_conference_id: video_conference_id,
      duration: parseFloat(duration),
    });

    if (!video_minutes) {
      return res.status(400).json();
    }

    return res.status(200).json();
  } catch (error) {
    console.log(error);
  }
};
