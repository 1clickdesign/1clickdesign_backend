import AnalyticsFrameDuration from "../../models/AnalyticsFrameDurationModel.js"
import AnalyticsViewedProducts from "../../models/AnalyticsViewedProductModel.js"
import AnalyticsVirtualFlythrough from "../../models/AnalyticsVirtualFlythroughModel.js"
import AnalyticsVisitDuration from "../../models/AnalyticsVisitDurationModel.js"

//-----Visit Duration API-------//
export const shapeSparkStayDuration = async (req, res) => {
  const { duration, customer_id, virtual_experience_id } = req.body
  try {
    const shapespark_duration = await AnalyticsVisitDuration.create({
      duration: parseFloat(duration),
      virtual_experience_id: virtual_experience_id,
      customer_id: customer_id,
      created_by: {
        id: null,
        name: "System",
        role: null,
      },
    })

    if (!shapespark_duration) {
      return res.status(400).json({ message: "Not created" })
    }

    return res.status(200).json({ message: "Successfully created" })
  } catch (error) {
    console.log(error)
  }
}

//-----Frame Duration API-------//
export const productStayDuration = async (req, res) => {
  const { duration, customer_id, product_id } = req.body
  try {
    const frame_duration = await AnalyticsFrameDuration.create({
      duration: parseFloat(duration),
      product_id: product_id,
      customer_id: customer_id,
      created_by: {
        id: null,
        name: "System",
        role: null,
      },
    })

    if (!frame_duration) {
      return res.status(400).json({ message: "Not created" })
    }

    return res.status(200).json({ message: "Successfully created" })
  } catch (error) {
    console.log(error)
  }
}

//-----Frame Viewed API-------//
export const productViewedCount = async (req, res) => {
  const { products, customer_id, virtual_experience_id } = req.body
  try {
    let viewed_products = []
    for (let index = 0; index < products.length; index++) {
      const element = products[index]
      viewed_products.push({
        count: element.count,
        product_id: element.product_id,
        virtual_experience_id: virtual_experience_id,
        customer_id: customer_id,
        created_by: {
          id: null,
          name: "System",
          role: null,
        },
      })
    }

    const product_viewed_count = await AnalyticsViewedProducts.bulkCreate(
      viewed_products,
    )

    if (!product_viewed_count) {
      return res.status(400).json({ message: "Not created" })
    }

    return res.status(200).json({ message: "Successfully created" })
  } catch (error) {
    console.log(error)
  }
}

export const create_virtual_flythrough_analytics = async (req, res) => {
  console.log(req.body)
  const {
    video_duration,
    watch_duration,
    isFinish,
    customer_id,
    invitation_flythrough_id,
    virtual_flythrough_id,
  } = req.body
  try {
    const virtual_flythrough_analytics =
      await AnalyticsVirtualFlythrough.create({
        video_duration: parseFloat(video_duration),
        watch_duration: parseFloat(watch_duration),
        isFinish: isFinish,
        customer_id: customer_id,
        invitation_flythrough_id: invitation_flythrough_id,
        virtual_flythrough_id: virtual_flythrough_id,
        created_by: {
          id: null,
          name: "System",
          role: null,
        },
      })

    if (!virtual_flythrough_analytics) {
      return res.status(400).json({ message: "Not created" })
    }

    return res.status(200).json({ message: "Successfully created" })
  } catch (error) {
    console.log(error)
  }
}
