import Customers from "../../models/CustomerModel.js"
import Invitations from "../../models/InvitationModel.js"
import Client from "../../models/ClientModel.js"
import SentEmails from "../../models/SentEmailModel.js"
import UserBackupUsers from "../../models/UserBackupUserModel.js"
import Users from "../../models/UserModel.js"
import VirtualExperiences from "../../models/VirtualExperienceModel.js"
import { Op } from "sequelize"
import InvitationFlythrough from "../../models/InvitationFlythroughModel.js"
import Clients from "../../models/ClientModel.js"

export const check_customer_invited = async (req, res) => {
  const { full_name, email, link_name } = req.query

  try {
    const customer = await Customers.findOne({
      where: {
        [Op.or]: {
          full_name: full_name,
          email: email,
        },
      },
    })

    if (!customer) {
      return res.status(204).json()
    }

    const if_invited = await Invitations.findOne({
      where: {
        customer_id: customer.dataValues.id,
      },
      include: [
        {
          model: Customers,
          attributes: ["full_name", "id"],
        },
        {
          model: VirtualExperiences,
          where: {
            link_name: link_name,
          },
        },
        {
          model: Users,
          as: "invitation_user",
        },
        {
          model: Users,
          as: "invitation_backup_user",
        },
      ],
      order: [["created_at", "DESC"]],
    })

    if (!if_invited) {
      const data = {
        pin_code: "",
        customer_id: customer.dataValues.id,
        customer_name: customer.dataValues.full_name,
        link_name: link_name,
        agents: [],
      }
      return res.status(200).json({ message: "Not Invited", data: data })
    }

    let agents = []

    if (if_invited.dataValues.invitation_backup_user) {
      agents = [
        if_invited.dataValues.invitation_user,
        if_invited.dataValues.invitation_backup_user,
      ]
    } else {
      agents = [if_invited.dataValues.invitation_user]
    }

    const data = {
      pin_code: if_invited.dataValues.passcode,
      customer_id: customer.dataValues.id,
      customer_name: customer.dataValues.full_name,
      link_name: link_name,
      agents: agents,
    }

    return res.status(200).json({ message: "Invited", data: data })
  } catch (error) {
    console.log(error)
  }
}

export const check_invitation = async (req, res) => {
  const { id, email, pincode } = req.query

  try {
    const invitation = await Invitations.findOne({
      nest: true,
      where: {
        id: id,
        passcode: pincode,
      },
      include: [
        {
          model: Customers,
          where: {
            email: email,
          },
        },
        {
          model: Users,
          as: "invitation_user",
        },
        {
          model: Users,
          as: "invitation_backup_user",
        },
      ],
    })

    console.log(invitation)

    if (!invitation) {
      return res.status(204).json()
    }

    console.log(new Date(invitation.access_expiration_date), new Date())

    let expired = false
    if (new Date(invitation.access_expiration_date) < new Date()) {
      expired = false
    } else {
      expired = true
    }

    return res.status(200).json({
      message: "Valid credentials",
      expired: expired,
      invitation: invitation,
    })
  } catch (error) {
    console.log(error)
  }
}

export const check_invitation_expiration = async (req, res) => {
  const { pincode, link_name } = req.query

  try {
    const invitation = await Invitations.findOne({
      nest: true,
      where: {
        passcode: pincode,
      },
      include: [
        {
          model: VirtualExperiences,
          // where: {
          //   link_name: link_name,
          // },
          include: [{ model: Client, as: "client" }],
        },
        { model: Customers },
        {
          model: Users,
          as: "invitation_user",
        },
        {
          model: Users,
          as: "invitation_backup_user",
        },
      ],
    })

    console.log(invitation)

    if (!invitation) {
      return res.status(204).json()
    }

    console.log(new Date(invitation.access_expiration_date), new Date())

    let expired = false
    if (new Date(invitation.access_expiration_date) > new Date()) {
      expired = false
    } else {
      expired = true
    }

    return res.status(200).json({
      message: "Valid credentials",
      expired: expired,
      invitation: invitation,
    })
  } catch (error) {
    console.log(error)
  }
}

export const check_flythrough_invitation_expiration = async (req, res) => {
  const { id, link_name } = req.query

  try {
    const invitation = await InvitationFlythrough.findOne({
      nest: true,
      where: {
        id: id,
      },
      include: [
        {
          model: VirtualExperiences,
          // where: {
          //   link_name: link_name,
          // },
          include: [{ model: Client, as: "client" }],
        },
        { model: Customers },
        {
          model: Users,
          as: "invitation_flythrough_user",
        },
        {
          model: Users,
          as: "invitation_flythrough_backup_user",
        },
      ],
    })

    console.log(invitation)

    if (!invitation) {
      return res.status(200).json({
        message: "Valid credentials",
        expired: true,
        invitation: invitation,
      })
    }

    console.log(new Date(invitation.access_expiration_date), new Date())

    let expired = false
    if (new Date(invitation.access_expiration_date) > new Date()) {
      expired = false
    } else {
      expired = true
    }

    return res.status(200).json({
      message: "Valid credentials",
      expired: expired,
      invitation: invitation,
    })
  } catch (error) {
    console.log(error)
  }
}
export const get_invitations = async (req, res) => {
  console.log(req.query)
  try {
    const userInclude = {
      model: Users,
      as: "invitation_user",
    }

    if (req.query.id !== "982789392") {
      userInclude.where = {
        client_id: req.query.id,
      }
    }
    const invitation = await Invitations.findAll({
      nest: true,
      order: [["created_at", "ASC"]],
      include: [
        userInclude,
        {
          model: Clients,
        },
        {
          model: VirtualExperiences,
          as: "virtual_experience",
        },
        {
          model: Customers,
          as: "customer",
        },
        {
          model: SentEmails,
          as: "sent_email",
        },
      ],
    })

    return res.status(200).json({
      message: "Invitation successfully retrieved",
      invitation: invitation,
    })
  } catch (error) {
    console.error("Error getting Invitation :", error)
    return res.status(500).json({
      message: "Error getting Invitation",
      error: error.message,
    })
  }
}
export const get_flythrough_invitation = async (req, res) => {
  console.log(req.query)
  try {
    const userInclude = {
      model: Users,
      as: "invitation_flythrough_user",
    }

    if (req.query.id !== "982789392") {
      userInclude.where = {
        client_id: req.query.id,
      }
    }
    const invitation_flythrough = await InvitationFlythrough.findAll({
      nest: true,
      order: [["created_at", "ASC"]],
      include: [
        {
          model: VirtualExperiences,
          include: [{ model: Client, as: "client" }],
        },
        { model: Customers },
        userInclude,
        {
          model: Users,
          as: "invitation_flythrough_backup_user",
        },
      ],
    })

    return res.status(200).json({
      message: "Invitation flythrough successfully retrieved",
      invitation_flythrough: invitation_flythrough,
    })
  } catch (error) {
    console.error("Error getting Invitation flythrough :", error)
    return res.status(500).json({
      message: "Error getting Invitation flythrough ",
      error: error.message,
    })
  }
}
export const update_email_status = async (req, res) => {
  const body = req.body
  console.log(body)

  let type = "sent"
  switch (body.type) {
    case "email.sent":
      type = "Sent"
      break
    case "email.delivered":
      type = "Delivered"
      break
    case "email.delivery_delayed":
      type = "Delivered Delay"
      break
    case "email.complained":
      type = "Complained"
      break
    case "email.bounced":
      type = "Bounced"
      break
    case "email.opened":
      type = "Opened"
      break
    case "email.clicked":
      type = "Clicked"
      break
    default:
      break
  }

  const status = await SentEmails.update(
    {
      status: type,
      updated_by: {
        id: null,
        name: "System",
        role: null,
      },
    },
    {
      where: {
        email_id: body.data.email_id,
      },
    },
  )

  if (!status) {
    return res.sendStatus(400)
  }

  return res.sendStatus(200)
}

export const update_email_id = async (req, res) => {
  const body = req.body
  console.log(req.body)

  const status = await SentEmails.update(
    {
      email_id: body.email_id,
      updated_by: body.updated_by,
    },
    {
      where: {
        id: body.id,
      },
    },
  )

  if (!status) {
    return res.status(400).json()
  }

  return res.status(200).json()
}
