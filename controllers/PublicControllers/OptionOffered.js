import OptionOffered from "../../models/OptionOfferedModel.js"
import ProductOptionOffered from "../../models/ProductOptionOfferedModel.js"
import ProductOptionOfferedRequests from "../../models/ProductOptionOfferedRequestModel.js"
import ProductOtherOptions from "../../models/ProductOtherOptionsModel.js"

export const get_product_option_offered = async (req, res) => {
  try {
    const options = await OptionOffered.findAll({
      order: [["is_multiple", "DESC"]]
    })

    if(!options) {
      return res.status(400).json({message: "No Option Offered"})
    }

    return res.status(200).json({option_offered: options})
  } catch (error) {
    console.log(error)
  }
}

export const product_other_option = async (req, res) => {
  const {customer_id, product_id, option_offered_id, virtual_experience_id} = req.body;
  try {
    const other_option = await ProductOtherOptions.create({
      customer_id: customer_id,
      product_id: product_id,
      option_offered_id: option_offered_id,
      virtual_experience_id: virtual_experience_id,
      created_by: {
        id: null,
        name: "System",
        role: null
      }
    })

    if(!other_option) {
      return res.status(400).json()
    }

    return res.status(200).json({message: "Success"})
  } catch (error) {
    
  }
}

export const request_product_option_offered = async (req, res) => {
  const {option_offered_ids, product_id, customer_id, assigned_users, virtual_experience_id, client_id } = req.body;
  try {
    let option_offered = []
    if(option_offered_ids && option_offered_ids.length > 0) {
      for (let index = 0; index < option_offered_ids.length; index++) {
        const element = option_offered_ids[index];
        if(assigned_users && assigned_users.length > 0) {
          for (let index = 0; index < assigned_users.length; index++) {
            const data = assigned_users[index];
            option_offered.push({opt_offer_id: element, product_id: product_id, sent_email_id: null, customer_id: customer_id, virtual_exp_id: virtual_experience_id, client_id: client_id, assigned_user_id: data, created_by: {id: null, name: "System", role: null}})
          }
        } else {
          option_offered.push({opt_offer_id: element, product_id: product_id, sent_email_id: null, customer_id: customer_id, virtual_exp_id: virtual_experience_id, client_id: client_id, created_by: {id: null, name: "System", role: null}})
        }
      }
    }

    const offered = await ProductOptionOfferedRequests.bulkCreate(option_offered)

    if(!offered) {
      return res.status(400).json()
    }

    return res.status(200).json({message: "Success"})
  } catch (error) {
    console.log(error)
  }
}