import { Op } from "sequelize";
import Users from "../../models/UserModel.js";
import bcrypt from "bcrypt";
import jwt from "jsonwebtoken";

export const login = async (req, res) => {
  const { emailUsername, password } = req.body;
  console.log(req.body);

  try {
    const user = await Users.findOne({
      where: {
        [Op.or]: {
          email: emailUsername,
          username: emailUsername,
        },
        status: "Active",
      },
      plain: true,
    });
    console.log(user);

    if(!user) {
      return res.status(400).json({ message: "Username or Email doesn't exists" });
    }

    const existingToken = user?.dataValues?.token;
    console.log("Existing: ", existingToken);

    let expired = false;

    if(existingToken) {
      jwt.verify(existingToken, process.env.TOKEN, function (err, decoded) {
        if (err) {
          if(err.message === "jwt expired") {
            expired = true;
          }
        }
      });
    }


    bcrypt.compare(password, user.password).then(async (data) => {
      if (data) {
        const token = jwt.sign(
          {
            userId: user.id,
            userEmail: user.email,
          },
          process.env.TOKEN,
          { expiresIn: "365d" }
        );

        const passToken = expired || !existingToken ? token : existingToken;

        await Users.update(
          {
            token: passToken,
            last_sign_in: new Date(),
          },
          {
            where: {
              id: user.id,
            },
          }
        );
        return res.status(200).json({
          message: "Login successful",
          email: user.email,
          token: passToken,
        });
      } else {
        return res.status(400).json({ message: "Password is incorrect" });
      }
    });
  } catch (error) {
    console.log("error: ", error);
  }
};
