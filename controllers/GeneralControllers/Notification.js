import { Op } from "sequelize";
import Notifications from "../../models/NotificationModel.js";

export const get_all_notifications = async (req, res) => {
  let query = req.query;

  console.log(query);

  let notifications;

  try {
    if (query && query.search !== "null") {
      notifications = await Notifications.findAndCountAll({
        offset: parseInt(query.offset),
        limit: parseInt(query.limit),
        nest: true,
        where: {
          user_id: query.id,
          [Op.or]: {
            title: {
              [Op.substring]: query.search,
            },
            content: {
              [Op.substring]: query.search,
            },
          },
        },
        order: [["created_at", "DESC"]],
      });
    } else {
      notifications = await Notifications.findAndCountAll({
        offset: parseInt(query.offset),
        limit: parseInt(query.limit),
        where: {
          user_id: query.id,
        },
        nest: true,
        order: [["created_at", "DESC"]],
      });
    }

    if (!notifications) {
      return res.status(400).json({ message: "No Notifications found" });
    }

    return res.status(200).json({ notifications: notifications });
  } catch (error) {
    console.log("Error: ", error);
  }
};

export const get_all_notifications_unread = async (req, res) => {
  const { id } = req.query;
  let notifications;
  try {
    if (req.query && req.query.id !== "null") {
      notifications = await Notifications.findAndCountAll({
        where: {
          read: false,
          user_id: id,
        },
        nest: true,
      });
    } else {
      notifications = await Notifications.findAndCountAll({
        where: {
          read: false,
        },
        nest: true,
      });
    }

    if (!notifications) {
      return res.status(400).json({ message: "No Notifications found" });
    }

    return res.status(200).json({ notifications: notifications });
  } catch (error) {
    console.log("Error: ", error);
  }
};

export const update_all_notifications_unread = async (req, res) => {
  const { id } = req.body;
  let notifications;
  try {
    if (req.query && req.query.id !== "null") {
      notifications = await Notifications.update(
        {
          read: true,
          updated_by: {
            id: null,
            name: "System",
            role: null,
          },
        },
        {
          where: {
            read: false,
            user_id: id,
          },
        }
      );
    } else {
      notifications = await Notifications.update(
        {
          read: true,
          updated_by: {
            id: null,
            name: "System",
            role: null,
          },
        },
        {
          where: {
            read: false,
          },
        }
      );
    }

    if (!notifications) {
      return res.status(400).json({ message: "No Notifications found" });
    }

    return res.status(200).json({ notifications: notifications });
  } catch (error) {
    console.log("Error: ", error);
  }
};

export const get_notification = async (req, res) => {
  let { id } = req.params;

  let notification;

  try {
    notification = await Notifications.findOne({
      where: {
        id: id,
      },
      plain: true,
    });

    if (!notification) {
      return res.status(400).json({ message: "No Notification found" });
    }

    return res.status(200).json(notification);
  } catch (error) {
    console.log(error);
  }
};

export const create_notification = async (req, res) => {
  const { title, content, user_id, created_by } = req.body;

  try {
    const notification = await Notifications.create({
      title: title,
      content: content,
      user_id: user_id,
      created_by: created_by,
    });

    if (!notification) {
      return res
        .status(400)
        .json({ message: "Notification was not successfully created" });
    }

    return res
      .status(201)
      .json({ message: "Notification was successfully created" });
  } catch (error) {
    console.log(error);
  }
};

export const delete_notification = async (req, res) => {
  const { id } = req.params;

  try {
    await Notifications.destroy({
      where: {
        id: id,
      },
    });
    return res
      .status(200)
      .json({ message: "Notifications was successfully deleted" });
  } catch (error) {
    console.log(error);
    return res.status(400).json();
  }
};
