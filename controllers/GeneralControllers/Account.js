import ClientCategories from "../../models/ClientCategoryModel.js";
import ClientCustomers from "../../models/ClientCustomerModel.js";
import Clients from "../../models/ClientModel.js";
import Customers from "../../models/CustomerModel.js";
import Permissions from "../../models/PermissionModel.js";
import Roles from "../../models/RoleModel.js";
import UserBackupUsers from "../../models/UserBackupUserModel.js";
import Users from "../../models/UserModel.js";
import VirtualExperiences from "../../models/VirtualExperienceModel.js";

export const generateTokenToCustomer = async (req, res) => {
  const token = jwt.sign(
    {
      userId: "38c2270d-9a5b-4006-b2a0-67f57a2df208",
      userEmail: "ailouranada@gmail.com",
    },
    process.env.TOKEN,
    { expiresIn: "365d" }
  );
};

export const get_account_information = async (req, res) => {
  const authorization = req.headers.authorization;
  console.log(authorization);
  const auth = authorization.substring(7, authorization.length);
  console.log(auth);

  try {
    const user = await Users.findOne({
      where: {
        token: auth,
      },
      include: [
        {
          model: Clients,
          attributes: ["id", "name", "logo_url"],
          include: [
            {
              model: ClientCategories,
              attributes: ["id", "name"],
            },
          ],
        },
        {
          model: Users,
          as: "backup_user",
          include: [
            {
              model: VirtualExperiences,
              as: "sales",
            },
          ],
        },
        {
          model: VirtualExperiences,
          as: "sales",
        },
        {
          model: ClientCustomers,
          include: [
            {
              model: Customers,
            },
          ],
        },
        {
          model: Roles,
          attributes: ["name", "for_client"],
          include: [
            {
              model: Permissions,
              as: "role",
              attributes: ["id", "name", "category"],
            },
          ],
        },
      ],
    });

    if (!user) {
      console.log("No User");
    }

    return res.status(200).json(user);
  } catch (error) {
    console.log("Error ", error);
  }
};
