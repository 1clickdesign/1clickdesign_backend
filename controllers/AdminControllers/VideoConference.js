import { Op } from "sequelize";
import VideoConferences from "../../models/VideoConferenceModel.js";
import VideoConferenceParticipants from "../../models/VideoConferenceParticipantModel.js";
import VideoConferenceShowrooms from "../../models/VideoConferenceShowroomModel.js";
import Customers from "../../models/CustomerModel.js";
import VirtualExperiences from "../../models/VirtualExperienceModel.js";
import Users from "../../models/UserModel.js";
import got from "got";

export const get_all_video_conference = async (req, res) => {
  let query = req.query;

  let video_conferences;

  try {
    if (query && query.search !== "null" && query.search) {
      if (req.query.id !== "null") {
        video_conferences = await VideoConferences.findAndCountAll({
          where: {
            [Op.or]: {
              start_time: {
                [Op.gte]: new Date(query.start_date),
                [Op.lt]: new Date(query.end_date),
              },
              end_time: {
                [Op.gte]: new Date(query.start_date),
              },
            },
            [Op.or]: {
              "$host.client_id$": query.id,
              "$backup.client_id$": query.id,
            },
            status: "Active",
          },
          include: [
            {
              model: Customers,
              as: "video_conference",
            },
            {
              model: VirtualExperiences,
              as: "video_conf_showroom",
            },
            {
              model: Users,
              as: "host",
              attributes: ["id", "screen_name", "image_url", "email"],
            },
            {
              model: Users,
              as: "backup",
              attributes: ["id", "screen_name", "image_url", "email"],
            },
          ],
        });
      } else {
        video_conferences = await VideoConferences.findAndCountAll({
          where: {
            [Op.or]: {
              start_time: {
                [Op.gte]: new Date(query.start_date),
                [Op.lt]: new Date(query.end_date),
              },
              end_time: {
                [Op.gte]: new Date(query.start_date),
              },
            },
            status: "Active",
          },
          include: [
            {
              model: Customers,
              as: "video_conference",
            },
            {
              model: VirtualExperiences,
              as: "video_conf_showroom",
            },
            {
              model: Users,
              as: "host",
              attributes: ["id", "screen_name", "image_url", "email"],
            },
            {
              model: Users,
              as: "backup",
              attributes: ["id", "screen_name", "image_url", "email"],
            },
          ],
        });
      }
    } else {
      if (req.query.id !== "null") {
        video_conferences = await VideoConferences.findAndCountAll({
          where: {
            [Op.or]: {
              start_time: {
                [Op.gte]: new Date(query.start_date),
                [Op.lt]: new Date(query.end_date),
              },
              end_time: {
                [Op.gte]: new Date(query.start_date),
              },
            },
            [Op.or]: {
              "$host.client_id$": query.id,
              "$backup.client_id$": query.id,
            },
            status: "Active",
          },
          include: [
            {
              model: Customers,
              as: "video_conference",
            },
            {
              model: VirtualExperiences,
              as: "video_conf_showroom",
            },
            {
              model: Users,
              as: "host",
              attributes: ["id", "screen_name", "image_url", "email"],
            },
            {
              model: Users,
              as: "backup",
              attributes: ["id", "screen_name", "image_url", "email"],
            },
          ],
        });
      } else {
        video_conferences = await VideoConferences.findAndCountAll({
          where: {
            [Op.or]: {
              start_time: {
                [Op.gte]: new Date(query.start_date),
                [Op.lt]: new Date(query.end_date),
              },
              end_time: {
                [Op.gte]: new Date(query.start_date),
              },
            },
            status: "Active",
          },
          include: [
            {
              model: Customers,
              as: "video_conference",
            },
            {
              model: VirtualExperiences,
              as: "video_conf_showroom",
            },
            {
              model: Users,
              as: "host",
              attributes: ["id", "screen_name", "image_url", "email"],
            },
            {
              model: Users,
              as: "backup",
              attributes: ["id", "screen_name", "image_url", "email"],
            },
          ],
        });
      }
    }

    if (!video_conferences) {
      return res.status(400).json({ message: "No Video Conferences found" });
    }

    return res.status(200).json({ video_conferences: video_conferences });
  } catch (error) {
    console.log("Error: ", error);
  }
};

export const get_all_video_conference_requests = async (req, res) => {
  let query = req.query;

  let video_conferences;

  try {
    if (query && query.search !== "null" && query.search) {
      if (req.query.id !== "null") {
        video_conferences = await VideoConferences.findAndCountAll({
          offset: parseInt(query.offset),
          limit: parseInt(query.limit),
          subQuery: true,
          where: {
            status: "Pending",
            "$host.client_id$": query.id,
            "$backup.client_id$": query.id,
            title: {
              [Op.substring]: query.search,
            },
            start_time: {
              [Op.gt]: new Date(),
            },
          },
          include: [
            {
              model: Customers,
              as: "video_conference",
            },
            {
              model: VirtualExperiences,
              as: "video_conf_showroom",
            },
            {
              model: Users,
              as: "host",
              attributes: ["id", "screen_name", "image_url", "email"],
            },
            {
              model: Users,
              as: "backup",
              attributes: ["id", "screen_name", "image_url", "email"],
            },
          ],
          order: [["created_at", "ASC"]],
        });
      } else {
        video_conferences = await VideoConferences.findAndCountAll({
          offset: parseInt(query.offset),
          limit: parseInt(query.limit),
          subQuery: true,
          where: {
            status: "Pending",
            title: {
              [Op.substring]: query.search,
            },
            start_time: {
              [Op.gt]: new Date(),
            },
          },
          include: [
            {
              model: Customers,
              as: "video_conference",
            },
            {
              model: VirtualExperiences,
              as: "video_conf_showroom",
            },
            {
              model: Users,
              as: "host",
              attributes: ["id", "screen_name", "image_url", "email"],
            },
            {
              model: Users,
              as: "backup",
              attributes: ["id", "screen_name", "image_url", "email"],
            },
          ],
          order: [["created_at", "ASC"]],
        });
      }
    } else {
      if (req.query.id !== "null") {
        video_conferences = await VideoConferences.findAndCountAll({
          subQuery: true,
          offset: parseInt(query.offset),
          limit: parseInt(query.limit),
          where: {
            status: "Pending",
            "$host.client_id$": query.id,
            "$backup.client_id$": query.id,
            start_time: {
              [Op.gt]: new Date(),
            },
          },
          include: [
            {
              model: Customers,
              as: "video_conference",
            },
            {
              model: VirtualExperiences,
              as: "video_conf_showroom",
            },
            {
              model: Users,
              as: "host",
              attributes: ["id", "screen_name", "image_url", "email"],
            },
            {
              model: Users,
              as: "backup",
              attributes: ["id", "screen_name", "image_url", "email"],
            },
          ],
          order: [["created_at", "ASC"]],
        });
      } else {
        video_conferences = await VideoConferences.findAndCountAll({
          subQuery: true,
          offset: parseInt(query.offset),
          limit: parseInt(query.limit),
          where: {
            status: "Pending",
            start_time: {
              [Op.gt]: new Date(),
            },
          },
          include: [
            {
              model: Customers,
              as: "video_conference",
            },
            {
              model: VirtualExperiences,
              as: "video_conf_showroom",
            },
            {
              model: Users,
              as: "host",
              attributes: ["id", "screen_name", "image_url", "email"],
            },
            {
              model: Users,
              as: "backup",
              attributes: ["id", "screen_name", "image_url", "email"],
            },
          ],
          order: [["created_at", "ASC"]],
        });
      }
    }

    if (!video_conferences) {
      return res.status(400).json({ message: "No Video Conferences found" });
    }

    return res.status(200).json({ requests: video_conferences });
  } catch (error) {
    console.log("Error: ", error);
  }
};

export const get_video_conference_room = async (req, res) => {
  let { room } = req.params;

  try {
    let video_conference = await VideoConferences.findOne({
      where: {
        id: room,
      },
      include: [
        {
          model: Customers,
          as: "video_conference",
        },
        {
          model: VirtualExperiences,
          as: "video_conf_showroom",
        },
        {
          model: Users,
          as: "backup",
        },
        {
          model: Users,
          as: "host",
        },
      ],
    });

    if (!video_conference) {
      return res.status(400).json({ message: "No Video Conference found" });
    }

    return res.status(200).json(video_conference);
  } catch (error) {
    console.log("Error: ", error);
  }
};

export const create_video_conference = async (req, res) => {
  const {
    title,
    status,
    host_id,
    backup_host_id,
    start_time,
    end_time,
    participants,
    showrooms,
    sender,
    virtual_experiences,
    customers,
    created_by,
  } = req.body;

  console.log(req.body);

  try {
    const conference = await VideoConferences.create({
      title: title,
      status: status,
      start_time: start_time,
      end_time: end_time,
      host_id: host_id,
      backup_host_id: backup_host_id,
      customer_id: null,
      created_by: created_by,
    });

    if (!conference) {
      return res
        .status(202)
        .json({ message: "Package was not successfully created" });
    }

    if (participants && participants.length > 0) {
      let customers = [];
      for (let index = 0; index < participants.length; index++) {
        const element = participants[index];
        customers.push({
          customer_id: element,
          video_con_id: conference.dataValues.id,
          created_by: created_by,
        });
      }

      await VideoConferenceParticipants.bulkCreate(customers);
    }

    if (showrooms && showrooms.length > 0) {
      let experiences = [];
      for (let index = 0; index < showrooms.length; index++) {
        const element = showrooms[index];
        experiences.push({
          virtual_exp_id: element,
          video_con_id: conference.dataValues.id,
          created_by: created_by,
        });
      }

      await VideoConferenceShowrooms.bulkCreate(experiences);
    }

    for (let index = 0; index < customers.length; index++) {
      const element = customers[index];
      const video_conference_invitation = await got
      .post("https://platform.1clickdesign.com/api/notification/video_conference", {
        headers: {
          "Access-Control-Allow-Credentials": "true",
          "Access-Control-Allow-Origin": "*",
          "Access-Control-Allow-Methods": "POST",
        },

        json: {
          start_time: start_time,
          end_time: end_time,
          customer_id: element.id,
          customer: element.full_name,
          customer_email: element.email,
          virtual_experiences: virtual_experiences,
          user_id: sender.id,
          user: sender.first_name + " " + sender.last_name,
          email: sender.email,
          position: sender.position,
          company: sender.client.name,
          client_id: sender.client.id,
          accept_link: `https://platform.1clickdesign.com/video_conference/invitation?invitation=${conference.dataValues.id}&status=accepted`,
          reject_link: `https://platform.1clickdesign.com/video_conference/invitation?invitation=${conference.dataValues.id}&status=rejected`,
          reschedule_link: "",
          room_link: `https://platform.1clickdesign.com/video_conference/${conference.dataValues.id}/${element.id}`
        },
      })
      .json();

    console.log(video_conference_invitation);
    }

    

    return res
      .status(200)
      .json({ message: "Package was successfully created" });
  } catch (error) {
    console.log(error);
    return res.status(400).json();
  }
};

export const check_video_conference_invitation_respond = async (req, res) => {
  const {id} = req.params;
  try {
    const check_status = await VideoConferences.findOne({
      where: {
        id: id
      }
    })

    if(!check_status) {
      return res.status(400).json()
    }

    return res.status(200).json({respond: check_status.dataValues.respond, status: check_status.dataValues.status})
  } catch (error) {
    console.log(error)
  }
}

export const video_conference_invitation_respond = async (req, res) => {
  const {invitation_id, status, respond} = req.body;

  try {
    const video_conference_invitation = await VideoConferences.update({
      status: status,
      respond: respond
    }, {
      where: {
        id: invitation_id
      }
    })

    if(!video_conference_invitation) {
      return res.status(400).json()
    }


    return res.status(200).json({status: status})
  } catch (error) {
    console.log(error)
  }
}

export const edit_video_conference_status = async (req, res) => {
  const { id } = req.params;

  const { status, updated_by } = req.body;

  try {
    const video_conference = await VideoConferences.update(
      {
        status: status,
        updated_by: updated_by,
      },
      {
        where: {
          id: id,
        },
      }
    );

    if (!video_conference) {
      return res
        .status(400)
        .json({
          message: "Video Conference Status was not successfully edited",
        });
    }

    return res
      .status(200)
      .json({ message: "Video Conference Status was successfully edited" });
  } catch (error) {
    console.log(error);
  }
};

export const delete_package = async (req, res) => {
  const { id } = req.params;

  try {
    await Packages.destroy({
      where: {
        id: id,
      },
    });
    return res
      .status(200)
      .json({ message: "Packages was successfully deleted" });
  } catch (error) {
    console.log(error);
    return res.status(400).json();
  }
};
