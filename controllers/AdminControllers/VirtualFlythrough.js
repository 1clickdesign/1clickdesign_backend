import VirtualFlythrough from "../../models/VirtualFlythrough.js";
import VirtualExperienceTypes from "../../models/VirtualExperienceTypeModel.js";

export const create_virtual_flythrough = async (req, res) => {
  // https://dvhrjwym2rm6g.cloudfront.net/
  try {
    const body = JSON.parse(JSON.stringify(req.body));
    const files = JSON.parse(JSON.stringify(req.files));

    console.log("body and files", body, files);

    if (!files || !body) {
      return res
        .status(400)
        .json({ message: "Error: No files or body provided" });
    }

    const data = JSON.parse(body.data);
    console.log(data);

    const {
      name,
      showroom_url,
      description,
      status,
      created_by,
      client_id,
      type_id,
    } = data;

    console.log(
      files?.flythrough_video[0]?.location,
      files.flythrough_thumbnail[0].location
    );
    // let thumbnailLink = `https://dvhrjwym2rm6g.cloudfront.net`
    // let flythroughLink = `https://dvhrjwym2rm6g.cloudfront.net`
    // Create virtual experience
    const virtual_flythrough = await VirtualFlythrough.create({
      name: name,
      showroom_url: showroom_url,
      thumbnail_url: files.flythrough_thumbnail[0].location,
      status: status,
      description: description,
      fly_through_video_urls: files?.flythrough_video[0]?.location,
      type_id: type_id,
      client_id: client_id,
      created_by: created_by,
    });
    return res.status(201).json({
      message: "Flythrough was successfully created",
      virtual_flythrough: virtual_flythrough,
    });
  } catch (error) {
    console.error("Error creating virtual flythrough:", error);
    return res.status(500).json({
      message: "Error creating virtual flythrough",
      error: error.message,
    });
  }
};

export const get_all_virtual_flythroughs = async (req, res) => {
  try {
    const whereClause = {
      status: "Active",
    };
    if (req.query.id) {
      whereClause.client_id = req.query.id;
    }
    const virtual_flythrough = await VirtualFlythrough.findAll({
      nest: true,
      where: whereClause,
      order: [["name", "ASC"]],
      include: [
        {
          model: VirtualExperienceTypes,
          attributes: ["id", "name"],
        },
      ],
    });

    return res.status(200).json({
      message: "Flythroughs successfully retrieved",
      virtual_flythrough: virtual_flythrough,
    });
  } catch (error) {
    console.error("Error getting virtual flythroughs:", error);
    return res.status(500).json({
      message: "Error getting virtual flythroughs",
      error: error.message,
    });
  }
};

export const get_virtual_flythrough = async (req, res) => {
  try {
    const whereClause = {
      status: "Active",
      id: req.params.id,
    };
    const virtual_flythrough = await VirtualFlythrough.findOne({
      nest: true,
      where: whereClause,
      include: [
        {
          model: VirtualExperienceTypes,
          attributes: ["id", "name"],
        },
      ],
    });

    return res.status(200).json({
      message: "Flythrough successfully retrieved",
      virtual_flythrough: virtual_flythrough,
    });
  } catch (error) {
    console.error("Error getting virtual flythrough:", error);
    return res.status(500).json({
      message: "Error getting virtual flythrough",
      error: error.message,
    });
  }
};
export const edit_virtual_flythrough = async (req, res) => {
  try {
    const body = JSON.parse(JSON.stringify(req.body));
    const files = JSON.parse(JSON.stringify(req.files));
    const data = JSON.parse(body.data);

    const whereClause = {
      status: "Active",
      id: req.params.id,
    };

    const virtual_flythrough = await VirtualFlythrough.findOne({
      nest: true,
      where: whereClause,
      include: [
        {
          model: VirtualExperienceTypes,
          attributes: ["id", "name"],
        },
      ],
    });

    if (!virtual_flythrough) {
      return res.status(404).json({
        message: "Virtual flythrough not found.",
      });
    }

    const {
      name,
      showroom_url,
      description,
      status,
      created_by,
      client_id,
      type_id,
    } = data;

    let payload = {
      name: name,
      showroom_url: showroom_url,
      thumbnail_url:
        files.flythrough_thumbnail && files.flythrough_thumbnail[0]
          ? files.flythrough_thumbnail[0].location
          : virtual_flythrough.thumbnail_url,
      status: status,
      description: description,
      fly_through_video_urls:
        files.flythrough_video && files.flythrough_video[0]
          ? files.flythrough_video[0].location
          : virtual_flythrough.fly_through_video_urls,
      type_id: type_id,
      client_id: client_id,
      created_by: created_by,
    };

    const updateVirtualFlythroughResp = await VirtualFlythrough.update(
      payload,
      {
        where: {
          id: req.params.id,
        },
        returning: true,
      }
    );

    if (!updateVirtualFlythroughResp) {
      return res.status(500).json({
        message: "Failed to update virtual flythrough.",
      });
    }

    return res.status(200).json({
      message: "Flythrough successfully updated.",
      virtual_flythrough: updateVirtualFlythroughResp,
    });
  } catch (error) {
    console.error("Error updating virtual flythrough:", error);
    return res.status(500).json({
      message: "Error updating virtual flythrough",
      error: error.message,
    });
  }
};

export const delete_virtual_flythrough = async (req, res) => {
  try {
    const whereClause = {
      id: req.params.id,
    };
    const virtual_flythrough = await VirtualFlythrough.destroy({
      nest: true,
      where: whereClause,
    });
    return res.status(200).json({
      message: `Flythrough deleted successfully deleted`,
      virtual_flythrough: virtual_flythrough,
    });
  } catch (error) {
    console.error("Error deleting virtual flythroughs:", error);
    return res.status(500).json({
      message: "Error deleting virtual flythroughs",
      error: error.message,
    });
  }
};
