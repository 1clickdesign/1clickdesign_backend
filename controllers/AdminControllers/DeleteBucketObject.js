import { S3Client, DeleteObjectsCommand } from "@aws-sdk/client-s3";

const deleteMultipleFiles = async (req, res) => {

  const {paths} = req.body;
  const bucketName = process.env.S3_BUCKET;

  // const filesPathInS3 = ['example-path/profile.jpg', 'example-path/something.md']

  const params = {
        Bucket: bucketName,
        Delete: {
          Objects: paths.map((key) => ({ Key: key })),
        },
  };
  

  const client = new S3Client({
    region: process.env.S3_REGION,
    credentials: {
      accessKeyId: process.env.AWS_ACCESS_KEY_ID,
      secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY,
    },
  });
  await client.send(new DeleteObjectsCommand(params));
};

export default deleteMultipleFiles;