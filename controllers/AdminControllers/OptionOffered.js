import { Op } from "sequelize";
import OptionOffered from "../../models/OptionOfferedModel.js";

export const get_all_option_offered = async (req, res) => {
  
  let query = req.query;

  let option_offered;

  try {
    if(query && query.search !== "null") {
      option_offered = await OptionOffered.findAndCountAll({
        offset: Number(query.offset),
        limit: Number(query.limit),
        nest: true,
        where: {
          name: {
            [Op.substring]: query.search
          }
        },
        order: [["created_at", "ASC"]]
      })
    } else {
      option_offered = await OptionOffered.findAndCountAll({
        offset: Number(query.offset),
        limit: Number(query.limit),
        nest: true,
        order: [["created_at", "ASC"]]
      })
    }

    if(!option_offered) {
      return res.status(400).json({message: "No Option Offered found"})
    }

    return res.status(200).json({option_offered: option_offered})

  } catch (error) {
    console.log("Error: ", error)
  }
}

export const get_option_offered = async (req, res) => {
  
  let {id} = req.params;

  let option_offered;

  try {
    option_offered = await OptionOffered.findOne({
      where: {
        id: id
      },
      plain: true,
      nest: true,
    })

    if(!option_offered) {
      return res.status(400).json({message: "No Option Offered found"})
    }

    return res.status(200).json(option_offered)

  } catch (error) {
    console.log(error)
  }
}

export const get_all_option_offered_option = async (req, res) => {
  
  try {
    let option_offered = await OptionOffered.findAll({
      nest: true,
      where: {
        is_active: true,
      }
    })

    if(!option_offered) {
      return res.status(400).json({message: "No Option Offered found"})
    }

    return res.status(200).json({option_offered: option_offered})

  } catch (error) {
    console.log("Error: ", error)
  }
}

export const create_option_offered = async (req, res) => {
  
  let {name, description, icon_image_url, is_required, is_multiple, status, created_by} = req.body;

  if(!name || !icon_image_url || !created_by) {
    return res.status(400).json({message: "Incomplete Information"})
  } else {
    try {
      const option_offered = await OptionOffered.create({
        name: name,
        description: description,
        icon_image_url: icon_image_url,
        is_required: is_required,
        is_multiple: is_multiple,
        status: status,
        created_by: created_by
      })

      if(!option_offered) {
        return res.status(400).json({message: "Option Offered was not successfully created"})
      }
      return res.status(201).json({message: "Option Offered was successfully created"})
    } catch (error) {
      
    }
  }

}