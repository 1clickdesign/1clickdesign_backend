import { Op, Sequelize } from "sequelize"
import ProductNodes from "../../models/ProductNodeModel.js"
import Products from "../../models/ProductModel.js"
import Clients from "../../models/ClientModel.js"
// New Get
export const get_product_nodes = async (req, res) => {
  let product_nodes
  let query = req.query
  try {
    const whereClause = {}

    // Handle search by name or related product name/model if search parameter exists
    if (query.search && query.search !== "null") {
      whereClause[Op.or] = [
        {
          name: {
            [Op.substring]: query.search,
          },
        },
        {
          "$product.name$": {
            [Op.substring]: query.search,
          },
        },
        {
          "$product.model$": {
            [Op.substring]: query.search,
          },
        },
      ]
    }
    if (query.filter && query.filter !== "null") {
      whereClause.showroom_link = query.filter
    }

    product_nodes = await ProductNodes.findAndCountAll({
      offset: parseInt(query.offset),
      limit: parseInt(query.limit),
      order: [["created_at", "DESC"]],
      nest: true,
      where: whereClause,
      include: [
        {
          model: Products,
          attributes: ["id", "name", "model"],
        },
      ],
    })
    if (!product_nodes) {
      return res.status(400).json({ message: "No Product Nodes found" })
    }

    return res.status(200).json({ product_nodes: product_nodes })
  } catch (error) {
    console.log("Error: ", error)
  }
}
export const get_product_nodes_showroom_links = async (req, res) => {
  try {
    const productNodes = await ProductNodes.findAll({
      where: {
        showroom_link: {
          [Op.not]: null, // Exclude NULL values
        },
      },
      attributes: [
        [
          Sequelize.fn("DISTINCT", Sequelize.col("showroom_link")),
          "showroom_link",
        ],
        "created_at", // Include other attributes you need
      ],
      order: [["showroom_link", "ASC"]],
      nest: true,
    })

    if (!productNodes || productNodes.length === 0) {
      return res.status(400).json({ message: "No Product Nodes found" })
    }

    // Convert to Map to ensure uniqueness by showroom_link
    const uniqueProductNodesMap = new Map()
    productNodes.forEach((node) => {
      if (!uniqueProductNodesMap.has(node.showroom_link)) {
        uniqueProductNodesMap.set(node.showroom_link, node)
      }
    })

    // Convert Map values back to array
    const uniqueProductNodes = Array.from(uniqueProductNodesMap.values())

    return res.status(200).json({ product_nodes: uniqueProductNodes })
  } catch (error) {
    console.log("Error: ", error)
    return res.status(500).json({ message: "Internal Server Error" })
  }
}
export const get_all_product_nodes = async (req, res) => {
  let query = req.query

  let product_nodes

  try {
    if (query && query.search !== "null" && query.search) {
      if (query.id !== "null") {
        if (!!query.filter) {
          product_nodes = await ProductNodes.findAndCountAll({
            offset: parseInt(query.offset),
            limit: parseInt(query.limit),
            order: [["created_at", "DESC"]],
            nest: true,
            where: {
              [Op.or]: {
                name: {
                  [Op.substring]: query.search,
                },
                "$product.name$": {
                  [Op.substring]: query.search,
                },
                "$product.model$": {
                  [Op.substring]: query.search,
                },
              },
            },
            include: [
              {
                model: Products,
                as: "product",
                attributes: ["id", "name", "model"],
                where: {
                  client_id: query.id,
                  virtual_experience_id: query.filter,
                },
                include: [
                  {
                    model: Clients,
                    as: "client",
                    attributes: ["id", "name"],
                  },
                ],
              },
            ],
          })
        } else {
          product_nodes = await ProductNodes.findAndCountAll({
            offset: parseInt(query.offset),
            limit: parseInt(query.limit),
            order: [["created_at", "DESC"]],
            nest: true,
            where: {
              [Op.or]: {
                name: {
                  [Op.substring]: query.search,
                },
                "$product.name$": {
                  [Op.substring]: query.search,
                },
                "$product.model$": {
                  [Op.substring]: query.search,
                },
              },
            },
            include: [
              {
                model: Products,
                as: "product",
                attributes: ["id", "name", "model"],
                where: {
                  client_id: query.id,
                },
                include: [
                  {
                    model: Clients,
                    as: "client",
                    attributes: ["id", "name"],
                  },
                ],
              },
            ],
          })
        }
      } else {
        if (!!query.filter) {
          product_nodes = await ProductNodes.findAndCountAll({
            offset: parseInt(query.offset),
            limit: parseInt(query.limit),
            order: [["created_at", "DESC"]],
            nest: true,
            where: {
              [Op.or]: {
                name: {
                  [Op.substring]: query.search,
                },
                "$product.name$": {
                  [Op.substring]: query.search,
                },
                "$product.model$": {
                  [Op.substring]: query.search,
                },
              },
            },
            include: [
              {
                model: Products,
                as: "product",
                attributes: ["id", "name", "model"],
                where: {
                  virtual_experience_id: query.filter,
                },
                include: [
                  {
                    model: Clients,
                    as: "client",
                    attributes: ["id", "name"],
                  },
                ],
              },
            ],
          })
        } else {
          product_nodes = await ProductNodes.findAndCountAll({
            offset: parseInt(query.offset),
            limit: parseInt(query.limit),
            order: [["created_at", "DESC"]],
            nest: true,
            where: {
              [Op.or]: {
                name: {
                  [Op.substring]: query.search,
                },
                "$product.name$": {
                  [Op.substring]: query.search,
                },
                "$product.model$": {
                  [Op.substring]: query.search,
                },
              },
            },
            include: [
              {
                model: Products,
                as: "product",
                attributes: ["id", "name", "model"],
                include: [
                  {
                    model: Clients,
                    as: "client",
                    attributes: ["id", "name"],
                  },
                ],
              },
            ],
          })
        }
      }
    } else {
      if (query.id !== "null") {
        if (!!query.filter) {
          product_nodes = await ProductNodes.findAndCountAll({
            offset: parseInt(query.offset),
            limit: parseInt(query.limit),
            order: [["created_at", "DESC"]],
            nest: true,
            include: [
              {
                model: Products,
                attributes: ["id", "name", "model"],
                where: {
                  client_id: query.id,
                  virtual_experience_id: query.filter,
                },
              },
            ],
          })
        } else {
          product_nodes = await ProductNodes.findAndCountAll({
            offset: parseInt(query.offset),
            limit: parseInt(query.limit),
            order: [["created_at", "DESC"]],
            nest: true,
            include: [
              {
                model: Products,
                attributes: ["id", "name", "model"],
                where: {
                  client_id: query.id,
                },
              },
            ],
          })
        }
      } else {
        if (!!query.filter) {
          product_nodes = await ProductNodes.findAndCountAll({
            offset: parseInt(query.offset),
            limit: parseInt(query.limit),
            order: [["created_at", "DESC"]],
            nest: true,
            include: [
              {
                model: Products,
                attributes: ["id", "name", "model"],
                where: {
                  virtual_experience_id: query.filter,
                },
              },
            ],
          })
        } else {
          product_nodes = await ProductNodes.findAndCountAll({
            offset: parseInt(query.offset),
            limit: parseInt(query.limit),
            order: [["created_at", "DESC"]],
            nest: true,
            include: [
              {
                model: Products,
                attributes: ["id", "name", "model"],
              },
            ],
          })
        }
      }
    }

    if (!product_nodes) {
      return res.status(400).json({ message: "No Product Nodes found" })
    }

    return res.status(200).json({ product_nodes: product_nodes })
  } catch (error) {
    console.log("Error: ", error)
  }
}

export const check_if_product_node_unique_name = async (req, res) => {
  const { name } = req.query
  console.log(name)

  try {
    const check_name = await ProductNodes.findOne({
      where: {
        name: name,
      },
    })
    if (!check_name) {
      return res.status(204).json()
    }
    return res.status(200).json()
  } catch (error) {
    console.log(error)
    return res.status(400).json()
  }
}

export const create_product_node = async (req, res) => {
  const {
    pitch,
    x,
    y,
    yaw,
    z,
    name,
    product_id,
    model,
    showroom_link,
    status,
    created_by,
    updated_by,
  } = req.body

  try {
    const product_node = await ProductNodes.create({
      pitch: parseFloat(pitch),
      z: parseFloat(z),
      x: parseFloat(x),
      y: parseFloat(y),
      yaw: parseFloat(yaw),
      name: name,
      model: model,
      showroom_link: showroom_link,
      status: status,
      product_id: product_id,
      created_by: created_by,
    })

    if (!product_node) {
      console.log("error")
    }

    return res.status(200).json({ message: "Success" })
  } catch (error) {
    console.log(error)
  }
}

export const edit_product_node = async (req, res) => {
  const { id } = req.params

  const {
    model,
    showroom_link,
    pitch,
    x,
    y,
    yaw,
    z,
    name,
    product_id,
    status,
    updated_by,
  } = req.body

  try {
    const product_node = await ProductNodes.update(
      {
        pitch: parseFloat(pitch),
        z: parseFloat(z),
        x: parseFloat(x),
        y: parseFloat(y),
        yaw: parseFloat(yaw),
        name: name,
        model: model,
        showroom_link: showroom_link,
        status: status,
        product_id: product_id,
        updated_by: updated_by,
      },
      {
        where: {
          id: id,
        },
      },
    )

    if (!product_node) {
      console.log("error")
    }

    return res.status(200).json({ message: "Success" })
  } catch (error) {
    console.log(error)
  }
}

export const get_product_node = async (req, res) => {
  let { id } = req.params

  let product_node

  try {
    product_node = await ProductNodes.findOne({
      where: {
        id: id,
      },
      plain: true,
      nest: true,
      include: [
        {
          model: Products,
          attributes: ["id", "name"],
        },
      ],
    })

    if (!product_node) {
      return res.status(400).json({ message: "No Product Node found" })
    }

    return res.status(200).json(product_node)
  } catch (error) {
    console.log(error)
  }
}
export const delete_product_nodes = async (req, res) => {
  const { id } = req.params

  try {
    await ProductNodes.destroy({
      where: {
        id: id,
      },
    })
    return res
      .status(200)
      .json({ message: "Product Node was successfully deleted" })
  } catch (error) {
    console.log(error)
    return res.status(400).json()
  }
}
