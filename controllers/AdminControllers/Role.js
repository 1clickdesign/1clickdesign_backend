import { Op } from "sequelize";
import Permissions from "../../models/PermissionModel.js";
import Roles from "../../models/RoleModel.js";
import RolePermissions from "../../models/RolePermissionModel.js";
import UserPermissions from "../../models/UserPermissionModel.js";

export const get_all_roles = async (req, res) => {
  let query = req.query;

  let roles;

  try {
    if (query && query.search !== "null") {
      if (query.for_client === "true") {
        roles = await Roles.findAndCountAll({
          offset: parseInt(query.offset),
          limit: parseInt(query.limit),
          nest: true,
          where: {
            [Op.and]: {
              name: {
                [Op.substring]: query.search,
              },
              for_client: Boolean(query.for_client),
            },
          },
        });
      } else {
        roles = await Roles.findAndCountAll({
          offset: parseInt(query.offset),
          limit: parseInt(query.limit),
          nest: true,
          where: {
            name: {
              [Op.substring]: query.search,
            },
          },
        });
      }
    } else {
      if (query.for_client === "true") {
        roles = await Roles.findAndCountAll({
          offset: parseInt(query.offset),
          limit: parseInt(query.limit),
          nest: true,
          where: {
            for_client: Boolean(query.for_client),
          },
        });
      } else {
        roles = await Roles.findAndCountAll({
          offset: parseInt(query.offset),
          limit: parseInt(query.limit),
          nest: true,
        });
      }
    }

    if (!roles) {
      return res.status(400).json({ message: "No Roles found" });
    }

    return res.status(200).json({ roles: roles });
  } catch (error) {
    console.log(error);
  }
};

export const get_all_roles_option = async (req, res) => {
  const query = req.query;

  let roles;
  try {
    if (query && query.for_client === "true") {
      roles = await Roles.findAll({
        nest: true,
        where: {
          status: "Active",
          for_client: true,
        },
        attributes: ["id", "name"],
        include: [
          {
            model: Permissions,
            as: "role",
          },
        ],
      });
    } else {
      roles = await Roles.findAll({
        nest: true,
        where: {
          status: "Active",
        },
        attributes: ["id", "name"],
        include: [
          {
            model: Permissions,
            as: "role",
          },
        ],
      });
    }

    if (!roles) {
      return res.status(400).json({ message: "No Roles found" });
    }

    return res.status(200).json({ roles: roles });
  } catch (error) {
    console.log("Error: ", error);
  }
};

export const get_role = async (req, res) => {
  let { id } = req.params;

  let role;

  try {
    role = await Roles.findOne({
      where: {
        id: id,
      },
      plain: true,
      include: [
        {
          model: Permissions,
          as: "role",
          attributes: ["id", "name"],
        },
      ],
      order: [["role", "name", "ASC"]],
    });

    if (!role) {
      return res.status(400).json({ message: "No Role found" });
    }

    return res.status(200).json(role);
  } catch (error) {
    console.log(error);
  }
};

export const create_role = async (req, res) => {
  let { name, description, for_client, permissions, status, created_by } =
    req.body;

  if (!name || !permissions || permissions.length === 0 || !created_by) {
    return res.status(400).json({ message: "Incomplete Information" });
  } else {
    try {
      if (permissions && permissions.length > 0) {
        const role = await Roles.create({
          name: name,
          description: description,
          status: status,
          for_client: for_client,
          created_by: created_by,
        });

        console.log("Role ID: ", role);

        if (!role) {
          return res
            .status(400)
            .json({ message: "Role was not successfully created" });
        }

        let rolePermissions = [];
        for (let index = 0; index < permissions.length; index++) {
          const element = permissions[index];
          rolePermissions.push({
            role_id: role.dataValues.id,
            permission_id: element,
            status: status,
            created_by: created_by,
          });
        }

        console.log(rolePermissions);

        let role_permissions;

        if (rolePermissions && rolePermissions.length > 1) {
          role_permissions = await RolePermissions.bulkCreate(rolePermissions);
        } else {
          role_permissions = await RolePermissions.create(rolePermissions[0]);
        }

        if (!role_permissions) {
          await Roles.destroy({
            where: {
              id: role.dataValues.id,
            },
          });

          return res
            .status(400)
            .json({ message: "Role was not successfully created" });
        }

        return res
          .status(201)
          .json({ message: "Role was successfully created" });
      }

      return res
        .status(400)
        .json({ message: "Role Permissions cannot be empty" });
    } catch (error) {
      console.log(error);
    }
  }
};

export const edit_role = async (req, res) => {
  let { id } = req.params;
  let { name, status, description, for_client, permissions, updated_by } =
    req.body;
  console.log(req.body);
  try {
    const role = await Roles.update(
      {
        name: name,
        status: status,
        description: description,
        for_client: for_client,
        updated_by: updated_by,
      },
      { where: { id: id } }
    );

    if (!role) {
    }

    const get_not_listed_permission = await RolePermissions.findAll({
      where: {
        permission_id: {
          [Op.notIn]: permissions,
        },
        role_id: id,
      },
    });

    if (!get_not_listed_permission) {
    }

    console.log("Hey", get_not_listed_permission);

    const get_listed_permission = await RolePermissions.findAll({
      where: {
        permission_id: {
          [Op.in]: permissions,
        },
        role_id: id,
      },
      raw: true,
      nest: true,
    });

    if (!get_listed_permission) {
    }

    console.log("Hey", get_listed_permission);
    let need_to_add = [];
    need_to_add = permissions;

    if (get_not_listed_permission.length > 0) {
      for (let index = 0; index < get_not_listed_permission.length; index++) {
        const element = get_not_listed_permission[index];
        const filtered = need_to_add.filter(
          (data) => data !== element.permission_id
        );
        need_to_add = filtered;
        const delete_not_listed_permission = await RolePermissions.destroy({
          where: {
            permission_id: element.permission_id,
          },
        });
      }
    }

    if (get_listed_permission.length > 0) {
      for (let index = 0; index < get_listed_permission.length; index++) {
        const element = get_listed_permission[index];
        const filtered = need_to_add.filter(
          (data) => data !== element.permission_id
        );
        need_to_add = filtered;
      }
    }

    console.log(need_to_add);

    let permission = [];

    if (need_to_add.length > 0) {
      for (let index = 0; index < need_to_add.length; index++) {
        const element = need_to_add[index];
        permission.push({
          role_id: id,
          permission_id: element,
          created_by: updated_by,
        });
      }

      if (permission.length > 1) {
        const add_permission = await RolePermissions.bulkCreate(permission);
      } else {
        const add_permission = await RolePermissions.create(permission[0]);
      }
    }

    return res.status(200).json();
  } catch (error) {
    console.log(error);
  }
};

export const check_if_role_unique_name = async (req, res) => {
  const { name } = req.query;

  try {
    const check_name = await Roles.findOne({
      where: {
        name: name,
      },
    });
    if (!check_name) {
      return res.status(204).json();
    }
    return res.status(200).json();
  } catch (error) {
    console.log(error);
    return res.status(400).json();
  }
};

export const delete_role = async (req, res) => {
  const { id } = req.params;

  try {
    await Roles.destroy({
      where: {
        id: id,
      },
    });
    return res.status(200).json({ message: "Roles was successfully deleted" });
  } catch (error) {
    console.log(error);
    return res.status(400).json();
  }
};
