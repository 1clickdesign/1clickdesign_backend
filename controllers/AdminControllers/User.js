import { Op } from "sequelize";
import Clients from "../../models/ClientModel.js";
import Permissions from "../../models/PermissionModel.js";
import Roles from "../../models/RoleModel.js";
import Users from "../../models/UserModel.js";
import UserBackupUsers from "../../models/UserBackupUserModel.js";
import UserPermissions from "../../models/UserPermissionModel.js";
import ClientCustomers from "../../models/ClientCustomerModel.js";
import Customers from "../../models/CustomerModel.js";
import bcrypt from "bcrypt";
import { S3Client, DeleteObjectsCommand } from "@aws-sdk/client-s3";
import db from "../../config/Database.js";
import UserCategories from "../../models/UserCategoryModel.js";
import VirtualExperienceCategories from "../../models/VirtualExperienceCategoryModel.js";

export const get_all_users = async (req, res) => {
  let query = req.query;

  let users;

  try {
    if (query && query.search !== "null" && query.search) {
      if (req.query.id !== "null") {
        users = await Users.findAndCountAll({
          offset: parseInt(query.offset),
          limit: parseInt(query.limit),
          nest: true,
          order: [["first_name", "ASC"]],
          subQuery: false,
          where: {
            client_id: query.id,
            [Op.or]: {
              first_name: {
                [Op.substring]: query.search,
              },
              middle_name: {
                [Op.substring]: query.search,
              },
              last_name: {
                [Op.substring]: query.search,
              },
              email: {
                [Op.substring]: query.search,
              },
              "$role.name$": {
                [Op.substring]: query.search,
              },
            },
          },
          include: [
            {
              model: Roles,
              attributes: ["id", "name"],
              as: "role",
            },
            {
              model: Clients,
              attributes: ["id", "name"],
            },
            {
              model: Users,
              as: "main_user",
            },
          ],
        });
      } else {
        users = await Users.findAndCountAll({
          offset: parseInt(query.offset),
          limit: parseInt(query.limit),
          nest: true,
          subQuery: false,
          order: [["first_name", "ASC"]],
          where: {
            [Op.or]: {
              first_name: {
                [Op.substring]: query.search,
              },
              middle_name: {
                [Op.substring]: query.search,
              },
              last_name: {
                [Op.substring]: query.search,
              },
              email: {
                [Op.substring]: query.search,
              },
              "$role.name$": {
                [Op.substring]: query.search,
              },
            },
          },
          include: [
            {
              model: Roles,
              attributes: ["id", "name"],
              as: "role",
            },
            {
              model: Clients,
              attributes: ["id", "name"],
            },
            {
              model: Users,
              as: "main_user",
            },
          ],
        });
      }
    } else {
      if (query.id !== "null") {
        users = await Users.findAndCountAll({
          order: [["first_name", "ASC"]],
          where: {
            client_id: query.id,
          },
          offset: parseInt(query.offset),
          limit: parseInt(query.limit),
          nest: true,
          include: [
            {
              model: Roles,
              attributes: ["id", "name"],
            },
            {
              model: Clients,
              attributes: ["id", "name"],
            },
            {
              model: Users,
              as: "main_user",
            },
          ],
        });
      } else {
        users = await Users.findAndCountAll({
          order: [["first_name", "ASC"]],
          offset: parseInt(query.offset),
          limit: parseInt(query.limit),
          nest: true,
          include: [
            {
              model: Roles,
              attributes: ["id", "name"],
            },
            {
              model: Clients,
              attributes: ["id", "name"],
            },
            {
              model: Users,
              as: "main_user",
            },
          ],
        });
      }
    }

    if (!users) {
      return res.status(400).json({ message: "No Users found" });
    }

    return res.status(200).json({ users: users });
  } catch (error) {
    console.log("Error: ", error);
  }
};
export const get_user_by_email = async (req, res) => {
  try {
    const { email } = req.query;
    if (!email) {
      return res.status(400).json({ error: "Email parameter is required" });
    }
    const user = await Users.findOne({ where: { email } });
    if (user) {
      return res.status(200).json({ user });
    } else {
      return res.status(404).json({ error: "User not found" });
    }
  } catch (error) {
    console.error("Error fetching user by email:", error);
    return res.status(500).json({ error: "Internal server error" });
  }
};
export const get_all_pending_users = async (req, res) => {
  let query = req.query;

  let users;

  try {
    if (query && query.search !== "null") {
      if (req.query.id !== "null") {
        users = await Users.findAndCountAll({
          offset: parseInt(query.offset),
          limit: parseInt(query.limit),
          nest: true,
          order: [["first_name", "ASC"]],
          where: {
            status: "Pending",
            client_id: query.id,
            [Op.or]: {
              first_name: {
                [Op.substring]: query.search,
              },
              middle_name: {
                [Op.substring]: query.search,
              },
              last_name: {
                [Op.substring]: query.search,
              },
              email: {
                [Op.substring]: query.search,
              },
            },
          },
          include: [
            {
              model: Roles,
              attributes: ["id", "name"],
            },
            {
              model: Clients,
              attributes: ["id", "name"],
            },
            {
              model: Users,
              as: "main_user",
            },
          ],
        });
      } else {
        users = await Users.findAndCountAll({
          offset: parseInt(query.offset),
          limit: parseInt(query.limit),
          nest: true,
          order: [["first_name", "ASC"]],
          where: {
            status: "Pending",
            [Op.or]: {
              first_name: {
                [Op.substring]: query.search,
              },
              middle_name: {
                [Op.substring]: query.search,
              },
              last_name: {
                [Op.substring]: query.search,
              },
              email: {
                [Op.substring]: query.search,
              },
            },
          },
          include: [
            {
              model: Roles,
              attributes: ["id", "name"],
            },
            {
              model: Clients,
              attributes: ["id", "name"],
            },
            {
              model: Users,
              as: "main_user",
            },
          ],
        });
      }
    } else {
      if (query.id !== "null") {
        users = await Users.findAndCountAll({
          order: [["first_name", "ASC"]],
          where: {
            status: "Pending",
            client_id: query.id,
          },
          offset: parseInt(query.offset),
          limit: parseInt(query.limit),
          nest: true,
          include: [
            {
              model: Roles,
              attributes: ["id", "name"],
            },
            {
              model: Clients,
              attributes: ["id", "name"],
            },
            {
              model: Users,
              as: "main_user",
            },
          ],
        });
      } else {
        users = await Users.findAndCountAll({
          order: [["first_name", "ASC"]],
          offset: parseInt(query.offset),
          limit: parseInt(query.limit),
          nest: true,
          where: {
            status: "Pending",
          },
          include: [
            {
              model: Roles,
              attributes: ["id", "name"],
            },
            {
              model: Clients,
              attributes: ["id", "name"],
            },
            {
              model: Users,
              as: "main_user",
            },
          ],
        });
      }
    }

    if (!users) {
      return res.status(400).json({ message: "No Users found" });
    }

    return res.status(200).json({ users: users });
  } catch (error) {
    console.log("Error: ", error);
  }
};

export const get_all_users_option = async (req, res) => {
  const query = req.query;
  let users;

  console.log(query.client_id);
  try {
    if (query && query.client_id) {
      users = await Users.findAll({
        nest: true,
        where: {
          status: "Active",
          client_id: query.client_id,
        },
        attributes: [
          "id",
          "first_name",
          "middle_name",
          "last_name",
          "image_url",
          "position",
          "phone_number",
          "email",
        ],
        include: [
          {
            model: Clients,
            attributes: ["id", "name"],
          },
          {
            model: Users,
            as: "backup_user",
            attributes: [
              "id",
              "first_name",
              "middle_name",
              "last_name",
              "image_url",
              "position",
              "phone_number",
              "email",
            ],
          },
        ],
      });
    } else {
      users = await Users.findAll({
        nest: true,
        where: {
          status: "Active",
        },
        attributes: [
          "id",
          "first_name",
          "middle_name",
          "last_name",
          "image_url",
          "position",
          "phone_number",
          "email",
        ],
        include: [
          {
            model: Clients,
            attributes: ["id", "name"],
          },
          {
            model: Users,
            as: "backup_user",
            attributes: [
              "id",
              "first_name",
              "middle_name",
              "last_name",
              "image_url",
              "position",
              "phone_number",
              "email",
            ],
          },
        ],
      });
    }

    if (!users) {
      return res.status(400).json({ message: "No Users found" });
    }

    return res.status(200).json({ users: users });
  } catch (error) {
    console.log("Error: ", error);
  }
};

export const get_all_users_location = async (req, res) => {
  const query = req.query;
  let users;
  try {
    if (query && query.category_id) {
      users = await Users.findAll({
        nest: true,
        where: {
          status: "Active",
          client_id: query.client_id,
        },
        attributes: [
          "id",
          "first_name",
          "middle_name",
          "last_name",
          "latitude",
          "longitude",
          "image_url",
          "email",
        ],
        include: [
          {
            model: VirtualExperienceCategories,
            as: "user_category",
            where: {
              id: query.category_id,
            },
          },
        ],
      });
    } else {
      users = await Users.findAll({
        nest: true,
        where: {
          status: "Active",
          client_id: query.client_id,
        },
        attributes: [
          "id",
          "first_name",
          "middle_name",
          "last_name",
          "latitude",
          "longitude",
          "image_url",
          "email",
        ],
        include: [
          {
            model: VirtualExperienceCategories,
            as: "user_category",
          },
        ],
      });
    }

    if (!users) {
      return res.status(400).json({ message: "No Users found" });
    }

    return res.status(200).json({ users: users });
  } catch (error) {
    console.log("Error: ", error);
  }
};

export const check_if_user_unique_name = async (req, res) => {
  const { username, email } = req.query;

  if (username) {
    try {
      const check_username = await Users.findOne({
        where: {
          username: username,
        },
      });
      console.log(check_username);

      if (!check_username) {
        return res.status(204).json();
      }
      return res.status(200).json();
    } catch (error) {
      console.log(error);
      return res.status(400).json();
    }
  } else if (email) {
    try {
      const check_email = await Users.findOne({
        where: {
          email: email,
        },
      });
      console.log(check_email);

      if (!check_email) {
        return res.status(204).json();
      }
      return res.status(200).json();
    } catch (error) {
      console.log(error);
      return res.status(400).json();
    }
  }
};

export const get_my_backup_users = async (req, res) => {
  const { id } = req.params;
  try {
    const backup_users = await UserBackupUsers.findAll({
      where: {
        backup_user_id: id,
      },
      attributes: ["main_user_id", "backup_user_id"],
    });

    console.log(backup_users);

    if (!backup_users) {
      return res.status(400).json();
    }

    return res.status(200).json(backup_users);
  } catch (error) {
    console.log(error);
  }
};

export const get_nearest_agents = async (req, res) => {
  const { latitude, longitude, client_id } = req.query;
  const distance = 100000;

  //3959 miles
  //6371 km

  const haversine = `(
    3959 * acos(
        cos(radians(${latitude}))
        * cos(radians(latitude))
        * cos(radians(longitude) - radians(${longitude}))
        + sin(radians(${latitude})) * sin(radians(latitude))
    )
)`;

  try {
    const users = await Users.findAll({
      attributes: [
        "id",
        [db.literal(haversine), "distance"],
        "first_name",
        "last_name",
        "email",
        "image_url",
        "phone_number",
        "receive_email",
      ],
      where: {
        client_id: client_id,
        status: "Active",
      },
      include: [
        {
          model: Users,
          as: "backup_user",
        },
        {
          model: Clients,
          attributes: ["id", "name"],
        },
      ],
      order: db.col("distance"),
      having: db.literal(`distance <= ${distance}`),
      limit: 2,
    });

    console.log(users);
    return res.status(200).json({ users: users });
  } catch (error) {
    console.log(error);
  }
};

export const check_agent_status_online = async (req, res) => {
  const { agents } = req.body;

  console.log(agents);

  try {
    const agent_online = await Users.findAll({
      where: {
        id: {
          [Op.or]: agents,
        },
      },
      attributes: [
        "is_online",
        "id",
        "first_name",
        "email",
        "phone_number",
        "last_name",
        "phone_number",
        "image_url",
      ],
    });

    if (!agent_online) {
    }

    return res.status(200).json({ agents: agent_online });
  } catch (error) {}
};

export const get_user = async (req, res) => {
  let { id } = req.params;

  let user;

  try {
    user = await Users.findOne({
      where: {
        id: id,
      },
      plain: true,
      nest: true,
      include: [
        {
          model: VirtualExperienceCategories,
          through: UserCategories,
          as: "user_category",
        },
        {
          model: Roles,
          attributes: ["name", "for_client"],
          include: [
            {
              model: Permissions,
              as: "role",
              attributes: ["id", "name"],
            },
          ],
        },
        {
          model: Clients,
          attributes: ["id", "name"],
        },
        {
          model: Users,
          as: "backup_user",
        },
      ],
    });

    if (!user) {
      return res.status(400).json({ message: "No User found" });
    }

    return res.status(200).json(user);
  } catch (error) {
    console.log(error);
  }
};

export const create_user = async (req, res) => {
  const body = JSON.parse(JSON.stringify(req.body));
  const files = req.files;

  if (!files && !body) {
    return res.status(400).json();
  }

  // console.log(body);
  // console.log(files);
  const data = JSON.parse(body.data);
  // console.log(data);

  let {
    first_name,
    middle_name,
    last_name,
    email,
    screen_name,
    username,
    gender,
    latitude,
    longitude,
    phone_number,
    country,
    state,
    city,
    region,
    zip_code,
    place_id,
    role_id,
    email_verification_status,
    receive_email,
    position,
    client_id,
    status,
    created_by,
    backup_users,
    permissions,
    category_id,
  } = data;
  console.log(body);

  if (
    !first_name ||
    !last_name ||
    !email ||
    !screen_name ||
    !username ||
    !gender ||
    !latitude ||
    !longitude ||
    !place_id ||
    !phone_number ||
    !email_verification_status ||
    !position ||
    !client_id ||
    !status ||
    !created_by
  ) {
    return res.status(400).json({ message: "Incomplete Information" });
  } else {
    try {
      const user = await Users.create({
        first_name: first_name,
        middle_name: middle_name,
        last_name: last_name,
        email: email,
        screen_name: screen_name,
        username: username,
        gender: gender,
        password: "!clickDesignNamba1",
        latitude: parseFloat(latitude),
        longitude: parseFloat(longitude),
        phone_number: phone_number,
        country: country,
        state: state,
        city: city,
        region: region,
        place_id: place_id,
        zip_code: zip_code,
        role_id: role_id ? role_id : null,
        email_verification_status: email_verification_status,
        receive_email: Boolean(receive_email),
        last_sign_in: null,
        image_url: files[0].location,
        position: position,
        client_id: client_id ? client_id : null,
        status: status,
        created_by: created_by,
      });

      if (!user) {
        return res
          .status(400)
          .json({ message: "User was not successfully created" });
      }

      if (backup_users && backup_users.length > 0) {
        let backups = [];
        for (let index = 0; index < backup_users.length; index++) {
          const element = backup_users[index];
          backups.push({
            main_user_id: user.dataValues.id,
            backup_user_id: element,
            created_by: created_by,
          });
        }

        await UserBackupUsers.bulkCreate(backups);
      }

      const assigned_category = await UserCategories.create({
        user_id: user.dataValues.id,
        category_id: category_id,
        created_by: created_by,
      });

      // let all_permissions = [];

      // if (permissions && permissions.length > 0) {
      //   for (let index = 0; index < permissions.length; index++) {
      //     const element = permissions[index];
      //     all_permissions.push({
      //       user_id: user.dataValues.id,
      //       permission_id: element.value,
      //       created_by: created_by,
      //     });
      //   }
      //   const permission = await UserPermissions.bulkCreate(all_permissions);
      // }

      return res.status(201).json({ message: "User was successfully created" });
    } catch (error) {
      console.log(error);
    }
  }
};

export const edit_user_with_file = async (req, res) => {
  const { id } = req.params;
  const body = JSON.parse(JSON.stringify(req.body));
  const files = req.files;

  // console.log(body);
  // console.log(files);
  const data = JSON.parse(body.data);
  // console.log(data);

  let {
    first_name,
    middle_name,
    last_name,
    email,
    screen_name,
    username,
    gender,
    latitude,
    longitude,
    phone_number,
    country,
    state,
    city,
    region,
    zip_code,
    place_id,
    role_id,
    image_urls,
    email_verification_status,
    receive_email,
    position,
    client_id,
    status,
    updated_by,
    backup_users,
    image_url,
    permissions,
    category_id,
  } = data;
  console.log(body);

  if (
    !first_name ||
    !last_name ||
    !email ||
    !screen_name ||
    !username ||
    !gender ||
    !latitude ||
    !longitude ||
    !place_id ||
    !phone_number ||
    !email_verification_status ||
    !position ||
    !client_id ||
    !status ||
    !updated_by
  ) {
    return res.status(400).json({ message: "Incomplete Information" });
  } else {
    let user;
    try {
      if (image_url === files[0].location) {
        user = await Users.update(
          {
            first_name: first_name,
            middle_name: middle_name,
            last_name: last_name,
            email: email,
            screen_name: screen_name,
            username: username,
            gender: gender,

            latitude: parseFloat(latitude),
            longitude: parseFloat(longitude),
            phone_number: phone_number,
            country: country,
            state: state,
            city: city,
            region: region,
            place_id: place_id,
            zip_code: zip_code,
            role_id: role_id ? role_id : null,
            email_verification_status: email_verification_status,
            receive_email: Boolean(receive_email),
            position: position,
            client_id: client_id ? client_id : null,
            status: status,
            updated_by: updated_by,
          },
          {
            where: { id: id }, // Ensure id is defined and contains a valid value
          }
        );
      } else {
        user = await Users.update(
          {
            first_name: first_name,
            middle_name: middle_name,
            last_name: last_name,
            email: email,
            screen_name: screen_name,
            username: username,
            gender: gender,
            latitude: parseFloat(latitude),
            longitude: parseFloat(longitude),
            phone_number: phone_number,
            image_url: files[0].location,
            country: country,
            state: state,
            city: city,
            region: region,
            place_id: place_id,
            zip_code: zip_code,
            role_id: role_id ? role_id : null,
            email_verification_status: email_verification_status,
            receive_email: Boolean(receive_email),
            position: position,
            client_id: client_id ? client_id : null,
            status: status,
            updated_by: updated_by,
          },
          {
            where: { id: id }, // Ensure id is defined and contains a valid value
          }
        );

        const bucketName = process.env.S3_BUCKET;

        const params = {
          Bucket: bucketName,
          Delete: {
            Objects: image_urls.map((key) => ({ Key: key })),
          },
        };

        const s3 = new S3Client({
          region: process.env.S3_REGION,
          credentials: {
            accessKeyId: process.env.AWS_ACCESS_KEY_ID,
            secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY,
          },
        });
        const s3_delete = await s3.send(new DeleteObjectsCommand(params));
        console.log(s3_delete);
      }

      if (!user) {
        return res
          .status(400)
          .json({ message: "User was not successfully updated" });
      }

      if (backup_users && backup_users.length > 0) {
        let backups = [];
        for (let index = 0; index < backup_users.length; index++) {
          const element = backup_users[index];
          const backup = await UserBackupUsers.findOrCreate({
            where: {
              main_user_id: id,
            },
            defaults: {
              backup_user_id: element,
              created_by: updated_by,
            },
          });
        }
      } else {
        const delete_backup = await UserBackupUsers.destroy({
          where: {
            main_user_id: id,
          },
        });
      }

      const user_category_exists = await UserCategories.findOne({
        where: {
          user_id: id,
        },
      });

      if (user_category_exists) {
        await UserCategories.update(
          {
            category_id: category_id,
            updated_by: updated_by,
          },
          {
            where: {
              user_id: id,
            },
          }
        );
      } else {
        await UserCategories.create({
          category_id: category_id,
          user_id: id,
          created_by: updated_by,
        });
      }

      // let all_permissions = [];

      // if (permissions && permissions.length > 0) {
      //   for (let index = 0; index < permissions.length; index++) {
      //     const element = permissions[index];
      //     all_permissions.push({
      //       user_id: user.dataValues.id,
      //       permission_id: element.value,
      //       created_by: created_by,
      //     });
      //   }
      //   const permission = await UserPermissions.bulkCreate(all_permissions);
      // }

      return res.status(201).json({ message: "User was successfully updated" });
    } catch (error) {
      console.log(error);
    }
  }
};

export const edit_user = async (req, res) => {
  const { id } = req.params;
  console.log(req.params);

  let {
    first_name,
    middle_name,
    last_name,
    email,
    screen_name,
    username,
    gender,
    latitude,
    longitude,
    phone_number,
    country,
    state,
    city,
    region,
    zip_code,
    place_id,
    role_id,
    email_verification_status,
    receive_email,
    position,
    client_id,
    status,
    updated_by,
    backup_users,
    permissions,
    category_id,
  } = req.body;
  console.log(req.body);

  if (
    !first_name ||
    !last_name ||
    !email ||
    !screen_name ||
    !username ||
    !gender ||
    !place_id ||
    !phone_number ||
    !email_verification_status ||
    !position ||
    !client_id ||
    !status ||
    !updated_by
  ) {
    console.log("This is called");
    return res.status(400).json({ message: "Incomplete Information" });
  } else {
    try {
      const user = await Users.update(
        {
          first_name: first_name,
          middle_name: middle_name,
          last_name: last_name,
          email: email,
          screen_name: screen_name,
          username: username,
          gender: gender,
          latitude: parseFloat(latitude),
          longitude: parseFloat(longitude),
          phone_number: phone_number,
          country: country,
          state: state,
          city: city,
          region: region,
          place_id: place_id,
          zip_code: zip_code,
          role_id: role_id ? role_id : null,
          email_verification_status: email_verification_status,
          receive_email: Boolean(receive_email),
          position: position,
          client_id: client_id ? client_id : null,
          status: status,
          updated_by: updated_by,
        },
        {
          where: {
            id: id,
          },
        }
      );

      if (!user) {
        console.log("This is called too");
        return res
          .status(400)
          .json({ message: "User was not successfully updated" });
      }

      if (backup_users && backup_users.length > 0) {
        let backups = [];
        for (let index = 0; index < backup_users.length; index++) {
          const element = backup_users[index];
          const backup = await UserBackupUsers.findOrCreate({
            where: {
              main_user_id: id,
              backup_user_id: element,
            },
            defaults: {
              created_by: updated_by,
            },
          });
        }
      } else {
        const delete_backup = await UserBackupUsers.destroy({
          where: {
            main_user_id: id,
          },
        });
      }

      const user_category_exists = await UserCategories.findOne({
        where: {
          user_id: id,
        },
      });

      if (user_category_exists) {
        await UserCategories.update(
          {
            category_id: category_id,
            updated_by: updated_by,
          },
          {
            where: {
              user_id: id,
            },
          }
        );
      } else {
        await UserCategories.create({
          category_id: category_id,
          user_id: id,
          created_by: updated_by,
        });
      }

      // let all_permissions = [];

      // if (permissions && permissions.length > 0) {
      //   for (let index = 0; index < permissions.length; index++) {
      //     const element = permissions[index];
      //     all_permissions.push({
      //       user_id: user.dataValues.id,
      //       permission_id: element.value,
      //       created_by: created_by,
      //     });
      //   }
      //   const permission = await UserPermissions.bulkCreate(all_permissions);
      // }

      return res.status(201).json({ message: "User was successfully updated" });
    } catch (error) {
      console.log(error);
    }
  }
};

export const update_user_status = async (req, res) => {
  const { status, id } = req.body;

  try {
    const update = await Users.update(
      {
        status: status,
      },
      {
        where: {
          id: id,
        },
      }
    );

    if (!update) {
      return res.status(400).json();
    }

    return res.status(200).json();
  } catch (error) {
    console.log(error);
  }
};

export const update_user_online = async (req, res) => {
  const { is_online, user_id } = req.body;

  try {
    const isOnline = await Users.update(
      {
        is_online: is_online,
      },
      {
        where: {
          id: user_id,
        },
      }
    );

    if (!isOnline) {
      return res
        .status(400)
        .json({ message: "User online status was not successfully updated" });
    }

    const user = await Users.findOne({
      where: {
        id: user_id,
      },
      include: [
        {
          model: Clients,
          attributes: ["name", "logo_url"],
        },
        {
          model: Users,
          as: "backup_user",
        },
        {
          model: ClientCustomers,
          include: [
            {
              model: Customers,
            },
          ],
        },
        {
          model: Roles,
          attributes: ["name"],
          include: [
            {
              model: Permissions,
              as: "role",
              attributes: ["name"],
            },
          ],
        },
      ],
    });

    if (!user) {
      return res.status(400).json({ message: "User cannot be found" });
    }

    return res.status(201).json({ user: user });
  } catch (error) {}
};

export const delete_user = async (req, res) => {
  const { id } = req.params;

  try {
    await Users.destroy({
      where: {
        id: id,
      },
    });
    return res.status(200).json({ message: "User was successfully deleted" });
  } catch (error) {
    console.log(error);
    return res.status(400).json();
  }
};

export const change_password = async (req, res) => {
  const { email, oldPassword, newPassword } = req.body;
  try {
    const user = await Users.findOne({ where: { email: email } });

    if (!user) {
      return res.status(400).json();
    }

    const data = await bcrypt.compare(oldPassword, user.dataValues.password);

    if (!data) {
      return res.status(400).json({ message: "Incorrect password" });
    }

    const salt = bcrypt.genSaltSync(10);
    const password = bcrypt.hashSync(newPassword, salt);
    const update = await Users.update(
      {
        password: password,
      },
      {
        where: {
          email: email,
        },
      }
    );

    if (!update) {
      return res.status(400).json();
    }

    return res.status(200).json({ message: "Successfully change password" });
  } catch (error) {}
};
