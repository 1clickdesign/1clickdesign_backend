

export const create_qr_code_image = async (req, res) => {
  const files = req.files;
  console.log(files)

  let locations = []
  for (let index = 0; index < files.length; index++) {
    const element = files[index];
    locations.push(element.location)
  }

  return res.status(200).json({locations: locations})
}