import Clients from "../../models/ClientModel.js"
import InvitationFlythrough from "../../models/InvitationFlythroughModel.js"
import Users from "../../models/UserModel.js"
import VirtualExperiences from "../../models/VirtualExperienceModel.js"

export const get_invitation_flythrough = async (req, res) => {
  let { id } = req.params
  let invitation

  try {
    invitation = await InvitationFlythrough.findOne({
      where: {
        id: id,
      },
      plain: true,
      nest: true,
      include: [
        {
          model: VirtualExperiences,
          include: [{ model: Clients, as: "client" }],
        },
        {
          model: Users,
          as: "invitation_flythrough_user",
        },
        {
          model: Users,
          as: "invitation_flythrough_backup_user",
        },
      ],
    })

    if (!invitation) {
      return res.status(400).json({ message: "No Invitation found" })
    }

    return res.status(200).json(invitation)
  } catch (error) {
    console.log(error)
  }
}
