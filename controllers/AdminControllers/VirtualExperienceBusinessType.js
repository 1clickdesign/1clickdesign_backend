import VirtualExperienceBusinessType from "../../models/VirtualExperienceBusinessTypeModel.js";
import {Op} from "sequelize";

export const get_all_business_types = async (req, res) => {
  
  let query = req.query;

  let business_types;

  try {
    if(query && query.search !== "null") {
      business_types = await VirtualExperienceBusinessType.findAndCountAll({
        offset: parseInt(query.offset),
        limit: parseInt(query.limit),
        nest: true,
        where: {
          name: {
            [Op.substring]: query.search
          }
        }
      })
    } else {
      business_types = await VirtualExperienceBusinessType.findAndCountAll({
        offset: parseInt(query.offset),
        limit: parseInt(query.limit),
        nest: true,
      })
    }

    console.log(business_types)
    console.log(query)

    if(!business_types) {
      return res.status(400).json({message: "No Business Types found"})
    }

    return res.status(200).json({business_types: business_types})

  } catch (error) {
    console.log("Error: ", error)
  }
}

export const get_all_business_types_option = async (req, res) => {
  
  try {
    let business_types = await VirtualExperienceBusinessType.findAll({
      nest: true,
      where: {
        status: "Active"
      }
    })

    if(!business_types) {
      return res.status(400).json({message: "No Business Type found"})
    }

    return res.status(200).json({business_types: business_types})

  } catch (error) {
    console.log("Error: ", error)
  }
}

export const get_business_type = async (req, res) => {
  
  let {id} = req.params;

  let business_type;

  try {
    business_type = await VirtualExperienceBusinessType.findOne({
      where: {
        id: id
      },
      plain: true,
      nest: true,
    })

    if(!business_type) {
      return res.status(400).json({message: "No Business Type found"})
    }

    return res.status(200).json(business_type)

  } catch (error) {
    
  }
}

export const check_if_business_type_unique_name = async (req, res) => {

  const {name} = req.query;

  try {
    const check_name = await VirtualExperienceBusinessType.findOne({
      where: {
        name: name
      }
    })
    if(!check_name) {
      return res.status(204).json()
    }
    return res.status(200).json()
  } catch (error) {
    console.log(error)
    return res.status(400).json();
  }
}

export const create_business_type = async (req, res) => {
  
  let {name, description, status, created_by} = req.body;

  if(!name || !created_by) {
    return res.status(400).json({message: "Incomplete Information"})
  } else {
    try {
      const business_type = await VirtualExperienceBusinessType.create({
        name: name,
        description: description,
        status: status,
        created_by: created_by
      })

      if(!business_type) {
        return res.status(400).json({message: "Business Type was not successfully created"})
      }
      return res.status(201).json({message: "Business Type was successfully created"})
    } catch (error) {
      
    }
  }

}

export const edit_business_type = async (req, res) => {
  const {id} = req.params;
  
  const {name, description, status, updated_by} = req.body;

  try {
    const business_type = await VirtualExperienceBusinessType.update({
      name: name,
      description: description,
      status: status,
      updated_by: updated_by
    }, {
      where: {
        id: id
      }
    })

    if(!business_type) {
      return res.status(400).json({message: "Business Type was not successfully edited"})
    }

    return res.status(200).json({message: "Business Type was successfully edited"})
  } catch (error) {
    console.log(error)
  }

}

export const delete_business_type = async (req, res) => {

  const {id} = req.params;

  try {
    await VirtualExperienceBusinessType.destroy({
      where: {
        id: id,
      }
    })
    return res.status(200).json({message: "Business Type was successfully deleted"})
  } catch (error) {
    console.log(error)
    return res.status(400).json();
  }
}