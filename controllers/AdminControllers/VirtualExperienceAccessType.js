import VirtualExperienceAccessTypes from "../../models/VirtualExperienceAccessTypeModel.js";

import { Op } from "sequelize";

export const get_all_access_types = async (req, res) => {
  
  let query = req.query;

  let access_types;

  try {
    if(query && query.search !== "null") {
      access_types = await VirtualExperienceAccessTypes.findAndCountAll({
        offset: parseInt(query.offset),
        limit: parseInt(query.limit),
        nest: true,
        where: {
          name: {
            [Op.substring]: query.search
          }
        }
      })
    } else {
      access_types = await VirtualExperienceAccessTypes.findAndCountAll({
        offset: parseInt(query.offset),
        limit: parseInt(query.limit),
        nest: true,
      })
    }

    console.log(access_types)
    console.log(query)

    if(!access_types) {
      return res.status(400).json({message: "No Access Types found"})
    }

    return res.status(200).json({access_types: access_types})

  } catch (error) {
    console.log("Error: ", error)
  }
}

export const check_if_access_type_unique_name = async (req, res) => {

  const {name} = req.query;

  try {
    const check_name = await VirtualExperienceAccessTypes.findOne({
      where: {
        name: name
      }
    })
    if(!check_name) {
      return res.status(204).json()
    }
    return res.status(200).json()
  } catch (error) {
    console.log(error)
    return res.status(400).json();
  }
}

export const get_all_access_types_option = async (req, res) => {
  
  try {
    let access_types = await VirtualExperienceAccessTypes.findAll({
      nest: true,
      where: {
        status: "Active"
      }
    })

    if(!access_types) {
      return res.status(400).json({message: "No Access Type found"})
    }

    return res.status(200).json({access_types: access_types})

  } catch (error) {
    console.log("Error: ", error)
  }
}

export const get_access_type = async (req, res) => {
  
  let {id} = req.params;

  let access_type;

  try {
    access_type = await VirtualExperienceAccessTypes.findOne({
      where: {
        id: id
      },
      plain: true,
      nest: true,
    })

    if(!access_type) {
      return res.status(400).json({message: "No Access Type found"})
    }

    return res.status(200).json(access_type)

  } catch (error) {
    
  }
}

export const create_access_type = async (req, res) => {
  
  let {name, description, status, created_by} = req.body;

  if(!name || !created_by) {
    return res.status(400).json({message: "Incomplete Information"})
  } else {
    try {
      const access_type = await VirtualExperienceAccessTypes.create({
        name: name,
        description: description,
        status: status,
        created_by: created_by
      })

      if(!access_type) {
        return res.status(400).json({message: "Access Type was not successfully created"})
      }
      return res.status(201).json({message: "Access Type was successfully created"})
    } catch (error) {
      
    }
  }

}

export const edit_access_type = async (req, res) => {
  const {id} = req.params;
  
  const {name, description, status, updated_by} = req.body;

  try {
    const access_types = await VirtualExperienceAccessTypes.update({
      name: name,
      description: description,
      status: status,
      updated_by: updated_by
    }, {
      where: {
        id: id
      }
    })

    if(!access_types) {
      return res.status(400).json({message: "Access Type was not successfully edited"})
    }

    return res.status(200).json({message: "Access Type was successfully edited"})
  } catch (error) {
    console.log(error)
  }

}

export const delete_access_type = async (req, res) => {

  const {id} = req.params;

  try {
    await VirtualExperienceAccessTypes.destroy({
      where: {
        id: id,
      }
    })
    return res.status(200).json({message: "Access Type was successfully deleted"})
  } catch (error) {
    console.log(error)
    return res.status(400).json();
  }
}