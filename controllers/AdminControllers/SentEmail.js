import Customers from "../../models/CustomerModel.js"
import InvitationFlythrough from "../../models/InvitationFlythroughModel.js"
import Invitations from "../../models/InvitationModel.js"
import InvitationsSchedule from "../../models/InvitationsSchedule.js"
import SentEmails from "../../models/SentEmailModel.js"
import Users from "../../models/UserModel.js"
import { Op } from "sequelize"

export const get_all_emails = async (req, res) => {
  let query = req.query

  let emails

  try {
    if (query && query.search !== "null" && query.search) {
      if (req.query.id !== "null") {
        emails = await SentEmails.findAndCountAll({
          offset: parseInt(query.offset),
          limit: parseInt(query.limit),
          nest: true,
          where: {
            subject: {
              [Op.substring]: query.search,
            },
          },
          include: [
            {
              model: Users,
              where: {
                client_id: req.query.id,
              },
            },
            {
              model: Customers,
            },
          ],
          order: [["created_at", "DESC"]],
        })
      } else {
        emails = await SentEmails.findAndCountAll({
          offset: parseInt(query.offset),
          limit: parseInt(query.limit),
          nest: true,
          where: {
            subject: {
              [Op.substring]: query.search,
            },
          },
          include: [
            {
              model: Users,
            },
            {
              model: Customers,
            },
          ],
          order: [["created_at", "DESC"]],
        })
      }
    } else {
      if (req.query.id !== "null") {
        emails = await SentEmails.findAndCountAll({
          offset: parseInt(query.offset),
          limit: parseInt(query.limit),
          nest: true,
          include: [
            {
              model: Users,
              where: {
                client_id: req.query.id,
              },
            },
            {
              model: Customers,
            },
          ],
          order: [["created_at", "DESC"]],
        })
      } else {
        emails = await SentEmails.findAndCountAll({
          offset: parseInt(query.offset),
          limit: parseInt(query.limit),
          nest: true,
          include: [
            {
              model: Users,
            },
            {
              model: Customers,
            },
          ],
          order: [["created_at", "DESC"]],
        })
      }
    }

    if (!emails) {
      return res.status(204).json({ message: "No Emails found" })
    }

    return res.status(200).json({ emails: emails })
  } catch (error) {
    console.log(error)
    return res.status(400).json()
  }
}

export const get_sent_email = async (req, res) => {
  let { id } = req.params

  let sent_email

  try {
    sent_email = await SentEmails.findOne({
      where: {
        id: id,
      },
      plain: true,
      nest: true,
      include: [
        {
          model: Users,
          attributes: ["id", "first_name", "last_name"],
        },
        {
          model: Customers,
          attributes: ["id", "full_name"],
        },
      ],
    })

    if (!sent_email) {
      return res.status(400).json({ message: "No Sent Email found" })
    }

    return res.status(200).json(sent_email)
  } catch (error) {
    console.log(error)
  }
}

export const create_sent_email = async (req, res) => {
  const body = req.body

  try {
    const sent_email = await SentEmails.create({
      type: body.type,
      status: body.status,
      to: body.to,
      sender_name: body.sender_name,
      from: body.from,
      subject: body.subject,
      bcc: body.bcc,
      cc: body.cc,
      reply_to: body.reply_to,
      headers: body.headers,
      attachments: body.attachments,
      tags: body.tags,
      html: body.html,
      text: body.text,
      content: body.content,
      greeting: body.greeting,
      regards: body.regards,
      user_id: body.user_id,
      customer_id: body.customer_id,
      email_id: body.email_id,
      response: body.response,
      created_by: body.created_by,
    })

    if (!sent_email) {
      console.log("Not Success")
    }

    return res.status(200).json({
      message: "Successfully created Sent Emails",
      sent_email: sent_email,
    })
  } catch (error) {
    console.log(error)
  }
}

export const create_sent_email_invitation = async (req, res) => {
  const {
    status,
    to,
    sender_name,
    from,
    subject,
    bcc,
    cc,
    reply_to,
    headers,
    attachments,
    tags,
    html,
    text,
    content,
    greeting,
    regards,
    user_id,
    backup_id,
    customer_id,
    email_id,
    response,
    can_share,
    access_expiration_date,
    passcode,
    virtual_experience_id,
    client_id,
    invitation_showrooms,
    invitation_participant,
    schedule_send_date,
    created_by,
    type,
  } = req.body

  console.log(req.body, "123", invitation_participant)
  try {
    const sent_email = await SentEmails.create({
      type: type,
      status: status,
      to: to,
      sender_name: sender_name,
      from: from,
      subject: subject,
      bcc: bcc,
      cc: cc,
      reply_to: reply_to,
      headers: headers,
      attachments: attachments,
      tags: tags,
      html: html,
      text: text,
      content: content,
      greeting: greeting,
      regards: regards,
      user_id: user_id,
      customer_id: customer_id,
      email_id: email_id,
      response: response,
      created_by: created_by,
    })

    if (!sent_email) {
      console.log("Not Success")
    }

    const invitation = await Invitations.create({
      passcode: passcode,
      can_share: can_share,
      access_expiration_date: access_expiration_date,
      user_id: user_id,
      customer_id: customer_id,
      virtual_experience_id: virtual_experience_id,
      client_id: client_id,
      backup_user_id: backup_id,
      invitation_showrooms,
      invitation_participant,
      sent_email_id: sent_email.dataValues.id,
      schedule_send_date: schedule_send_date,
      created_by: created_by,
    })

    if (!!schedule_send_date) {
      const scheduled_invitation = await InvitationsSchedule.create({
        passcode: passcode,
        can_share: can_share,
        access_expiration_date: access_expiration_date,
        user_id: user_id,
        customer_id: customer_id,
        virtual_experience_id: virtual_experience_id,
        client_id: client_id,
        backup_user_id: backup_id,
        invitation_showrooms,
        invitation_participant,
        sent_email_id: sent_email.dataValues.id,
        schedule_send_date: schedule_send_date,
        created_by: created_by,
      })
    }


    if (!invitation) {
      console.log("Not Success")
    }

    return res.status(200).json({
      message: "Successfully created Sent Emails",
      invitation: invitation,
      sent_email: sent_email,
    })
  } catch (error) {
    console.log(error)
  }
}
export const create_sent_email_invitation_flythrough = async (req, res) => {
  const {
    status,
    to,
    sender_name,
    from,
    subject,
    bcc,
    cc,
    reply_to,
    headers,
    attachments,
    tags,
    html,
    text,
    content,
    greeting,
    regards,
    user_id,
    backup_id,
    customer_id,
    email_id,
    response,
    can_share,
    access_expiration_date,
    passcode,
    virtual_experience_id,
    client_id,
    invitation_showrooms,
    invitation_participant,
    created_by,
    type,
  } = req.body

  console.log(req.body, "123", invitation_participant)
  try {
    const sent_email = await SentEmails.create({
      type: type,
      status: status,
      to: to,
      sender_name: sender_name,
      from: from,
      subject: subject,
      bcc: bcc,
      cc: cc,
      reply_to: reply_to,
      headers: headers,
      attachments: attachments,
      tags: tags,
      html: html,
      text: text,
      content: content,
      greeting: greeting,
      regards: regards,
      user_id: user_id,
      customer_id: customer_id,
      email_id: email_id,
      response: response,
      created_by: created_by,
    })

    if (!sent_email) {
      console.log("Not Success")
    }

    return res.status(200).json({
      message: "Successfully created Sent Emails",
      sent_email: sent_email,
    })
  } catch (error) {
    console.log(error)
  }
}
export const create_sent_email_invitation_flythrough_create = async (
  req,
  res,
) => {
  const {
    status,
    to,
    sender_name,
    from,
    subject,
    bcc,
    cc,
    reply_to,
    headers,
    attachments,
    tags,
    html,
    text,
    content,
    greeting,
    regards,
    user_id,
    backup_id,
    customer_id,
    email_id,
    response,
    can_share,
    access_expiration_date,
    passcode,
    virtual_experience_id,
    client_id,
    invitation_showrooms,
    invitation_participant,
    schedule_send_date,
    created_by,
    type,

  } = req.body

  // TODO: 
  // const showroom_url = `https://platform.1clickdesign.com/connect_live/${showroomId}/${userId}/${customerId}?invitation=${invitationIdd}`
  try {
    const invitation = await InvitationFlythrough.create({
      can_share: can_share,
      access_expiration_date: access_expiration_date,
      user_id: user_id,
      customer_id: customer_id,
      virtual_experience_id: virtual_experience_id,
      client_id: client_id,
      backup_user_id: backup_id,
      invitation_showrooms,
      invitation_participant,
      schedule_send_date: schedule_send_date,
      // sent_email_id: sent_email.dataValues.id,
      created_by: created_by,
      showroom_url: ''
    })

    if (!invitation) {
      console.log("Not Success")
    }

    return res.status(200).json({
      message: "Successfully created Sent Emails",
      invitation: invitation,
    })
  } catch (error) {
    console.log(error)
  }
}

export const update_sent_email_status = async (req, res) => {
  const { id, status } = req.params
  try {
    const status_update = await SentEmails.update(
      {
        status: status,
        updated_by: {
          id: null,
          name: "System",
          role: null,
        },
      },
      {
        where: {
          id: id,
        },
      },
    )

    if (!status_update) {
      return res.status(400).json()
    }

    return res.status(200).json()
  } catch (error) {
    console.log(error)
  }
}

export const delete_sent_email = async (req, res) => {
  const { id } = req.params

  try {
    await SentEmails.destroy({
      where: {
        id: id,
      },
    })
    return res
      .status(200)
      .json({ message: "Sent Email was successfully deleted" })
  } catch (error) {
    console.log(error)
    return res.status(400).json()
  }
}

export const delete_invitation = async (req, res) => {
  const { id } = req.params

  try {
    await Invitations.destroy({
      where: {
        id: id,
      },
    })
    return res
      .status(200)
      .json({ message: "Invitation was successfully deleted" })
  } catch (error) {
    console.log(error)
    return res.status(400).json()
  }
}
export const delete_flythrough_invitation = async (req, res) => {
  const { id } = req.params

  try {
    await InvitationFlythrough.destroy({
      where: {
        id: id,
      },
    })
    return res
      .status(200)
      .json({ message: "Flythrough Invitation was successfully deleted" })
  } catch (error) {
    console.log(error)
    return res.status(400).json()
  }
}

export const update_sent_email_is_viewed = async (req, res) => {
  const { id } = req.params;

  SentEmails.update(
    {
      is_viewed: true,
    },
    {
      where: {
        id: id,
      },
    },
  )

  // const img = Buffer.from(
  //   'iVBORw0KGgoAAAANSUhEUgAAAGQAAABkCAIAAAD/gAIDAAAACXBIWXMAAAsTAAALEwEAmpwYAAACU0lEQVR4nO3TQQrCQBAG0Sd7/39llAghRCp7cWUnAMhu3P+z4ZfsNrEKc6YzD/hFvupKmggAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADwdQekKgDCAAAAJXRFWHRkYXRlOmNyZWF0ZQAyMDIyLTA4LTA4VDIwOjQwOjAwKzAwOjAwMNO5KwAAACV0RVh0ZGF0ZTptb2RpZnkAMjAyMi0wOC0wOFQyMDo0MDowMCswMDowMGd8fCgAAAAASUVORK5CYII=',
  //   'base64'
  // );
  // res.writeHead(200, {
  //   'Content-Type': 'image/png',
  //   'Content-Length': img.length
  // });

  const filePath = path.resolve('./test.png');

  res.sendFile(filePath);
}

export const update_flythrough_is_viewed = async (req, res) => {
  const { id } = req.params;
  console.log("viewing---------------");

  InvitationFlythrough.update(
    {
      is_viewed: true,
    },
    {
      where: {
        id: id,
      },
    },
  )

  // const img = Buffer.from(
  //   'iVBORw0KGgoAAAANSUhEUgAAAGQAAABkCAIAAAD/gAIDAAAACXBIWXMAAAsTAAALEwEAmpwYAAACU0lEQVR4nO3TQQrCQBAG0Sd7/39llAghRCp7cWUnAMhu3P+z4ZfsNrEKc6YzD/hFvupKmggAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADwdQekKgDCAAAAJXRFWHRkYXRlOmNyZWF0ZQAyMDIyLTA4LTA4VDIwOjQwOjAwKzAwOjAwMNO5KwAAACV0RVh0ZGF0ZTptb2RpZnkAMjAyMi0wOC0wOFQyMDo0MDowMCswMDowMGd8fCgAAAAASUVORK5CYII=',
  //   'base64'
  // );
  // res.writeHead(200, {
  //   'Content-Type': 'image/png',
  //   'Content-Length': img.length
  // });

  const filePath = path.resolve('./test.png');
  // const filePath = `./test.png`

  res.sendFile(filePath);
}
