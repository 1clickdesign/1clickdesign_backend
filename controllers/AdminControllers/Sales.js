import Roles from "../../models/RoleModel.js";
import Users from "../../models/UserModel.js";
import { Op } from "sequelize";
import VirtualExperienceCategories from "../../models/VirtualExperienceCategoryModel.js";
import VirtualExperiences from "../../models/VirtualExperienceModel.js";
import Clients from "../../models/ClientModel.js";
import Permissions from "../../models/PermissionModel.js";
import UserCategories from "../../models/UserCategoryModel.js";
import UserBackupUsers from "../../models/UserBackupUserModel.js";
import VirtualExperienceAccessTypes from "../../models/VirtualExperienceAccessTypeModel.js";
import VirtualExperienceBusinessTypes from "../../models/VirtualExperienceBusinessTypeModel.js";
import VirtualExperienceTypes from "../../models/VirtualExperienceTypeModel.js";
import BackgroundMusics from "../../models/BackgroundMusicModel.js";
import Invitations from "../../models/InvitationModel.js";
import Customers from "../../models/CustomerModel.js";
import SentEmails from "../../models/SentEmailModel.js";
import Products from "../../models/ProductModel.js";
import CustomerGroups from "../../models/CustomerGroupModel.js";
import ClientCustomers from "../../models/ClientCustomerModel.js";

export const get_all_sales = async (req, res) => {
  const { limit, offset, search } = req.query;

  let sales;
  try {
    if (req.query && req.query.search !== "null" && req.query.search) {
      sales = await Users.findAndCountAll({
        limit: parseInt(limit),
        offset: parseInt(offset),
        nest: true,
        order: [["first_name", "ASC"]],
        where: {
          [Op.or]: {
            first_name: {
              [Op.substring]: search,
            },
            last_name: {
              [Op.substring]: search,
            },
            email: {
              [Op.substring]: search,
            },
          },
        },
        include: [
          {
            model: VirtualExperienceCategories,
            as: "user_category",
          },
          {
            model: VirtualExperiences,
            as: "sales",
          },
          {
            model: Roles,
            where: {
              name: "1CD Sales",
            },
          },
          {
            model: Clients,
            attributes: ["id", "name"],
          },
        ],
      });
    } else {
      sales = await Users.findAndCountAll({
        limit: parseInt(limit),
        offset: parseInt(offset),
        nest: true,
        order: [["first_name", "ASC"]],
        include: [
          {
            model: VirtualExperienceCategories,
            as: "user_category",
          },
          {
            model: VirtualExperiences,
            as: "sales",
          },
          {
            model: Roles,
            where: {
              name: "1CD Sales",
            },
          },
          {
            model: Clients,
            attributes: ["id", "name"],
          },
        ],
      });
    }

    if (!sales) return res.status(400).json();

    return res.status(200).json({ sales: sales });
  } catch (error) {
    console.log(error);
  }
};

export const get_all_sales_option = async (req, res) => {
  const { category_id } = req.query;

  try {
    let sales = await Users.findAll({
      nest: true,
      order: [["first_name", "ASC"]],
      include: [
        {
          model: VirtualExperienceCategories,
          as: "user_category",
          where: {
            id: category_id,
          },
        },
        {
          model: Roles,
          where: {
            name: "1CD Sales",
          },
        },
      ],
    });

    if (!sales) return res.status(400).json();

    return res.status(200).json({ sales: sales });
  } catch (error) {
    console.log(error);
  }
};

export const get_sales = async (req, res) => {
  let { id } = req.params;

  let sales;

  try {
    sales = await Users.findOne({
      where: {
        id: id,
      },
      plain: true,
      nest: true,
      include: [
        {
          model: VirtualExperiences,
          as: "sales",
        },
        {
          model: Roles,
          attributes: ["name", "for_client"],
          include: [
            {
              model: Permissions,
              as: "role",
              attributes: ["id", "name"],
            },
          ],
        },
        {
          model: Clients,
          attributes: ["id", "name"],
        },
        {
          model: Users,
          as: "backup_user",
        },
      ],
    });

    if (!sales) {
      return res.status(400).json({ message: "No Sales found" });
    }

    return res.status(200).json(sales);
  } catch (error) {
    console.log(error);
  }
};

export const get_all_sales_virtual_experiences = async (req, res) => {
  let query = req.query;

  let virtual_experiences;

  const user_backup = await UserBackupUsers.findAll({
    where: {
      backup_user_id: req.query.user_id,
    },
    attributes: ["id"],
  });
  let ids = [];
  ids.push(req.query.user_id);
  if (user_backup) {
    for (let index = 0; index < user_backup.length; index++) {
      const element = user_backup[index];
      ids.push(element.dataValues.id);
    }
  }
  console.log(ids);

  try {
    if (query && query.search !== "null" && query.search) {
      virtual_experiences = await VirtualExperiences.findAndCountAll({
        offset: parseInt(query.offset),
        limit: parseInt(query.limit),
        nest: true,
        where: {
          sales_id: {
            [Op.in]: ids,
          },
          name: {
            [Op.substring]: query.search,
          },
        },
        include: [
          {
            model: VirtualExperienceAccessTypes,
            attributes: ["id", "name"],
          },
          {
            model: VirtualExperienceBusinessTypes,
            attributes: ["id", "name"],
          },
          {
            model: VirtualExperienceCategories,
            attributes: ["id", "name"],
          },
          {
            model: VirtualExperienceTypes,
            attributes: ["id", "name"],
          },
          {
            model: BackgroundMusics,
            attributes: ["id", "name", "music_url"],
          },
          {
            model: Clients,
            attributes: [
              "id",
              "name",
              "logo_url",
              "website_url",
              "description",
            ],
            include: [
              {
                model: Users,
                attributes: ["image_url", "first_name", "last_name", "email"],
              },
            ],
          },
        ],
      });
    } else {
      virtual_experiences = await VirtualExperiences.findAndCountAll({
        offset: parseInt(query.offset),
        limit: parseInt(query.limit),
        nest: true,
        where: {
          sales_id: {
            [Op.in]: ids,
          },
        },
        include: [
          {
            model: VirtualExperienceAccessTypes,
            attributes: ["id", "name"],
          },
          {
            model: VirtualExperienceBusinessTypes,
            attributes: ["id", "name"],
          },
          {
            model: VirtualExperienceCategories,
            attributes: ["id", "name"],
          },
          {
            model: VirtualExperienceTypes,
            attributes: ["id", "name"],
          },
          {
            model: BackgroundMusics,
            attributes: ["id", "name", "music_url"],
          },
          {
            model: Clients,
            attributes: [
              "id",
              "name",
              "logo_url",
              "website_url",
              "description",
            ],
            include: [
              {
                model: Users,
                attributes: ["image_url", "first_name", "last_name", "email"],
              },
            ],
          },
        ],
      });
    }

    if (!virtual_experiences) {
      return res.status(400).json({ message: "No Virtual Experiences found" });
    }

    return res.status(200).json({ virtual_experiences: virtual_experiences });
  } catch (error) {
    console.log("Error: ", error);
  }
};

export const get_all_sales_invitation = async (req, res) => {
  let query = req.query;

  let invitations;

  const user_backup = await UserBackupUsers.findAll({
    where: {
      backup_user_id: req.query.user_id,
    },
    attributes: ["id"],
  });
  let ids = [];
  ids.push(req.query.user_id);
  if (user_backup) {
    for (let index = 0; index < user_backup.length; index++) {
      const element = user_backup[index];
      ids.push(element.dataValues.id);
    }
  }
  console.log(ids);

  try {
    if (query && query.search !== "null" && query.search) {
      invitations = await Invitations.findAndCountAll({
        offset: Number(query.offset),
        limit: Number(query.limit),
        nest: true,
        subQuery: false,
        where: {
          [Op.or]: {
            user_id: {
              [Op.in]: ids,
            },
            virtual_experience_id: req.query.virtual_experience_id,
            "$invitation_user.first_name$": {
              [Op.substring]: query.search,
            },
            "$invitation_user.last_name$": {
              [Op.substring]: query.search,
            },
            "$customer.full_name$": {
              [Op.substring]: query.search,
            },
            "$virtual_experience.name$": {
              [Op.substring]: query.search,
            },
            "$sent_email.status$": {
              [Op.substring]: query.search,
            },
          },
        },
        include: [
          {
            model: Users,
            as: "invitation_user",
          },
          {
            model: Clients,
          },
          {
            model: VirtualExperiences,
            as: "virtual_experience",
          },
          {
            model: Customers,
            as: "customer",
          },
          {
            model: SentEmails,
            as: "sent_email",
          },
        ],
        order: [["created_at", "DESC"]],
      });
    } else {
      invitations = await Invitations.findAndCountAll({
        offset: Number(query.offset),
        limit: Number(query.limit),
        nest: true,
        subQuery: false,
        where: {
          [Op.or]: {
            user_id: {
              [Op.in]: ids,
            },
            virtual_experience_id: req.query.virtual_experience_id,
          },
        },
        include: [
          {
            model: Users,
            as: "invitation_user",
          },
          {
            model: Clients,
          },
          {
            model: VirtualExperiences,
            as: "virtual_experience",
          },
          {
            model: Customers,
            as: "customer",
          },
          {
            model: SentEmails,
            as: "sent_email",
          },
        ],
        order: [["created_at", "DESC"]],
      });
    }

    if (!invitations) {
      return res.status(204).json({ message: "No Invitations found" });
    }

    return res.status(200).json({ invitations: invitations });
  } catch (error) {
    console.log(error);
    return res.status(400).json();
  }
};

export const get_all_sales_emails = async (req, res) => {
  let query = req.query;

  let emails;

  const user_backup = await UserBackupUsers.findAll({
    where: {
      backup_user_id: req.query.user_id,
    },
    attributes: ["id"],
  });
  let ids = [];
  ids.push(req.query.user_id);
  if (user_backup) {
    for (let index = 0; index < user_backup.length; index++) {
      const element = user_backup[index];
      ids.push(element.dataValues.id);
    }
  }
  console.log(ids);

  try {
    if (query && query.search !== "null" && query.search) {
      emails = await SentEmails.findAndCountAll({
        offset: parseInt(query.offset),
        limit: parseInt(query.limit),
        nest: true,
        subQuery: false,
        where: {
          subject: {
            [Op.substring]: query.search,
          },
          [Op.or]: {
            user_id: {
              [Op.in]: ids,
            },
            "$user.client.virtual_experience.sales_id$": {
              [Op.in]: ids,
            },
            "$invitations.user_id$": {
              [Op.in]: ids,
            },
            "$user.client.virtual_experience.id$":
              req.query.virtual_experience_id,
          },
        },
        include: [
          {
            model: Users,
            as: "user",
            include: [
              {
                model: Clients,
                as: "client",
                include: [
                  {
                    model: VirtualExperiences,
                    as: "virtual_experience",
                  },
                ],
              },
            ],
          },
          {
            model: Customers,
          },
          {
            model: Invitations,
          },
        ],
        order: [["created_at", "DESC"]],
      });
    } else {
      emails = await SentEmails.findAndCountAll({
        offset: parseInt(query.offset),
        limit: parseInt(query.limit),
        nest: true,
        subQuery: false,
        where: {
          [Op.or]: {
            user_id: {
              [Op.in]: ids,
            },
            "$user.client.virtual_experience.sales_id$": {
              [Op.in]: ids,
            },
            "$invitations.user_id$": {
              [Op.in]: ids,
            },
            "$user.client.virtual_experience.id$":
              req.query.virtual_experience_id,
          },
        },
        include: [
          {
            model: Users,
            as: "user",
            include: [
              {
                model: Clients,
                as: "client",
                include: [
                  {
                    model: VirtualExperiences,
                    as: "virtual_experience",
                  },
                ],
              },
            ],
          },
          {
            model: Invitations,
          },
          {
            model: Customers,
          },
        ],
        order: [["created_at", "DESC"]],
      });
    }

    if (!emails) {
      return res.status(204).json({ message: "No Emails found" });
    }

    return res.status(200).json({ emails: emails });
  } catch (error) {
    console.log(error);
    return res.status(400).json();
  }
};

export const get_all_sales_users = async (req, res) => {
  let query = req.query;

  let users;

  const user_backup = await UserBackupUsers.findAll({
    where: {
      backup_user_id: req.query.user_id,
    },
    attributes: ["id"],
  });
  let ids = [];
  ids.push(req.query.user_id);
  if (user_backup) {
    for (let index = 0; index < user_backup.length; index++) {
      const element = user_backup[index];
      ids.push(element.dataValues.id);
    }
  }
  console.log(ids);

  try {
    if (query && query.search !== "null" && query.search) {
      users = await Users.findAndCountAll({
        offset: parseInt(query.offset),
        limit: parseInt(query.limit),
        nest: true,
        order: [["first_name", "ASC"]],
        subQuery: false,
        where: {
          [Op.or]: {
            "$main_user.id$": {
              [Op.in]: ids,
            },
            "$client.virtual_experience.sales_id$": {
              [Op.in]: ids,
            },
          },
          [Op.or]: {
            first_name: {
              [Op.substring]: query.search,
            },
            middle_name: {
              [Op.substring]: query.search,
            },
            last_name: {
              [Op.substring]: query.search,
            },
            email: {
              [Op.substring]: query.search,
            },
            "$role.name$": {
              [Op.substring]: query.search,
            },
          },
        },
        include: [
          {
            model: Roles,
            attributes: ["id", "name"],
            as: "role",
          },
          {
            model: Clients,
            as: "client",
            include: [
              {
                model: VirtualExperiences,
                as: "virtual_experience",
              },
            ],
          },
          {
            model: Users,
            as: "main_user",
          },
        ],
      });
    } else {
      users = await Users.findAndCountAll({
        offset: parseInt(query.offset),
        limit: parseInt(query.limit),
        nest: true,
        order: [["first_name", "ASC"]],
        subQuery: false,
        where: {
          [Op.or]: {
            "$main_user.id$": {
              [Op.in]: ids,
            },
            "$client.virtual_experience.sales_id$": {
              [Op.in]: ids,
            },
          },
        },
        include: [
          {
            model: Roles,
            attributes: ["id", "name"],
            as: "role",
          },
          {
            model: Clients,
            as: "client",
            include: [
              {
                model: VirtualExperiences,
                as: "virtual_experience",
              },
            ],
          },
          {
            model: Users,
            as: "main_user",
          },
        ],
      });
    }

    if (!users) {
      return res.status(400).json({ message: "No Users found" });
    }

    return res.status(200).json({ users: users });
  } catch (error) {
    console.log("Error: ", error);
  }
};

export const get_all_sales_products = async (req, res) => {
  let query = req.query;

  let products;

  const user_backup = await UserBackupUsers.findAll({
    where: {
      backup_user_id: req.query.user_id,
    },
    attributes: ["id"],
  });
  let ids = [];
  ids.push(req.query.user_id);
  if (user_backup) {
    for (let index = 0; index < user_backup.length; index++) {
      const element = user_backup[index];
      ids.push(element.dataValues.id);
    }
  }
  console.log(ids);

  try {
    if (query && query.search !== "null") {
      if (!!req.query.filter) {
        products = await Products.findAndCountAll({
          offset: parseInt(query.offset),
          limit: parseInt(query.limit),
          nest: true,
          subQuery: false,
          where: {
            "$virtual_experience.sales_id$": {
              [Op.in]: ids,
            },
            name: {
              [Op.substring]: query.search,
            },
            virtual_experience_id: req.query.filter
          },
          include: [
            {
              model: VirtualExperiences,
              as: "virtual_experience",
            },
            {
              model: Clients,
              attributes: ["id", "name"],
            },
          ],
        });
      } else {
        products = await Products.findAndCountAll({
          offset: parseInt(query.offset),
          limit: parseInt(query.limit),
          nest: true,
          subQuery: false,
          where: {
            "$virtual_experience.sales_id$": {
              [Op.in]: ids,
            },
            name: {
              [Op.substring]: query.search,
            },
          },
          include: [
            {
              model: VirtualExperiences,
              as: "virtual_experience",
            },
            {
              model: Clients,
              attributes: ["id", "name"],
            },
          ],
        });        
      }

    } else {
      if (!!req.query.filter) {
        products = await Products.findAndCountAll({
          offset: parseInt(query.offset),
          limit: parseInt(query.limit),
          nest: true,
          where: {
            "$virtual_experience.sales_id$": {
              [Op.in]: ids,
            },
            virtual_experience_id: req.query.filter
          },
          include: [
            {
              model: VirtualExperiences,
              as: "virtual_experience",
            },
            {
              model: Clients,
              attributes: ["id", "name"],
            },
          ],
        });        
      } else {
        products = await Products.findAndCountAll({
          offset: parseInt(query.offset),
          limit: parseInt(query.limit),
          nest: true,
          where: {
            "$virtual_experience.sales_id$": {
              [Op.in]: ids,
            },
          },
          include: [
            {
              model: VirtualExperiences,
              as: "virtual_experience",
            },
            {
              model: Clients,
              attributes: ["id", "name"],
            },
          ],
        });        
      }

    }

    if (!products) {
      return res.status(400).json({ message: "No Products found" });
    }

    return res.status(200).json({ products: products });
  } catch (error) {
    console.log("Error: ", error);
  }
};

// export const get_all_sales_customers = async (req, res) => {
//   let query = req.query;

//   let customers;

//   const user_backup = await UserBackupUsers.findAll({
//     where: {
//       backup_user_id: req.query.user_id,
//     },
//     attributes: ["id"],
//   });
//   let ids = [];
//   ids.push(req.query.user_id);
//   if (user_backup) {
//     for (let index = 0; index < user_backup.length; index++) {
//       const element = user_backup[index];
//       ids.push(element.dataValues.id);
//     }
//   }
//   console.log(ids);

//   try {
//     if (query && query.search !== "null" && query.search) {
//       customers = await Customers.findAndCountAll({
//         offset: Number(query.offset),
//         limit: Number(query.limit),
//         nest: true,
//         subQuery: false,
//         where: {
//           full_name: {
//             [Op.substring]: query.search,
//           },
//           [Op.or]: {
//             "$client_customers.user_id$": {
//               [Op.in]: ids,
//             },
//             "$client_customers.client.virtual_experience.sales_id$": {
//               [Op.in]: ids,
//             },
//             "$client_customers.client.virtual_experience.id$":
//               req.query.virtual_experience_id,
//           },
//         },
//         include: [
//           {
//             model: CustomerGroups,
//             as: "customer_member",
//           },
//           {
//             model: ClientCustomers,
//             include: [
//               {
//                 model: Clients,
//                 as: "client",
//                 include: [
//                   {
//                     model: VirtualExperiences,
//                     as: "virtual_experience",
//                   },
//                 ],
//               },
//               {
//                 model: Users,
//                 as: "user",
//               },
//             ],
//           },
//         ],
//       });
//     } else {
//       customers = await Customers.findAndCountAll({
//         offset: Number(query.offset),
//         limit: Number(query.limit),
//         nest: true,
//         subQuery: false,
//         where: {
//           [Op.or]: {
//             "$client_customers.user_id$": {
//               [Op.in]: ids,
//             },
//             "$client_customers.client.virtual_experience.sales_id$": {
//               [Op.in]: ids,
//             },
//             "$client_customers.client.virtual_experience.id$":
//               req.query.virtual_experience_id,
//           },
//         },
//         include: [
//           {
//             model: CustomerGroups,
//             as: "customer_member",
//           },
//           {
//             model: ClientCustomers,
//             include: [
//               {
//                 model: Clients,
//                 as: "client",
//                 include: [
//                   {
//                     model: VirtualExperiences,
//                     as: "virtual_experience",
//                   },
//                 ],
//               },
//               {
//                 model: Users,
//                 as: "user",
//               },
//             ],
//           },
//         ],
//       });
//     }

//     if (!customers) {
//       return res.status(400).json({ message: "No Customers found" });
//     }

//     return res.status(200).json({ customers: customers });
//   } catch (error) {
//     console.log("Error: ", error);
//   }
// };

export const get_all_sales_customers = async (req, res) => {
  let query = req.query;
  console.log("req query", req.query);
  let customers;

  // const user_backup = await UserBackupUsers.findAll({
  //   where: {
  //     backup_user_id: req.query.user_id,
  //   },
  //   attributes: ["id"],
  // });
  // let ids = [];
  // ids.push(req.query.user_id);
  // if (user_backup) {
  //   for (let index = 0; index < user_backup.length; index++) {
  //     const element = user_backup[index];
  //     ids.push(element.dataValues.id);
  //   }
  // }
  // console.log(ids);

  try {
    if (query && query.search !== "null" && query.search) {
      console.log("search");
      customers = await Customers.findAndCountAll({
        offset: Number(query.offset),
        limit: Number(query.limit),
        nest: true,
        subQuery: false,
        where: {
          full_name: {
            [Op.substring]: query.search,
          },
          [Op.or]: {
            // "$client_customers.user_id$": {
            //   [Op.in]: ids,
            // },
            // "$client_customers.client.virtual_experience.sales_id$": {
            //   [Op.in]: ids,
            // },
            "$client_customers.client_id$": req.query.id,
          },
        },
        include: [
          {
            model: CustomerGroups,
            as: "customer_member",
          },
          {
            model: ClientCustomers,
            include: [
              {
                model: Clients,
                as: "client",
                include: [
                  {
                    model: VirtualExperiences,
                    as: "virtual_experience",
                  },
                ],
              },
              {
                model: Users,
                as: "user",
              },
            ],
          },
        ],
      });
    } else {
      console.log("not search");
      customers = await Customers.findAndCountAll({
        offset: Number(query.offset),
        limit: Number(query.limit),
        nest: true,
        subQuery: false,
        where: {
          [Op.or]: {
            // "$client_customers.user_id$": {
            //   [Op.in]: ids,
            // },
            // "$client_customers.client_id$": {
            //   [Op.in]: ids,
            // },
            "$client_customers.client_id$": req.query.id,
          },
        },
        include: [
          {
            model: CustomerGroups,
            as: "customer_member",
          },
          {
            model: ClientCustomers,
            include: [
              {
                model: Clients,
                as: "client",
                include: [
                  {
                    model: VirtualExperiences,
                    as: "virtual_experience",
                  },
                ],
              },
              {
                model: Users,
                as: "user",
              },
            ],
          },
        ],
      });
    }

    if (!customers) {
      return res.status(400).json({ message: "No Customers found" });
    }

    return res.status(200).json({ customers: customers });
  } catch (error) {
    console.log("Error: ", error);
  }
};
