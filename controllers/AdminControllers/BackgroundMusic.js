import { Op } from "sequelize";
import BackgroundMusics from "../../models/BackgroundMusicModel.js";
import { S3Client, DeleteObjectsCommand } from "@aws-sdk/client-s3";

export const get_all_background_musics = async (req, res) => {
  
  let query = req.query;

  let background_musics;

  try {
    if(query && query.search !== "null") {
      background_musics = await BackgroundMusics.findAndCountAll({
        offset: parseInt(query.offset),
        limit: parseInt(query.limit),
        nest: true,
        where: {
          name: {
            [Op.substring]: query.search
          }
        }
      })
    } else {
      background_musics = await BackgroundMusics.findAndCountAll({
        offset: parseInt(query.offset),
        limit: parseInt(query.limit),
        nest: true,
      })
    }

    if(!background_musics) {
      return res.status(400).json({message: "No Background Musics found"})
    }

    return res.status(200).json({background_musics: background_musics})

  } catch (error) {
    console.log("Error: ", error)
  }
}


export const get_background_music = async (req, res) => {
  
  let {id} = req.params;

  let background_music;

  try {
    background_music = await BackgroundMusics.findOne({
      where: {
        id: id
      },
      plain: true,
      nest: true,
    })

    if(!background_music) {
      return res.status(400).json({message: "No Background Music found"})
    }

    return res.status(200).json(background_music)

  } catch (error) {
    console.log(error)
  }
}

export const get_background_music_option = async (req, res) => {
  try {
    const options = await BackgroundMusics.findAll({
      where: {
        status: "Active"
      }
    })

    console.log(options)

    if(!options) {
      return res.status(400).json({message: "No Background Music found"})
    }

    return res.status(200).json({musics: options})
  } catch (error) {
    console.log(error)
  }
}

export const check_if_background_music_unique_name = async (req, res) => {

  const {name} = req.query;

  try {
    const check_name = await BackgroundMusics.findOne({
      where: {
        name: name
      }
    })
    if(!check_name) {
      return res.status(204).json()
    }
    return res.status(200).json()
  } catch (error) {
    console.log(error)
    return res.status(400).json();
  }
}

export const create_background_music = async (req, res) => {
  const body = JSON.parse(JSON.stringify(req.body));
  const files = req.files;

  console.log(files)

  console.log("Body ", body);
  // console.log(files);
  const data = JSON.parse(body.data);
  // console.log(data);

  const {name, status, created_by} = data;

  try {
    const music = await BackgroundMusics.create({
      name: name,
      music_url: files[0].location,
      status: status,
      created_by: created_by
    })

    if(!music) {
      return res.status(400).json({message: "Background Music was not successfully created"})
    }

    return res.status(200).json({message: "Background Music was successfully created"})
  } catch (error) {
    
  }
}

export const edit_background_music = async (req, res) => {
  const {id} = req.params;
  
  const {name, status, music_url, music_urls, updated_by} = req.body;

  try {
    const music = await BackgroundMusics.update({
      name: name,
      status: status,
      updated_by: updated_by
    }, {
      where: {
        id: id
      }
    })

    if(!music) {
      return res.status(400).json({message: "Background Music was not successfully edited"})
    }

    return res.status(200).json({message: "Background Music was successfully edited"})
  } catch (error) {
    console.log(error)
  }

}

export const edit_background_music_with_file = async (req, res, next) => {
  const {id} = req.params;
  
  const body = JSON.parse(JSON.stringify(req.body));
  const files = req.files;

  console.log(files)

  console.log("Body ", body);
  // console.log(files);
  const data = JSON.parse(body.data);
  // console.log(data);

  const {name, status, music_urls, music_url, updated_by} = data;

  let music;

  try {
    if(music_url === files[0].location) {
      music = await BackgroundMusics.update({
        name: name,
        status: status,
        updated_by: updated_by
      }, {
        where: {
          id: id
        }
      })
      if(!music) {
        return res.status(400).json({message: "Background Music was not successfully edited"})
      }
  
      return res.status(200).json({message: "Background Music was successfully edited"})
    } else {
      music = await BackgroundMusics.update({
        name: name,
        status: status,
        music_url: files[0].location,
        updated_by: updated_by
      }, {
        where: {
          id: id
        }
      })

      const bucketName = process.env.S3_BUCKET;
    
      const params = {
            Bucket: bucketName,
            Delete: {
              Objects: music_urls.map((key) => ({ Key: key })),
            },
      };
      
    
      const client = new S3Client({
        region: process.env.S3_REGION,
        credentials: {
          accessKeyId: process.env.AWS_ACCESS_KEY_ID,
          secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY,
        },
      });
      const s3_delete = await client.send(new DeleteObjectsCommand(params));
      console.log(s3_delete)

      return res.status(200).json({message: "Background Music was successfully edited"})
    }

  } catch (error) {
    console.log(error)
  }

}

export const delete_background_music = async (req, res) => {

  const {id} = req.params;

  try {
    await BackgroundMusics.destroy({
      where: {
        id: id,
      }
    })
    return res.status(200).json({message: "Background Music was successfully deleted"})
  } catch (error) {
    console.log(error)
    return res.status(400).json();
  }
}