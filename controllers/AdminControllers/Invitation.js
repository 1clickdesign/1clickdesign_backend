import Clients from "../../models/ClientModel.js";
import Customers from "../../models/CustomerModel.js";
import Invitations from "../../models/InvitationModel.js";
import SentEmails from "../../models/SentEmailModel.js";
import Users from "../../models/UserModel.js";
import VirtualExperiences from "../../models/VirtualExperienceModel.js";
import VirtualExperienceTypes from "../../models/VirtualExperienceTypeModel.js";
import {Op} from "sequelize";
import path from 'path';

export const get_all_invitation = async (req, res) => {
  
  let query = req.query;

  let invitations;

  try {
    if (query && query.search !== "null" && query.search) {
      if(req.query.id !== "null") {
        invitations = await Invitations.findAndCountAll({
          offset: Number(query.offset),
          limit: Number(query.limit),
          nest: true,
          where: {
            client_id: req.query.id,
            [Op.or]: {
              '$invitation_user.first_name$': {
                [Op.substring]: query.search
              },
              '$invitation_user.last_name$': {
                [Op.substring]: query.search
              },
              '$customer.full_name$': {
                [Op.substring]: query.search
              },
              '$virtual_experience.name$': {
                [Op.substring]: query.search
              },
              '$sent_email.status$': {
                [Op.substring]: query.search
              }
            }
          },
          include: [
            {
              model: Users,
              as: "invitation_user"
            },
            {
              model: Clients
            },
            {
              model: VirtualExperiences,
              as: "virtual_experience"
            },
            {
              model: Customers,
              as: "customer"
            },
            {
              model: SentEmails,
              as: "sent_email"
            },
          ],
          order: [["created_at", "DESC"]]
        });
      } else {
        invitations = await Invitations.findAndCountAll({
          offset: Number(query.offset),
          limit: Number(query.limit),
          nest: true,
          where: {
            [Op.or]: {
              '$invitation_user.first_name$': {
                [Op.substring]: query.search
              },
              '$invitation_user.last_name$': {
                [Op.substring]: query.search
              },
              '$customer.full_name$': {
                [Op.substring]: query.search
              },
              '$virtual_experience.name$': {
                [Op.substring]: query.search
              },
              '$sent_email.status$': {
                [Op.substring]: query.search
              }
            }
          },
          include: [
            {
              model: Users,
              as: "invitation_user"
            },
            {
              model: Clients
            },
            {
              model: VirtualExperiences,
              as: "virtual_experience"
            },
            {
              model: Customers,
              as: "customer"
            },
            {
              model: SentEmails,
              as: "sent_email"
            },
          ],
          order: [["created_at", "DESC"]]
        });
      }
    } else {
      if(req.query.id !== "null") {
        invitations = await Invitations.findAndCountAll({
          offset: Number(query.offset),
          limit: Number(query.limit),
          nest: true,
          where: {
            client_id: query.id,
          },
          include: [
            {
              model: Users,
              as: "invitation_user"
            },
            {
              model: Clients
            },
            {
              model: VirtualExperiences
            },
            {
              model: Customers
            },
            {
              model: SentEmails
            },
          ],
          order: [["created_at", "DESC"]]
        });
      } else {
        invitations = await Invitations.findAndCountAll({
          offset: Number(query.offset),
          limit: Number(query.limit),
          nest: true,
          include: [
            {
              model: Users,
              as: "invitation_user"
            },
            {
              model: Clients
            },
            {
              model: VirtualExperiences
            },
            {
              model: Customers
            },
            {
              model: SentEmails
            },
          ],
          order: [["created_at", "DESC"]]
        });
      }
    }

    if (!invitations) {
      return res.status(204).json({ message: "No Invitations found" });
    }

    return res.status(200).json({ invitations: invitations });
  } catch (error) {
    console.log(error);
    return res.status(400).json();
  }
}

export const check_if_invitation_passcode_unique = async (req, res) => {
  const {code} = req.query;

  try {
    const check_code = await Invitations.findOne({
      where: {
        passcode: code
      }
    })
    if(!check_code) {
      return res.status(204).json()
    }
    return res.status(200).json()
  } catch (error) {
    console.log(error)
    return res.status(400).json();
  }
}

export const get_invitation = async (req, res) => {
  let { id } = req.params;

  let invitation;

  try {
    invitation = await Invitations.findOne({
      where: {
        id: id,
      },
      plain: true,
      nest: true,
      include: [
        {
          model: Users,
          as: "invitation_user"
        },
        {
          model: Users,
          as: "invitation_backup_user",
        },
        {
          model: Clients
        },
        {
          model: VirtualExperiences,
          include: [
            {
              model: VirtualExperienceTypes,
              attributes: ["id", "name"],
            },
          ]
        },
        {
          model: Customers
        },
        {
          model: SentEmails
        },
      ],
    });

    if (!invitation) {
      return res.status(400).json({ message: "No Invitation found" });
    }

    return res.status(200).json(invitation);
  } catch (error) {
    console.log(error);
  }
};

export const update_invitation_is_viewed = async (req, res) => {
    const { id } = req.params;
    console.log("viewing---------------");

    Invitations.update(
      {
        is_viewed: true,
      },
      {
        where: {
          id: id,
        },
      },
    )

    // const img = Buffer.from(
    //   'iVBORw0KGgoAAAANSUhEUgAAAGQAAABkCAIAAAD/gAIDAAAACXBIWXMAAAsTAAALEwEAmpwYAAACU0lEQVR4nO3TQQrCQBAG0Sd7/39llAghRCp7cWUnAMhu3P+z4ZfsNrEKc6YzD/hFvupKmggAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADwdQekKgDCAAAAJXRFWHRkYXRlOmNyZWF0ZQAyMDIyLTA4LTA4VDIwOjQwOjAwKzAwOjAwMNO5KwAAACV0RVh0ZGF0ZTptb2RpZnkAMjAyMi0wOC0wOFQyMDo0MDowMCswMDowMGd8fCgAAAAASUVORK5CYII=',
    //   'base64'
    // );
    // res.writeHead(200, {
    //   'Content-Type': 'image/png',
    //   'Content-Length': img.length
    // });

    const filePath = path.resolve('./test.png');
    // const filePath = `./test.png`

    res.sendFile(filePath);
}
