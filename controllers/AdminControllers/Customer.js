import { Op } from "sequelize"
import Clients from "../../models/ClientModel.js"
import CustomerGroupMembers from "../../models/CustomerGroupMemberModel.js"
import Customers from "../../models/CustomerModel.js"
import Users from "../../models/UserModel.js"
import CustomerGroups from "../../models/CustomerGroupModel.js"
import ClientCustomers from "../../models/ClientCustomerModel.js"
import { Resend } from "resend"

export const get_all_customers = async (req, res) => {
  let query = req.query

  let customers

  try {
    if (query && query.search !== "null" && query.search) {
      if (req.query.id !== "null") {
        customers = await Customers.findAndCountAll({
          offset: Number(query.offset),
          limit: Number(query.limit),
          nest: true,
          where: {
            full_name: {
              [Op.substring]: query.search,
            },
          },
          include: [
            {
              model: CustomerGroups,
              as: "customer_member",
            },
            {
              model: ClientCustomers,
              include: [
                {
                  model: Clients,
                  attributes: ["name", "id"],
                },
                {
                  model: Users,
                  attributes: ["first_name", "last_name", "id"],
                },
              ],
              where: {
                client_id: query.id,
              },
            },
          ],
        })
      } else {
        customers = await Customers.findAndCountAll({
          offset: Number(query.offset),
          limit: Number(query.limit),
          nest: true,
          where: {
            full_name: {
              [Op.substring]: query.search,
            },
          },
          include: [
            {
              model: CustomerGroups,
              as: "customer_member",
            },
            {
              model: ClientCustomers,
              include: [
                {
                  model: Clients,
                  attributes: ["name", "id"],
                },
                {
                  model: Users,
                  attributes: ["first_name", "last_name", "id"],
                },
              ],
            },
          ],
        })
      }
    } else {
      if (req.query.id !== "null") {
        customers = await Customers.findAndCountAll({
          offset: Number(query.offset),
          limit: Number(query.limit),
          nest: true,
          include: [
            {
              model: CustomerGroups,
              as: "customer_member",
            },
            {
              model: ClientCustomers,
              include: [
                {
                  model: Clients,
                  attributes: ["name", "id"],
                },
                {
                  model: Users,
                  attributes: ["first_name", "last_name", "id"],
                },
              ],
              where: {
                client_id: query.id,
              },
            },
          ],
        })
      } else {
        customers = await Customers.findAndCountAll({
          offset: Number(query.offset),
          limit: Number(query.limit),
          nest: true,
          include: [
            {
              model: CustomerGroups,
              as: "customer_member",
            },
            {
              model: ClientCustomers,
              include: [
                {
                  model: Clients,
                  attributes: ["name", "id"],
                },
                {
                  model: Users,
                  attributes: ["first_name", "last_name", "id"],
                },
              ],
            },
          ],
        })
      }
    }

    if (!customers) {
      return res.status(400).json({ message: "No Customers found" })
    }

    return res.status(200).json({ customers: customers })
  } catch (error) {
    console.log("Error: ", error)
  }
}

export const get_all_customer_pendings = async (req, res) => {
  let query = req.query

  let customers

  try {
    if (query && query.search !== "null" && query.search) {
      if (req.query.id !== "null") {
        customers = await Customers.findAndCountAll({
          offset: parseInt(query.offset),
          limit: parseInt(query.limit),
          nest: true,
          where: {
            [Op.or]: {
              full_name: {
                [Op.substring]: query.search,
              },
              email: {
                [Op.substring]: query.search,
              },
            },
            status: "Pending",
          },
          include: [
            {
              model: CustomerGroups,
              as: "customer_member",
            },
            {
              model: ClientCustomers,
              include: [
                {
                  model: Clients,
                  attributes: ["name", "id"],
                },
                {
                  model: Users,
                  attributes: ["first_name", "last_name", "id"],
                },
              ],
              where: {
                client_id: query.id,
              },
            },
          ],
        })
      } else {
        customers = await Customers.findAndCountAll({
          offset: parseInt(query.offset),
          limit: parseInt(query.limit),
          nest: true,
          where: {
            [Op.or]: {
              full_name: {
                [Op.substring]: query.search,
              },
              email: {
                [Op.substring]: query.search,
              },
            },
            status: "Pending",
          },
          include: [
            {
              model: CustomerGroups,
              as: "customer_member",
            },
            {
              model: ClientCustomers,
              include: [
                {
                  model: Clients,
                  attributes: ["name", "id"],
                },
                {
                  model: Users,
                  attributes: ["first_name", "last_name", "id"],
                },
              ],
            },
          ],
        })
      }
    } else {
      if (req.query.id !== "null") {
        customers = await Customers.findAndCountAll({
          offset: parseInt(query.offset),
          limit: parseInt(query.limit),
          nest: true,
          where: {
            status: "Pending",
          },
          include: [
            {
              model: CustomerGroups,
              as: "customer_member",
            },
            {
              model: ClientCustomers,
              include: [
                {
                  model: Clients,
                  attributes: ["name", "id"],
                },
                {
                  model: Users,
                  attributes: ["first_name", "last_name", "id"],
                },
              ],
              where: {
                client_id: query.id,
              },
            },
          ],
        })
      } else {
        customers = await Customers.findAndCountAll({
          offset: parseInt(query.offset),
          limit: parseInt(query.limit),
          nest: true,
          where: {
            status: "Pending",
          },
          include: [
            {
              model: CustomerGroups,
              as: "customer_member",
            },
            {
              model: ClientCustomers,
              include: [
                {
                  model: Clients,
                  attributes: ["name", "id"],
                },
                {
                  model: Users,
                  attributes: ["first_name", "last_name", "id"],
                },
              ],
            },
          ],
        })
      }
    }

    if (!customers) {
      return res.status(400).json({ message: "No Customers found" })
    }

    return res.status(200).json({ customers: customers })
  } catch (error) {
    console.log("Error: ", error)
  }
}

export const get_customer = async (req, res) => {
  let { id } = req.params

  let customer

  try {
    customer = await Customers.findOne({
      where: {
        id: id,
      },
      plain: true,
      nest: true,
      include: [
        {
          model: CustomerGroups,
          as: "customer_member",
        },
        {
          model: ClientCustomers,
          include: [
            {
              model: Clients,
              attributes: ["name", "id"],
            },
            {
              model: Users,
              attributes: ["first_name", "last_name", "id"],
            },
          ],
        },
      ],
    })

    if (!customer) {
      return res.status(400).json({ message: "No Customer found" })
    }

    return res.status(200).json(customer)
  } catch (error) {
    console.log(error)
  }
}

export const enterNotification = async (req, res) => {
  try {
    const { data, type } = req.body
    console.log(data)
    const resend = new Resend("re_ZDjQoiZK_FV8BiJTwz338diygw5Brqf8v")
    const content1CD =
      type == "enter-notification"
        ? `
        <h2>Customer Entered Shoppable Website</h2>
        <p>This is an automated notification to inform you that a customer has entered the shoppable website.</p>
        <br/>
        <h3>Customer Details:</h3>
        <ul>
          <li><strong>Name:</strong> ${data.name}</li>
          <li><strong>Email:</strong> ${data.email}</li>
          <li><strong>Entry Time:</strong> ${new Date().toLocaleString()}</li>
        </ul>
        `
        : ""

    const resendRes = await resend.emails.send({
      from: "1 Click Design <no-reply@1clickdesign.us>",
      to: "info@1clickdesign.us",
      subject:
        type == "enter-notification"
          ? "New Customer Entry - Shoppable Website"
          : "",
      html: content1CD || "",
      text: content1CD.replace(/<[^>]+>/g, ""), // Strip HTML for plain text version
      headers: {
        "List-Unsubscribe": "<https://emailer.1clickdesign.com/unsubscribe>",
      },
    })

    console.log("Email sent:", resendRes)

    return res.json({ message: "success" })
  } catch (error) {
    console.error("Error in enterNotification:", error)
    return res.status(500).json({ message: "Error sending notification" })
  }
}
export const get_customer_pending = async (req, res) => {
  let { id } = req.params

  let customer

  try {
    customer = await Customers.findOne({
      where: {
        id: id,
      },
      plain: true,
      nest: true,
      include: [
        {
          model: CustomerGroups,
          as: "customer_member",
        },
        {
          model: ClientCustomers,
        },
      ],
    })

    if (!customer) {
      return res.status(400).json({ message: "No Customer found" })
    }

    return res.status(200).json(customer)
  } catch (error) {
    console.log(error)
  }
}

export const get_all_customers_option = async (req, res) => {
  try {
    let customers
    if (req.query.id && req.query.id !== null) {
      customers = await Customers.findAll({
        nest: true,
        where: {
          // [Op.or]: {
          //   status: "Active",
          //   status: "Registered",
          // },
          status: {
            [Op.in]: ["Registered", "Active"],
            [Op.not]: "Blocked",
          },
        },
        attributes: ["id", "full_name", "email", "company"],
        include: [
          {
            model: ClientCustomers,
          },
          {
            model: ClientCustomers,
            include: [
              {
                model: Clients,
                attributes: ["name", "id"],
              },
              {
                model: Users,
                attributes: ["first_name", "last_name", "id"],
              },
            ],
            where: {
              client_id: req.query.id,
            },
          },
        ],
      })
    } else {
      customers = await Customers.findAll({
        nest: true,
        where: {
          // [Op.or]: {
          //   status: "Active",
          //   status: "Registered",
          // },
          status: {
            [Op.in]: ["Registered", "Active"],
            [Op.not]: "Blocked",
          },
        },
        attributes: ["id", "full_name", "email", "company"],
        include: [
          {
            model: ClientCustomers,
          },
        ],
      })
    }
    if (!customers) {
      return res.status(400).json({ message: "No Customers found" })
    }

    return res.status(200).json({ customers: customers })
  } catch (error) {
    console.log("Error: ", error)
  }
}

export const assign_customer = async (req, res) => {
  let { customer_id, client_id, user_id, created_by } = req.body

  try {
    const assign = await ClientCustomers.create({
      client_id: client_id,
      customer_id: customer_id,
      user_id: user_id,
      created_by: created_by,
    })

    if (!assign) {
    }

    return res.status(200).json({ message: "Success" })
  } catch (error) {
    console.log(error)
  }
}

export const create_customer = async (req, res) => {
  const body = JSON.parse(JSON.stringify(req.body))
  const files = JSON.parse(JSON.stringify(req.files))

  if (!files && !body) {
    return res.status(400).json()
  }

  console.log("Files: ", files)
  console.log("Files: ", JSON.parse(JSON.stringify(files)))

  console.log("Body ", body)
  // console.log(files);
  const data = JSON.parse(body.data)
  console.log(data)

  let {
    full_name,
    email,
    gender,
    shipping_address,
    phone_number,
    country,
    state,
    city,
    region,
    zip_code,
    status,
    place_id,
    position,
    telephone_number,
    company,
    latitude,
    longitude,
    customer_group,
    email_verification_status,
    client_id,
    created_by,
  } = data

  if (!created_by) {
    return res.status(400).json({ message: "Incomplete Information" })
  } else {
    try {
      const customer = await Customers.create({
        full_name: full_name,
        email: email,
        image_url: files[0]?.location ?? "",
        gender: gender,
        latitude: latitude,
        longitude: longitude,
        place_id: place_id,
        country: country,
        state: state,
        city: city,
        region: region,
        zip_code: zip_code,
        shipping_address: shipping_address,
        phone_number: phone_number,
        company: company,
        telephone_number: telephone_number,
        position: position,
        email_verification_status: email_verification_status,
        token: null,
        status: status,
        created_by: created_by,
      })

      if (!customer) {
        return res
          .status(400)
          .json({ message: "Customer was not successfully created" })
      }

      if (customer_group && customer_group.length > 0) {
        let groups = []
        for (let index = 0; index < customer_group.length; index++) {
          const element = customer_group[index]
          groups.push({
            customer_id: customer.dataValues.id,
            customer_group_id: element,
            created_by: created_by,
          })
        }

        await CustomerGroupMembers.bulkCreate(groups)
      }

      if (client_id) {
        const client = await ClientCustomers.create({
          client_id: client_id,
          customer_id: customer.dataValues.id,
          user_id: null,
        })
      }

      return res.status(201).json({
        id: customer.dataValues.id,
        message: "Customer was successfully created",
      })
    } catch (error) {
      console.log(error)
    }
  }
}

export const edit_customer_status = async (req, res) => {
  const { status, customer_id } = req.body

  try {
    const customer = await Customers.update(
      {
        status: status,
      },
      {
        where: {
          id: customer_id,
        },
      },
    )

    if (!customer) return res.status(400).json()
    return res.status(200).json()
  } catch (error) {}
}

export const edit_customer = async (req, res) => {
  const { id } = req.params
  const body = JSON.parse(JSON.stringify(req.body))
  const files = req.files
  const all_files = JSON.parse(JSON.stringify(files))

  const data = JSON.parse(body.data)

  console.log("Hey", files)
  console.log("Hey", JSON.parse(JSON.stringify(files)))

  const {
    full_name,
    email,
    gender,
    shipping_address,
    latitude,
    longitude,
    city,
    state,
    region,
    country,
    zip_code,
    place_id,
    phone_number,
    telephone_number,
    company,
    position,
    email_verification_status,
    status,
    updated_by,
  } = data
  console.log("data", data)
  let payload = {
    full_name: full_name,
    email: email,
    gender: gender,
    shipping_address: shipping_address,
    latitude: latitude,
    longitude: longitude,
    city: city,
    state: state,
    region: region,
    country: country,
    zip_code: zip_code,
    place_id: place_id,
    phone_number: phone_number,
    telephone_number: telephone_number,
    company: company,
    position: position,
    email_verification_status: email_verification_status,
    status: status,
    updated_by: updated_by,
  }
  console.log("payload", payload)
  if (Object.keys(all_files).length !== 0) {
    if (all_files.files && all_files.files.length !== 0) {
      payload.image_url = all_files.files[0].location
    }
  }

  try {
    const customer = await Customers.update(payload, {
      where: {
        id: id,
      },
    })

    if (!customer) {
      return res.status(400).json()
    }

    return res.status(200).json()
  } catch (error) {
    console.log(error)
  }
}

export const delete_customer = async (req, res) => {
  const { id } = req.params
  try {
    const customer = await Customers.destroy({
      where: {
        id: id,
      },
    })

    if (!customer) {
      return res.status(400).json()
    }

    return res.status(200).json()
  } catch (error) {
    console.log(error)
  }
}

export const uploadCustomer = async (req, res) => {
  const { data, created_by, client_id, customer_group } = req.body
  if (!data || !data.length || !client_id || typeof created_by !== "object") {
    return res.status(400).json({ message: "Incomplete Information" })
  }

  const customers = data.map((item) => {
    item.created_by = created_by
    return item
  })

  let createdCustomers = await Customers.bulkCreate(customers)
  createdCustomers = createdCustomers?.map((item) => {
    return item.dataValues.id
  })

  if (createdCustomers && createdCustomers.length && customer_group) {
    const customerGroup = []
    customer_group?.map((customerGroupId) => {
      createdCustomers.map((customer_id) => {
        customerGroup.push({
          customer_id,
          customer_group_id: customerGroupId,
          created_by: created_by,
        })
      })
    })
    await CustomerGroupMembers.bulkCreate(customerGroup)
  }

  if (createdCustomers && createdCustomers.length && client_id) {
    const clientCustomer = createdCustomers.map((customer_id) => {
      return {
        client_id,
        customer_id,
        user_id: null,
      }
    })
    await ClientCustomers.bulkCreate(clientCustomer)
  }

  return res.send({ data, created_by, client_id, customer_group })
}
