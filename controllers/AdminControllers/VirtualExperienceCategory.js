import VirtualExperienceCategories from "../../models/VirtualExperienceCategoryModel.js";
import {Op} from "sequelize"

export const get_all_ve_categories = async (req, res) => {
  
  let query = req.query;

  let categories;

  try {
    if(query && query.search !== "null") {
      categories = await VirtualExperienceCategories.findAndCountAll({
        offset: parseInt(query.offset),
        limit: parseInt(query.limit),
        nest: true,
        where: {
          name: {
            [Op.substring]: query.search
          }
        }
      })
    } else {
      categories = await VirtualExperienceCategories.findAndCountAll({
        offset: parseInt(query.offset),
        limit: parseInt(query.limit),
        nest: true,
      })
    }

    console.log(categories)
    console.log(query)

    if(!categories) {
      return res.status(400).json({message: "No Categories found"})
    }

    return res.status(200).json({categories: categories})

  } catch (error) {
    console.log("Error: ", error)
  }
}

export const get_all_ve_categories_option = async (req, res) => {
  
  try {
    let categories = await VirtualExperienceCategories.findAll({
      nest: true,
      where: {
        status: "Active"
      }
    })

    if(!categories) {
      return res.status(400).json({message: "No Categories found"})
    }

    return res.status(200).json({categories: categories})

  } catch (error) {
    console.log("Error: ", error)
  }
}

export const get_ve_category = async (req, res) => {
  
  let {id} = req.params;

  let category;

  try {
    category = await VirtualExperienceCategories.findOne({
      where: {
        id: id
      },
      plain: true,
      nest: true,
    })

    if(!category) {
      return res.status(400).json({message: "No Category found"})
    }

    return res.status(200).json(category)

  } catch (error) {
    
  }
}

export const check_if_ve_category_unique_name = async (req, res) => {

  const {name} = req.query;

  try {
    const check_name = await VirtualExperienceCategories.findOne({
      where: {
        name: name
      }
    })
    if(!check_name) {
      return res.status(204).json()
    }
    return res.status(200).json()
  } catch (error) {
    console.log(error)
    return res.status(400).json();
  }
}

export const create_ve_category = async (req, res) => {
  
  let {name, description, status, created_by} = req.body;

  if(!name || !created_by) {
    return res.status(400).json({message: "Incomplete Information"})
  } else {
    try {
      const category = await VirtualExperienceCategories.create({
        name: name,
        description: description,
        status: status,
        created_by: created_by
      })

      if(!category) {
        return res.status(400).json({message: "Category was not successfully created"})
      }
      return res.status(201).json({message: "Category was successfully created"})
    } catch (error) {
      
    }
  }

}

export const edit_ve_category = async (req, res) => {
  const {id} = req.params;
  
  const {name, description, status, updated_by} = req.body;

  try {
    const category = await VirtualExperienceCategories.update({
      name: name,
      description: description,
      status: status,
      updated_by: updated_by
    }, {
      where: {
        id: id
      }
    })

    if(!category) {
      return res.status(400).json({message: "Category was not successfully edited"})
    }

    return res.status(200).json({message: "Category was successfully edited"})
  } catch (error) {
    console.log(error)
  }

}

export const delete_ve_category = async (req, res) => {

  const {id} = req.params;

  try {
    await VirtualExperienceCategories.destroy({
      where: {
        id: id,
      }
    })
    return res.status(200).json({message: "Category was successfully deleted"})
  } catch (error) {
    console.log(error)
    return res.status(400).json();
  }
}