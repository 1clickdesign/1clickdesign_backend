import VirtualFlythrough from "../../models/VirtualFlythrough.js";
import ClientFlythrough from "../../models/ClientFlythroughModel.js";
import VirtualExperienceTypes from "../../models/VirtualExperienceTypeModel.js";
import Clients from "../../models/ClientModel.js";
export const create_client_flythrough = async (req, res) => {
  let client_flythrough;
  try {
    const { client, flythrough, created_by } = req.body;
    console.log(client, flythrough, created_by);
    client_flythrough = await ClientFlythrough.create({
      virtual_flythrough_id: flythrough,
      client_id: client,
      created_by: created_by,
    });

    return res.status(201).json({
      message: "Client flythrough was successfully created",
      client_flythrough: client_flythrough,
    });
  } catch (error) {
    console.error("Error creating client flythrough:", error);
    return res.status(500).json({
      message: "Error creating client flythrough",
      error: error.message, // Return the error message in the response
    });
  }
};

export const get_client_flythrough = async (req, res) => {
  try {
    const clientId = req.params.id;

    const clientFlythroughs = await ClientFlythrough.findAll({
      nest: true,
      where: {
        client_id: clientId,
      },
      include: [
        {
          model: VirtualFlythrough,
          include: [
            {
              model: VirtualExperienceTypes,
              attributes: ["id", "name"],
            },
          ],
        },
      ],
      order: [["id", "ASC"]],
    });

    return res.status(200).json({
      message: "Client Flythroughs successfully retrieved",
      clientFlythroughs: clientFlythroughs,
    });
  } catch (error) {
    console.error("Error getting Client Flythroughs:", error);
    return res.status(500).json({
      message: "Error getting Client Flythroughs",
      error: error.message,
    });
  }
};
export const get_client_flythroughs = async (req, res) => {
  try {
    const clientFlythroughs = await ClientFlythrough.findAll({
      nest: true,
      include: [
        {
          model: VirtualFlythrough,
          include: [
            {
              model: VirtualExperienceTypes,
              attributes: ["id", "name"],
            },
          ],
        },
        {
          model: Clients,
          attributes: ["id", "name"],
        },
      ],
      order: [["id", "ASC"]],
    });

    return res.status(200).json({
      message: "Client Flythroughs successfully retrieved",
      clientFlythroughs: clientFlythroughs,
    });
  } catch (error) {
    console.error("Error getting Client Flythroughs:", error);
    return res.status(500).json({
      message: "Error getting Client Flythroughs",
      error: error.message,
    });
  }
};
