import { Op } from "sequelize";
import Packages from "../../models/PackageModel.js";


export const get_all_packages = async (req, res) => {
  
  let query = req.query;

  console.log(req.query)

  let packages;

  try {
    if(query && query.search !== "null") {
      packages = await Packages.findAndCountAll({
        offset: parseInt(query.offset),
        limit: parseInt(query.limit),
        nest: true,
        where: {
          name: {
            [Op.substring]: query.search
          }
        }
      })
    } else {
      packages = await Packages.findAndCountAll({
        offset: parseInt(query.offset),
        limit: parseInt(query.limit),
        nest: true,
        order: [["created_at", "ASC"]],
      })
    }

    if(!packages) {
      return res.status(400).json({message: "No Packages found"})
    }

    return res.status(200).json({packages: packages})

  } catch (error) {
    console.log("Error: ", error)
  }
}

export const get_all_package_option = async (req, res) => {
  try {
    let packages = await Packages.findAll({
      nest: true,
      where: {
        status: "Active"
      },
      attributes: ["id", "name"],
    });

    if (!packages) {
      return res.status(400).json({ message: "No Packages found" });
    }

    return res.status(200).json({ packages: packages });
  } catch (error) {
    console.log("Error: ", error);
  }
};

export const check_if_package_unique_name = async (req, res) => {

  const {name} = req.query;

  try {
    const check_name = await Packages.findOne({
      where: {
        name: name
      }
    })
    if(!check_name) {
      return res.status(204).json()
    }
    return res.status(200).json()
  } catch (error) {
    console.log(error)
    return res.status(400).json();
  }
}

export const get_package = async (req, res) => {
  
  let {id} = req.params;

  let packages;

  try {
    packages = await Packages.findOne({
      where: {
        id: id
      },
      plain: true,
      nest: true,
    })

    if(!packages) {
      return res.status(400).json({message: "No Package found"})
    }

    return res.status(200).json(packages)

  } catch (error) {
    console.log(error)
    return res.status(400).json();
  }
}

export const create_package = async (req, res) => {
  
  const {name, description, status, created_by} = req.body;

  try {
    const packages = await Packages.create({
      name: name,
      description: description,
      status: status,
      created_by: created_by
    })

    if(!packages) {
      return res.status(202).json({message: "Package was not successfully created"})
    }

    return res.status(200).json({message: "Package was successfully created"})
  } catch (error) {
    console.log(error)
    return res.status(400).json();
  }

}

export const edit_package = async (req, res) => {
  const {id} = req.params;
  
  const {name, description, status, updated_by} = req.body;

  try {
    const packages = await Packages.update({
      name: name,
      description: description,
      status: status,
      updated_by: updated_by
    }, {
      where: {
        id: id
      }
    })

    if(!packages) {
      return res.status(400).json({message: "Package was not successfully edited"})
    }

    return res.status(200).json({message: "Package was successfully edited"})
  } catch (error) {
    console.log(error)
  }

}

export const delete_package = async (req, res) => {

  const {id} = req.params;

  try {
    await Packages.destroy({
      where: {
        id: id,
      }
    })
    return res.status(200).json({message: "Packages was successfully deleted"})
  } catch (error) {
    console.log(error)
    return res.status(400).json();
  }
}