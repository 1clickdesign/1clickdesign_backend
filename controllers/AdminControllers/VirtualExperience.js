import { Op } from "sequelize";
import BackgroundMusics from "../../models/BackgroundMusicModel.js";
import Clients from "../../models/ClientModel.js";
import Users from "../../models/UserModel.js";
import VirtualExperienceAccessTypes from "../../models/VirtualExperienceAccessTypeModel.js";
import VirtualExperienceBusinessTypes from "../../models/VirtualExperienceBusinessTypeModel.js";
import VirtualExperiences from "../../models/VirtualExperienceModel.js";
import VirtualExperienceTypes from "../../models/VirtualExperienceTypeModel.js";
import VirtualExperienceUsers from "../../models/VirtualExperienceUserModel.js";
import VirtualExperienceCategories from "../../models/VirtualExperienceCategoryModel.js";
import Products from "../../models/ProductModel.js";

export const get_all_virtual_experiences = async (req, res) => {
  let query = req.query;

  let virtual_experiences;

  try {
    if (query && query.search !== "null" && query.search) {
      if (req.query.id !== "null") {
        virtual_experiences = await VirtualExperiences.findAndCountAll({
          offset: parseInt(query.offset),
          limit: parseInt(query.limit),
          nest: true,
          where: {
            client_id: req.query.id,
            name: {
              [Op.substring]: query.search,
            },
          },
          include: [
            {
              model: VirtualExperienceAccessTypes,
              attributes: ["id", "name"],
            },
            {
              model: VirtualExperienceBusinessTypes,
              attributes: ["id", "name"],
            },
            {
              model: VirtualExperienceCategories,
              attributes: ["id", "name"],
            },
            {
              model: VirtualExperienceTypes,
              attributes: ["id", "name"],
            },
            {
              model: BackgroundMusics,
              attributes: ["id", "name", "music_url"],
            },
            {
              model: Clients,
              attributes: [
                "id",
                "name",
                "logo_url",
                "website_url",
                "description",
              ],
              include: [
                {
                  model: Users,
                  attributes: ["image_url", "first_name", "last_name", "email"],
                },
              ],
            },
          ],
        });
      } else {
        virtual_experiences = await VirtualExperiences.findAndCountAll({
          offset: parseInt(query.offset),
          limit: parseInt(query.limit),
          nest: true,
          where: {
            name: {
              [Op.substring]: query.search,
            },
          },
          include: [
            {
              model: VirtualExperienceAccessTypes,
              attributes: ["id", "name"],
            },
            {
              model: VirtualExperienceBusinessTypes,
              attributes: ["id", "name"],
            },
            {
              model: VirtualExperienceCategories,
              attributes: ["id", "name"],
            },
            {
              model: VirtualExperienceTypes,
              attributes: ["id", "name"],
            },
            {
              model: BackgroundMusics,
              attributes: ["id", "name", "music_url"],
            },
            {
              model: Clients,
              attributes: [
                "id",
                "name",
                "logo_url",
                "website_url",
                "description",
              ],
              include: [
                {
                  model: Users,
                  attributes: ["image_url", "first_name", "last_name", "email"],
                },
              ],
            },
          ],
        });
      }
    } else {
      if (req.query.id !== "null") {
        virtual_experiences = await VirtualExperiences.findAndCountAll({
          where: {
            client_id: req.query.id,
          },
          offset: parseInt(query.offset),
          limit: parseInt(query.limit),
          nest: true,
          include: [
            {
              model: VirtualExperienceAccessTypes,
              attributes: ["id", "name"],
            },
            {
              model: VirtualExperienceBusinessTypes,
              attributes: ["id", "name"],
            },
            {
              model: VirtualExperienceCategories,
              attributes: ["id", "name"],
            },
            {
              model: VirtualExperienceTypes,
              attributes: ["id", "name"],
            },
            {
              model: BackgroundMusics,
              attributes: ["id", "name", "music_url"],
            },
            {
              model: Clients,
              attributes: [
                "id",
                "name",
                "logo_url",
                "website_url",
                "description",
              ],
              include: [
                {
                  model: Users,
                  attributes: ["image_url", "first_name", "last_name", "email"],
                },
              ],
            },
          ],
        });
      } else {
        console.log("eto", req.query.sortBy);
        if (req.query.sortBy !== "null" && req.query.sortBy !== undefined) {
          virtual_experiences = await VirtualExperiences.findAndCountAll({
            offset: parseInt(query.offset),
            limit: parseInt(query.limit),
            nest: true,
            include: [
              {
                model: VirtualExperienceAccessTypes,
                attributes: ["id", "name"],
              },
              {
                model: VirtualExperienceBusinessTypes,
                attributes: ["id", "name"],
              },
              {
                model: VirtualExperienceCategories,
                attributes: ["id", "name"],
              },
              {
                model: VirtualExperienceTypes,
                attributes: ["id", "name"],
                where: { name: req.query.sortBy },
              },
              {
                model: BackgroundMusics,
                attributes: ["id", "name", "music_url"],
              },
              {
                model: Clients,
                attributes: [
                  "id",
                  "name",
                  "logo_url",
                  "website_url",
                  "description",
                ],
                include: [
                  {
                    model: Users,
                    attributes: [
                      "image_url",
                      "first_name",
                      "last_name",
                      "email",
                    ],
                  },
                ],
              },
            ],
          });
        } else {
          virtual_experiences = await VirtualExperiences.findAndCountAll({
            offset: parseInt(query.offset),
            limit: parseInt(query.limit),
            nest: true,
            include: [
              {
                model: VirtualExperienceAccessTypes,
                attributes: ["id", "name"],
              },
              {
                model: VirtualExperienceBusinessTypes,
                attributes: ["id", "name"],
              },
              {
                model: VirtualExperienceCategories,
                attributes: ["id", "name"],
              },
              {
                model: VirtualExperienceTypes,
                attributes: ["id", "name"],
              },
              {
                model: BackgroundMusics,
                attributes: ["id", "name", "music_url"],
              },
              {
                model: Clients,
                attributes: [
                  "id",
                  "name",
                  "logo_url",
                  "website_url",
                  "description",
                ],
                include: [
                  {
                    model: Users,
                    attributes: [
                      "image_url",
                      "first_name",
                      "last_name",
                      "email",
                    ],
                  },
                ],
              },
            ],
          });
        }
      }
    }

    if (!virtual_experiences) {
      return res.status(400).json({ message: "No Virtual Experiences found" });
    }

    return res.status(200).json({ virtual_experiences: virtual_experiences });
  } catch (error) {
    console.log("Error: ", error);
  }
};

export const get_virtual_experience = async (req, res) => {
  let { id } = req.params;

  let virtual_experience;

  try {
    virtual_experience = await VirtualExperiences.findOne({
      where: {
        id: id,
      },
      plain: true,
      nest: true,
      include: [
        {
          model: VirtualExperienceAccessTypes,
          attributes: ["id", "name"],
        },
        {
          model: Users,
          as: "sales",
          attributes: ["id", "first_name", "last_name"],
        },
        {
          model: VirtualExperienceBusinessTypes,
          attributes: ["id", "name"],
        },
        {
          model: VirtualExperienceCategories,
          attributes: ["id", "name"],
        },
        {
          model: VirtualExperienceTypes,
          attributes: ["id", "name"],
        },
        {
          model: BackgroundMusics,
          attributes: ["id", "name", "music_url"],
        },
        {
          model: Clients,
          attributes: ["id", "name", "logo_url", "website_url", "description"],
          include: [
            {
              model: Users,
              attributes: ["image_url", "first_name", "last_name", "email"],
            },
          ],
        },
        {
          model: Users,
          as: "virtual_experience_of_user",
          through: {
            attributes: ["user_id", "virtual_experience_id"],
          },
        },
      ],
    });

    if (!virtual_experience) {
      return res.status(400).json({ message: "No Virtual Experience found" });
    }

    return res.status(200).json(virtual_experience);
  } catch (error) {
    console.log(error);
  }
};

export const get_all_virtual_experience_option = async (req, res) => {
  let virtual_experiences;
  try {
    if (req.query && req.query.id) {
      if (req.query.business_type) {
        virtual_experiences = await VirtualExperiences.findAll({
          nest: true,
          where: {
            status: "Active",
            client_id: req.query.id,
          },
          include: [
            {
              model: Users,
              as: "virtual_experience_of_user",
              through: {
                attributes: ["user_id", "virtual_experience_id"],
              },
            },
            {
              model: VirtualExperienceBusinessTypes,
              where: {
                name: req.query.business_type,
              },
            },
            {
              model: VirtualExperienceCategories,
              attributes: ["id", "name"],
            },
            {
              model: VirtualExperienceTypes,
              attributes: ["id", "name"],
            },
          ],
        });
      } else {
        virtual_experiences = await VirtualExperiences.findAll({
          nest: true,
          where: {
            status: "Active",
            client_id: req.query.id,
          },
          include: [
            {
              model: Users,
              as: "virtual_experience_of_user",
              through: {
                attributes: ["user_id", "virtual_experience_id"],
              },
            },
            {
              model: VirtualExperienceCategories,
              attributes: ["id", "name"],
            },
            {
              model: VirtualExperienceTypes,
              attributes: ["id", "name"],
            },
          ],
        });
      }
    } else {
      if (req.query.business_type) {
        virtual_experiences = await VirtualExperiences.findAll({
          nest: true,
          where: {
            status: "Active",
          },
          include: [
            {
              model: Users,
              as: "virtual_experience_of_user",
              through: {
                attributes: ["user_id", "virtual_experience_id"],
              },
            },
            {
              model: VirtualExperienceBusinessTypes,
              where: {
                name: req.query.business_type,
              },
            },
            {
              model: VirtualExperienceCategories,
              attributes: ["id", "name"],
            },
            {
              model: VirtualExperienceTypes,
              attributes: ["id", "name"],
            },
          ],
        });
      } else {
        virtual_experiences = await VirtualExperiences.findAll({
          nest: true,
          where: {
            status: "Active",
          },
          include: [
            {
              model: Users,
              as: "virtual_experience_of_user",
              through: {
                attributes: ["user_id", "virtual_experience_id"],
              },
            },
            {
              model: VirtualExperienceCategories,
              attributes: ["id", "name"],
            },
            {
              model: VirtualExperienceTypes,
              attributes: ["id", "name"],
            },
          ],
        });
      }
    }

    if (!virtual_experiences) {
      return res.status(400).json({ message: "No Virtual Experiences found" });
    }

    return res.status(200).json({ virtual_experiences: virtual_experiences });
  } catch (error) {
    console.log("Error: ", error);
  }
};

export const create_virtual_experience = async (req, res) => {
  const body = JSON.parse(JSON.stringify(req.body));
  const files = JSON.parse(JSON.stringify(req.files));

  if (!files && !body) {
    return res.status(400).json();
  }

  console.log("Files: ", files);
  console.log("Files: ", JSON.parse(JSON.stringify(files)));

  console.log("Body ", body);
  // console.log(files);
  const data = JSON.parse(body.data);
  console.log(data);

  let photos = [];

  for (let index = 0; index < files.photos.length; index++) {
    const element = files.photos[index];
    photos.push(element.location);
  }

  const {
    name,
    showroom_url,
    description,
    show_hotspot,
    is_third_party_space,
    access_expiration_date,
    link_name,
    authorized_users,
    status,
    background_music_id,
    type_id,
    business_type_id,
    access_type_id,
    assigned_user_id,
    category_id,
    client_id,
    created_by,
    dealer_locator_url,
    sales_id,
  } = data;

  let virtual_experience;

  try {
    virtual_experience = await VirtualExperiences.create({
      name: name,
      showroom_url: showroom_url,
      status: status,
      category_id: category_id,
      photo_urls: JSON.stringify(photos),
      thumbnail_url: files.thumbnail[0].location,
      dealer_locator_url: dealer_locator_url,
      description: description,
      show_hotspot: Boolean(show_hotspot),
      is_third_party_space: Boolean(is_third_party_space),
      access_expiration_date: access_expiration_date,
      status: status,
      fly_through_video_urls: {
        interior: files?.internal_flythrough[0]?.location,
        exterior: files?.external_flythrough
          ? files?.external_flythrough[0].location
          : null,
      },
      background_music_id: background_music_id,
      type_id: type_id,
      business_type_id: business_type_id,
      access_type_id: access_type_id,
      assigned_user_id: assigned_user_id,
      client_id: client_id,
      link_name: link_name,
      created_by: created_by,
      sales_id: sales_id,
    });

    if (!virtual_experience) {
      return res
        .status(400)
        .json({ message: "Virtual Experience was not successfully created" });
    }

    let authorized = [];
    if (authorized_users && authorized_users.length > 0) {
      for (let index = 0; index < authorized_users.length; index++) {
        const element = authorized_users[index];
        authorized.push({
          user_id: element,
          virtual_experience_id: virtual_experience.dataValues.id,
          created_by: created_by,
        });
      }

      const users = await VirtualExperienceUsers.bulkCreate(authorized);

      if (!users) {
        return res
          .status(400)
          .json({ message: "Authorized Users was not successfully created" });
      }
    }

    return res
      .status(200)
      .json({ message: "Virtual Experience was successfully created" });
  } catch (error) {
    console.log(error);
  }
};

export const create_virtual_experience_v2 = async (req, res) => {
  const data = JSON.parse(JSON.stringify(req.body));

  if (!data) {
    return res.status(400).json();
  }

  const {
    name,
    showroom_url,
    description,
    show_hotspot,
    is_third_party_space,
    access_expiration_date,
    link_name,
    authorized_users,
    status,
    background_music_id,
    type_id,
    business_type_id,
    access_type_id,
    assigned_user_id,
    category_id,
    client_id,
    created_by,
    dealer_locator_url,
    sales_id,
    photos,
    thumbnail_url,
    internal_flythrough,
    external_flythrough
  } = data;

  let virtual_experience;

  try {
    virtual_experience = await VirtualExperiences.create({
      name: name,
      showroom_url: showroom_url,
      status: status,
      category_id: category_id,
      photo_urls: JSON.stringify(photos),
      thumbnail_url: thumbnail_url,
      dealer_locator_url: dealer_locator_url,
      description: description,
      show_hotspot: Boolean(show_hotspot),
      is_third_party_space: Boolean(is_third_party_space),
      access_expiration_date: access_expiration_date,
      status: status,
      fly_through_video_urls: {
        interior: internal_flythrough,
        exterior: external_flythrough,
      },
      background_music_id: background_music_id,
      type_id: type_id,
      business_type_id: business_type_id,
      access_type_id: access_type_id,
      assigned_user_id: assigned_user_id,
      client_id: client_id,
      link_name: link_name,
      created_by: created_by,
      sales_id: sales_id,
    });

    if (!virtual_experience) {
      return res
        .status(400)
        .json({ message: "Virtual Experience was not successfully created" });
    }

    let authorized = [];
    if (authorized_users && authorized_users.length > 0) {
      for (let index = 0; index < authorized_users.length; index++) {
        const element = authorized_users[index];
        authorized.push({
          user_id: element,
          virtual_experience_id: virtual_experience.dataValues.id,
          created_by: created_by,
        });
      }

      const users = await VirtualExperienceUsers.bulkCreate(authorized);

      if (!users) {
        return res
          .status(400)
          .json({ message: "Authorized Users was not successfully created" });
      }
    }

    return res
      .status(200)
      .json({ message: "Virtual Experience was successfully created" });
  } catch (error) {
    console.log(error);
  }
};

export const edit_virtual_experience_v2 = async (req, res) => {
  if (!req.body) {
    return res.status(400).json()
  }

  const { id } = req.params;

  const data = JSON.parse(JSON.stringify(req.body));

  const {
    name,
    showroom_url,
    description,
    show_hotspot,
    is_third_party_space,
    access_expiration_date,
    link_name,
    authorized_users,
    status,
    background_music_id,
    type_id,
    business_type_id,
    access_type_id,
    category_id,
    client_id,
    updated_by,
    dealer_locator_url,
    sales_id,
    thumbnail_url,
    photo_urls,
    internal_flythrough,
    external_flythrough
  } = data;

  let payload = {
    name: name,
    showroom_url: showroom_url,
    status: status,
    category_id: category_id,
    dealer_locator_url: dealer_locator_url,
    description: description,
    show_hotspot: Boolean(show_hotspot),
    is_third_party_space: Boolean(is_third_party_space),
    access_expiration_date: access_expiration_date,
    status: status,
    background_music_id: background_music_id,
    type_id: type_id,
    business_type_id: business_type_id,
    access_type_id: access_type_id,
    client_id: client_id,
    link_name: link_name,
    updated_by: updated_by,
    sales_id: sales_id,
    fly_through_video_urls: {
      interior: null,
      exterior: null
    }
  };

  if (!!photo_urls && !!photo_urls.length){
    payload.photo_urls = JSON.stringify(photo_urls)
  }

  if (!!thumbnail_url){
    payload.thumbnail_url = thumbnail_url
  }

  if (!!internal_flythrough){
    payload.fly_through_video_urls.interior = internal_flythrough
  }

  if (!!external_flythrough){
    payload.fly_through_video_urls.exterior = external_flythrough
  }

  console.log("new", payload.client_id);
  console.log("current", data.currentClientID);
  
  try {
    if (data.currentClientID !== payload.client_id) {
      console.log("Client ID changed. Performing some action...");
      try {
        await Products.update(
          { client_id: payload.client_id },
          {
            where: {
              client_id: data.currentClientID,
              virtual_experience_id: id,
            },
          }
        );
      } catch (error) {
        console.log(error);
      }
    }

    const virtual_experience = await VirtualExperiences.update(payload, {
      where: {
        id: id,
      },
    });
    let authorized = [];
    if (authorized_users && authorized_users.length > 0) {
      await VirtualExperienceUsers.destroy({
        where: {
          virtual_experience_id: id,
        },
      });
      for (let index = 0; index < authorized_users.length; index++) {
        const element = authorized_users[index];
        authorized.push({
          user_id: element,
          virtual_experience_id: id,
          updated_by: updated_by,
        });
      }
      await VirtualExperienceUsers.bulkCreate(authorized);
    }

    if (!virtual_experience) {
      return res.status(400).json();
    }

    return res.status(200).json();
  } catch (error) {
    console.log(error);
  }
}

export const edit_virtual_experience = async (req, res) => {
  const { id } = req.params;
  const body = JSON.parse(JSON.stringify(req.body));
  const files = req.files;
  const all_files = JSON.parse(JSON.stringify(files));

  const data = JSON.parse(body.data);

  console.log("Here", all_files);

  const {
    name,
    showroom_url,
    description,
    show_hotspot,
    is_third_party_space,
    access_expiration_date,
    link_name,
    authorized_users,
    status,
    background_music_id,
    type_id,
    business_type_id,
    access_type_id,
    category_id,
    client_id,
    updated_by,
    dealer_locator_url,
    sales_id,
  } = data;

  let payload = {
    name: name,
    showroom_url: showroom_url,
    status: status,
    category_id: category_id,
    dealer_locator_url: dealer_locator_url,
    description: description,
    show_hotspot: Boolean(show_hotspot),
    is_third_party_space: Boolean(is_third_party_space),
    access_expiration_date: access_expiration_date,
    status: status,
    background_music_id: background_music_id,
    type_id: type_id,
    business_type_id: business_type_id,
    access_type_id: access_type_id,
    client_id: client_id,
    link_name: link_name,
    updated_by: updated_by,
    sales_id: sales_id,
    fly_through_video_urls: {
      interior: files?.internal_flythrough
        ? files?.internal_flythrough[0].location
        : null,
      exterior: files?.external_flythrough
        ? files?.external_flythrough[0].location
        : null,
    },
  };

  if (Object.keys(all_files).length !== 0) {
    if (all_files.thumbnail && all_files.thumbnail.length !== 0) {
      payload.thumbnail_url = all_files.thumbnail[0].location;
    }
    if (all_files.photos && all_files.photos.length !== 0) {
      let photos = [];

      for (let index = 0; index < all_files.photos.length; index++) {
        const element = all_files.photos[index];
        photos.push(element.location);
      }

      payload.photo_urls = JSON.stringify(photos);
    }
    if (
      all_files.internal_flythrough &&
      all_files.internal_flythrough.length !== 0
    ) {
      console.log("Updating interior");
      payload.fly_through_video_urls.interior =
        all_files.internal_flythrough[0].location;
    }
    if (
      all_files.external_flythrough &&
      all_files.external_flythrough.length !== 0
    ) {
      console.log("Updating exteopr");
      payload.fly_through_video_urls.exterior =
        all_files.external_flythrough[0].location;
    }
    // if (
    //   all_files.internal_flythrough &&
    //   all_files.internal_flythrough.length !== 0
    // ) {
    //   if (
    //     all_files.external_flythrough &&
    //     all_files.external_flythrough.length !== 0
    //   ) {
    //     payload.fly_through_video_urls.interior =
    //       all_files.internal_flythrough[0].location;
    //     payload.fly_through_video_urls.exterior =
    //       all_files.external_flythrough[0].location;
    //   } else {
    //     payload.fly_through_video_urls.interior =
    //       all_files.internal_flythrough[0].location;
    //     payload.fly_through_video_urls.exterior = null;
    //   }
    // }
    // else {
    //   payload.fly_through_video_urls.interior = null;
    //   payload.fly_through_video_urls.exterior = null;
    // }
  }

  console.log("new", payload.client_id);
  console.log("current", data.currentClientID);
  try {
    if (data.currentClientID !== payload.client_id) {
      console.log("Client ID changed. Performing some action...");
      try {
        await Products.update(
          { client_id: payload.client_id },
          {
            where: {
              client_id: data.currentClientID,
              virtual_experience_id: id,
            },
          }
        );
      } catch (error) {
        console.log(error);
      }
    }

    const virtual_experience = await VirtualExperiences.update(payload, {
      where: {
        id: id,
      },
    });
    let authorized = [];
    if (authorized_users && authorized_users.length > 0) {
      await VirtualExperienceUsers.destroy({
        where: {
          virtual_experience_id: id,
        },
      });
      for (let index = 0; index < authorized_users.length; index++) {
        const element = authorized_users[index];
        authorized.push({
          user_id: element,
          virtual_experience_id: id,
          updated_by: updated_by,
        });
      }
      await VirtualExperienceUsers.bulkCreate(authorized);
    }

    if (!virtual_experience) {
      return res.status(400).json();
    }

    return res.status(200).json();
  } catch (error) {
    console.log(error);
  }
};

export const delete_virtual_experience = async (req, res) => {
  const { id } = req.params;
  try {
    const virtual_experience = await VirtualExperiences.destroy({
      where: {
        id: id,
      },
    });

    if (!virtual_experience) {
      return res.status(400).json();
    }

    return res.status(200).json();
  } catch (error) {
    console.log(error);
  }
};
