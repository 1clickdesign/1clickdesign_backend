import { Op } from "sequelize";
import Permissions from "../../models/PermissionModel.js";

export const get_all_permissions = async (req, res) => {
  let query = req.query;

  console.log(typeof query.for_client);

  let permissions;

  try {
    if (query && query.search !== "null" && query.search) {
      if (query.for_client === "true") {
        permissions = await Permissions.findAndCountAll({
          offset: parseInt(query.offset),
          limit: parseInt(query.limit),
          nest: true,
          where: {
            [Op.and]: {
              name: {
                [Op.substring]: query.search,
              },
              category: "Client",
            },
          },
          order: [["name", "ASC"]],
        });
      } else {
        console.log("This is called", query);
        permissions = await Permissions.findAndCountAll({
          offset: parseInt(query.offset),
          limit: parseInt(query.limit),
          nest: true,
          where: {
            name: {
              [Op.substring]: query.search,
            },
          },
          order: [["name", "ASC"]],
        });
      }
    } else {
      if (query.for_client === "true") {
        permissions = await Permissions.findAndCountAll({
          offset: parseInt(query.offset),
          limit: parseInt(query.limit),
          nest: true,
          where: {
            category: "Client",
          },
          order: [["name", "ASC"]],
        });
      } else {
        permissions = await Permissions.findAndCountAll({
          offset: parseInt(query.offset),
          limit: parseInt(query.limit),
          nest: true,
          order: [["name", "ASC"]],
        });
      }
    }

    if (!permissions) {
      return res.status(204).json({ message: "No Permissions found" });
    }

    return res.status(200).json({ permissions: permissions });
  } catch (error) {
    console.log(error);
    return res.status(400).json();
  }
};

export const get_all_permissions_option = async (req, res) => {
  let permissions;
  try {
    if (req.query && req.query.for_client === "true") {
      permissions = await Permissions.findAll({
        nest: true,
        where: {
          status: "Active",
          category: "Client",
        },
        order: [["name", "ASC"]],
      });
    } else {
      permissions = await Permissions.findAll({
        raw: true,
        nest: true,
        where: {
          status: "Active",
        },
        order: [["name", "ASC"]],
      });
    }

    if (!permissions) {
      return res.status(400).json({ message: "No Permissions found" });
    }

    return res.status(200).json({ permissions: permissions });
  } catch (error) {
    console.log("Error: ", error);
  }
};

export const get_permission = async (req, res) => {
  let { id } = req.params;

  let permission;

  try {
    permission = await Permissions.findOne({
      where: {
        id: id,
      },
      plain: true,
      nest: true,
    });

    if (!permission) {
      return res.status(400).json({ message: "No Permission found" });
    }

    return res.status(200).json(permission);
  } catch (error) {
    console.log(error);
  }
};

export const create_permission = async (req, res) => {
  let { name, description, category, status, created_by } = req.body;

  if (!name || !created_by) {
    return res.status(400).json({ message: "Incomplete Information" });
  } else {
    try {
      const permission = await Permissions.create({
        name: name,
        description: description,
        category: category,
        status: status,
        created_by: created_by,
      });

      if (!permission) {
        return res
          .status(204)
          .json({ message: "Permission was not successfully created" });
      }
      return res
        .status(201)
        .json({ message: "Permission was successfully created" });
    } catch (error) {
      console.log(error);
      return res.status(400).json();
    }
  }
};

export const check_if_permission_unique_name = async (req, res) => {
  const { name } = req.query;

  try {
    const check_name = await Permissions.findOne({
      where: {
        name: name,
      },
    });
    if (!check_name) {
      return res.status(204).json();
    }
    return res.status(200).json();
  } catch (error) {
    console.log(error);
    return res.status(400).json();
  }
};

export const edit_permission = async (req, res) => {
  const { id } = req.params;

  const { name, description, category, status, updated_by } = req.body;

  try {
    const business_type = await Permissions.update(
      {
        name: name,
        description: description,
        status: status,
        category: category,
        updated_by: updated_by,
      },
      {
        where: {
          id: id,
        },
      }
    );

    if (!business_type) {
      return res
        .status(400)
        .json({ message: "Permission was not successfully edited" });
    }

    return res
      .status(200)
      .json({ message: "Permission was successfully edited" });
  } catch (error) {
    console.log(error);
  }
};

export const delete_permission = async (req, res) => {
  const { id } = req.params;

  try {
    await Permissions.destroy({
      where: {
        id: id,
      },
    });
    return res
      .status(200)
      .json({ message: "Permission was successfully deleted" });
  } catch (error) {
    console.log(error);
    return res.status(400).json();
  }
};
