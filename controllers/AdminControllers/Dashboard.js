import { Op } from "sequelize"
import Clients from "../../models/ClientModel.js"
import Users from "../../models/UserModel.js"
import Customers from "../../models/CustomerModel.js"
import VirtualExperiences from "../../models/VirtualExperienceModel.js"
import Products from "../../models/ProductModel.js"
import VirtualExperienceCategories from "../../models/VirtualExperienceCategoryModel.js"
import ClientCustomers from "../../models/ClientCustomerModel.js"
import RealEstates from "../../models/RealEstateModel.js"
import VideoConferences from "../../models/VideoConferenceModel.js"
import SentEmails from "../../models/SentEmailModel.js"
import UserBackupUsers from "../../models/UserBackupUserModel.js"
import Invitations from "../../models/InvitationModel.js"
import VirtualFlythrough from "../../models/VirtualFlythrough.js"
import InvitationFlythrough from "../../models/InvitationFlythroughModel.js"
import ClientFlythrough from "../../models/ClientFlythroughModel.js"

export const get_dashboard_information = async (req, res) => {
  const { id } = req.query

  const prevMonth = new Date()
  prevMonth.setMonth(prevMonth.getMonth() - 1)

  console.log(prevMonth)

  function getLastWeeksDate() {
    const now = new Date()

    return new Date(now.getFullYear(), now.getMonth(), now.getDate() - 7)
  }

  function getFirstLastWeeksDate() {
    const now = new Date()

    return new Date(now.getFullYear(), now.getMonth(), now.getDate() - 14)
  }
  if (req.query && id !== "null") {
    try {
      const prev_clients = await Clients.count({
        where: {
          // status: "Active",
          created_at: {
            [Op.and]: {
              [Op.lt]: getLastWeeksDate(),
            },
          },
        },
      })

      const new_clients = await Clients.count({
        where: {
          // status: "Active",
          created_at: {
            [Op.and]: {
              [Op.lt]: new Date(),
            },
          },
        },
      })

      let client_percentage = 0

      if (prev_clients === 0 && new_clients === 0) {
        client_percentage = 0
      } else if (prev_clients === 0) {
        client_percentage = 100
      } else {
        const difference = new_clients - prev_clients
        if (difference === 0) {
          client_percentage = 0
        } else {
          client_percentage = (difference / prev_clients) * 100
        }
      }

      const client_info = {
        count: new_clients,
        percentage: client_percentage,
      }

      //users
      const prev_users = await Users.count({
        where: {
          client_id: id,
          created_at: {
            [Op.and]: {
              [Op.lt]: getLastWeeksDate(),
            },
          },
        },
      })

      const new_users = await Users.count({
        where: {
          client_id: id,
          created_at: {
            [Op.and]: {
              [Op.lt]: new Date(),
            },
          },
        },
      })

      let user_percentage = 0

      if (prev_users === 0 && new_users === 0) {
        user_percentage = 0
      } else if (prev_users === 0) {
        user_percentage = 100
      } else {
        const difference = new_users - prev_users
        if (difference === 0) {
          user_percentage = 0
        } else {
          user_percentage = (difference / prev_users) * 100
        }
      }

      const user_info = {
        count: new_users,
        percentage: user_percentage,
      }

      //customers
      const prev_customers = await Customers.count({
        where: {
          // status: "Active",
          created_at: {
            [Op.and]: {
              [Op.lt]: getLastWeeksDate(),
            },
          },
        },
        include: [
          {
            model: ClientCustomers,
            where: {
              client_id: id,
            },
          },
        ],
      })

      const new_customers = await Customers.count({
        where: {
          // status: "Active",
          created_at: {
            [Op.and]: {
              [Op.lt]: new Date(),
            },
          },
        },
        include: [
          {
            model: ClientCustomers,
            where: {
              client_id: id,
            },
          },
        ],
      })
      const customers = await Customers.count({
        subQuery: false,
        include: [
          {
            model: ClientCustomers,
            as: "client_customers",
            where: {
              client_id: id,
              customer_id: {
                [Op.ne]: null,
              },
            },
          },
        ],
      })

      let customer_percentage = 0

      if (prev_customers === 0 && new_customers === 0) {
        customer_percentage = 0
      } else if (prev_customers === 0) {
        customer_percentage = 100
      } else {
        const difference = new_customers - prev_customers
        if (difference === 0) {
          customer_percentage = 0
        } else {
          customer_percentage = (difference / prev_customers) * 100
        }
      }

      const customer_info = {
        count: new_customers,
        percentage: customer_percentage,
        customers: customers,
      }

      //virtual experience
      const prev_virtual_experiences = await VirtualExperiences.count({
        where: {
          // status: "Active",
          client_id: id,
          created_at: {
            [Op.and]: {
              [Op.lt]: getLastWeeksDate(),
            },
          },
        },
      })

      const new_virtual_experiences = await VirtualExperiences.count({
        where: {
          client_id: id,
          created_at: {
            [Op.and]: {
              [Op.lt]: new Date(),
            },
          },
        },
      })

      let virtual_experience_percentage = 0

      if (prev_virtual_experiences === 0 && new_virtual_experiences === 0) {
        virtual_experience_percentage = 0
      } else if (prev_virtual_experiences === 0) {
        virtual_experience_percentage = 100
      } else {
        const difference = new_virtual_experiences - prev_virtual_experiences
        if (difference === 0) {
          virtual_experience_percentage = 0
        } else {
          virtual_experience_percentage =
            (difference / prev_virtual_experiences) * 100
        }
      }

      const virtual_experience_info = {
        count: new_virtual_experiences,
        percentage: virtual_experience_percentage,
      }

      //real_estate
      const prev_real_estates = await RealEstates.count({
        where: {
          // status: "Active",
          client_id: id,
          created_at: {
            [Op.and]: {
              [Op.lt]: getLastWeeksDate(),
            },
          },
        },
      })

      const new_real_estates = await RealEstates.count({
        where: {
          client_id: id,
          created_at: {
            [Op.and]: {
              [Op.lt]: new Date(),
            },
          },
        },
      })

      let real_estate_percentage = 0

      if (prev_real_estates === 0 && new_real_estates === 0) {
        real_estate_percentage = 0
      } else if (prev_virtual_experiences === 0) {
        real_estate_percentage = 100
      } else {
        const difference = new_real_estates - prev_real_estates
        if (difference === 0) {
          real_estate_percentage = 0
        } else {
          real_estate_percentage = (difference / prev_real_estates) * 100
        }
      }

      const real_estate_info = {
        count: new_real_estates,
        percentage: real_estate_percentage,
      }

      //email
      const prev_emails = await SentEmails.count({
        where: {
          // status: "Active",
          created_at: {
            [Op.and]: {
              [Op.lt]: getLastWeeksDate(),
            },
          },
        },
        include: [
          {
            model: Users,
            where: {
              client_id: id,
            },
          },
        ],
      })

      const new_emails = await SentEmails.count({
        where: {
          // status: "Active",
          created_at: {
            [Op.and]: {
              [Op.lt]: new Date(),
            },
          },
        },
        include: [
          {
            model: Users,
            where: {
              client_id: id,
            },
          },
        ],
      })

      let email_percentage = 0

      if (prev_emails === 0 && new_emails === 0) {
        email_percentage = 0
      } else if (prev_emails === 0) {
        email_percentage = 100
      } else {
        const difference = new_emails - prev_emails
        if (difference === 0) {
          email_percentage = 0
        } else {
          email_percentage = (difference / prev_emails) * 100
        }
      }

      const email_info = {
        count: new_emails,
        percentage: email_percentage,
      }

      //product
      const prev_products = await Products.count({
        where: {
          // status: "Active",
          client_id: id,
          created_at: {
            [Op.and]: {
              [Op.lt]: getLastWeeksDate(),
            },
          },
        },
      })

      const new_products = await Products.count({
        where: {
          client_id: id,
          // status: "Active",
          created_at: {
            [Op.and]: {
              [Op.lt]: new Date(),
            },
          },
        },
      })

      let product_percentage = 0

      if (prev_products === 0 && new_products === 0) {
        product_percentage = 0
      } else if (prev_products === 0) {
        product_percentage = 100
      } else {
        const difference = new_products - prev_products
        if (difference === 0) {
          product_percentage = 0
        } else {
          product_percentage = (difference / prev_products) * 100
        }
      }

      const product_info = {
        count: new_products,
        percentage: product_percentage,
      }

      const prev_flythroughs = await ClientFlythrough.count({
        where: {
          client_id: id,
          created_at: {
            [Op.and]: {
              [Op.lt]: getLastWeeksDate(),
            },
          },
        },
      })

      const new_flythroughs = await ClientFlythrough.count({
        where: {
          client_id: id,
          created_at: {
            [Op.and]: {
              [Op.lt]: new Date(),
            },
          },
        },
      })

      let flythroughs_percentage = 0

      if (prev_flythroughs === 0 && new_flythroughs === 0) {
        flythroughs_percentage = 0
      } else if (prev_flythroughs === 0) {
        flythroughs_percentage = 100
      } else {
        const difference = new_flythroughs - prev_flythroughs
        if (difference === 0) {
          flythroughs_percentage = 0
        } else {
          flythroughs_percentage = (difference / prev_flythroughs) * 100
        }
      }

      const flythrough_info = {
        count: new_flythroughs,
        percentage: flythroughs_percentage,
      }

      const prev_invitations = await SentEmails.count({
        include: [
          {
            model: Users,
            where: {
              client_id: id,
            },
          },
        ],
        where: {
          type: "Invitation",
          created_at: {
            [Op.and]: {
              [Op.lt]: getLastWeeksDate(),
            },
          },
        },
      })

      const new_invitations = await SentEmails.count({
        include: [
          {
            model: Users,
            where: {
              client_id: id,
            },
          },
        ],
        where: {
          type: "Invitation",
          created_at: {
            [Op.and]: {
              [Op.lt]: new Date(),
            },
          },
        },
        order: [["created_at", "DESC"]],
      })

      let invitations_percentage = 0

      if (prev_invitations === 0 && new_invitations === 0) {
        invitations_percentage = 0
      } else if (prev_invitations === 0) {
        invitations_percentage = 100
      } else {
        const difference = new_invitations - prev_invitations
        if (difference === 0) {
          invitations_percentage = 0
        } else {
          invitations_percentage = (difference / prev_invitations) * 100
        }
      }

      const invitation_info = {
        count: new_invitations,
        percentage: invitations_percentage,
      }

      // const prev_video_conferences = await VideoConferences.count({
      //   where: {
      //     client_id: id,
      //     created_at: {
      //       [Op.and]: {
      //         [Op.lt]: getLastWeeksDate(),
      //       },
      //     },
      //   },
      // })

      // const new_video_conferences = await VideoConferences.count({
      //   where: {
      //     client_id: id,
      //     created_at: {
      //       [Op.and]: {
      //         [Op.lt]: new Date(),
      //       },
      //     },
      //   },
      // })

      // let video_conferences_percentage = 0

      // if (prev_video_conferences === 0 && new_video_conferences === 0) {
      //   video_conferences_percentage = 0;
      // } else if (prev_video_conferences === 0) {
      //   video_conferences_percentage = 100;
      // } else {
      //   const difference = new_video_conferences - prev_video_conferences;
      //   if (difference === 0) {
      //     video_conferences_percentage = 0;
      //   } else {
      //     video_conferences_percentage = (difference / prev_video_conferences) * 100;
      //   }
      // }

      // const video_conference_info = {
      //   count: new_video_conferences,
      //   percentage: video_conferences_percentage
      // }

      // const prev_flythrough_invitations = await SentEmails.count({
      //   where: {
      //     client_id: id,
      //     type: "Flyhrough Invitation",
      //     created_at: {
      //       [Op.and]: {
      //         [Op.lt]: getLastWeeksDate(),
      //       },
      //     },
      //   },
      // })

      // const new_flythrough_invitations = await SentEmails.count({
      //   where: {
      //     client_id: id,
      //     type: "Flyhrough Invitation",
      //     created_at: {
      //       [Op.and]: {
      //         [Op.lt]: new Date(),
      //       },
      //     },
      //   },
      // })

      // let flythrough_invitations_percentage = 0

      // if (prev_flythrough_invitations === 0 && new_flythrough_invitations === 0) {
      //   flythrough_invitations_percentage = 0;
      // } else if (prev_flythrough_invitations === 0) {
      //   flythrough_invitations_percentage = 100;
      // } else {
      //   const difference = new_flythrough_invitations - prev_flythrough_invitations;
      //   if (difference === 0) {
      //     flythrough_invitations_percentage = 0;
      //   } else {
      //     flythrough_invitations_percentage = (difference / prev_flythrough_invitations) * 100;
      //   }
      // }

      // const flythrough_invitation_info = {
      //   count: new_flythrough_invitations,
      //   percentage: flythrough_invitations_percentage
      // }

      // const prev_flythrough_invitations = await SentEmails.count({
      //   where: {
      //     type: "Flyhrough Invitation",
      //     created_at: {
      //       [Op.and]: {
      //         [Op.lt]: getLastWeeksDate(),
      //       },
      //     },
      //   },
      // })
      const prev_flythrough_invitations = await SentEmails.count({
        include: [
          {
            model: Users,
            where: {
              client_id: id,
            },
          },
        ],
        where: {
          type: "Flythrough Invitation",
          created_at: {
            [Op.and]: {
              [Op.lt]: getLastWeeksDate(),
            },
          },
        },
        order: [["created_at", "DESC"]],
      })
      const new_flythrough_invitations = await SentEmails.count({
        include: [
          {
            model: Users,
            where: {
              client_id: id,
            },
          },
        ],
        where: {
          type: "Flythrough Invitation",
          created_at: {
            [Op.and]: {
              [Op.lt]: new Date(),
            },
          },
        },
        order: [["created_at", "DESC"]],
      })

      // const new_flythrough_invitations = await SentEmails.count({
      //   where: {
      //     type: "Flyhrough Invitation",
      //     created_at: {
      //       [Op.and]: {
      //         [Op.lt]: new Date(),
      //       },
      //     },
      //   },
      // })

      let flythrough_invitations_percentage = 0

      if (
        prev_flythrough_invitations === 0 &&
        new_flythrough_invitations === 0
      ) {
        flythrough_invitations_percentage = 0
      } else if (prev_flythrough_invitations === 0) {
        flythrough_invitations_percentage = 100
      } else {
        const difference =
          new_flythrough_invitations - prev_flythrough_invitations
        if (difference === 0) {
          flythrough_invitations_percentage = 0
        } else {
          flythrough_invitations_percentage =
            (difference / prev_flythrough_invitations) * 100
        }
      }

      const flythrough_invitation_info = {
        count: new_flythrough_invitations,
        percentage: flythrough_invitations_percentage,
      }

      const recent_invitations = await Invitations.findAll({
        where: {
          client_id: id,
        },
        limit: 3,
        include: [
          {
            model: Users,
            as: "invitation_user",
          },
          {
            model: VirtualExperiences,
            as: "virtual_experience",
          },
          {
            model: Customers,
            as: "customer",
          },
        ],
        order: [["created_at", "DESC"]],
      })

      const recent_clients = await Clients.findAll({
        limit: 3,
        include: [
          {
            model: Users,
            attributes: ["id", "first_name"],
          },
        ],
        order: [["created_at", "DESC"]],
      })

      const recent_users = await Users.findAll({
        where: {
          client_id: id,
        },
        limit: 3,
        attributes: ["id", "first_name", "last_name", "image_url", "status"],
        include: [
          {
            model: Clients,
            attributes: ["id", "name"],
          },
        ],
        order: [["created_at", "DESC"]],
      })

      const recent_virtual_experiences = await VirtualExperiences.findAll({
        where: {
          client_id: id,
        },
        limit: 3,
        attributes: ["id", "name", "thumbnail_url", "status"],
        include: [
          {
            model: Clients,
            attributes: ["id", "name"],
          },
        ],
        order: [["created_at", "DESC"]],
      })

      const recent_real_estates = await RealEstates.findAll({
        where: {
          client_id: id,
        },
        limit: 3,
        attributes: ["id", "type"],
        include: [
          {
            model: Clients,
            attributes: ["id", "name"],
          },
          {
            model: VirtualExperiences,
            attributes: ["id", "name", "thumbnail_url", "status"],
          },
        ],
        order: [["created_at", "DESC"]],
      })

      const recent_video_conferences = await VideoConferences.findAll({
        limit: 3,
        raw: true,
        nest: true,
        attributes: ["id", "start_time", "end_time", "status", "host_id"],
        include: [
          {
            model: Users,
            attributes: ["id", "first_name", "last_name", "client_id"],
            as: "host",
            where: {
              client_id: id,
            },
          },
        ],
        order: [["created_at", "DESC"]],
      })

      const recent_emails = await SentEmails.findAll({
        limit: 3,
        attributes: ["id", "to", "status"],
        include: [
          {
            model: Users,
            attributes: ["first_name", "last_name"],
            where: {
              client_id: id,
            },
            include: [
              {
                model: Clients,
              },
            ],
          },
        ],
        order: [["created_at", "DESC"]],
      })

      const recent_customers = await Customers.findAll({
        limit: 3,
        attributes: ["id", "full_name", "email", "image_url", "status"],
        include: [
          {
            model: ClientCustomers,
            where: {
              client_id: id,
            },
            include: [{ model: Clients }],
            order: [["created_at", "DESC"]],
          },
        ],
        // order: [["created_at", "DESC"]],
      })

      const recent_products = await Products.findAll({
        where: {
          client_id: id,
        },
        limit: 3,
        attributes: [
          "id",
          "name",
          "sku",
          "price",
          "status",
          "in_stock",
          "image_urls",
        ],
        include: [
          {
            model: Clients,
            attributes: ["id", "name"],
          },
          {
            model: VirtualExperiences,
            attributes: ["id", "name"],
          },
        ],
        order: [["created_at", "DESC"]],
      })

      return res.status(200).json({
        client: client_info,
        user: user_info,
        customer: customer_info,
        virtual_experience: virtual_experience_info,
        real_estate: real_estate_info,
        email: email_info,
        product: product_info,
        invitations: invitation_info,
        // video_conferences: video_conference_info,
        flythroughs: flythrough_info,
        flythrough_invitations: flythrough_invitation_info,
        recent_clients: recent_clients,
        recent_users: recent_users,
        recent_virtual_experiences: recent_virtual_experiences,
        recent_real_estates: recent_real_estates,
        recent_video_conferences: recent_video_conferences,
        recent_emails: recent_emails,
        recent_customers: recent_customers,
        recent_products: recent_products,
        recent_invitations: recent_invitations,
      })
    } catch (error) {
      console.log(error)
    }
  } else {
    try {
      const prev_clients = await Clients.count({
        where: {
          // status: "Active",
          created_at: {
            [Op.and]: {
              [Op.lt]: getLastWeeksDate(),
            },
          },
        },
      })

      const new_clients = await Clients.count({
        where: {
          // status: "Active",
          created_at: {
            [Op.and]: {
              [Op.lt]: new Date(),
            },
          },
        },
      })

      let client_percentage = 0

      if (prev_clients === 0 && new_clients === 0) {
        client_percentage = 0
      } else if (prev_clients === 0) {
        client_percentage = 100
      } else {
        const difference = new_clients - prev_clients
        if (difference === 0) {
          client_percentage = 0
        } else {
          client_percentage = (difference / prev_clients) * 100
        }
      }

      const client_info = {
        count: new_clients,
        percentage: client_percentage,
      }

      //users
      const prev_users = await Users.count({
        where: {
          // status: "Active",
          created_at: {
            [Op.and]: {
              [Op.lt]: getLastWeeksDate(),
            },
          },
        },
      })

      const new_users = await Users.count({
        where: {
          // status: "Active",
          created_at: {
            [Op.and]: {
              [Op.lt]: new Date(),
            },
          },
        },
      })

      let user_percentage = 0

      if (prev_users === 0 && new_users === 0) {
        user_percentage = 0
      } else if (prev_users === 0) {
        user_percentage = 100
      } else {
        const difference = new_users - prev_users
        if (difference === 0) {
          user_percentage = 0
        } else {
          user_percentage = (difference / prev_users) * 100
        }
      }

      const user_info = {
        count: new_users,
        percentage: user_percentage,
      }

      //customers
      const prev_customers = await Customers.count({
        where: {
          // status: "Active",
          created_at: {
            [Op.and]: {
              [Op.lt]: getLastWeeksDate(),
            },
          },
        },
      })

      const new_customers = await Customers.count({
        where: {
          // status: "Active",
          created_at: {
            [Op.and]: {
              [Op.lt]: new Date(),
            },
          },
        },
      })

      let customer_percentage = 0

      if (prev_customers === 0 && new_customers === 0) {
        customer_percentage = 0
      } else if (prev_customers === 0) {
        customer_percentage = 100
      } else {
        const difference = new_customers - prev_customers
        if (difference === 0) {
          customer_percentage = 0
        } else {
          customer_percentage = (difference / prev_customers) * 100
        }
      }

      const customer_info = {
        count: new_customers,
        percentage: customer_percentage,
      }

      //virtual experience
      const prev_virtual_experiences = await VirtualExperiences.count({
        where: {
          // status: "Active",
          created_at: {
            [Op.and]: {
              [Op.lt]: getLastWeeksDate(),
            },
          },
        },
      })

      const new_virtual_experiences = await VirtualExperiences.count({
        where: {
          created_at: {
            [Op.and]: {
              [Op.lt]: new Date(),
            },
          },
        },
      })

      let virtual_experience_percentage = 0

      if (prev_virtual_experiences === 0 && new_virtual_experiences === 0) {
        virtual_experience_percentage = 0
      } else if (prev_virtual_experiences === 0) {
        virtual_experience_percentage = 100
      } else {
        const difference = new_virtual_experiences - prev_virtual_experiences
        if (difference === 0) {
          virtual_experience_percentage = 0
        } else {
          virtual_experience_percentage =
            (difference / prev_virtual_experiences) * 100
        }
      }

      const virtual_experience_info = {
        count: new_virtual_experiences,
        percentage: virtual_experience_percentage,
      }

      //real_estate
      const prev_real_estates = await RealEstates.count({
        where: {
          // status: "Active",
          created_at: {
            [Op.and]: {
              [Op.lt]: getLastWeeksDate(),
            },
          },
        },
      })

      const new_real_estates = await RealEstates.count({
        where: {
          created_at: {
            [Op.and]: {
              [Op.lt]: new Date(),
            },
          },
        },
      })

      let real_estate_percentage = 0

      if (prev_real_estates === 0 && new_real_estates === 0) {
        real_estate_percentage = 0
      } else if (prev_virtual_experiences === 0) {
        real_estate_percentage = 100
      } else {
        const difference = new_real_estates - prev_real_estates
        if (difference === 0) {
          real_estate_percentage = 0
        } else {
          real_estate_percentage = (difference / prev_real_estates) * 100
        }
      }

      const real_estate_info = {
        count: new_real_estates,
        percentage: real_estate_percentage,
      }

      //email
      const prev_emails = await SentEmails.count({
        where: {
          // status: "Active",
          created_at: {
            [Op.and]: {
              [Op.lt]: getLastWeeksDate(),
            },
          },
        },
      })

      const new_emails = await SentEmails.count({
        where: {
          // status: "Active",
          created_at: {
            [Op.and]: {
              [Op.lt]: new Date(),
            },
          },
        },
      })

      let email_percentage = 0

      if (prev_emails === 0 && new_emails === 0) {
        email_percentage = 0
      } else if (prev_emails === 0) {
        email_percentage = 100
      } else {
        const difference = new_emails - prev_emails
        if (difference === 0) {
          email_percentage = 0
        } else {
          email_percentage = (difference / prev_emails) * 100
        }
      }

      const email_info = {
        count: new_emails,
        percentage: email_percentage,
      }

      //product
      const prev_products = await Products.count({
        where: {
          // status: "Active",
          created_at: {
            [Op.and]: {
              [Op.lt]: getLastWeeksDate(),
            },
          },
        },
      })

      const new_products = await Products.count({
        where: {
          // status: "Active",
          created_at: {
            [Op.and]: {
              [Op.lt]: new Date(),
            },
          },
        },
      })

      let product_percentage = 0

      if (prev_products === 0 && new_products === 0) {
        product_percentage = 0
      } else if (prev_products === 0) {
        product_percentage = 100
      } else {
        const difference = new_products - prev_products
        if (difference === 0) {
          product_percentage = 0
        } else {
          product_percentage = (difference / prev_products) * 100
        }
      }

      const product_info = {
        count: new_products,
        percentage: product_percentage,
      }

      const prev_flythroughs = await VirtualFlythrough.count({
        where: {
          created_at: {
            [Op.and]: {
              [Op.lt]: getLastWeeksDate(),
            },
          },
        },
      })

      const new_flythroughs = await VirtualFlythrough.count({
        where: {
          created_at: {
            [Op.and]: {
              [Op.lt]: new Date(),
            },
          },
        },
      })

      let flythroughs_percentage = 0

      if (prev_flythroughs === 0 && new_flythroughs === 0) {
        flythroughs_percentage = 0
      } else if (prev_flythroughs === 0) {
        flythroughs_percentage = 100
      } else {
        const difference = new_flythroughs - prev_flythroughs
        if (difference === 0) {
          flythroughs_percentage = 0
        } else {
          flythroughs_percentage = (difference / prev_flythroughs) * 100
        }
      }

      const flythrough_info = {
        count: new_flythroughs,
        percentage: flythroughs_percentage,
      }

      const prev_invitations = await SentEmails.count({
        where: {
          type: "Invitation",
          created_at: {
            [Op.and]: {
              [Op.lt]: getLastWeeksDate(),
            },
          },
        },
      })

      const new_invitations = await SentEmails.count({
        where: {
          type: "Invitation",
          created_at: {
            [Op.and]: {
              [Op.lt]: new Date(),
            },
          },
        },
      })

      let invitations_percentage = 0

      if (prev_invitations === 0 && new_invitations === 0) {
        invitations_percentage = 0
      } else if (prev_invitations === 0) {
        invitations_percentage = 100
      } else {
        const difference = new_invitations - prev_invitations
        if (difference === 0) {
          invitations_percentage = 0
        } else {
          invitations_percentage = (difference / prev_invitations) * 100
        }
      }

      const invitation_info = {
        count: new_invitations,
        percentage: invitations_percentage,
      }

      const prev_video_conferences = await VideoConferences.count({
        where: {
          created_at: {
            [Op.and]: {
              [Op.lt]: getLastWeeksDate(),
            },
          },
        },
      })

      const new_video_conferences = await VideoConferences.count({
        where: {
          created_at: {
            [Op.and]: {
              [Op.lt]: new Date(),
            },
          },
        },
      })

      let video_conferences_percentage = 0

      if (prev_video_conferences === 0 && new_video_conferences === 0) {
        video_conferences_percentage = 0
      } else if (prev_video_conferences === 0) {
        video_conferences_percentage = 100
      } else {
        const difference = new_video_conferences - prev_video_conferences
        if (difference === 0) {
          video_conferences_percentage = 0
        } else {
          video_conferences_percentage =
            (difference / prev_video_conferences) * 100
        }
      }

      const video_conference_info = {
        count: new_video_conferences,
        percentage: video_conferences_percentage,
      }

      const prev_flythrough_invitations = await SentEmails.count({
        where: {
          type: "Flythrough Invitation",
          created_at: {
            [Op.and]: {
              [Op.lt]: getLastWeeksDate(),
            },
          },
        },
      })

      const new_flythrough_invitations = await SentEmails.count({
        where: {
          type: "Flythrough Invitation",
          created_at: {
            [Op.and]: {
              [Op.lt]: new Date(),
            },
          },
        },
      })

      let flythrough_invitations_percentage = 0

      if (
        prev_flythrough_invitations === 0 &&
        new_flythrough_invitations === 0
      ) {
        flythrough_invitations_percentage = 0
      } else if (prev_flythrough_invitations === 0) {
        flythrough_invitations_percentage = 100
      } else {
        const difference =
          new_flythrough_invitations - prev_flythrough_invitations
        if (difference === 0) {
          flythrough_invitations_percentage = 0
        } else {
          flythrough_invitations_percentage =
            (difference / prev_flythrough_invitations) * 100
        }
      }

      const flythrough_invitation_info = {
        count: new_flythrough_invitations,
        percentage: flythrough_invitations_percentage,
      }

      const recent_invitations = await Invitations.findAll({
        limit: 3,
        include: [
          {
            model: Users,
            as: "invitation_user",
          },
          {
            model: VirtualExperiences,
            as: "virtual_experience",
          },
          {
            model: Customers,
            as: "customer",
          },
        ],
        order: [["created_at", "DESC"]],
      })

      const recent_clients = await Clients.findAll({
        limit: 3,
        include: [
          {
            model: Users,
            attributes: ["id", "first_name"],
          },
        ],
        order: [["created_at", "DESC"]],
      })

      const recent_users = await Users.findAll({
        limit: 3,
        attributes: ["id", "first_name", "last_name", "image_url", "status"],
        include: [
          {
            model: Clients,
            attributes: ["id", "name"],
          },
        ],
        order: [["created_at", "DESC"]],
      })

      const recent_virtual_experiences = await VirtualExperiences.findAll({
        limit: 3,
        attributes: ["id", "name", "thumbnail_url", "status"],
        include: [
          {
            model: Clients,
            attributes: ["id", "name"],
          },
        ],
        order: [["created_at", "DESC"]],
      })

      const recent_real_estates = await RealEstates.findAll({
        limit: 3,
        attributes: ["id", "type"],
        include: [
          {
            model: Clients,
            attributes: ["id", "name"],
          },
          {
            model: VirtualExperiences,
            attributes: ["id", "name", "thumbnail_url", "status"],
          },
        ],
        order: [["created_at", "DESC"]],
      })

      const recent_video_conferences = await VideoConferences.findAll({
        limit: 3,
        attributes: ["id", "start_time", "end_time", "status"],
        include: [
          {
            model: Users,
            attributes: ["id", "first_name", "last_name"],
            as: "host",
            include: [
              {
                model: Clients,
              },
            ],
          },
        ],
        order: [["created_at", "DESC"]],
      })

      const recent_emails = await SentEmails.findAll({
        limit: 3,
        attributes: ["id", "to", "status"],
        include: [
          {
            model: Users,
            attributes: ["first_name", "last_name"],
            include: [
              {
                model: Clients,
              },
            ],
          },
        ],
        order: [["created_at", "DESC"]],
      })

      const recent_products = await Products.findAll({
        limit: 3,
        attributes: [
          "id",
          "name",
          "sku",
          "price",
          "status",
          "in_stock",
          "image_urls",
        ],
        include: [
          {
            model: Clients,
            attributes: ["id", "name"],
          },
          {
            model: VirtualExperiences,
            attributes: ["id", "name"],
          },
        ],
        order: [["created_at", "DESC"]],
      })

      return res.status(200).json({
        client: client_info,
        user: user_info,
        customer: customer_info,
        virtual_experience: virtual_experience_info,
        real_estate: real_estate_info,
        email: email_info,
        product: product_info,
        invitations: invitation_info,
        video_conferences: video_conference_info,
        flythroughs: flythrough_info,
        flythrough_invitations: flythrough_invitation_info,
        recent_clients: recent_clients,
        recent_users: recent_users,
        recent_virtual_experiences: recent_virtual_experiences,
        recent_real_estates: recent_real_estates,
        recent_video_conferences: recent_video_conferences,
        recent_emails: recent_emails,
        recent_products: recent_products,
        recent_invitations: recent_invitations,
      })
    } catch (error) {
      console.log(error)
    }
  }
}

export const get_sales_dashboard = async (req, res) => {
  const { virtual_experience_id, user_id, client_id } = req.query
  console.log("here", req.query)
  const prevMonth = new Date()
  prevMonth.setMonth(prevMonth.getMonth() - 1)

  console.log(prevMonth)

  function getLastWeeksDate() {
    const now = new Date()

    return new Date(now.getFullYear(), now.getMonth(), now.getDate() - 7)
  }

  function getFirstLastWeeksDate() {
    const now = new Date()

    return new Date(now.getFullYear(), now.getMonth(), now.getDate() - 14)
  }

  try {
    const user_backup = await UserBackupUsers.findAll({
      where: {
        backup_user_id: user_id,
      },
      attributes: ["id"],
    })
    let ids = []
    ids.push(user_id)
    if (user_backup) {
      for (let index = 0; index < user_backup.length; index++) {
        const element = user_backup[index]
        ids.push(element.dataValues.id)
      }
    }
    console.log(ids)
    const prev_clients = await Clients.count({
      subQuery: false,
      where: {
        // status: "Active",
        created_at: {
          [Op.and]: {
            [Op.lt]: getLastWeeksDate(),
          },
        },
        [Op.or]: {
          "$virtual_experience.sales_id$": {
            [Op.in]: ids,
          },
        },
      },
      include: [
        {
          model: VirtualExperiences,
          as: "virtual_experience",
        },
      ],
    })

    const new_clients = await Clients.count({
      subQuery: false,
      where: {
        // status: "Active",
        created_at: {
          [Op.and]: {
            [Op.lt]: new Date(),
          },
        },
        [Op.or]: {
          "$virtual_experience.sales_id$": {
            [Op.in]: ids,
          },
        },
      },
      include: [
        {
          model: VirtualExperiences,
          as: "virtual_experience",
        },
      ],
    })

    let client_percentage = 0

    if (prev_clients === 0 && new_clients === 0) {
      client_percentage = 0
    } else if (prev_clients === 0) {
      client_percentage = 100
    } else {
      const difference = new_clients - prev_clients
      if (difference === 0) {
        client_percentage = 0
      } else {
        client_percentage = (difference / prev_clients) * 100
      }
    }

    const client_info = {
      count: new_clients,
      percentage: client_percentage,
    }

    //users
    const prev_users = await Users.count({
      subQuery: false,
      where: {
        // status: "Active",
        created_at: {
          [Op.and]: {
            [Op.lt]: getLastWeeksDate(),
          },
        },
        [Op.or]: {
          "$client.virtual_experience.sales_id$": {
            [Op.in]: ids,
          },
          "$backup_user.id$": {
            [Op.in]: ids,
          },
        },
      },
      include: [
        {
          model: Clients,
          as: "client",
          include: [
            {
              model: VirtualExperiences,
              as: "virtual_experience",
            },
          ],
        },
        {
          model: Users,
          as: "backup_user",
        },
      ],
    })

    const new_users = await Users.count({
      subQuery: false,
      where: {
        // status: "Active",
        created_at: {
          [Op.and]: {
            [Op.lt]: new Date(),
          },
        },
        [Op.or]: {
          "$client.virtual_experience.sales_id$": {
            [Op.in]: ids,
          },
          "$backup_user.id$": {
            [Op.in]: ids,
          },
        },
      },
      include: [
        {
          model: Clients,
          as: "client",
          include: [
            {
              model: VirtualExperiences,
              as: "virtual_experience",
            },
          ],
        },
        {
          model: Users,
          as: "backup_user",
        },
      ],
    })

    let user_percentage = 0

    if (prev_users === 0 && new_users === 0) {
      user_percentage = 0
    } else if (prev_users === 0) {
      user_percentage = 100
    } else {
      const difference = new_users - prev_users
      if (difference === 0) {
        user_percentage = 0
      } else {
        user_percentage = (difference / prev_users) * 100
      }
    }

    const user_info = {
      count: new_users,
      percentage: user_percentage,
    }

    //customers
    const prev_customers = await Customers.count({
      subQuery: false,
      where: {
        // status: "Active",
        created_at: {
          [Op.and]: {
            [Op.lt]: getLastWeeksDate(),
          },
        },
        "$client_customers.user_id$": {
          [Op.in]: ids,
        },
      },
      include: [
        {
          model: ClientCustomers,
          where: {
            client_id: client_id,
          },
        },
      ],
    })

    const new_customers = await Customers.count({
      subQuery: false,
      where: {
        // status: "Active",
        created_at: {
          [Op.and]: {
            [Op.lt]: new Date(),
          },
        },
      },
      include: [
        {
          model: ClientCustomers,
          where: {
            client_id: client_id,
          },
        },
      ],
    })
    const customers = await Customers.count({
      subQuery: false,
      include: [
        {
          model: ClientCustomers,
          as: "client_customers",
          where: {
            client_id: client_id,
            customer_id: {
              [Op.ne]: null,
            },
          },
        },
      ],
    })
    let customer_percentage = 0

    if (prev_customers === 0 && new_customers === 0) {
      customer_percentage = 0
    } else if (prev_customers === 0) {
      customer_percentage = 100
    } else {
      const difference = new_customers - prev_customers
      if (difference === 0) {
        customer_percentage = 0
      } else {
        customer_percentage = (difference / prev_customers) * 100
      }
    }

    const customer_info = {
      count: new_customers,
      percentage: customer_percentage,
      customers: customers,
    }

    //virtual experience
    const prev_virtual_experiences = await VirtualExperiences.count({
      where: {
        // status: "Active",
        created_at: {
          [Op.and]: {
            [Op.lt]: getLastWeeksDate(),
          },
        },
        sales_id: {
          [Op.in]: ids,
        },
      },
    })

    const new_virtual_experiences = await VirtualExperiences.count({
      where: {
        created_at: {
          [Op.and]: {
            [Op.lt]: new Date(),
          },
        },
        sales_id: {
          [Op.in]: ids,
        },
      },
    })

    let virtual_experience_percentage = 0

    if (prev_virtual_experiences === 0 && new_virtual_experiences === 0) {
      virtual_experience_percentage = 0
    } else if (prev_virtual_experiences === 0) {
      virtual_experience_percentage = 100
    } else {
      const difference = new_virtual_experiences - prev_virtual_experiences
      if (difference === 0) {
        virtual_experience_percentage = 0
      } else {
        virtual_experience_percentage =
          (difference / prev_virtual_experiences) * 100
      }
    }

    const virtual_experience_info = {
      count: new_virtual_experiences,
      percentage: virtual_experience_percentage,
    }

    //real_estate
    const prev_real_estates = await RealEstates.count({
      subQuery: false,
      where: {
        // status: "Active",
        created_at: {
          [Op.and]: {
            [Op.lt]: getLastWeeksDate(),
          },
        },
        "$virtual_experience.sales_id$": {
          [Op.in]: ids,
        },
      },
      include: [
        {
          model: VirtualExperiences,
          as: "virtual_experience",
        },
      ],
    })

    const new_real_estates = await RealEstates.count({
      subQuery: false,
      where: {
        created_at: {
          [Op.and]: {
            [Op.lt]: new Date(),
          },
        },
        "$virtual_experience.sales_id$": {
          [Op.in]: ids,
        },
      },
      include: [
        {
          model: VirtualExperiences,
          as: "virtual_experience",
        },
      ],
    })

    let real_estate_percentage = 0

    if (prev_real_estates === 0 && new_real_estates === 0) {
      real_estate_percentage = 0
    } else if (prev_virtual_experiences === 0) {
      real_estate_percentage = 100
    } else {
      const difference = new_real_estates - prev_real_estates
      if (difference === 0) {
        real_estate_percentage = 0
      } else {
        real_estate_percentage = (difference / prev_real_estates) * 100
      }
    }

    const real_estate_info = {
      count: new_real_estates,
      percentage: real_estate_percentage,
    }

    //email
    const prev_emails = await SentEmails.count({
      where: {
        // status: "Active",
        created_at: {
          [Op.and]: {
            [Op.lt]: getLastWeeksDate(),
          },
        },
        user_id: {
          [Op.in]: ids,
        },
      },
    })

    const new_emails = await SentEmails.count({
      where: {
        // status: "Active",
        created_at: {
          [Op.and]: {
            [Op.lt]: new Date(),
          },
        },
        user_id: {
          [Op.in]: ids,
        },
      },
    })

    let email_percentage = 0

    if (prev_emails === 0 && new_emails === 0) {
      email_percentage = 0
    } else if (prev_emails === 0) {
      email_percentage = 100
    } else {
      const difference = new_emails - prev_emails
      if (difference === 0) {
        email_percentage = 0
      } else {
        email_percentage = (difference / prev_emails) * 100
      }
    }

    const email_info = {
      count: new_emails,
      percentage: email_percentage,
    }

    //product
    const prev_products = await Products.count({
      subQuery: false,
      where: {
        // status: "Active",
        created_at: {
          [Op.and]: {
            [Op.lt]: getLastWeeksDate(),
          },
        },
        "$virtual_experience.sales_id$": {
          [Op.in]: ids,
        },
      },
      include: [
        {
          model: VirtualExperiences,
          as: "virtual_experience",
        },
      ],
    })

    const new_products = await Products.count({
      subQuery: false,
      where: {
        // status: "Active",
        created_at: {
          [Op.and]: {
            [Op.lt]: new Date(),
          },
        },
        "$virtual_experience.sales_id$": {
          [Op.in]: ids,
        },
      },
      include: [
        {
          model: VirtualExperiences,
          as: "virtual_experience",
        },
      ],
    })

    let product_percentage = 0

    if (prev_products === 0 && new_products === 0) {
      product_percentage = 0
    } else if (prev_products === 0) {
      product_percentage = 100
    } else {
      const difference = new_products - prev_products
      if (difference === 0) {
        product_percentage = 0
      } else {
        product_percentage = (difference / prev_products) * 100
      }
    }

    const product_info = {
      count: new_products,
      percentage: product_percentage,
    }

    const prev_flythroughs = await VirtualFlythrough.count({
      where: {
        created_at: {
          [Op.and]: {
            [Op.lt]: getLastWeeksDate(),
          },
        },
      },
    })

    const new_flythroughs = await VirtualFlythrough.count({
      where: {
        created_at: {
          [Op.and]: {
            [Op.lt]: new Date(),
          },
        },
      },
    })

    let flythroughs_percentage = 0

    if (prev_flythroughs === 0 && new_flythroughs === 0) {
      flythroughs_percentage = 0
    } else if (prev_flythroughs === 0) {
      flythroughs_percentage = 100
    } else {
      const difference = new_flythroughs - prev_flythroughs
      if (difference === 0) {
        flythroughs_percentage = 0
      } else {
        flythroughs_percentage = (difference / prev_flythroughs) * 100
      }
    }

    const flythrough_info = {
      count: new_flythroughs,
      percentage: flythroughs_percentage,
    }

    const prev_invitations = await SentEmails.count({
      include: [
        {
          model: Users,
          where: {
            client_id: client_id,
          },
        },
      ],
      where: {
        type: "Invitation",
        created_at: {
          [Op.and]: {
            [Op.lt]: getLastWeeksDate(),
          },
        },
      },
    })

    const new_invitations = await SentEmails.count({
      include: [
        {
          model: Users,
          where: {
            client_id: client_id,
          },
        },
      ],
      where: {
        type: "Invitation",
        created_at: {
          [Op.and]: {
            [Op.lt]: new Date(),
          },
        },
      },
      order: [["created_at", "DESC"]],
    })

    let invitations_percentage = 0

    if (prev_invitations === 0 && new_invitations === 0) {
      invitations_percentage = 0
    } else if (prev_invitations === 0) {
      invitations_percentage = 100
    } else {
      const difference = new_invitations - prev_invitations
      if (difference === 0) {
        invitations_percentage = 0
      } else {
        invitations_percentage = (difference / prev_invitations) * 100
      }
    }

    const invitation_info = {
      count: new_invitations,
      percentage: invitations_percentage,
    }

    const prev_video_conferences = await VideoConferences.count({
      where: {
        // client_id: client_id,
        created_at: {
          [Op.and]: {
            [Op.lt]: getLastWeeksDate(),
          },
        },
      },
    })

    const new_video_conferences = await VideoConferences.count({
      where: {
        // client_id: client_id,
        created_at: {
          [Op.and]: {
            [Op.lt]: new Date(),
          },
        },
      },
    })

    let video_conferences_percentage = 0

    if (prev_video_conferences === 0 && new_video_conferences === 0) {
      video_conferences_percentage = 0
    } else if (prev_video_conferences === 0) {
      video_conferences_percentage = 100
    } else {
      const difference = new_video_conferences - prev_video_conferences
      if (difference === 0) {
        video_conferences_percentage = 0
      } else {
        video_conferences_percentage =
          (difference / prev_video_conferences) * 100
      }
    }

    const video_conference_info = {
      count: new_video_conferences,
      percentage: video_conferences_percentage,
    }

    const prev_flythrough_invitations = await SentEmails.count({
      include: [
        {
          model: Users,
          where: {
            client_id: client_id,
          },
        },
      ],
      where: {
        type: "Flythrough Invitation",
        created_at: {
          [Op.and]: {
            [Op.lt]: getLastWeeksDate(),
          },
        },
      },
    })

    const new_flythrough_invitations = await SentEmails.count({
      include: [
        {
          model: Users,
          where: {
            client_id: client_id,
          },
        },
      ],
      where: {
        type: "Flythrough Invitation",
        created_at: {
          [Op.and]: {
            [Op.lt]: new Date(),
          },
        },
      },
      order: [["created_at", "DESC"]],
    })

    let flythrough_invitations_percentage = 0

    if (prev_flythrough_invitations === 0 && new_flythrough_invitations === 0) {
      flythrough_invitations_percentage = 0
    } else if (prev_flythrough_invitations === 0) {
      flythrough_invitations_percentage = 100
    } else {
      const difference =
        new_flythrough_invitations - prev_flythrough_invitations
      if (difference === 0) {
        flythrough_invitations_percentage = 0
      } else {
        flythrough_invitations_percentage =
          (difference / prev_flythrough_invitations) * 100
      }
    }

    const flythrough_invitation_info = {
      count: new_flythrough_invitations,
      percentage: flythrough_invitations_percentage,
    }

    const recent_invitations = await Invitations.findAll({
      limit: 3,
      include: [
        {
          model: Users,
          as: "invitation_user",
        },
        {
          model: VirtualExperiences,
          as: "virtual_experience",
        },
        {
          model: Customers,
          as: "customer",
        },
      ],
      order: [["created_at", "DESC"]],
    })

    const recent_clients = await Clients.findAll({
      subQuery: false,
      limit: 3,
      where: {
        "$virtual_experience.sales_id$": {
          [Op.in]: ids,
        },
      },
      include: [
        {
          model: VirtualExperiences,
          as: "virtual_experience",
        },
        {
          model: Users,
          attributes: ["id", "first_name"],
        },
      ],
      order: [["created_at", "DESC"]],
    })

    const recent_users = await Users.findAll({
      subQuery: false,
      limit: 3,
      where: {
        [Op.or]: {
          "$client.virtual_experience.sales_id$": {
            [Op.in]: ids,
          },
          "$backup_user.id$": {
            [Op.in]: ids,
          },
        },
      },
      include: [
        {
          model: Clients,
          as: "client",
          include: [
            {
              model: VirtualExperiences,
              as: "virtual_experience",
            },
          ],
        },
        {
          model: Users,
          as: "backup_user",
        },
      ],
      order: [["created_at", "DESC"]],
    })

    const recent_virtual_experiences = await VirtualExperiences.findAll({
      limit: 3,
      where: {
        sales_id: {
          [Op.in]: ids,
        },
      },
      attributes: ["id", "name", "thumbnail_url", "status"],
      include: [
        {
          model: Clients,
          attributes: ["id", "name"],
        },
      ],
      order: [["created_at", "DESC"]],
    })

    const recent_real_estates = await RealEstates.findAll({
      subQuery: false,
      limit: 3,
      where: {
        "$virtual_experience.sales_id$": {
          [Op.in]: ids,
        },
      },
      attributes: ["id", "type"],
      include: [
        {
          model: Clients,
          attributes: ["id", "name"],
        },
        {
          model: VirtualExperiences,
          attributes: ["id", "name", "thumbnail_url", "status", "sales_id"],
          as: "virtual_experience",
        },
      ],
      order: [["created_at", "DESC"]],
    })

    const recent_video_conferences = await VideoConferences.findAll({
      limit: 3,
      where: {
        host_id: {
          [Op.in]: ids,
        },
        backup_host_id: {
          [Op.in]: ids,
        },
      },
      attributes: ["id", "start_time", "end_time", "status"],
      include: [
        {
          model: Users,
          attributes: ["id", "first_name", "last_name"],
          as: "host",
          include: [
            {
              model: Clients,
            },
          ],
        },
      ],
      order: [["created_at", "DESC"]],
    })

    const recent_emails = await SentEmails.findAll({
      limit: 3,
      where: {
        user_id: {
          [Op.in]: ids,
        },
      },
      attributes: ["id", "to", "status"],
      include: [
        {
          model: Users,
          attributes: ["first_name", "last_name"],
          include: [
            {
              model: Clients,
            },
          ],
        },
      ],
      order: [["created_at", "DESC"]],
    })

    const recent_products = await Products.findAll({
      subQuery: false,
      limit: 3,
      where: {
        "$virtual_experience.sales_id$": {
          [Op.in]: ids,
        },
      },
      attributes: [
        "id",
        "name",
        "sku",
        "price",
        "status",
        "in_stock",
        "image_urls",
      ],
      include: [
        {
          model: Clients,
          attributes: ["id", "name"],
        },
        {
          model: VirtualExperiences,
          attributes: ["id", "name", "sales_id"],
          as: "virtual_experience",
        },
      ],
      order: [["created_at", "DESC"]],
    })

    return res.status(200).json({
      client: client_info,
      user: user_info,
      customer: customer_info,
      virtual_experience: virtual_experience_info,
      real_estate: real_estate_info,
      email: email_info,
      product: product_info,
      invitations: invitation_info,
      video_conferences: video_conference_info,
      flythroughs: flythrough_info,
      flythrough_invitations: flythrough_invitation_info,
      recent_clients: recent_clients,
      recent_users: recent_users,
      recent_virtual_experiences: recent_virtual_experiences,
      recent_real_estates: recent_real_estates,
      recent_video_conferences: recent_video_conferences,
      recent_emails: recent_emails,
      recent_products: recent_products,
      recent_invitations: recent_invitations,
    })
  } catch (error) {
    console.log(error)
  }
}
