import { Op } from "sequelize";
import VirtualExperienceCategories from "../../models/VirtualExperienceCategoryModel.js";
import ProductCategories from "../../models/ProductCategoryModel.js";

export const get_all_categories = async (req, res) => {
  
  let query = req.query;

  let categories;

  try {
    if(query && query.search !== "null") {
      categories = await ProductCategories.findAndCountAll({
        limit: parseInt(query.limit),
        offset: parseInt(query.offset),
        where: {
          name: {
            [Op.substring]: query.search,
          }
        },
        nest: true,
      })
    } else {
      categories = await ProductCategories.findAndCountAll({
        limit: parseInt(query.limit),
        offset: parseInt(query.offset),
        nest: true,
      })
    }

    if(!categories) {
      return res.status(400).json({message: "No Categories found"})
    }

    return res.status(200).json({categories: categories})

  } catch (error) {
    console.log("Error: ", error)
  }
}

export const get_all_categories_option = async (req, res) => {
  
  try {
    let categories = await ProductCategories.findAll({
      nest: true,
      where: {
        status: "Active"
      }
    })

    if(!categories) {
      return res.status(400).json({message: "No Categories found"})
    }

    return res.status(200).json({categories: categories})

  } catch (error) {
    console.log("Error: ", error)
  }
}

export const get_category = async (req, res) => {
  
  let {id} = req.params;

  let category;

  try {
    category = await ProductCategories.findOne({
      where: {
        id: id
      },
      plain: true,
      nest: true,
    })

    if(!category) {
      return res.status(400).json({message: "No Category found"})
    }

    return res.status(200).json(category)

  } catch (error) {
    
  }
}

export const check_if_category_unique_name = async (req, res) => {

  const {name} = req.query;

  try {
    const check_name = await ProductCategories.findOne({
      where: {
        name: name
      }
    })
    if(!check_name) {
      return res.status(204).json()
    }
    return res.status(200).json()
  } catch (error) {
    console.log(error)
    return res.status(400).json();
  }
}

export const create_category = async (req, res) => {
  
  let {name, description, status, created_by} = req.body;

  if(!name || !created_by) {
    return res.status(400).json({message: "Incomplete Information"})
  } else {
    try {
      const category = await ProductCategories.create({
        name: name,
        description: description,
        status: status,
        created_by: created_by
      })

      if(!category) {
        return res.status(400).json({message: "Category was not successfully created"})
      }
      return res.status(201).json({message: "Category was successfully created"})
    } catch (error) {
      
    }
  }

}

export const edit_category = async (req, res) => {
  const {id} = req.params;
  
  const {name, description, status, updated_by} = req.body;

  try {
    const business_type = await ProductCategories.update({
      name: name,
      description: description,
      status: status,
      updated_by: updated_by
    }, {
      where: {
        id: id
      }
    })

    if(!business_type) {
      return res.status(400).json({message: "Category was not successfully edited"})
    }

    return res.status(200).json({message: "Category was successfully edited"})
  } catch (error) {
    console.log(error)
  }

}

export const delete_category = async (req, res) => {

  const {id} = req.params;

  try {
    await ProductCategories.destroy({
      where: {
        id: id,
      }
    })
    return res.status(200).json({message: "Category was successfully deleted"})
  } catch (error) {
    console.log(error)
    return res.status(400).json();
  }
}