import moment from "moment"
import db from "../../config/Database.js"
import Clients from "../../models/ClientModel.js"
import { Op } from "sequelize"
import Customers from "../../models/CustomerModel.js"
import ClientCustomers from "../../models/ClientCustomerModel.js"
import SentEmails from "../../models/SentEmailModel.js"
import Users from "../../models/UserModel.js"
import AnalyticsVisitDuration from "../../models/AnalyticsVisitDurationModel.js"
import ProductOptionOfferedRequests from "../../models/ProductOptionOfferedRequestModel.js"
import Products from "../../models/ProductModel.js"
import OptionOffered from "../../models/OptionOfferedModel.js"
import VirtualExperiences from "../../models/VirtualExperienceModel.js"
import AnalyticsVideoMinutes from "../../models/AnalyticsVideoMinuteModel.js"
import AnalyticsProductPreview from "../../models/AnalyticsProductPreviewModel.js"
import AnalyticsProductQuantity from "../../models/AnalyticsProductQuantityModel.js"
import AnalyticsVirtualFlythrough from "../../models/AnalyticsVirtualFlythroughModel.js"
import VirtualFlythrough from "../../models/VirtualFlythrough.js"
import InvitationFlythrough from "../../models/InvitationFlythroughModel.js"

export const all_client_map_analytics = async (req, res) => {
  let { time } = req.query

  let start = moment()
  let end = moment()
  switch (time) {
    case "Today":
      start = start.startOf("day")
      end = end.endOf("day")
      console.log(start, end)
      break
    case "This Week":
      start = start.startOf("week")
      end = end.endOf("week")
      console.log(start, end)
      break
    case "This Month":
      start = start.startOf("month")
      end = end.endOf("month")
      console.log(start, end)
      break
    case "This Year":
      start = start.startOf("year")
      end = end.endOf("year")
      console.log(start, end)
      break
    default:
      break
  }

  try {
    const clients = await Clients.findAll({
      where: {
        status: "Active",
        created_at: {
          [Op.gte]: start,
          [Op.lte]: end,
        },
      },
      nest: true,
      raw: true,
      attributes: ["country", [db.fn("COUNT", db.col("country")), "count"]],
      group: "country",
    })

    if (!clients) {
      return res.status(400).json({ message: "No Client was found" })
    }

    return res.status(200).json({ all_client_map: clients })
  } catch (error) {
    console.log(error)
  }
}

export const all_customer_map_analytics = async (req, res) => {
  let { time, client } = req.query

  let start = moment()
  let end = moment()
  switch (time) {
    case "Today":
      start = start.startOf("day")
      end = end.endOf("day")
      console.log(start, end)
      break
    case "This Week":
      start = start.startOf("week")
      end = end.endOf("week")
      console.log(start, end)
      break
    case "This Month":
      start = start.startOf("month")
      end = end.endOf("month")
      console.log(start, end)
      break
    case "This Year":
      start = start.startOf("year")
      end = end.endOf("year")
      console.log(start, end)
      break
    default:
      break
  }

  let customers

  try {
    if (client !== "0") {
      customers = await ClientCustomers.findAll({
        where: {
          client_id: client,
        },
        raw: true,
        nest: true,
        where: {
          client_id: client,
          created_at: {
            [Op.gte]: start,
            [Op.lte]: end,
          },
        },
        attributes: [],
        include: [
          {
            model: Customers,
            attributes: [
              [db.fn("COUNT", db.col("country")), "country_count"],
              "country",
            ],
          },
        ],
        group: ["customer.country"],
      })
    } else {
      customers = await ClientCustomers.findAll({
        raw: true,
        nest: true,
        where: {
          created_at: {
            [Op.gte]: start,
            [Op.lte]: end,
          },
        },
        attributes: [],
        include: [
          {
            model: Customers,
            attributes: [
              [db.fn("COUNT", db.col("country")), "country_count"],
              "country",
            ],
          },
        ],
        group: ["customer.country"],
      })
    }

    if (!customers) {
      return res.status(400).json({ message: "No Customers was found" })
    }

    return res.status(200).json({ all_customer_map: customers })
  } catch (error) {
    console.log(error)
  }
}

export const all_option_offered_analytics = async (req, res) => {
  let { time, client } = req.query

  let start = moment()
  let end = moment()
  switch (time) {
    case "Today":
      start = start.startOf("day")
      end = end.endOf("day")
      console.log(start, end)
      break
    case "This Week":
      start = start.startOf("week")
      end = end.endOf("week")
      console.log(start, end)
      break
    case "This Month":
      start = start.startOf("month")
      end = end.endOf("month")
      console.log(start, end)
      break
    case "This Year":
      start = start.startOf("year")
      end = end.endOf("year")
      console.log(start, end)
      break
    default:
      break
  }

  let option_offered

  try {
    if (client !== "0") {
      option_offered = await ProductOptionOfferedRequests.findAll({
        nest: true,
        where: {
          client_id: client,
          created_at: {
            [Op.gte]: start,
            [Op.lte]: end,
          },
        },
        attributes: ["id"],
        include: [
          {
            model: OptionOffered,
            attributes: [
              [db.fn("COUNT", db.col("name")), "name_count"],
              "name",
            ],
          },
        ],
        group: ["opt_offer_id"],
      })
    } else {
      option_offered = await ProductOptionOfferedRequests.findAll({
        nest: true,
        where: {
          created_at: {
            [Op.gte]: start,
            [Op.lte]: end,
          },
        },
        attributes: ["id"],
        include: [
          {
            model: OptionOffered,
            attributes: [
              [db.fn("COUNT", db.col("name")), "name_count"],
              "name",
            ],
          },
        ],
        group: ["opt_offer_id"],
      })
    }

    if (!option_offered) {
      return res
        .status(400)
        .json({ message: "No Option Offered Requests was found" })
    }

    return res.status(200).json({ all_requests: option_offered })
  } catch (error) {
    console.log(error)
  }
}

export const all_emails_analytics = async (req, res) => {
  let { time, client } = req.query

  let start = moment.utc()
  let end = moment.utc()
  let queryFn = "hour"
  switch (time) {
    case "Today":
      start = start.startOf("day")
      end = end.endOf("day")
      queryFn = "hour"
      console.log(start, end)
      break
    case "This Week":
      start = start.startOf("week")
      end = end.endOf("week")
      queryFn = "day"
      console.log(start, end)
      break
    case "This Month":
      start = start.startOf("month")
      end = end.endOf("month")
      queryFn = "day"
      console.log(start, end)
      break
    case "This Year":
      start = start.startOf("year")
      end = end.endOf("year")
      queryFn = "month"
      console.log(start, end)
      break
    default:
      break
  }

  let sent_emails
  try {
    if (client !== "0") {
      sent_emails = await SentEmails.findAll({
        raw: true,
        nest: true,
        where: {
          created_at: {
            [Op.gte]: start,
            [Op.lte]: end,
          },
        },
        attributes: [
          [db.fn("COUNT", db.col("sent_emails.status")), "status_count"],
          "sent_emails.status",
          [db.fn(queryFn, db.col("sent_emails.created_at")), "created"],
        ],
        include: [
          {
            model: Users,
            where: {
              client_id: client,
            },
          },
        ],
        group: [
          "sent_emails.status",
          db.fn(queryFn, db.col("sent_emails.created_at")),
        ],
      })
    } else {
      sent_emails = await SentEmails.findAll({
        raw: true,
        nest: true,
        where: {
          user_id: { [Op.not]: null },
          created_at: {
            [Op.gte]: start,
            [Op.lte]: end,
          },
        },
        attributes: [
          [db.fn("COUNT", db.col("sent_emails.status")), "status_count"],
          "status",
          [db.fn(queryFn, db.col("sent_emails.created_at")), "created"],
        ],
        // include: [
        //   {
        //     model: Users,
        //     attributes: [],
        //   },
        // ],
        group: ["status", db.fn(queryFn, db.col("created_at"))],
      })
    }
    if (!sent_emails) {
      return res.status(400).json({ message: "No Sent Emails was found" })
    }

    return res.status(200).json({ all_sent_emails: sent_emails })
  } catch (error) {
    console.log(error)
  }
}

export const all_visit_duration_analytics = async (req, res) => {
  let { time, client } = req.query

  let start = moment.utc()
  let end = moment.utc()
  let queryFn = "hour"
  switch (time) {
    case "Today":
      start = start.startOf("day")
      end = end.endOf("day")
      queryFn = "hour"
      console.log(start, end)
      break
    case "This Week":
      start = start.startOf("week")
      end = end.endOf("week")
      queryFn = "day"
      console.log(start, end)
      break
    case "This Month":
      start = start.startOf("month")
      end = end.endOf("month")
      queryFn = "day"
      console.log(start, end)
      break
    case "This Year":
      start = start.startOf("year")
      end = end.endOf("year")
      queryFn = "month"
      console.log(start, end)
      break
    default:
      break
  }

  let visit_duration

  try {
    if (client !== "0") {
      visit_duration = await AnalyticsVisitDuration.findAll({
        raw: true,
        nest: true,
        where: {
          created_at: {
            [Op.gte]: start,
            [Op.lte]: end,
          },
        },
        attributes: [
          [
            db.fn("AVG", db.col("analytics_visit_duration.duration")),
            "duration",
          ],
          "duration",
          [
            db.fn(queryFn, db.col("analytics_visit_duration.created_at")),
            "created",
          ],
        ],
        include: [
          {
            model: VirtualExperiences,
            attributes: [],
            where: {
              client_id: client,
            },
          },
        ],
        group: [
          "duration",
          db.fn(queryFn, db.col("analytics_visit_duration.created_at")),
        ],
      })
    } else {
      visit_duration = await AnalyticsVisitDuration.findAll({
        raw: true,
        nest: true,
        where: {
          created_at: {
            [Op.gte]: start,
            [Op.lte]: end,
          },
        },
        attributes: [
          [
            db.fn("AVG", db.col("analytics_visit_duration.duration")),
            "duration",
          ],
          "duration",
          [
            db.fn(queryFn, db.col("analytics_visit_duration.created_at")),
            "created",
          ],
        ],
        group: [
          "duration",
          db.fn(queryFn, db.col("analytics_visit_duration.created_at")),
        ],
      })
    }

    if (!visit_duration) {
      return res.status(400).json({ message: "No Visit Duration was found" })
    }

    return res.status(200).json({ all_visit_durations: visit_duration })
  } catch (error) {
    console.log(error)
  }
}

export const all_video_conference_duration_analytics = async (req, res) => {
  let { time, client } = req.query

  let start = moment.utc()
  let end = moment.utc()
  let queryFn = "hour"
  switch (time) {
    case "Today":
      start = start.startOf("day")
      end = end.endOf("day")
      queryFn = "hour"
      console.log(start, end)
      break
    case "This Week":
      start = start.startOf("week")
      end = end.endOf("week")
      queryFn = "day"
      console.log(start, end)
      break
    case "This Month":
      start = start.startOf("month")
      end = end.endOf("month")
      queryFn = "day"
      console.log(start, end)
      break
    case "This Year":
      start = start.startOf("year")
      end = end.endOf("year")
      queryFn = "month"
      console.log(start, end)
      break
    default:
      break
  }

  let video_duration

  try {
    if (client !== "0") {
      video_duration = await AnalyticsVideoMinutes.findAll({
        raw: true,
        nest: true,
        where: {
          created_at: {
            [Op.gte]: start,
            [Op.lte]: end,
          },
        },
        attributes: [
          [
            db.fn("SUM", db.col("analytics_video_minutes.duration")),
            "duration",
          ],
          "duration",
          [
            db.fn(queryFn, db.col("analytics_video_minutes.created_at")),
            "created",
          ],
        ],
        include: [
          {
            model: Users,
            attributes: [],
            where: {
              client_id: client,
            },
          },
        ],
        group: ["duration", db.fn(queryFn, db.col("created_at"))],
      })
    } else {
      video_duration = await AnalyticsVideoMinutes.findAll({
        raw: true,
        nest: true,
        where: {
          created_at: {
            [Op.gte]: start,
            [Op.lte]: end,
          },
        },
        attributes: [
          [
            db.fn("SUM", db.col("analytics_video_minutes.duration")),
            "duration",
          ],
          "duration",
          [
            db.fn(queryFn, db.col("analytics_video_minutes.created_at")),
            "created",
          ],
        ],
        group: ["duration", db.fn(queryFn, db.col("created_at"))],
      })
    }

    if (!video_duration) {
      return res.status(400).json({ message: "No Video Duration was found" })
    }

    return res.status(200).json({ all_video_durations: video_duration })
  } catch (error) {
    console.log(error)
  }
}

export const all_visitor_analytics = async (req, res) => {
  let { time, client } = req.query

  let start = moment.utc()
  let end = moment.utc()
  let queryFn = "hour"
  switch (time) {
    case "Today":
      start = start.startOf("day")
      end = end.endOf("day")
      queryFn = "hour"
      console.log(start, end)
      break
    case "This Week":
      start = start.startOf("week")
      end = end.endOf("week")
      queryFn = "day"
      console.log(start, end)
      break
    case "This Month":
      start = start.startOf("month")
      end = end.endOf("month")
      queryFn = "day"
      console.log(start, end)
      break
    case "This Year":
      start = start.startOf("year")
      end = end.endOf("year")
      queryFn = "month"
      console.log(start, end)
      break
    default:
      break
  }

  let visitor

  try {
    if (client !== "0") {
      visitor = await AnalyticsVisitDuration.findAll({
        raw: true,
        nest: true,
        where: {
          created_at: {
            [Op.gte]: start,
            [Op.lte]: end,
          },
          customer_id: {
            [Op.ne]: null,
          },
        },
        attributes: [
          [
            db.fn("DISTINCT", db.col("analytics_visit_duration.customer_id")),
            "customer",
          ],
          "customer_id",
          [
            db.fn(queryFn, db.col("analytics_visit_duration.created_at")),
            "created",
          ],
        ],
        include: [
          {
            model: VirtualExperiences,
            attributes: [],
            where: {
              client_id: client,
            },
          },
        ],
        group: ["customer", db.fn(queryFn, db.col("created_at"))],
      })
    } else {
      visitor = await AnalyticsVisitDuration.findAll({
        raw: true,
        nest: true,
        where: {
          created_at: {
            [Op.gte]: start,
            [Op.lte]: end,
          },
          customer_id: {
            [Op.ne]: null,
          },
        },
        attributes: [
          [
            db.fn("DISTINCT", db.col("analytics_visit_duration.customer_id")),
            "customer",
          ],
          "customer_id",
          [
            db.fn(queryFn, db.col("analytics_visit_duration.created_at")),
            "created",
          ],
        ],
        group: [
          "customer",
          db.fn(queryFn, db.col("analytics_visit_duration.created_at")),
        ],
      })
    }

    if (!visitor) {
      return res.status(400).json({ message: "No Visitor was found" })
    }

    return res.status(200).json({ all_visitors: visitor })
  } catch (error) {
    console.log(error)
  }
}

export const all_product_preview_analytics = async (req, res) => {
  let { time, client } = req.query

  let start = moment.utc()
  let end = moment.utc()
  let queryFn = "hour"
  switch (time) {
    case "Today":
      start = start.startOf("day")
      end = end.endOf("day")
      queryFn = "hour"
      console.log(start, end)
      break
    case "This Week":
      start = start.startOf("week")
      end = end.endOf("week")
      queryFn = "day"
      console.log(start, end)
      break
    case "This Month":
      start = start.startOf("month")
      end = end.endOf("month")
      queryFn = "day"
      console.log(start, end)
      break
    case "This Year":
      start = start.startOf("year")
      end = end.endOf("year")
      queryFn = "month"
      console.log(start, end)
      break
    default:
      break
  }

  let product_preview

  try {
    if (client !== "0") {
      product_preview = await AnalyticsProductPreview.findAll({
        raw: true,
        nest: true,
        where: {
          created_at: {
            [Op.gte]: start,
            [Op.lte]: end,
          },
          customer_id: {
            [Op.ne]: null,
          },
          client_id: client,
        },
        attributes: [[db.fn("COUNT", db.col("name")), "preview"], "name"],
        group: ["name"],
      })
    } else {
      product_preview = await AnalyticsProductPreview.findAll({
        raw: true,
        nest: true,
        where: {
          created_at: {
            [Op.gte]: start,
            [Op.lte]: end,
          },
          customer_id: {
            [Op.ne]: null,
          },
        },
        attributes: [[db.fn("COUNT", db.col("name")), "preview"], "name"],

        group: ["name"],
      })
    }

    if (!product_preview) {
      return res.status(400).json({ message: "No Preview was found" })
    }

    return res.status(200).json({ all_previews: product_preview })
  } catch (error) {
    console.log(error)
  }
}

export const all_product_quantity_analytics = async (req, res) => {
  let { time, client } = req.query

  let start = moment.utc()
  let end = moment.utc()
  let queryFn = "hour"
  switch (time) {
    case "Today":
      start = start.startOf("day")
      end = end.endOf("day")
      queryFn = "hour"
      console.log(start, end)
      break
    case "This Week":
      start = start.startOf("week")
      end = end.endOf("week")
      queryFn = "day"
      console.log(start, end)
      break
    case "This Month":
      start = start.startOf("month")
      end = end.endOf("month")
      queryFn = "day"
      console.log(start, end)
      break
    case "This Year":
      start = start.startOf("year")
      end = end.endOf("year")
      queryFn = "month"
      console.log(start, end)
      break
    default:
      break
  }

  let product_quantity

  try {
    if (client !== "0") {
      product_quantity = await AnalyticsProductQuantity.findAll({
        raw: true,
        nest: true,
        where: {
          created_at: {
            [Op.gte]: start,
            [Op.lte]: end,
          },
          customer_id: {
            [Op.ne]: null,
          },
        },
        include: [
          {
            model: VirtualExperiences,
            where: {
              client_id: client,
            },
          },
        ],
        attributes: [
          [db.fn("SUM", db.col("analytics_product_quantity.count")), "count"],
          "count",
          [
            db.fn(queryFn, db.col("analytics_product_quantity.created_at")),
            "created",
          ],
        ],
        group: ["count", db.fn(queryFn, db.col("created_at"))],
      })
    } else {
      product_quantity = await AnalyticsProductQuantity.findAll({
        raw: true,
        nest: true,
        where: {
          created_at: {
            [Op.gte]: start,
            [Op.lte]: end,
          },
        },
        attributes: [
          [db.fn("SUM", db.col("analytics_product_quantity.count")), "count"],
          "count",
          [
            db.fn(queryFn, db.col("analytics_product_quantity.created_at")),
            "created",
          ],
        ],
        group: ["count", db.fn(queryFn, db.col("created_at"))],
      })
    }

    if (!product_quantity) {
      return res.status(400).json({ message: "No Quantity was found" })
    }

    return res.status(200).json({ all_quantity: product_quantity })
  } catch (error) {
    console.log(error)
  }
}

export const all_virtual_flythrough_analytics = async (req, res) => {
  let { time, client } = req.query

  let start = moment.utc()
  let end = moment.utc()
  let queryFn = "hour"
  switch (time) {
    case "Today":
      start = start.startOf("day")
      end = end.endOf("day")
      queryFn = "hour"
      console.log(start, end)
      break
    case "This Week":
      start = start.startOf("week")
      end = end.endOf("week")
      queryFn = "day"
      console.log(start, end)
      break
    case "This Month":
      start = start.startOf("month")
      end = end.endOf("month")
      queryFn = "day"
      console.log(start, end)
      break
    case "This Year":
      start = start.startOf("year")
      end = end.endOf("year")
      queryFn = "month"
      console.log(start, end)
      break
    default:
      break
  }

  let product_quantity

  try {
    if (client !== "0") {
      product_quantity = await AnalyticsVirtualFlythrough.findAll({
        raw: true,
        nest: true,
        where: {
          created_at: {
            [Op.gte]: start,
            [Op.lte]: end,
          },
        },
        include: [
          {
            model: VirtualFlythrough,
            // where: {
            //   client_id: client,
            // },
          },
          {
            model: Customers,
            // where: {
            //   client_id: client,
            // },
          },
          {
            model: InvitationFlythrough,
            // where: {
            //   client_id: client,
            // },
          },
        ],
        attributes: [
          [
            db.fn("SUM", db.col("analytics_virtual_flythrough.video_duration")),
            "video_duration",
          ],
          "video_duration",
          [
            db.fn(queryFn, db.col("analytics_virtual_flythrough.created_at")),
            "created",
          ],
        ],
        group: ["video_duration", db.fn(queryFn, db.col("created_at"))],
      })
    } else {
      product_quantity = await AnalyticsVirtualFlythrough.findAll({
        raw: true,
        nest: true,
        where: {
          created_at: {
            [Op.gte]: start,
            [Op.lte]: end,
          },
        },
        include: [
          {
            model: VirtualFlythrough,
            // where: {
            //   client_id: client,
            // },
          },
          {
            model: Customers,
            // where: {
            //   client_id: client,
            // },
          },
          {
            model: InvitationFlythrough,
            // where: {
            //   client_id: client,
            // },
          },
        ],
        attributes: [
          [
            db.fn("SUM", db.col("analytics_virtual_flythrough.watch_duration")),
            "watch_duration",
          ],
          [
            db.fn(queryFn, db.col("analytics_virtual_flythrough.created_at")),
            "created",
          ],
        ],
        group: ["watch_duration", db.fn(queryFn, db.col("created_at"))],
      })
    }

    if (!product_quantity) {
      return res.status(400).json({ message: "No Quantity was found" })
    }

    return res.status(200).json({ all_quantity: product_quantity })
  } catch (error) {
    console.log(error)
  }
}
