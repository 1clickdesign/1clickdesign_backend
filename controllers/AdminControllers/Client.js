import { Op } from "sequelize";
import Clients from "../../models/ClientModel.js";
import Roles from "../../models/RoleModel.js";
import Users from "../../models/UserModel.js";
import Permissions from "../../models/PermissionModel.js";
import { S3Client, DeleteObjectsCommand } from "@aws-sdk/client-s3";
import ClientCategories from "../../models/ClientCategoryModel.js";

export const get_all_clients = async (req, res) => {
  let query = req.query;

  let clients;

  try {
    if (query && query.search !== "null") {
      clients = await Clients.findAndCountAll({
        offset: parseInt(query.offset),
        limit: parseInt(query.limit),
        nest: true,
        where: {
          [Op.or]: {
            name: {
              [Op.substring]: query.search,
            },
            email: {
              [Op.substring]: query.search,
            },
          },
        },
        include: [
          {
            model: Users,
            attributes: ["id", "first_name", "last_name"],
          },
        ],
      });
    } else {
      clients = await Clients.findAndCountAll({
        offset: parseInt(query.offset),
        limit: parseInt(query.limit),
        nest: true,
        include: [
          {
            model: Users,
            attributes: ["id", "first_name", "last_name"],
          },
        ],
      });
    }

    if (!clients) {
      return res.status(400).json({ message: "No Clients found" });
    }

    return res.status(200).json({ clients: clients });
  } catch (error) {
    console.log("Error: ", error);
  }
};

export const get_client = async (req, res) => {
  let { id } = req.params;

  let client;

  try {
    client = await Clients.findOne({
      where: {
        id: id,
      },
      plain: true,
      nest: true,
      include: [
        {
          model: Users,
          attributes: ["id", "first_name", "last_name"],
        },
        {
          model: ClientCategories,
          attributes: ["id", "name"],
        },
      ],
    });

    if (!client) {
      return res.status(400).json({ message: "No Client found" });
    }

    return res.status(200).json(client);
  } catch (error) {
    console.log(error);
  }
};

export const get_all_clients_option = async (req, res) => {
  try {
    let clients = await Clients.findAll({
      nest: true,
      where: {
        status: "Active",
      },
      attributes: ["id", "name", "logo_url", "description"],
      include: [
        {
          model: Users,
        },
      ],
    });

    if (!clients) {
      return res.status(400).json({ message: "No Clients found" });
    }

    return res.status(200).json({ clients: clients });
  } catch (error) {
    console.log("Error: ", error);
  }
};

export const get_client_account_holder = async (req, res) => {
  let { id } = req.params;

  let client;

  try {
    client = await Clients.findOne({
      where: {
        id: id,
      },
      plain: true,
      nest: true,
      attributes: [],
      include: [
        {
          model: Users,
          include: [
            {
              model: Roles,
              attributes: ["id", "name"],
              include: [
                {
                  model: Permissions,
                  attributes: ["id", "name"],
                },
              ],
            },
            {
              model: Clients,
              attributes: ["id", "name"],
            },
            {
              model: Users,
              as: "user",
              attributes: ["id", "first_name", "middle_name", "last_name"],
            },
          ],
        },
      ],
    });

    if (!client) {
      return res
        .status(400)
        .json({ message: "No Client Account Holder found" });
    }

    return res.status(200).json(client);
  } catch (error) {
    console.log(error);
  }
};

export const create_client = async (req, res) => {
  const body = JSON.parse(JSON.stringify(req.body));
  const files = req.files;

  if (!files && !body) {
    return res.status(400).json();
  }

  console.log("Body ", body);
  // console.log(files);
  const data = body.data;
  // console.log(data);

  let {
    name,
    email,
    website_url,
    free_video_conference_minutes,
    free_emails,
    latitude,
    longitude,
    status,
    user_limit,
    automatic_approved_requests,
    description,
    country,
    state,
    city,
    region,
    zip_code,
    place_id,
    additional_minutes_charge,
    additional_minutes_charge_every,
    additional_email_charge,
    additional_email_charge_every,
    additional_user_charge,
    package_id,
    account_holder_id,
    phone_number,
    category_id,
    created_by,
  } = JSON.parse(data);

  if (
    !name ||
    !email ||
    !website_url ||
    !user_limit ||
    parseInt(user_limit) <= 0 ||
    !description ||
    !free_video_conference_minutes ||
    parseInt(free_video_conference_minutes) < 0 ||
    !phone_number ||
    !created_by
  ) {
    return res.status(400).json({ message: "Incomplete Information" });
  } else {
    try {
      const client = await Clients.create({
        name: name,
        email: email,
        logo_url: files[0].location,
        website_url: website_url,
        free_video_conference_minutes: parseInt(free_video_conference_minutes),
        free_emails: parseInt(free_emails),
        latitude: parseFloat(latitude),
        longitude: parseFloat(longitude),
        status: status,
        user_limit: parseInt(user_limit),
        automatic_approved_requests: Boolean(automatic_approved_requests),
        additional_minutes_charge: parseInt(additional_minutes_charge),
        additional_minutes_charge_every: parseInt(
          additional_minutes_charge_every
        ),
        additional_email_charge: parseInt(additional_email_charge),
        additional_email_charge_every: parseInt(additional_email_charge_every),
        additional_user_charge: parseFloat(additional_user_charge),
        package_id: package_id,
        account_holder_id: account_holder_id,
        client_category_id: category_id,
        description: description,
        phone_number: phone_number,
        country: country,
        state: state,
        city: city,
        region: region,
        zip_code: zip_code,
        place_id: place_id,
        created_by: created_by,
      });

      if (!client) {
        return res
          .status(400)
          .json({ message: "Client was not successfully created" });
      }
      return res
        .status(201)
        .json({ message: "Client was successfully created" });
    } catch (error) {
      console.log(error);
    }
  }
};

export const check_if_client_unique_name = async (req, res) => {
  const { name, email } = req.query;

  if (name) {
    try {
      const check_name = await Clients.findOne({
        where: {
          name: name,
        },
      });
      console.log(check_name);

      if (!check_name) {
        return res.status(204).json();
      }
      return res.status(200).json();
    } catch (error) {
      console.log(error);
      return res.status(400).json();
    }
  } else if (email) {
    try {
      const check_name = await Clients.findOne({
        where: {
          email: email,
        },
      });
      console.log(check_name);

      if (!check_name) {
        return res.status(204).json();
      }
      return res.status(200).json();
    } catch (error) {
      console.log(error);
      return res.status(400).json();
    }
  }
};

export const edit_client = async (req, res) => {
  const { id } = req.params;

  let {
    name,
    email,
    website_url,
    free_video_conference_minutes,
    free_emails,
    latitude,
    longitude,
    status,
    user_limit,
    automatic_approved_requests,
    description,
    country,
    state,
    city,
    region,
    logo_url,
    logo_urls,
    zip_code,
    place_id,
    additional_minutes_charge,
    additional_minutes_charge_every,
    additional_email_charge,
    additional_email_charge_every,
    additional_user_charge,
    package_id,
    account_holder_id,
    category_id,
    phone_number,
    updated_by,
  } = req.body;

  try {
    let client = await Clients.update(
      {
        name: name,
        email: email,
        website_url: website_url,
        free_video_conference_minutes: parseInt(free_video_conference_minutes),
        free_emails: parseInt(free_emails),
        latitude: parseFloat(latitude),
        longitude: parseFloat(longitude),
        status: status,
        user_limit: parseInt(user_limit),
        automatic_approved_requests: Boolean(automatic_approved_requests),
        additional_minutes_charge: parseInt(additional_minutes_charge),
        additional_minutes_charge_every: parseInt(
          additional_minutes_charge_every
        ),
        additional_email_charge: parseInt(additional_email_charge),
        additional_email_charge_every: parseInt(additional_email_charge_every),
        additional_user_charge: parseFloat(additional_user_charge),
        package_id: package_id,
        account_holder_id: account_holder_id,
        client_category_id: category_id,
        description: description,
        phone_number: phone_number,
        country: country,
        state: state,
        city: city,
        region: region,
        zip_code: zip_code,
        place_id: place_id,
        updated_by: updated_by,
      },
      {
        where: {
          id: id,
        },
      }
    );

    if (!client) {
      return res
        .status(400)
        .json({ message: "Client was not successfully edited" });
    }

    return res.status(200).json({ message: "Client was successfully edited" });
  } catch (error) {
    console.log(error);
  }
};

export const edit_client_with_file = async (req, res) => {
  const body = JSON.parse(JSON.stringify(req.body));
  const files = req.files;

  console.log("Body ", body);
  console.log(files);
  const data = JSON.parse(body.data);
  // console.log(data);

  const { id } = req.params;

  let {
    name,
    email,
    website_url,
    free_video_conference_minutes,
    free_emails,
    latitude,
    longitude,
    status,
    user_limit,
    automatic_approved_requests,
    description,
    country,
    state,
    city,
    region,
    logo_url,
    logo_urls,
    zip_code,
    place_id,
    additional_minutes_charge,
    additional_minutes_charge_every,
    additional_email_charge,
    additional_email_charge_every,
    additional_user_charge,
    package_id,
    account_holder_id,
    category_id,
    phone_number,
    updated_by,
  } = data;

  if (
    !name ||
    !email ||
    !website_url ||
    !user_limit ||
    parseInt(user_limit) <= 0 ||
    !description ||
    !free_video_conference_minutes ||
    parseInt(free_video_conference_minutes) < 0 ||
    !phone_number
  ) {
    return res.status(400).json({ message: "Incomplete Information" });
  } else {
    let client;
    try {
      if (logo_url === files[0].location) {
        console.log("HI");
        client = await Clients.update(
          {
            name: name,
            email: email,
            website_url: website_url,
            free_video_conference_minutes: parseInt(
              free_video_conference_minutes
            ),
            free_emails: parseInt(free_emails),
            latitude: parseFloat(latitude),
            longitude: parseFloat(longitude),
            status: status,
            user_limit: parseInt(user_limit),
            automatic_approved_requests: Boolean(automatic_approved_requests),
            additional_minutes_charge: parseInt(additional_minutes_charge),
            additional_minutes_charge_every: parseInt(
              additional_minutes_charge_every
            ),
            additional_email_charge: parseInt(additional_email_charge),
            additional_email_charge_every: parseInt(
              additional_email_charge_every
            ),
            additional_user_charge: parseFloat(additional_user_charge),
            package_id: package_id,
            account_holder_id: account_holder_id,
            client_category_id: category_id,
            description: description,
            phone_number: phone_number,
            country: country,
            state: state,
            city: city,
            region: region,
            zip_code: zip_code,
            place_id: place_id,
            updated_by: updated_by,
          },
          {
            where: {
              id: id,
            },
          }
        );
      } else {
        console.log("HEYYYY");
        client = await Clients.update(
          {
            name: name,
            email: email,
            logo_url: files[0].location,
            website_url: website_url,
            free_video_conference_minutes: parseInt(
              free_video_conference_minutes
            ),
            free_emails: parseInt(free_emails),
            latitude: parseFloat(latitude),
            longitude: parseFloat(longitude),
            status: status,
            user_limit: parseInt(user_limit),
            automatic_approved_requests: Boolean(automatic_approved_requests),
            additional_minutes_charge: parseInt(additional_minutes_charge),
            additional_minutes_charge_every: parseInt(
              additional_minutes_charge_every
            ),
            additional_email_charge: parseInt(additional_email_charge),
            additional_email_charge_every: parseInt(
              additional_email_charge_every
            ),
            additional_user_charge: parseFloat(additional_user_charge),
            package_id: package_id,
            account_holder_id: account_holder_id,
            client_category_id: category_id,
            description: description,
            phone_number: phone_number,
            country: country,
            state: state,
            city: city,
            region: region,
            zip_code: zip_code,
            place_id: place_id,
            updated_by: updated_by,
          },
          {
            where: {
              id: id,
            },
          }
        );

        const bucketName = process.env.S3_BUCKET;

        const params = {
          Bucket: bucketName,
          Delete: {
            Objects: logo_urls.map((key) => ({ Key: key })),
          },
        };

        const s3 = new S3Client({
          region: process.env.S3_REGION,
          credentials: {
            accessKeyId: process.env.AWS_ACCESS_KEY_ID,
            secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY,
          },
        });
        const s3_delete = await s3.send(new DeleteObjectsCommand(params));
        console.log(s3_delete);
      }

      if (!client) {
        return res
          .status(400)
          .json({ message: "Client was not successfully edited" });
      }
      return res
        .status(201)
        .json({ message: "Client was successfully edited" });
    } catch (error) {
      console.log(error);
    }
  }
};

export const delete_client = async (req, res) => {
  let { id } = req.params;

  console.log(id);

  try {
    const client = await Clients.destroy({
      where: {
        id: id,
      },
    });

    if (!client) {
      return res
        .status(400)
        .json({ message: "Client was not successfully deleted" });
    }

    return res.status(200).json({ message: "Client was successfully deleted" });
  } catch (error) {
    console.log(error);
  }
};
