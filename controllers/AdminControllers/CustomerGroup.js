import CustomerGroupMembers from "../../models/CustomerGroupMemberModel.js";
import CustomerGroups from "../../models/CustomerGroupModel.js";
import Customers from "../../models/CustomerModel.js";

import {Op} from "sequelize"


export const get_all_customer_groups = async (req, res) => {
  
  let query = req.query;

  let customer_groups;

  try {
    if(query && query.search !== "null") {
      customer_groups = await CustomerGroups.findAndCountAll({
        offset: parseInt(query.offset),
        limit: parseInt(query.limit),
        nest: true,
        where: {
          name: {
            [Op.substring]: query.search
          }
        },
        include: [
          {
            model: Customers,
            as: "customer_group",
            through: {
              attributes: ["customer_id", "customer_group_id"],
            },
          }
        ]
      })
    } else {
      customer_groups = await CustomerGroups.findAndCountAll({
        offset: parseInt(query.offset),
        limit: parseInt(query.limit),
        nest: true,
        include: [
          {
            model: Customers,
            as: "customer_group",
            through: {
              attributes: ["customer_id", "customer_group_id"],
            },
          }
        ]
      })
    }

    if(!customer_groups) {
      return res.status(400).json({message: "No Customer Groups found"})
    }

    return res.status(200).json({customer_groups: customer_groups})

  } catch (error) {
    console.log(error)
  }
}

export const get_all_customer_groups_option = async (req, res) => {
  
  try {
    let customer_groups = await CustomerGroups.findAll({
      nest: true,
      where: {
        status: "Active"
      }
    })

    if(!customer_groups) {
      return res.status(400).json({message: "No Customer Groups found"})
    }

    return res.status(200).json({customer_groups: customer_groups})

  } catch (error) {
    console.log("Error: ", error)
  }
}

export const get_customer_group = async (req, res) => {
  
  let {id} = req.params;

  let customer_group;

  try {
    customer_group = await CustomerGroups.findOne({
      where: {
        id: id
      },
      plain: true,
      nest: true,
      include: [
        {
          model: Customers,
          as: "customer_group",
          through: {
            attributes: ["customer_id", "customer_group_id"],
          },
        }
      ]
    })

    if(!customer_group) {
      return res.status(400).json({message: "No Customer Group found"})
    }

    return res.status(200).json(customer_group)

  } catch (error) {
    console.log(error)
  }
}

export const create_customer_group = async (req, res) => {
  
  let {name, description, status, created_by} = req.body;

  if(!name || !created_by) {
    return res.status(400).json({message: "Incomplete Information"})
  } else {
    try {
      const customer_group = await CustomerGroups.create({
        name: name,
        description: description,
        status: status,
        created_by: created_by
      })

      if(!customer_group) {
        return res.status(400).json({message: "Customer Group was not successfully created"})
      }
      return res.status(201).json({message: "Customer Group was successfully created"})
    } catch (error) {
      console.log("error ", error)
    }
  }

}

export const delete_customer_group = async (req, res) => {
  
  const {id} = req.params;
  try {
    const group = await CustomerGroups.destroy({
      where: {
        id: id
      }
    })

    if(!group) {
      return res.status(400).json({message: "Customer Group was not successfully deleted"})
    }

    return res.status(200).json({message: "Customer Group was successfully deleted"})
  } catch (error) {
    console.log(error)
  }
}