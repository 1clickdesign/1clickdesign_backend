import VirtualExperienceTypes from "../../models/VirtualExperienceTypeModel.js";
import VirtualExperienceType from "../../models/VirtualExperienceTypeModel.js";
import {Op} from "sequelize"

export const get_all_types = async (req, res) => {
  
  let query = req.query;

  let types;

  try {
    if(query && query.search !== "null") {
      types = await VirtualExperienceType.findAndCountAll({
        offset: parseInt(query.offset),
        limit: parseInt(query.limit),
        nest: true,
        where: {
          name: {
            [Op.substring]: query.search
          }
        }
      })
    } else {
      types = await VirtualExperienceType.findAndCountAll({
        offset: parseInt(query.offset),
        limit: parseInt(query.limit),
        nest: true,
      })
    }

    console.log(types)
    console.log(query)

    if(!types) {
      return res.status(400).json({message: "No Types found"})
    }

    return res.status(200).json({types: types})

  } catch (error) {
    console.log("Error: ", error)
  }
}

export const get_all_types_option = async (req, res) => {
  
  try {
    let types = await VirtualExperienceType.findAll({
      nest: true,
      where: {
        status: "Active"
      }
    })

    if(!types) {
      return res.status(400).json({message: "No Type found"})
    }

    return res.status(200).json({types: types})

  } catch (error) {
    console.log("Error: ", error)
  }
}

export const get_type = async (req, res) => {
  
  let {id} = req.params;

  let type;

  try {
    type = await VirtualExperienceType.findOne({
      where: {
        id: id
      },
      plain: true,
      nest: true,
    })

    if(!type) {
      return res.status(400).json({message: "No Type found"})
    }

    return res.status(200).json(type)

  } catch (error) {
    
  }
}

export const check_if_type_unique_name = async (req, res) => {

  const {name} = req.query;

  try {
    const check_name = await VirtualExperienceTypes.findOne({
      where: {
        name: name
      }
    })
    if(!check_name) {
      return res.status(204).json()
    }
    return res.status(200).json()
  } catch (error) {
    console.log(error)
    return res.status(400).json();
  }
}

export const create_type = async (req, res) => {
  
  let {name, description, status, created_by} = req.body;

  if(!name || !created_by) {
    return res.status(400).json({message: "Incomplete Information"})
  } else {
    try {
      const type = await VirtualExperienceType.create({
        name: name,
        description: description,
        status: status,
        created_by: created_by
      })

      if(!type) {
        return res.status(400).json({message: "Type was not successfully created"})
      }
      return res.status(201).json({message: "Type was successfully created"})
    } catch (error) {
      
    }
  }

}

export const edit_type = async (req, res) => {
  const {id} = req.params;
  
  const {name, description, status, updated_by} = req.body;

  try {
    const type = await VirtualExperienceType.update({
      name: name,
      description: description,
      status: status,
      updated_by: updated_by
    }, {
      where: {
        id: id
      }
    })

    if(!type) {
      return res.status(400).json({message: "Type was not successfully edited"})
    }

    return res.status(200).json({message: "Type was successfully edited"})
  } catch (error) {
    console.log(error)
  }

}

export const delete_type = async (req, res) => {

  const {id} = req.params;

  try {
    await VirtualExperienceType.destroy({
      where: {
        id: id,
      }
    })
    return res.status(200).json({message: "Type was successfully deleted"})
  } catch (error) {
    console.log(error)
    return res.status(400).json();
  }
}