
import {Op} from "sequelize"
import ClientCategories from "../../models/ClientCategoryModel.js";

export const get_all_client_categories = async (req, res) => {
  
  let query = req.query;

  let client_categories;

  try {
    if(query && query.search !== "null") {
      client_categories = await ClientCategories.findAndCountAll({
        offset: parseInt(query.offset),
        limit: parseInt(query.limit),
        nest: true,
        where: {
          name: {
            [Op.substring]: query.search
          }
        }
      })
    } else {
      client_categories = await ClientCategories.findAndCountAll({
        offset: parseInt(query.offset),
        limit: parseInt(query.limit),
        nest: true,
      })
    }


    if(!client_categories) {
      return res.status(400).json({message: "No Client Categories found"})
    }

    return res.status(200).json({categories: client_categories})

  } catch (error) {
    console.log("Error: ", error)
  }
}

export const get_all_client_categories_option = async (req, res) => {
  
  try {
    let client_categories = await ClientCategories.findAll({
      nest: true,
      where: {
        status: "Active"
      }
    })

    if(!client_categories) {
      return res.status(400).json({message: "No Client Categories found"})
    }

    return res.status(200).json({categories: client_categories})

  } catch (error) {
    console.log("Error: ", error)
  }
}

export const get_client_category = async (req, res) => {
  
  let {id} = req.params;

  let client_category;

  try {
    client_category = await ClientCategories.findOne({
      where: {
        id: id
      },
      plain: true,
      nest: true,
    })

    if(!client_category) {
      return res.status(400).json({message: "No Client Category found"})
    }

    return res.status(200).json(client_category)

  } catch (error) {
    console.log(error)
  }
}

export const check_if_client_category_unique_name = async (req, res) => {

  const {name} = req.query;

  try {
    const check_name = await ClientCategories.findOne({
      where: {
        name: name
      }
    })
    if(!check_name) {
      return res.status(204).json()
    }
    return res.status(200).json()
  } catch (error) {
    console.log(error)
    return res.status(400).json();
  }
}

export const create_client_category = async (req, res) => {
  
  let {name, description, status, created_by} = req.body;

  if(!name || !created_by) {
    return res.status(400).json({message: "Incomplete Information"})
  } else {
    try {
      const client_category = await ClientCategories.create({
        name: name,
        description: description,
        status: status,
        created_by: created_by
      })

      if(!client_category) {
        return res.status(400).json({message: "Client Category was not successfully created"})
      }
      return res.status(201).json({message: "Client Category was successfully created"})
    } catch (error) {
      
    }
  }

}

export const edit_client_category = async (req, res) => {
  const {id} = req.params;
  
  const {name, description, status, updated_by} = req.body;

  try {
    const client_category = await ClientCategories.update({
      name: name,
      description: description,
      status: status,
      updated_by: updated_by
    }, {
      where: {
        id: id
      }
    })

    if(!client_category) {
      return res.status(400).json({message: "Client Category was not successfully edited"})
    }

    return res.status(200).json({message: "Client Category was successfully edited"})
  } catch (error) {
    console.log(error)
  }

}

export const delete_client_category = async (req, res) => {

  const {id} = req.params;

  try {
    await ClientCategories.destroy({
      where: {
        id: id,
      }
    })
    return res.status(200).json({message: "Client Category was successfully deleted"})
  } catch (error) {
    console.log(error)
    return res.status(400).json();
  }
}