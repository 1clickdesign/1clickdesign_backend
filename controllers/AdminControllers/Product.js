import Clients from "../../models/ClientModel.js";
import ProductCategories from "../../models/ProductCategoryModel.js";
import Products from "../../models/ProductModel.js";
import ProductNodes from "../../models/ProductNodeModel.js";
import VirtualExperiences from "../../models/VirtualExperienceModel.js";
import VirtualExperienceTypes from "../../models/VirtualExperienceTypeModel.js";
import { Op } from "sequelize";

export const get_all_products = async (req, res) => {
  let query = req.query;

  let products;

  try {
    if (query && query.search !== "null") {
      if (req.query.id !== "null") {
        if (!!req.query.filter) {
          products = await Products.findAndCountAll({
            offset: parseInt(query.offset),
            limit: parseInt(query.limit),
            nest: true,
            where: {
              client_id: req.query.id,
              virtual_experience_id: req.query.filter,
              [Op.or]: {
                name: {
                    [Op.substring]: query.search,
                  },
                  model: {
                    [Op.substring]: query.search,
                  }
                }
            },
            include: [
              {
                model: VirtualExperiences,
                attributes: ["id", "name"],
              },
              {
                model: Clients,
                attributes: ["id", "name"],
              },
            ],
          });
        } else {
          products = await Products.findAndCountAll({
            offset: parseInt(query.offset),
            limit: parseInt(query.limit),
            nest: true,
            where: {
              client_id: req.query.id,
              [Op.or]: {
                name: {
                    [Op.substring]: query.search,
                  },
                  model: {
                    [Op.substring]: query.search,
                  }
                }
            },
            include: [
              {
                model: VirtualExperiences,
                attributes: ["id", "name"],
              },
              {
                model: Clients,
                attributes: ["id", "name"],
              },
            ],
          });
        }

      } else {
        if (!!req.query.filter) {
          products = await Products.findAndCountAll({
            offset: parseInt(query.offset),
            limit: parseInt(query.limit),
            nest: true,
            where: {
              virtual_experience_id: req.query.filter,
              [Op.or]: {
                name: {
                  [Op.substring]: query.search,
                },
                model: {
                  [Op.substring]: query.search,
                }
              }
            },
            include: [
              {
                model: VirtualExperiences,
                attributes: ["id", "name"],
              },
              {
                model: Clients,
                attributes: ["id", "name"],
              },
            ],
          });          
        } else {
          products = await Products.findAndCountAll({
            offset: parseInt(query.offset),
            limit: parseInt(query.limit),
            nest: true,
            where: {
              [Op.or]: {
                name: {
                  [Op.substring]: query.search,
                },
                model: {
                  [Op.substring]: query.search,
                }
              }
            },
            include: [
              {
                model: VirtualExperiences,
                attributes: ["id", "name"],
              },
              {
                model: Clients,
                attributes: ["id", "name"],
              },
            ],
          });          
        }

      }
    } else {
      if (query.id !== "null") {
        if (!!req.query.filter) {
          products = await Products.findAndCountAll({
            offset: parseInt(query.offset),
            limit: parseInt(query.limit),
            nest: true,
            where: {
              client_id: query.id,
              virtual_experience_id: req.query.filter
            },
            include: [
              {
                model: VirtualExperiences,
                attributes: ["id", "name"],
              },
              {
                model: Clients,
                attributes: ["id", "name"],
              },
            ],
          });
        }
        else {
          products = await Products.findAndCountAll({
            offset: parseInt(query.offset),
            limit: parseInt(query.limit),
            nest: true,
            where: {
              client_id: query.id,
            },
            include: [
              {
                model: VirtualExperiences,
                attributes: ["id", "name"],
              },
              {
                model: Clients,
                attributes: ["id", "name"],
              },
            ],
          });          
        }

      } else {
        if (!!req.query.filter) {
          products = await Products.findAndCountAll({
            offset: parseInt(query.offset),
            limit: parseInt(query.limit),
            nest: true,
            where: {
              virtual_experience_id: req.query.filter
            },
            include: [
              {
                model: VirtualExperiences,
                attributes: ["id", "name"],
              },
              {
                model: Clients,
                attributes: ["id", "name"],
              },
            ],
          });
        } else {
          products = await Products.findAndCountAll({
            offset: parseInt(query.offset),
            limit: parseInt(query.limit),
            nest: true,
            include: [
              {
                model: VirtualExperiences,
                attributes: ["id", "name"],
              },
              {
                model: Clients,
                attributes: ["id", "name"],
              },
            ],
          });          
        }

      }
    }

    if (!products) {
      return res.status(400).json({ message: "No Products found" });
    }

    return res.status(200).json({ products: products });
  } catch (error) {
    console.log("Error: ", error);
  }
};


export const get_all_product_names = async (req, res) => {
  let products
  let test
  try {
    products = await Products.findAndCountAll({
      nest: true,
      where: {
        virtual_experience_id: req.query.filter
      },
      attributes: ["model"]

    });

    test = products.rows.map((product) => {
      return product.model
    })

  } catch (error) {
    console.log("Error: ", error);
  }

    if (!products) {
      return res.status(400).json({ message: "No Products found" });
    }

    return res.status(200).json({ products: test });

}

export const get_product = async (req, res) => {
  let { id } = req.params;

  let product;

  try {
    product = await Products.findOne({
      where: {
        id: id,
      },
      plain: true,
      nest: true,
      include: [
        {
          model: VirtualExperiences,
          attributes: ["id", "name"],
          include: [
            {
              model: VirtualExperienceTypes,
              attributes: ["id", "name"],
            },
          ],
        },
        {
          model: Clients,
          attributes: ["id", "name"],
        },
        {
          model: ProductCategories,
          attributes: ["id", "name"],
        },
      ],
    });

    if (!product) {
      return res.status(400).json({ message: "No Product found" });
    }

    return res.status(200).json(product);
  } catch (error) {
    console.log(error);
  }
};

export const get_product_show_price = async (req, res) => {
  const { id } = req.query;
  try {
    const product_count = await Products.count({
      where: { status: "Active", client_id: id },
    });
    const show_price = await Products.count({
      where: { show_price: true, status: "Active", client_id: id },
    });
    console.log(show_price);
    if (show_price !== product_count) {
      return res.status(200).json({ show: false });
    }

    return res.status(200).json({ show: true });
  } catch (error) {
    console.log(error);
  }
};

export const update_product_show_price = async (req, res) => {
  const { show_price, client_id } = req.body;
  try {
    const product = await Products.update(
      {
        show_price: Boolean(show_price),
      },
      {
        where: {
          client_id: client_id,
        },
      }
    );

    if (!product) return res.status(400).json();
    return res.status(200).json();
  } catch (error) { }
};

export const get_all_product_option = async (req, res) => {
  try {
    let products = await Products.findAll({
      nest: true,
      where: {
        status: "Active",
      },
      attributes: ["id", "name"],
      include: [
        {
          model: ProductNodes,
          attributes: ["product_id"],
        },
      ],
    });

    if (!products) {
      return res.status(400).json({ message: "No Products found" });
    }

    return res.status(200).json({ products: products });
  } catch (error) {
    console.log("Error: ", error);
  }
};

export const check_if_product_unique_name = async (req, res) => {
  const { name, sku, model } = req.query;

  let check_if_unique;

  try {
    if (name) {
      check_if_unique = await Products.findOne({
        where: {
          name: name,
        },
      });
    } else if (sku) {
      check_if_unique = await Products.findOne({
        where: {
          sku: sku,
        },
      });
    } else if (model) {
      check_if_unique = await Products.findOne({
        where: {
          model: model,
        },
      });
    }
    if (!check_if_unique) {
      return res.status(204).json();
    }
    return res.status(200).json();
  } catch (error) {
    console.log(error);
    return res.status(400).json();
  }
};

export const create_product_v2 = async (req, res) => {

  if (!req.body) {
    return res.status(400).json()
  }  

  const data = JSON.parse(JSON.stringify(req.body));

  let {
    name,
    sku,
    price,
    model,
    description,
    discount,
    ecommerce_website_url,
    anchor,
    product_stock_api,
    dimension_height,
    dimension_width,
    dimension_length,
    dimension_depth,
    dimension_diameter,
    dimension_unit,
    weight,
    weight_unit,
    in_stock,
    show_price,
    client_id,
    virtual_experience_id,
    status,
    real_estate_id,
    application,
    finish,
    material,
    variations,
    color,
    category,
    manufacturer,
    created_by,
    category_id,
    brand,
    glb_files,
    photo_files,
    video_files,
    threed_photo_files,
  } = data;

  if (!name || !model || !description || !created_by) {
    console.log("Problem");
    return res.status(400).json({ message: "Incomplete Information" });
  } else {
    try {
      const product = await Products.create({
        status: status,
        real_estate_id: real_estate_id,
        application: application == "" ? null : application,
        finish: finish,
        materials: material == "" ? null : material,
        variations: variations == "" ? null : variations,
        color: color == "" ? null : color,
        category: category == "" ? null : category,
        manufacturer: manufacturer,
        name: name,
        sku: sku == "" ? null : sku,
        price: parseFloat(price),
        model: model,
        description: description,
        ecommerce_website_url: ecommerce_website_url,
        nft_link: "",
        anchor: anchor,
        category_id: category_id,
        product_stock_api: product_stock_api,
        dimension_height: parseFloat(dimension_height),
        dimension_width: parseFloat(dimension_width),
        dimension_length: parseFloat(dimension_length),
        dimension_depth: parseFloat(dimension_depth),
        dimension_diameter: parseFloat(dimension_diameter),
        dimension_unit: dimension_unit,
        weight: parseFloat(weight),
        weight_unit: weight_unit,
        product_3d_image_urls: JSON.stringify(threed_photo_files),
        product_3d_glb_url: JSON.stringify(glb_files),
        photo_video_url: video_files,
        image_urls: JSON.stringify(photo_files),
        in_stock: Boolean(in_stock),
        discount: parseFloat(discount),
        show_price: Boolean(show_price),
        client_id: client_id,
        virtual_experience_id: virtual_experience_id,
        created_by: created_by,
        brand: brand == "" ? null : brand
      });

      if (!product) {
        return res
          .status(400)
          .json({ message: "Product was not successfully created" });
      }

      const search_node = await ProductNodes.findOne({
        where: {
          model: model,
        }
      })

      if (search_node) {
        await ProductNodes.update({
          product_id: product.dataValues.id,
          updated_by: JSON.stringify(created_by),
        }, {
          where: {
            id: search_node.dataValues.id
          }
        });
      }

      return res
        .status(201)
        .json({ message: "Product was successfully created" });
    } catch (error) {
      console.log(error);
    }
  }
}

export const uploadProducts = async (req, res) => {
  const { data } = req.body;
  
  if (!data || !data.length) {
    return res.status(400).json({ message: 'Data is required' });
  }

  try {
    let products = await Products.bulkCreate(data);
    products = products?.map((item) => ({
      id: item.dataValues?.id,
      model: item.dataValues?.model,
    }));

    await Promise.all(
      products?.map(async (item) => {
        if (!item?.model) return;

        const search_node = await ProductNodes.findOne({
          where: { model: item.model },
          attributes: ['id'],
        });
        if (!search_node) return;
        await ProductNodes.update(
          {
            product_id: item.id,
            updated_by: JSON.stringify(data[0]?.created_by),
          },
          { where: { id: search_node.dataValues.id } }
        );
      })
    );

    res.status(200).json({ message: 'Products uploaded successfully' });
  } catch (error) {
    console.error('Error uploading products:', error);
    res.status(500).json({ message: 'Internal server error' });
  }
};

export const create_product = async (req, res) => {
  const body = JSON.parse(JSON.stringify(req.body));
  const files = JSON.parse(JSON.stringify(req.files));

  if (!files && !body) {
    return res.status(400).json()
  }

  console.log("Files: ", files);
  console.log("Files: ", JSON.parse(JSON.stringify(files)));

  console.log("Body ", body);
  // console.log(files);
  const data = JSON.parse(body.data);
  console.log(data);

  let images = [];

  for (let index = 0; index < files.images.length; index++) {
    const element = files.images[index];
    images.push(element.location);
  }

  let three_d_images = [];

  for (let index = 0; index < files.three_d_images.length; index++) {
    const element = files.three_d_images[index];
    three_d_images.push(element.location);
  }

  let {
    name,
    sku,
    price,
    model,
    description,
    discount,
    ecommerce_website_url,
    anchor,
    product_stock_api,
    dimension_height,
    dimension_width,
    dimension_length,
    dimension_depth,
    dimension_diameter,
    dimension_unit,
    weight,
    weight_unit,
    in_stock,
    show_price,
    client_id,
    virtual_experience_id,
    status,
    real_estate_id,
    application,
    finish,
    material,
    variations,
    color,
    category,
    manufacturer,
    created_by,
    category_id,
    brand
  } = data;

  if (!name || !model || !description || !created_by) {
    console.log("Problem");
    return res.status(400).json({ message: "Incomplete Information" });
  } else {
    try {
      const product = await Products.create({
        status: status,
        real_estate_id: real_estate_id,
        application: application == "" ? null : application,
        finish: finish,
        materials: material == "" ? null : material,
        variations: variations == "" ? null : variations,
        color: color == "" ? null : color,
        category: category == "" ? null : category,
        manufacturer: manufacturer,
        name: name,
        sku: sku == "" ? null : sku,
        price: parseFloat(price),
        model: model,
        description: description,
        ecommerce_website_url: ecommerce_website_url,
        photo_video_url: files.photo_video[0].location,
        nft_link: "",
        anchor: anchor,
        category_id: category_id,
        product_stock_api: product_stock_api,
        dimension_height: parseFloat(dimension_height),
        dimension_width: parseFloat(dimension_width),
        dimension_length: parseFloat(dimension_length),
        dimension_depth: parseFloat(dimension_depth),
        dimension_diameter: parseFloat(dimension_diameter),
        dimension_unit: dimension_unit,
        weight: parseFloat(weight),
        weight_unit: weight_unit,
        product_3d_image_urls: JSON.stringify(three_d_images),
        product_3d_glb_url:
          files.glb.length > 1
            ? JSON.stringify([files.glb[0].location, files.glb[1].location])
            : JSON.stringify([files.glb[0].location]),
        photo_video_url: files.photo_video[0].location,
        image_urls: JSON.stringify(images),
        in_stock: Boolean(in_stock),
        discount: parseFloat(discount),
        show_price: Boolean(show_price),
        client_id: client_id,
        virtual_experience_id: virtual_experience_id,
        created_by: created_by,
        brand: brand == "" ? null : brand
      });

      if (!product) {
        return res
          .status(400)
          .json({ message: "Product was not successfully created" });
      }

      const search_node = await ProductNodes.findOne({
        where: {
          model: model,
        }
      })

      if (search_node) {
        await ProductNodes.update({
          product_id: product.dataValues.id,
          updated_by: JSON.stringify(created_by),
        }, {
          where: {
            id: search_node.dataValues.id
          }
        });
      }

      return res
        .status(201)
        .json({ message: "Product was successfully created" });
    } catch (error) {
      console.log(error);
    }
  }
};

export const edit_product = async (req, res) => {
  const { id } = req.params;
  const body = JSON.parse(JSON.stringify(req.body));
  const files = req.files;
  const all_files = JSON.parse(JSON.stringify(files));

  const data = JSON.parse(body.data);

  console.log("Hey", body);
  console.log("Hey", JSON.parse(JSON.stringify(files)));

  const {
    name,
    sku,
    price,
    model,
    description,
    discount,
    ecommerce_website_url,
    anchor,
    product_stock_api,
    dimension_height,
    dimension_width,
    dimension_length,
    dimension_depth,
    dimension_diameter,
    dimension_unit,
    weight,
    weight_unit,
    in_stock,
    show_price,
    client_id,
    virtual_experience_id,
    status,
    real_estate_id,
    application,
    finish,
    material,
    variations,
    color,
    category,
    manufacturer,
    updated_by,
    category_id,
    photo_video_url,
  } = data;

  let payload = {
    status: status,
    real_estate_id: real_estate_id,
    application: application,
    finish: finish == "" ? null : finish,
    materials: material,
    variations: variations,
    color: color == "" ? null : color,
    category: category == "" ? null : category,
    manufacturer: manufacturer,
    name: name,
    sku: sku,
    price: parseFloat(price),
    model: model,
    description: description,
    ecommerce_website_url: ecommerce_website_url,
    nft_link: "",
    anchor: anchor,
    category_id: category_id,
    product_stock_api: product_stock_api,
    dimension_height: parseFloat(dimension_height),
    dimension_width: parseFloat(dimension_width),
    dimension_length: parseFloat(dimension_length),
    dimension_depth: parseFloat(dimension_depth),
    dimension_diameter: parseFloat(dimension_diameter),
    dimension_unit: dimension_unit,
    weight: parseFloat(weight),
    weight_unit: weight_unit,
    // product_3d_image_urls: JSON.stringify(three_d_images),
    // product_3d_glb_url: files.glb.length > 1 ? [files.glb[0].location, files.glb[1].location] : files.glb[0].location,
    // photo_video_url: files.photo_video[0].location,
    // image_urls: JSON.stringify(images),
    in_stock: Boolean(in_stock),
    discount: parseFloat(discount),
    show_price: Boolean(show_price),
    client_id: client_id,
    virtual_experience_id: virtual_experience_id,
    updated_by: updated_by,
  };

  if (Object.keys(all_files).length !== 0) {
    if (all_files.photo_video && all_files.photo_video.length !== 0) {
      if (all_files.photo_video[0].location !== photo_video_url) {
        payload.photo_video_url = all_files.photo_video[0].location;
      }
    }

    if (all_files.glb && all_files.glb.length !== 0) {
      let glbs = []
      for (let index = 0; index < all_files.glb.length; index++) {
        const element = all_files.glb[index];
        glbs.push(element.location)

      }
      payload.product_3d_glb_url = JSON.stringify(glbs);
    }

    if (all_files.three_d_images && all_files.three_d_images.length !== 0) {
      let three_d_images = []
      for (let index = 0; index < all_files.three_d_images.length; index++) {
        const element = all_files.three_d_images[index];
        three_d_images.push(element.location)

      }
      payload.product_3d_image_urls = JSON.parse(three_d_images);
    }

    if (all_files.images && all_files.images.length !== 0) {
      let images = []
      for (let index = 0; index < all_files.images.length; index++) {
        const element = all_files.images[index];
        images.push(element.location)

      }
      payload.image_urls = JSON.stringify(images);
    }


  }

  try {
    const product = await Products.update(payload, {
      where: {
        id: id,
      },
    });

    if (!product) {
      return res.status(400).json()
    }

    return res.status(200).json();
  } catch (error) {
    console.log(error);
  }
};

export const delete_product = async (req, res) => {

  const { id } = req.params;
  try {
    const product = await Products.destroy({
      where: {
        id: id
      }
    })

    if (!product) {
      return res.status(400).json()
    }

    return res.status(200).json()
  } catch (error) {
    console.log(error)
  }


}
