import Clients from "../../models/ClientModel.js";
import Products from "../../models/ProductModel.js";
import ProductNodes from "../../models/ProductNodeModel.js";
import RealEstates from "../../models/RealEstateModel.js";
import Users from "../../models/UserModel.js";
import VirtualExperienceAccessTypes from "../../models/VirtualExperienceAccessTypeModel.js";
import VirtualExperienceBusinessTypes from "../../models/VirtualExperienceBusinessTypeModel.js";
import VirtualExperiences from "../../models/VirtualExperienceModel.js";
import VirtualExperienceTypes from "../../models/VirtualExperienceTypeModel.js";
import {Op} from "sequelize";

export const get_all_real_estates = async (req, res) => {
  let query = req.query;

  let real_estates;

  try {
    if (query && query.search !== "null") {
      if(req.query.id !== "null") {
        real_estates = await RealEstates.findAndCountAll({
          offset: parseInt(query.offset),
          limit: parseInt(query.limit),
          nest: true,
          where: {
            client_id: query.id,
            name: {
              [Op.substring]: query.search,
            },
          },
          include: [
            {
              model: Clients,
            },
            {
              model: VirtualExperiences
            }
          ]
        });
      } else {
        real_estates = await RealEstates.findAndCountAll({
          offset: parseInt(query.offset),
          limit: parseInt(query.limit),
          nest: true,
          where: {
            name: {
              [Op.substring]: query.search,
            },
          },
          include: [
            {
              model: Clients,
            },
            {
              model: VirtualExperiences
            }
          ]
        });
      }
    } else {
      if(query.id !== "null") {
        real_estates = await RealEstates.findAndCountAll({
          where: {
            client_id: query.id,
          },
          offset: parseInt(query.offset),
          limit: parseInt(query.limit),
          nest: true,
          include: [
            {
              model: Clients,
            },
            {
              model: VirtualExperiences
            }
          ]
        });
      } else {
        real_estates = await RealEstates.findAndCountAll({
          offset: parseInt(query.offset),
          limit: parseInt(query.limit),
          nest: true,
          include: [
            {
              model: Clients,
            },
            {
              model: VirtualExperiences
            }
          ]
        });
      }
    }

    if (!real_estates) {
      return res.status(400).json({ message: "No Real Estates found" });
    }

    return res.status(200).json({ real_estates: real_estates });
  } catch (error) {
    console.log("Error: ", error);
  }
};

export const create_real_estate = async (req, res) => {
  const body = JSON.parse(JSON.stringify(req.body));
  const files = JSON.parse(JSON.stringify(req.files));

  if(!files && !body) {
    return res.status(400).json()
  }

  console.log(files);

  console.log("Body ", body);
  // console.log(files);
  const data = JSON.parse(body.data);
  // console.log(data);

  const {
    amenities,
    name,
    unit_amenities,
    price,
    unit_description,
    location,
    type,
    description,
    terms,
    contact_property,
    virtual_experience_id,
    client_id,
    created_by,
  } = data;

  const contact_property_final = {
    header: contact_property.header,
    subheader: contact_property.subheader,
    provider: contact_property.provider,
    logo: files.logo[0].location,
  };

  try {
    const estate = await RealEstates.create({
      amenities: JSON.stringify(amenities),
      name: name,
      unit_amenities: JSON.stringify(unit_amenities),
      price: parseFloat(price),
      location: location,
      unit_description: JSON.stringify(unit_description),
      type: type,
      description: JSON.stringify(description),
      terms: JSON.stringify(terms),
      floor_plan_url: files.floor_plan[0].location,
      contact_property: contact_property_final,
      virtual_experience_id: virtual_experience_id,
      client_id: client_id,
      created_by: created_by,
    });

    if (!estate) {
      return res
        .status(400)
        .json({ message: "Real estate was not successfully created" });
    }

    return res
      .status(201)
      .json({ message: "Real estate was successfully created" });
  } catch (error) {
    console.log(error);
  }
};

export const get_real_estate_info = async (req, res) => {
  const { name } = req.query;

  try {
    const real_estate = await RealEstates.findOne({
      include: [
        {
          model: Products,
          attributes: ["id", "image_urls"],
          include: [
            {
              model: ProductNodes
            }
          ]
        },
        {
          model: VirtualExperiences,
          where: {
            link_name: name,
          },
          include: [
            {
              model: Users,
              attributes: [
                "id",
                "first_name",
                "last_name",
                "middle_name",
                "email",
                "phone_number",
                "is_online",
                "image_url",
              ],
              as: "virtual_experience_of_user"
            },
            {
              model: VirtualExperienceAccessTypes,
              attributes: ["id", "name"],
            },
            {
              model: VirtualExperienceBusinessTypes,
              attributes: ["id", "name"],
            },
            {
              model: VirtualExperienceTypes,
              attributes: ["id", "name"],
            },
          ],
        },
        {
          model: Clients,
        },
      ],
    });

    if(!real_estate) {

    }

    return res.status(200).json(real_estate)
  } catch (error) {
    console.log(error)
  }
};



export const get_real_estate = async (req, res) => {
  const { id } = req.params;

  try {
    const real_estate = await RealEstates.findOne({
      where: {
        id: id
      },
      include: [
        {
          model: Products,
          attributes: ["id", "image_urls"],
          include: [
            {
              model: ProductNodes
            }
          ]
        },
        {
          model: VirtualExperiences,
          include: [
            {
              model: Users,
              attributes: [
                "id",
                "first_name",
                "last_name",
                "middle_name",
                "email",
                "phone_number",
                "is_online",
                "image_url",
              ],
              as: "virtual_experience_of_user"
            },
            {
              model: VirtualExperienceAccessTypes,
              attributes: ["id", "name"],
            },
            {
              model: VirtualExperienceBusinessTypes,
              attributes: ["id", "name"],
            },
            {
              model: VirtualExperienceTypes,
              attributes: ["id", "name"],
            },
          ],
        },
        {
          model: Clients,
        },
      ],
    });

    if(!real_estate) {

    }

    return res.status(200).json(real_estate)
  } catch (error) {
    console.log(error)
  }
};

export const edit_real_estate_with_file = async (req, res) => {
  const { id } = req.params;
  const body = JSON.parse(JSON.stringify(req.body));
  const files = req.files;
  const all_files = JSON.parse(JSON.stringify(files));

  const data = JSON.parse(body.data);

  console.log("Hey", files)
  console.log("Hey", JSON.parse(JSON.stringify(files)))

  const {
    floor_plan_url,
    floor_plan_urls,
    contact_property_logo,
    contact_property_logos,
    amenities,
    name,
    unit_amenities,
    price,
    unit_description,
    location,
    type,
    description,
    terms,
    contact_property,
    virtual_experience_id,
    client_id,
    updated_by,
  } = data;

  let payload = {
    name: name,
    amenities: JSON.stringify(amenities),
    unit_amenities: JSON.stringify(unit_amenities),
    unit_description: JSON.stringify(unit_description),
    price: parseFloat(price),
    location: location,
    type: type,
    description: JSON.stringify(description),
    terms: JSON.stringify(terms),
    contact_property: contact_property,
    virtual_experience_id: virtual_experience_id,
    client_id: client_id,
    updated_by: updated_by
  }

  if(Object.keys(all_files).length !== 0) {
    if(all_files.floor_plan && all_files.floor_plan.length !== 0) {
      if(all_files.floor_plan[0].location !== floor_plan_url) {
        payload.floor_plan_url = all_files.floor_plan[0].location;
      }
    }

    if(all_files.logo && all_files?.logo?.length !== 0) {
      if(all_files.logo[0].location !== contact_property_logo) {
        let property = {};
        property.header = contact_property.header;
        property.provider = contact_property.provider;
        property.subheader = contact_property.subheader;
        property.logo = all_files.logo[0].location;
        payload.contact_property = property;
      }
    }
  }

  try {
    const real_estate = await RealEstates.update(payload, {
      where: {
        id: id
      }
    })

    return res.status(200).json()
  } catch (error) {
    console.log(error)
  }


}

export const delete_real_estate = async (req, res) => {
  
  const {id} = req.params;
  try {
    const real_estate = await RealEstates.destroy({
      where: {
        id: id
      }
    })

    if(!real_estate) {
      return res.status(400).json({message: "Real Estate was not successfully deleted"})
    }

    return res.status(200).json({message: "Real Estate was successfully deleted"})
  } catch (error) {
    console.log(error)
  }
}
