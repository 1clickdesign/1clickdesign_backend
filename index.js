const WEBSOCKET_PORT = 8080
import express from "express"
import cors from "cors"
import http from "http"
import helmet from "helmet"
import morgan from "morgan"
import dotenv from "dotenv"
import cookieParser from "cookie-parser"
import bodyParser from "body-parser"
import { Server } from "socket.io"
import cron from "node-cron"
import schedule_live from "./helpers/schedule-live-cron.js"
import invitations_schedule_send from "./helpers/invitations-schedule-send.js"
import schedule_flythrough_invitation from "./helpers/schedule-flythrough-invitation.js"

import Permissions from "./models/PermissionModel.js"
import Roles from "./models/RoleModel.js"
import RolePermissions from "./models/RolePermissionModel.js"
import adminRouter from "./routes/admin-route.js"
import VirtualExperienceAccessTypes from "./models/VirtualExperienceAccessTypeModel.js"
import VirtualExperienceBusinessTypes from "./models/VirtualExperienceBusinessTypeModel.js"
import VirtualExperienceTypes from "./models/VirtualExperienceTypeModel.js"
import Users from "./models/UserModel.js"
import Clients from "./models/ClientModel.js"
import Customers from "./models/CustomerModel.js"
import CustomerGroups from "./models/CustomerGroupModel.js"
import CustomerGroupMembers from "./models/CustomerGroupMemberModel.js"
import publicRouter from "./routes/public-route.js"
import generalRouter from "./routes/general-route.js"
import Notifications from "./models/NotificationModel.js"
import BackgroundMusics from "./models/BackgroundMusicModel.js"
import VirtualExperiences from "./models/VirtualExperienceModel.js"
import Products from "./models/ProductModel.js"
import ProductNodes from "./models/ProductNodeModel.js"
import OptionOffered from "./models/OptionOfferedModel.js"
import ProductOptionOffered from "./models/ProductOptionOfferedModel.js"
import Packages from "./models/PackageModel.js"
import ProductOptionOfferedRequests from "./models/ProductOptionOfferedRequestModel.js"
import ClientCustomers from "./models/ClientCustomerModel.js"
import Invitations from "./models/InvitationModel.js"
import InvitationsSchedule from "./models/InvitationsSchedule.js"
import SentEmails from "./models/SentEmailModel.js"
import VideoConferenceMessages from "./models/VideoConferenceMessageModel.js"
import VideoConferences from "./models/VideoConferenceModel.js"
import VideoConferenceParticipants from "./models/VideoConferenceParticipantModel.js"
import VideoConferenceShowrooms from "./models/VideoConferenceShowroomModel.js"
import UserBackupUsers from "./models/UserBackupUserModel.js"
import UserPermissions from "./models/UserPermissionModel.js"
import VirtualExperienceUsers from "./models/VirtualExperienceUserModel.js"
import RealEstates from "./models/RealEstateModel.js"
import AnalyticsViewedProducts from "./models/AnalyticsViewedProductModel.js"
import AnalyticsVisitDuration from "./models/AnalyticsVisitDurationModel.js"
import RealEstateProducts from "./models/RealEstateProductModel.js"
import VirtualExperienceCategories from "./models/VirtualExperienceCategoryModel.js"
import AnalyticsFrameDuration from "./models/AnalyticsFrameDurationModel.js"
import ProductOtherOptions from "./models/ProductOtherOptionsModel.js"
import ExpressionOfInterests from "./models/ExpressionOfInterestModel.js"
import ClientCategories from "./models/ClientCategoryModel.js"
import AnalyticsVideoMinutes from "./models/AnalyticsVideoMinuteModel.js"
import ProductCategories from "./models/ProductCategoryModel.js"
import UserCategories from "./models/UserCategoryModel.js"
import AnalyticsProductPreview from "./models/AnalyticsProductPreviewModel.js"
import AnalyticsProductQuantity from "./models/AnalyticsProductQuantityModel.js"
import InvitationFlythrough from "./models/InvitationFlythroughModel.js"
import VirtualFlythrough from "./models/VirtualFlythrough.js"
import ClientFlythrough from "./models/ClientFlythroughModel.js"
import AnalyticsVirtualFlythrough from "./models/AnalyticsVirtualFlythroughModel.js"

const app = express()
const server = http.createServer(app)
// const io_server = http.createServer(app);
const io = new Server(server, {
  cors: {
    origin: "*",
    methods: ["GET", "POST", "EDIT", "DELETE"],
  },
})

// app.use(cors());

app.use(
  cors({
    origin: [
      "http://localhost:443",
      "http://localhost:5000",
      "https://vr.1clickdesign.com",
      "https://1clickdesign.shapespark.com",
      "https://platform.1clickdesign.com",
      "https://www.1clickdesign.com",
      "https://room.1clickdesign.com",
      "http://127.0.0.1:5502",
    ],
    methods: ["GET", "POST", "PUT", "DELETE"],
  }),
)
app.use(helmet())
// app.use(morgan("combined"));
app.use(cookieParser())
app.use(bodyParser.json())
app.use(express.json())
app.use(express.urlencoded({ extended: true }))
// app.use(function (req, res, next) {
//   res.header("Access-Control-Allow-Origin", "*");
//   res.header(
//     "Access-Control-Allow-Headers",
//     "Origin, X-Requested-With, Content-Type, Accept"
//   );
//   next();
// });
app.use(function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*")
  res.header(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept",
  )
  next()
})

app.use(adminRouter)
app.use(publicRouter)
app.use(generalRouter)
;(async () => {
  // await AnalyticsFrameDuration.sync();
  // await AnalyticsProductPreview.sync();
  // await AnalyticsProductQuantity.sync();
  // await AnalyticsVideoMinutes.sync();
  // await AnalyticsViewedProducts.sync();
  // await AnalyticsVisitDuration.sync();
  // await BackgroundMusics.sync()
  // await ClientCustomers.sync()
  // await ClientCategories.sync()
  // await ClientFlythrough.sync();
  // await Clients.sync();
  // await Customers.sync()
  // await CustomerGroups.sync()
  // await CustomerGroupMembers.sync()
  // await Invitations.sync()
  // await InvitationFlythrough.sync()
  // await Notifications.sync()
  // await OptionOffered.sync()
  // await Packages.sync()
  // await Permissions.sync()
  // await ProductNodes.sync()
  // await Products.sync()
  // await ProductCategories.sync();
  // await ProductOptionOffered.sync()
  // await ProductOtherOptions.sync()
  // await ProductOptionOfferedRequests.sync()
  // await RealEstates.sync()
  // await RealEstateProducts.sync();
  // await Roles.sync()
  // await RolePermissions.sync()
  // await SentEmails.sync();
  // await Users.sync();
  // await UserCategories.sync()
  // await UserPermissions.sync()
  // await UserBackupUsers.sync()
  // await VideoConferenceMessages.sync()
  // await VideoConferenceParticipants.sync()
  // await VideoConferenceShowrooms.sync()
  // await VideoConferences.sync()
  // await VirtualExperiences.sync()
  // await VirtualExperienceCategories.sync()
  // await VirtualExperienceUsers.sync()
  // await VirtualExperienceAccessTypes.sync()
  // await VirtualExperienceBusinessTypes.sync()
  // await VirtualExperienceTypes.sync()
  // await VirtualFlythrough.sync()
  // await SentEmails.sync({ alter: true })
})()

//AnalyticsProductQuantity
Customers.hasMany(AnalyticsProductQuantity, { foreignKey: "customer_id" })
AnalyticsProductQuantity.belongsTo(Customers, { foreignKey: "customer_id" })

Products.hasMany(AnalyticsProductQuantity, { foreignKey: "product_id" })
AnalyticsProductQuantity.belongsTo(Products, { foreignKey: "product_id" })

VirtualExperiences.hasMany(AnalyticsProductQuantity, {
  foreignKey: "virtual_experience_id",
})
AnalyticsProductQuantity.belongsTo(VirtualExperiences, {
  foreignKey: "virtual_experience_id",
})

//AnalyticsProductPreview
Clients.hasMany(AnalyticsProductPreview, { foreignKey: "client_id" })
AnalyticsProductPreview.belongsTo(Clients, { foreignKey: "client_id" })

Customers.hasMany(AnalyticsProductPreview, { foreignKey: "customer_id" })
AnalyticsProductPreview.belongsTo(Customers, { foreignKey: "customer_id" })

Products.hasMany(AnalyticsProductPreview, { foreignKey: "product_id" })
AnalyticsProductPreview.belongsTo(Products, { foreignKey: "product_id" })

VirtualExperiences.hasMany(AnalyticsProductPreview, {
  foreignKey: "virtual_experience_id",
})
AnalyticsProductPreview.belongsTo(VirtualExperiences, {
  foreignKey: "virtual_experience_id",
})

//AnalyticsFrameDuration
Customers.hasMany(AnalyticsFrameDuration, { foreignKey: "customer_id" })
AnalyticsFrameDuration.belongsTo(Customers, { foreignKey: "customer_id" })

Products.hasMany(AnalyticsFrameDuration, { foreignKey: "product_id" })
AnalyticsFrameDuration.belongsTo(Products, { foreignKey: "product_id" })

//AnalyticsViewedProducts
Customers.hasMany(AnalyticsViewedProducts, { foreignKey: "customer_id" })
AnalyticsViewedProducts.belongsTo(Customers, { foreignKey: "customer_id" })

Products.hasMany(AnalyticsViewedProducts, { foreignKey: "product_id" })
AnalyticsViewedProducts.belongsTo(Products, { foreignKey: "product_id" })

VirtualExperiences.hasMany(AnalyticsViewedProducts, {
  foreignKey: "virtual_experience_id",
})
AnalyticsViewedProducts.belongsTo(VirtualExperiences, {
  foreignKey: "virtual_experience_id",
})

//AnalyticsVisitDuration
Customers.hasMany(AnalyticsVisitDuration, { foreignKey: "customer_id" })
AnalyticsVisitDuration.belongsTo(Customers, { foreignKey: "customer_id" })

VirtualExperiences.hasMany(AnalyticsVisitDuration, {
  foreignKey: "virtual_experience_id",
})
AnalyticsVisitDuration.belongsTo(VirtualExperiences, {
  foreignKey: "virtual_experience_id",
})

//AnalyticsVirtualFlythrough
Customers.hasMany(AnalyticsVirtualFlythrough, { foreignKey: "customer_id" })
AnalyticsVirtualFlythrough.belongsTo(Customers, { foreignKey: "customer_id" })

VirtualFlythrough.hasMany(AnalyticsVirtualFlythrough, {
  foreignKey: "virtual_flythrough_id",
})
AnalyticsVirtualFlythrough.belongsTo(VirtualFlythrough, {
  foreignKey: "virtual_flythrough_id",
})
InvitationFlythrough.hasMany(AnalyticsVirtualFlythrough, {
  foreignKey: "invitation_flythrough_id",
})
AnalyticsVirtualFlythrough.belongsTo(InvitationFlythrough, {
  foreignKey: "invitation_flythrough_id",
})

//ClientCategories
ClientCategories.hasMany(Clients, { foreignKey: "client_category_id" })
Clients.belongsTo(ClientCategories, { foreignKey: "client_category_id" })

//ClientCustomers
Clients.hasMany(ClientCustomers, { foreignKey: "client_id" })
ClientCustomers.belongsTo(Clients, { foreignKey: "client_id" })

Customers.hasMany(ClientCustomers, { foreignKey: "customer_id" })
ClientCustomers.belongsTo(Customers, { foreignKey: "customer_id" })

Users.hasMany(ClientCustomers, { foreignKey: "user_id" })
ClientCustomers.belongsTo(Users, { foreignKey: "user_id" })

//Clients
Packages.hasMany(Clients, { foreignKey: "package_id" })
Clients.belongsTo(Packages, { foreignKey: "package_id" })

Users.hasOne(Clients, { foreignKey: "account_holder_id" })
Clients.belongsTo(Users, { foreignKey: "account_holder_id" })

//CustomerGroupMembers
Customers.belongsToMany(CustomerGroups, {
  through: CustomerGroupMembers,
  foreignKey: "customer_id",
  as: "customer_member",
})
CustomerGroups.belongsToMany(Customers, {
  through: CustomerGroupMembers,
  foreignKey: "customer_group_id",
  as: "customer_group",
})

//ExpressionOfInterests
RealEstates.hasMany(ExpressionOfInterests, { foreignKey: "real_estate_id" })
ExpressionOfInterests.belongsTo(RealEstates, { foreignKey: "real_estate_id" })

// Invitations
Clients.hasMany(Invitations, { foreignKey: "client_id" })
Invitations.belongsTo(Clients, { foreignKey: "client_id" })

Customers.hasMany(Invitations, { foreignKey: "customer_id" })
Invitations.belongsTo(Customers, { foreignKey: "customer_id" })

SentEmails.hasMany(Invitations, { foreignKey: "sent_email_id" })
Invitations.belongsTo(SentEmails, { foreignKey: "sent_email_id" })

Users.hasMany(Invitations, { foreignKey: "user_id", as: "invitation_user" })
Invitations.belongsTo(Users, { foreignKey: "user_id", as: "invitation_user" })

Users.hasMany(Invitations, {
  foreignKey: "backup_user_id",
  as: "invitation_backup_user",
})
Invitations.belongsTo(Users, {
  foreignKey: "backup_user_id",
  as: "invitation_backup_user",
})

VirtualExperiences.hasMany(Invitations, {
  foreignKey: "virtual_experience_id",
})
Invitations.belongsTo(VirtualExperiences, {
  foreignKey: "virtual_experience_id",
})

// InvitationsSchedule
Clients.hasMany(InvitationsSchedule, { foreignKey: "client_id" })
InvitationsSchedule.belongsTo(Clients, { foreignKey: "client_id" })

Customers.hasMany(InvitationsSchedule, { foreignKey: "customer_id" })
InvitationsSchedule.belongsTo(Customers, { foreignKey: "customer_id" })

SentEmails.hasMany(InvitationsSchedule, { foreignKey: "sent_email_id" })
InvitationsSchedule.belongsTo(SentEmails, { foreignKey: "sent_email_id" })

Users.hasMany(InvitationsSchedule, {
  foreignKey: "user_id",
  as: "schedule_user",
})
InvitationsSchedule.belongsTo(Users, {
  foreignKey: "user_id",
  as: "schedule_user",
})

Users.hasMany(InvitationsSchedule, {
  foreignKey: "backup_user_id",
  as: "schedule_backup_user",
})
InvitationsSchedule.belongsTo(Users, {
  foreignKey: "backup_user_id",
  as: "schedule_backup_user",
})

VirtualExperiences.hasMany(InvitationsSchedule, {
  foreignKey: "virtual_experience_id",
})
InvitationsSchedule.belongsTo(VirtualExperiences, {
  foreignKey: "virtual_experience_id",
})

//Virtual Flythrough Invitations
Clients.hasMany(InvitationFlythrough, { foreignKey: "client_id" })
InvitationFlythrough.belongsTo(Clients, { foreignKey: "client_id" })

Customers.hasMany(InvitationFlythrough, { foreignKey: "customer_id" })
InvitationFlythrough.belongsTo(Customers, { foreignKey: "customer_id" })

SentEmails.hasMany(InvitationFlythrough, { foreignKey: "sent_email_id" })
InvitationFlythrough.belongsTo(SentEmails, { foreignKey: "sent_email_id" })

Users.hasMany(InvitationFlythrough, {
  foreignKey: "user_id",
  as: "invitation_flythrough_user",
})
InvitationFlythrough.belongsTo(Users, {
  foreignKey: "user_id",
  as: "invitation_flythrough_user",
})

Users.hasMany(InvitationFlythrough, {
  foreignKey: "backup_user_id",
  as: "invitation_flythrough_backup_user",
})
InvitationFlythrough.belongsTo(Users, {
  foreignKey: "backup_user_id",
  as: "invitation_flythrough_backup_user",
})

VirtualExperiences.hasMany(InvitationFlythrough, {
  foreignKey: "virtual_experience_id",
})
InvitationFlythrough.belongsTo(VirtualExperiences, {
  foreignKey: "virtual_experience_id",
})

//Notifications
Users.hasMany(Notifications, { foreignKey: "user_id" })
Notifications.belongsTo(Users, { foreignKey: "user_id" })

//Products
Clients.hasMany(Products, { foreignKey: "client_id" })
Products.belongsTo(Clients, { foreignKey: "client_id" })

RealEstates.hasMany(Products, { foreignKey: "real_estate_id" })
Products.belongsTo(RealEstates, { foreignKey: "real_estate_id" })

VirtualExperiences.hasMany(Products, { foreignKey: "virtual_experience_id" })
Products.belongsTo(VirtualExperiences, { foreignKey: "virtual_experience_id" })

ProductCategories.hasOne(Products, {
  foreignKey: "category_id",
})
Products.belongsTo(ProductCategories, {
  foreignKey: "category_id",
})

//ProductOtherOption
Customers.hasMany(ProductOtherOptions, { foreignKey: "customer_id" })
ProductOtherOptions.belongsTo(Customers, { foreignKey: "customer_id" })

Products.hasMany(ProductOtherOptions, { foreignKey: "product_id" })
ProductOtherOptions.belongsTo(Products, { foreignKey: "product_id" })

OptionOffered.hasMany(ProductOtherOptions, { foreignKey: "option_offered_id" })
ProductOtherOptions.belongsTo(OptionOffered, {
  foreignKey: "option_offered_id",
})

VirtualExperiences.hasMany(ProductOtherOptions, {
  foreignKey: "virtual_experience_id",
})
ProductOtherOptions.belongsTo(VirtualExperiences, {
  foreignKey: "virtual_experience_id",
})

//ProductNodes
Products.hasMany(ProductNodes, { foreignKey: "product_id" })
ProductNodes.belongsTo(Products, { foreignKey: "product_id" })

//ProductOptionOffered
Products.belongsToMany(OptionOffered, {
  through: ProductOptionOffered,
  foreignKey: "product_id",
  as: "product_with_offer",
})
OptionOffered.belongsToMany(Products, {
  through: ProductOptionOffered,
  foreignKey: "option_offered_id",
  as: "offered",
})

//ProductOptionOfferedRequests
Clients.hasMany(ProductOptionOfferedRequests, { foreignKey: "client_id" })
ProductOptionOfferedRequests.belongsTo(Clients, { foreignKey: "client_id" })

Customers.hasMany(ProductOptionOfferedRequests, { foreignKey: "customer_id" })
ProductOptionOfferedRequests.belongsTo(Customers, {
  foreignKey: "customer_id",
})

OptionOffered.hasMany(ProductOptionOfferedRequests, {
  foreignKey: "opt_offer_id",
})
ProductOptionOfferedRequests.belongsTo(OptionOffered, {
  foreignKey: "opt_offer_id",
})

Products.hasMany(ProductOptionOfferedRequests, { foreignKey: "product_id" })
ProductOptionOfferedRequests.belongsTo(Products, { foreignKey: "product_id" })

SentEmails.hasMany(ProductOptionOfferedRequests, {
  foreignKey: "sent_email_id",
})
ProductOptionOfferedRequests.belongsTo(SentEmails, {
  foreignKey: "sent_email_id",
})

Users.hasMany(ProductOptionOfferedRequests, { foreignKey: "assigned_user_id" })
ProductOptionOfferedRequests.belongsTo(Users, {
  foreignKey: "assigned_user_id",
})

VirtualExperiences.hasMany(ProductOptionOfferedRequests, {
  foreignKey: "virtual_exp_id",
})
ProductOptionOfferedRequests.belongsTo(VirtualExperiences, {
  foreignKey: "virtual_exp_id",
})

//RealEstates
Clients.hasMany(RealEstates, { foreignKey: "client_id" })
RealEstates.belongsTo(Clients, { foreignKey: "client_id" })

VirtualExperiences.hasMany(RealEstates, {
  foreignKey: "virtual_experience_id",
})
RealEstates.belongsTo(VirtualExperiences, {
  foreignKey: "virtual_experience_id",
})

//RealEstateProducts
RealEstates.belongsToMany(Products, {
  through: RealEstateProducts,
  foreignKey: "real_estate_id",
  as: "real_estate",
})
Products.belongsToMany(RealEstates, {
  through: RealEstateProducts,
  foreignKey: "product_id",
  as: "product",
})

//RolePermissions
Roles.belongsToMany(Permissions, {
  through: RolePermissions,
  foreignKey: "role_id",
  as: "role",
})
Permissions.belongsToMany(Roles, {
  through: RolePermissions,
  foreignKey: "permission_id",
  as: "permission_with_role",
})

//SentEmails
Users.hasOne(SentEmails, { foreignKey: "user_id" })
SentEmails.belongsTo(Users, { foreignKey: "user_id" })

Customers.hasOne(SentEmails, { foreignKey: "customer_id" })
SentEmails.belongsTo(Customers, { foreignKey: "customer_id" })

//UserBackupUsers
Users.belongsToMany(Users, {
  through: UserBackupUsers,
  foreignKey: "main_user_id",
  as: "backup_user",
})
Users.belongsToMany(Users, {
  through: UserBackupUsers,
  foreignKey: "backup_user_id",
  as: "main_user",
})

//Users
Roles.hasOne(Users, { foreignKey: "role_id" })
Users.belongsTo(Roles, { foreignKey: "role_id" })

Clients.hasMany(Users, { foreignKey: "client_id" })
Users.belongsTo(Clients, { foreignKey: "client_id" })

//UsersCategories
Users.belongsToMany(VirtualExperienceCategories, {
  through: UserCategories,
  foreignKey: "user_id",
  as: "user_category",
})
VirtualExperienceCategories.belongsToMany(Users, {
  through: UserCategories,
  foreignKey: "category_id",
  as: "category",
})

//UserPermissions
Users.belongsToMany(Permissions, {
  through: UserPermissions,
  foreignKey: "user_id",
  as: "user_permission",
})
Permissions.belongsToMany(Users, {
  through: UserPermissions,
  foreignKey: "permission_id",
  as: "permission",
})

//VideoConferenceMessages
Users.hasOne(VideoConferenceMessages, { foreignKey: "user_id" })
VideoConferenceMessages.belongsTo(Users, { foreignKey: "user_id" })

Customers.hasOne(VideoConferenceMessages, { foreignKey: "customer_id" })
VideoConferenceMessages.belongsTo(Customers, { foreignKey: "customer_id" })

VideoConferences.hasOne(VideoConferenceMessages, {
  foreignKey: "video_conference_id",
})
VideoConferenceMessages.belongsTo(VideoConferences, {
  foreignKey: "video_conference_id",
})

//VideoConferences
Users.hasOne(VideoConferences, { foreignKey: "host_id", as: "host" })
VideoConferences.belongsTo(Users, { foreignKey: "host_id", as: "host" })

Users.hasOne(VideoConferences, { foreignKey: "backup_host_id", as: "backup" })
VideoConferences.belongsTo(Users, {
  foreignKey: "backup_host_id",
  as: "backup",
})

Customers.hasOne(VideoConferences, { foreignKey: "customer_id" })
VideoConferences.belongsTo(Customers, { foreignKey: "customer_id" })

//VideoConferenceParticipants
VideoConferences.belongsToMany(Customers, {
  through: VideoConferenceParticipants,
  foreignKey: "video_con_id",
  as: "video_conference",
})
Customers.belongsToMany(VideoConferences, {
  through: VideoConferenceParticipants,
  foreignKey: "customer_id",
  as: "customer",
})

//VideoConferenceShowrooms
VideoConferences.belongsToMany(VirtualExperiences, {
  through: VideoConferenceShowrooms,
  foreignKey: "video_con_id",
  as: "video_conf_showroom",
})
VirtualExperiences.belongsToMany(VideoConferences, {
  through: VideoConferenceShowrooms,
  foreignKey: "virtual_exp_id",
  as: "virtual_exp",
})

//VideoConferenceMinutes
VideoConferences.hasOne(AnalyticsVideoMinutes, {
  foreignKey: "video_conference_id",
})
AnalyticsVideoMinutes.belongsTo(VideoConferences, {
  foreignKey: "video_conference_id",
})
Users.hasOne(AnalyticsVideoMinutes, { foreignKey: "user_id" })
AnalyticsVideoMinutes.belongsTo(Users, {
  foreignKey: "user_id",
})

Customers.hasOne(AnalyticsVideoMinutes, { foreignKey: "customer_id" })
AnalyticsVideoMinutes.belongsTo(Customers, {
  foreignKey: "customer_id",
})

//VirtualExperiences
BackgroundMusics.hasOne(VirtualExperiences, {
  foreignKey: "background_music_id",
})
VirtualExperiences.belongsTo(BackgroundMusics, {
  foreignKey: "background_music_id",
})

Clients.hasOne(VirtualExperiences, { foreignKey: "client_id" })
VirtualExperiences.belongsTo(Clients, { foreignKey: "client_id" })

VirtualExperienceAccessTypes.hasOne(VirtualExperiences, {
  foreignKey: "access_type_id",
})
VirtualExperiences.belongsTo(VirtualExperienceAccessTypes, {
  foreignKey: "access_type_id",
})

VirtualExperienceBusinessTypes.hasOne(VirtualExperiences, {
  foreignKey: "business_type_id",
})
VirtualExperiences.belongsTo(VirtualExperienceBusinessTypes, {
  foreignKey: "business_type_id",
})

VirtualExperienceTypes.hasOne(VirtualExperiences, { foreignKey: "type_id" })
VirtualExperiences.belongsTo(VirtualExperienceTypes, { foreignKey: "type_id" })

VirtualExperienceCategories.hasOne(VirtualExperiences, {
  foreignKey: "category_id",
})
VirtualExperiences.belongsTo(VirtualExperienceCategories, {
  foreignKey: "category_id",
})
//Virtual Flythrough
Clients.hasOne(VirtualFlythrough, { foreignKey: "client_id" })
VirtualFlythrough.belongsTo(Clients, { foreignKey: "client_id" })

VirtualExperienceTypes.hasOne(VirtualFlythrough, { foreignKey: "type_id" })
VirtualFlythrough.belongsTo(VirtualExperienceTypes, { foreignKey: "type_id" })

//VirtualExperienceUsers
Users.belongsToMany(VirtualExperiences, {
  through: VirtualExperienceUsers,
  foreignKey: "user_id",
  as: "user",
})
VirtualExperiences.belongsToMany(Users, {
  through: VirtualExperienceUsers,
  foreignKey: "virtual_experience_id",
  as: "virtual_experience_of_user",
})

//SalesVirtualExperience
Users.hasOne(VirtualExperiences, { foreignKey: "sales_id", as: "sales" })
VirtualExperiences.belongsTo(Users, { foreignKey: "sales_id", as: "sales" })

// ClientFlythrough
// Clients.belongsToMany(VirtualFlythrough, {
//   through: ClientFlythrough,
//   foreignKey: "client_flythrough_virtual_flythrough_id_client_id_unique	",
//   as: "client_flythrough_virtual_flythrough_id_client_id_unique	",
// });
// VirtualFlythrough.belongsToMany(Clients, {
//   through: ClientFlythrough,
//   foreignKey: "virtual_flythrough_id",
// });

Clients.belongsToMany(VirtualFlythrough, {
  through: ClientFlythrough,
  foreignKey: "client_id",
  otherKey: "virtual_flythrough_id",
})

VirtualFlythrough.belongsToMany(Clients, {
  through: ClientFlythrough,
  foreignKey: "virtual_flythrough_id",
  otherKey: "client_id",
})

// Additional associations
ClientFlythrough.belongsTo(Clients, {
  foreignKey: "client_id",
})

ClientFlythrough.belongsTo(VirtualFlythrough, {
  foreignKey: "virtual_flythrough_id",
})

cron.schedule("* * * * *", () => {
  schedule_live()
  invitations_schedule_send()
  schedule_flythrough_invitation()
})

server.listen(process.env.PORT, () => {
  console.log(
    `Server listening to HOST ${process.env.HOST} PORT ${process.env.PORT}`,
  )
})

// io_server.listen(WEBSOCKET_PORT, () => {
//   console.log("Io server running on PORT " + WEBSOCKET_PORT);
// });

io.of("/connect_live").on("connection", (socket) => {
  console.log("connected to connect live")
  socket.on("join-room", async (rooms, userId, back_ups) => {
    const allRooms = Array.from(io.of("/connect_live")?.adapter?.rooms?.keys())
    console.log(allRooms)
    socket.userId = userId
    console.log(rooms)
    console.log(userId)
    socket.join(userId)

    if (back_ups.length !== 0) {
      const backup_customers = Array.from(
        io.of("/live_rooms").adapter.rooms.keys(),
      )
      const backup_all_customers = backup_customers.filter((customer) => {
        return (
          customer.includes(":") &&
          back_ups.find(
            (user) =>
              user.main_user_id ===
              customer.substring(
                getIndex(customer, ":", 1) + 1,
                getIndex(customer, ":", 2),
              ),
          )
        )
      })
      let customers = []
      if (backup_all_customers.length !== 0) {
        for (let index = 0; index < backup_all_customers.length; index++) {
          const element = backup_all_customers[index]
          const data = await io.of("/live_rooms").in(element).fetchSockets()
          customers.push({
            room: element,
            name: data[0].data.name,
            link: `/${data[0].data.room}/${data[0].data.user}/${data[0].data.id}`,
          })
        }
      }
      console.log("HEYY CUSTONERS: ", customers)
      io.of("/connect_live").to(userId).emit("my-backup-customers", customers)
    }

    io.of("/connect_live").to(userId).emit("join-successfully")
    // console.log(online);

    function getIndex(str, char, n) {
      return str.split(char).slice(0, n).join(char).length
    }

    let all_rooms = Array.from(io.of("/live_rooms").adapter.rooms.keys())
    const newRooms = all_rooms.filter((data) => {
      return (
        data.includes(":") &&
        data.substring(getIndex(data, ":", 1) + 1, getIndex(data, ":", 2)) ===
          userId
      )
    })

    let customers_list = []
    for (let index = 0; index < newRooms.length; index++) {
      const element = newRooms[index]
      const data = await io.of("/live_rooms").in(element).fetchSockets()
      customers_list.push({
        id: element,
        name: data[0].data.name,
        link: `/${data[0].data.room}/${data[0].data.user}/${data[0].data.id}`,
      })
    }
    console.log("CUSTOMERS ", customers_list)
    io.of("/connect_live")
      .to(userId)
      .emit("update-live-customers", customers_list)

    socket.on("disconnect", async () => {
      // socket.rooms.size === 0

      console.log("DISCONNECT")

      socket.leave(socket.userId)
      console.log("LEAVE")
      io.of("/connect_live").to(socket.userId).emit("disconnect-successfully")
      // console.log(online);
    })
  })
})

io.of("/user_customer_connection").on("connection", (socket) => {
  console.log("Connected to User Customer Connection")

  socket.on(
    "join-room",
    async (
      roomId,
      userId,
      id,
      customer_name,
      customer_id,
      token,
      searchparamsInvitation,
    ) => {
      console.log("SOMEONE JOIN ", roomId, userId, id)
      console.log("ID ", socket.id)
      const name = customer_name ?? "HOST"
      const assign_id = token ? userId : id
      socket.data.name = name
      socket.data.id = assign_id
      socket.data.isHost = token ? true : false
      socket.data.is_customer = customer_name ? true : false
      socket.join(
        roomId + ":" + userId + ":" + id + ":" + searchparamsInvitation,
      )
      const data = await io
        .of("/user_customer_connection")
        .in(roomId + ":" + userId + ":" + id + ":" + searchparamsInvitation)
        .fetchSockets()
      const assign_name = data.find((info) => info.data.id !== assign_id)?.data
        ?.name
      const is_customer = data.find((info) => info.data.id !== assign_id)?.data
        ?.is_customer
      console.log(
        "ASSIGN_NAME ",
        data.find((info) => info.data.id !== assign_id),
      )
      console.log(
        "ASSIGN_NAME ",
        data.find((info) => info.data.id !== assign_id)?.data?.name,
      )
      io.of("/user_customer_connection")
        .to(roomId + ":" + userId + ":" + id + ":" + searchparamsInvitation)
        .emit("success-connection", userId)
      socket
        .to(roomId + ":" + userId + ":" + id + ":" + searchparamsInvitation)
        .emit("user-connected", assign_id, assign_name, is_customer)

      socket.on("send-message", (data) => {
        console.log("MEssage Receive ", data)
        let message = data
        if (data.type === "host") {
          if (data.message === socket.data.id) {
            socket.data.isHost = true
          } else {
            socket.data.isHost = false
          }
        }
        io.of("/user_customer_connection")
          .to(roomId + ":" + userId + ":" + id + ":" + searchparamsInvitation)
          .emit("receive-message", message)
      })

      socket.on("send-shapespark-location", (data) => {
        io.of("/user_customer_connection")
          .to(roomId + ":" + userId + ":" + id + ":" + searchparamsInvitation)
          .emit("receive-shapespark-location", data)
      })

      socket.on("request-location", () => {
        socket
          .to(roomId + ":" + userId + ":" + id + ":" + searchparamsInvitation)
          .emit("receive-request-location")
      })

      socket.on("disconnect", () => {
        io.of("/user_customer_connection")
          .to(roomId + ":" + userId + ":" + id + ":" + searchparamsInvitation)
          .emit("user-disconnected", assign_id)
      })

      socket.on("request-change-host", (id) => {
        console.log("THIS IS CALLED: ", id)
        console.log(socket.data)
        if (id === socket.data.id) {
          socket.data.isHost = true
        } else {
          socket.data.isHost = false
        }

        io.of("/user_customer_connection")
          .to(roomId + ":" + userId + ":" + id + ":" + searchparamsInvitation)
          .emit("change-host", JSON.stringify({ id: id, data: socket.data }))
      })
    },
  )

  socket.on("disconnect", async () => {
    // socket.rooms.size === 0
    // console.log("USER ID: ", socket.userId);

    console.log("DISCONNECT")

    socket.leave(socket.data.id)
    console.log("LEAVE")
  })
})

io.of("/live_rooms").on("connection", (socket) => {
  console.log("LIVE ROOMS")
  socket.on(
    "join-room",
    async (
      roomId,
      userId,
      id,
      customer_id,
      token,
      searchparamsInvitation,
      callback,
    ) => {
      console.log(customer_id, token)
      if (typeof callback !== "function") {
        // event is not an acknowledgement, do something i.e. return
        console.log("HEY")
        return
      }

      if (!customer_id && token) {
        const user = await Users.findOne({
          where: {
            token: token,
          },
          attributes: ["id", "first_name", "last_name", "email", "image_url"],
        })
      } else if (customer_id && !token) {
        const customer = await Customers.findOne({
          where: {
            id: id,
          },
          attributes: ["id", "full_name"],
        })

        socket.data.name = customer?.dataValues?.full_name
        socket.data.id = customer?.dataValues?.id
        socket.data.room = roomId
        socket.data.user = userId
        socket.data.invitation_id = searchparamsInvitation
        socket.join(
          roomId + ":" + userId + ":" + id + ":" + searchparamsInvitation,
        )
      }

      callback({ status: "Success" })

      socket.on("disconnect", async () => {
        // socket.rooms.size === 0
        // console.log("USER ID: ", socket.userId);
        // const online = await Users.update(
        //   {
        //     is_online: false,
        //   },
        //   {
        //     where: {
        //       id: socket.userId,
        //     },
        //   }
        // );

        console.log("DISCONNECT")

        socket.leave(socket.data.id)
        console.log("LEAVE")
        // console.log(online);
      })
    },
  )
})

io.of("/user_customer_connection").adapter.on("join-room", (room, id) => {
  console.log("JOININGGG", room, id)
})

io.of("/live_rooms").adapter.on("join-room", async (room, id) => {
  console.log("HIIIIII ", room)
  let main_id
  function getIndex(str, char, n) {
    return str.split(char).slice(0, n).join(char).length
  }

  if (room.includes(":")) {
    console.log(room)
    let userId = room.substring(
      getIndex(room, ":", 1) + 1,
      getIndex(room, ":", 2),
    )
    console.log("User ID ", userId)
    main_id = userId
    let allRooms = Array.from(io.of("/live_rooms").adapter.rooms.keys())
    const newRooms = allRooms.filter((data) => {
      return (
        data.includes(":") &&
        data.substring(getIndex(data, ":", 1) + 1, getIndex(data, ":", 2)) ===
          userId
      )
    })

    let customers_list = []
    for (let index = 0; index < newRooms.length; index++) {
      const element = newRooms[index]
      const data = await io.of("/live_rooms").in(element).fetchSockets()
      customers_list.push({
        id: element,
        name: data[0].data.name,
        link: `/${data[0].data.room}/${data[0].data.user}/${data[0].data.id}`,
        invitation_id: data[0].data.invitation_id,
      })
    }
    console.log(customers_list[0])
    console.log("CUSTOMERS ", newRooms)
    io.of("/connect_live")
      .to(userId)
      .emit("update-live-customers", customers_list)

    const back_ups = await UserBackupUsers.findAll({
      where: {
        main_user_id: userId,
      },
    })

    let customers = []
    if (back_ups.length !== 0) {
      for (let index = 0; index < back_ups.length; index++) {
        const element = back_ups[index]
        const data = await io.of("/live_rooms").in(room).fetchSockets()
        customers.push({ room: room, name: data[0].data.name })
        io.of("/connect_live")
          .to(element.backup_user_id)
          .emit("my-backup-customers", [
            {
              room: room,
              name: data[0].data.name,
              link: `/${data[0].data.room}/${data[0].data.user}/${data[0].data.id}`,
            },
          ])
      }
    }
    // console.log("HIIII ", back_ups[0].backup_user_id);
    // io.of("/connect_live")
    //   .to(back_ups[0].backup_user_id)
    //   .emit("my-backup-customers", customers);
  }
})

io.of("/live_rooms").adapter.on("leave-room", async (room, id) => {
  console.log(room)
  function getIndex(str, char, n) {
    return str.split(char).slice(0, n).join(char).length
  }

  if (room.includes(":")) {
    console.log(room)
    let userId = room.substring(
      getIndex(room, ":", 1) + 1,
      getIndex(room, ":", 2),
    )
    console.log("User ID ", userId)
    let allRooms = Array.from(io.of("/live_rooms").adapter.rooms.keys())
    console.log("All", allRooms)
    const newRooms = allRooms.filter((data) => {
      return (
        data.includes(":") &&
        data.substring(getIndex(data, ":", 1) + 1, getIndex(data, ":", 2)) ===
          userId &&
        room !== data
      )
    })

    let customers_list = []
    if (newRooms.length !== 0) {
      for (let index = 0; index < newRooms.length; index++) {
        const element = newRooms[index]
        const data = await io.of("/live_rooms").in(element).fetchSockets()
        console.log("AILOUUU ", data)
        customers_list.push({
          id: element,
          name: data[0].data.name,
          link: `/${data[0].data.room}/${data[0].data.user}/${data[0].data.id}`,
        })
      }
    }
    io.of("/connect_live")
      .to(userId)
      .emit("update-live-customers", customers_list)

    const back_ups = await UserBackupUsers.findAll({
      where: {
        main_user_id: userId,
      },
    })

    let customers = []
    if (back_ups.length !== 0) {
      for (let index = 0; index < back_ups.length; index++) {
        const element = back_ups[index]
        const data = await io.of("/live_rooms").in(room).fetchSockets()
        if (data && data.length !== 0) {
          io.of("/connect_live")
            .to(element.backup_user_id)
            .emit("my-backup-customers", [
              {
                room: room,
                name: data[0].data.name,
                link: `/${data[0].data.room}/${data[0].data.user}/${data[0].data.id}`,
              },
            ])
        } else {
          io.of("/connect_live")
            .to(element.backup_user_id)
            .emit("my-backup-customers", [])
        }
      }
    }
  }
})

io.of("/connect_live").adapter.on("join-room", (room, id) => {
  const allRooms = Array.from(io.of("/connect_live").adapter.rooms.keys())
  io.of("/").emit("updated-live-users", allRooms)
})

io.of("/connect_live").adapter.on("leave-room", (room, id) => {
  let allRooms = Array.from(io.of("/connect_live").adapter.rooms.keys())
  const index = allRooms.indexOf(room)

  const x = allRooms.splice(index, 1)
  io.of("/").emit("updated-live-users", allRooms)
})

io.on("connection", (socket) => {
  console.log("connected to socketio")

  socket.on("customer-enter", (id) => {
    socket.join("customer:" + id)
  })

  socket.on("invite-customer", (link, id) => {
    socket.to("customer:" + id).emit("send-customer-invite", link, socket.id)
  })

  socket.on("invite-customer-rejected", (id) => {
    io.to(id).emit("send-customer-rejected")
  })

  socket.on("request-live-users", (args, callback) => {
    if (typeof callback !== "function") {
      // event is not an acknowledgement, do something i.e. return
      console.log("HEY")
      return
    }

    // const allRooms = io.of('/connect_live').adapter.rooms;
    const allRooms = Array.from(io.of("/connect_live")?.adapter?.rooms?.keys())
    console.log("All Rooms: ", allRooms)
    callback(allRooms)
  })

  socket.on("join-room", (roomId, userId, searchparamsInvitation) => {
    console.log(roomId, userId)
    socket.join(searchparamsInvitation)
    socket.to(searchparamsInvitation).emit("success-connection", userId)
    socket.to(searchparamsInvitation).emit("user-connected", userId)

    socket.on("disconnect", () => {
      console.log(userId, "USER LEAVE")
      socket.broadcast
        .to(searchparamsInvitation)
        .emit("user-leave-disconnected", userId)
    })

    socket.on("send-mic-muted", (data) => {
      socket.broadcast
        .to(searchparamsInvitation)
        .emit("received-mic-muted", data)
    })

    socket.on("send-user-syncing", (data) => {
      socket.broadcast
        .to(searchparamsInvitation)
        .emit("received-user-syncing", data)
    })

    socket.on("send-changed-showroom-url", (data) => {
      console.log(data)
      socket.broadcast
        .to(searchparamsInvitation)
        .emit("received-changed-showroom-url", data)
    })

    socket.on("send-chat-message", (data) => {
      io.to(searchparamsInvitation).emit("receive-chat-message", data)
    })

    socket.on("send-make-user-host", (data) => {
      console.log(data)
      socket.broadcast
        .to(searchparamsInvitation)
        .emit("received-make-user-host", data)
    })

    socket.on("send-message", (data) => {
      io.to(searchparamsInvitation).emit("receive-message", data)

      console.log("MEssage Receive ", data)
      let message = data
      if (data.type === "host") {
        if (data.message === socket.data.id) {
          socket.data.isHost = true
        } else {
          socket.data.isHost = false
        }
      }
      io.to(searchparamsInvitation).emit("receive-message", message)
    })

    socket.on("send-shapespark-location", (data) => {
      socket.broadcast
        .to(searchparamsInvitation)
        .emit("receive-shapespark-location", data)
    })
  })

  //notification
  socket.on("send-notification", (content) => {})
  //notification
})
