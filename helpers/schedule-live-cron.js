import VideoConferences from "../models/VideoConferenceModel.js"
import Customers from "../models/CustomerModel.js"
import Users from "../models/UserModel.js"
import { Op } from "sequelize"
import { Resend } from "resend"

const startingNowEmail = (title, schedId, userId, user) => {
  return `
    <div style="font-family: Arial, sans-serif; font-size: 16px; color: #333;">
        <h2>Your Scheduled Live Showing with is Starting Now</h2>
        <p>Your scheduled event <strong>${title}</strong> with <strong>${user}</strong> is starting now.</p>
        <p>Click the link here to join! 
            <a href="https://platform.1clickdesign.com/video_conference/${schedId}/${userId}">Scheduled live showing</a>
        </p>
        <p>We hope you enjoy your experience!</p>
        <p>Best regards,<br>1ClickDesign Team</p>
    </div>`
}

const thirtyMinutesFromNowEmail = (title, user) => {
  return `
        <div style="font-family: Arial, sans-serif; font-size: 16px; color: #333;">
            <h2>Your Schedule Reminder</h2>
            <p>This is a reminder that your scheduled event <strong>${title}</strong> with <strong>${user}</strong> is starting in 30 minutes.</p>
            <p>Another email will be sent to you containing the link to the the live showing in 30 minutes.</p>
            <p>Please make sure to be on time.</p>
            <p>Best regards,<br>1ClickDesign Team</p>
        </div>`
}

const schedule_live = async () => {
  const resend = new Resend("re_ZDjQoiZK_FV8BiJTwz338diygw5Brqf8v")

  try {
    const now = new Date()
    const thirtySecondsBefore = new Date(now.getTime() - 30 * 1000)
    const thirtySecondsAfter = new Date(now.getTime() + 30 * 1000)
    const thirtyMinutesFromNow = new Date(now.getTime() + 30 * 60000)

    const schedulesNow = await VideoConferences.findAll({
      where: {
        start_time: {
          [Op.gte]: thirtySecondsBefore,
          [Op.lte]: thirtySecondsAfter,
        },
      },
    })

    const schedulesInThirtyMinutes = await VideoConferences.findAll({
      where: {
        start_time: {
          [Op.eq]: thirtyMinutesFromNow,
        },
      },
    })

    if (schedulesNow.length > 0) {
      for (const schedule of schedulesNow) {
        const scheduleJSON = schedule.toJSON()

        const host = await Users.findOne({
          where: {
            id: scheduleJSON.host_id,
          },
        })

        const customer = await Customers.findOne({
          where: {
            id: scheduleJSON.customer_id,
          },
        })

        const hostEmail = await resend.emails.send({
          from: `Live showing <info@1clickdesign.us>`,
          to: host.email,
          subject: scheduleJSON.title,
          html: startingNowEmail(
            scheduleJSON.title,
            scheduleJSON.id,
            scheduleJSON.host_id,
            customer.full_name,
          ),
        })

        const customerEmail = await resend.emails.send({
          from: `Live showing <info@1clickdesign.us>`,
          to: customer.email,
          subject: scheduleJSON.title,
          html: startingNowEmail(
            scheduleJSON.title,
            scheduleJSON.id,
            scheduleJSON.customer_id,
            `${host.first_name} ${host.last_name}`,
          ),
        })
      }
    }

    if (schedulesInThirtyMinutes.length > 0) {
      for (const schedule of schedulesInThirtyMinutes) {
        const scheduleJSON = schedule.toJSON()

        const host = await Users.findOne({
          where: {
            id: scheduleJSON.host_id,
          },
        })

        const customer = await Customers.findOne({
          where: {
            id: scheduleJSON.customer_id,
          },
        })

        const hostEmail = await resend.emails.send({
          from: `Scheduled showing <info@1clickdesign.us>`,
          to: host.email,
          subject: scheduleJSON.title,
          html: thirtyMinutesFromNowEmail(
            scheduleJSON.title,
            scheduleJSON.start_time,
            customer.full_name,
          ),
        })

        const customerEmail = await resend.emails.send({
          from: `Scheduled showing <info@1clickdesign.us>`,
          to: customer.email,
          subject: scheduleJSON.title,
          html: thirtyMinutesFromNowEmail(
            scheduleJSON.title,
            scheduleJSON.start_time,
            `${host.first_name} ${host.last_name}`,
          ),
        })
      }
    }
  } catch (error) {
    console.error("Error fetching schedules:", error)
  }
}

export default schedule_live
