import InvitationsSchedule from "../models/InvitationsSchedule.js"
import Customers from "../models/CustomerModel.js"
import SentEmails from "../models/SentEmailModel.js"
import Users from "../models/UserModel.js"
import VirtualExperiences from "../models/VirtualExperienceModel.js"
import { Op } from "sequelize"
import Clients from "../models/ClientModel.js"
import got from "got";

const invitations_schedule_send = async () => {
    try {
        const now = new Date();
        const thirtySecondsBefore = new Date(now.getTime() - 30 * 1000);
        const thirtySecondsAfter = new Date(now.getTime() + 30 * 1000);

        const options = {
            timeZone: 'America/Los_Angeles', // UTC-7 during daylight saving time
            year: 'numeric',
            month: 'numeric',
            day: 'numeric',
            hour: 'numeric',
            minute: 'numeric',
            second: 'numeric',
        };

        const nowUTC7 = now.toLocaleString('en-US', options);
        const thirtySecondsBeforeUTC7 = thirtySecondsBefore.toLocaleString('en-US', options);
        const thirtySecondsAfterUTC7 = thirtySecondsAfter.toLocaleString('en-US', options);

        const scheduledInvites = await InvitationsSchedule.findAll({
            where: {
                schedule_send_date: {
                    [Op.gte]: thirtySecondsBeforeUTC7,
                    [Op.lte]: thirtySecondsAfterUTC7
                }
            },
            include: [
                {
                    model: Users,
                    as: 'schedule_user'
                },
                {
                    model: Users,
                    as: 'schedule_backup_user'
                },
                {
                    model: Clients
                },
                // {
                //   model: VirtualExperiences
                // },
                // {
                //   model: Customers
                // },
                // {
                //   model: SentEmails
                // }
            ]
        })


        if (!!scheduledInvites) {
            const schedArr = scheduledInvites.map(invite => invite.get({ plain: true }));
            const test = await got.post(
                "https://platform.1clickdesign.com/api/invitation/scheduled_walkthrough", {
                headers: {
                    "Access-Control-Allow-Credentials": "true",
                    "Access-Control-Allow-Origin": "*",
                    "Access-Control-Allow-Methods": "POST",
                },
                json: {
                    invitations: schedArr
                }
            }
            )

            if (test.ok) {
                schedArr.forEach(async (invite) => {
                    await InvitationsSchedule.destroy({
                        where: { id: invite.id },
                    });
                })
            }

        }

    } catch (error) {
        console.error('Error scheduling invitation', error);
    }
}

export default invitations_schedule_send