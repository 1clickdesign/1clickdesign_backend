import InvitationsSchedule from "../models/InvitationsSchedule.js"
import Customers from "../models/CustomerModel.js"
import SentEmails from "../models/SentEmailModel.js"
import Users from "../models/UserModel.js"
import VirtualExperiences from "../models/VirtualExperienceModel.js"
import InvitationFlythrough from "../models/InvitationFlythroughModel.js"
import { Op } from "sequelize"
import Clients from "../models/ClientModel.js"
import got from "got";
import Roles from "../models/RoleModel.js"

const schedule_flythrough_invitation = async () => {
    try {
        const now = new Date();
        const thirtySecondsBefore = new Date(now.getTime() - 30 * 1000);
        const thirtySecondsAfter = new Date(now.getTime() + 30 * 1000);

        const options = {
            timeZone: 'America/Los_Angeles', // UTC-7 during daylight saving time
            year: 'numeric',
            month: 'numeric',
            day: 'numeric',
            hour: 'numeric',
            minute: 'numeric',
            second: 'numeric',
        };

        const nowUTC7 = now.toLocaleString('en-US', options);
        const thirtySecondsBeforeUTC7 = thirtySecondsBefore.toLocaleString('en-US', options);
        const thirtySecondsAfterUTC7 = thirtySecondsAfter.toLocaleString('en-US', options);

        console.log('Now (UTC-7):', nowUTC7);
        // console.log('30 seconds before (UTC-7):', thirtySecondsBeforeUTC7);
        // console.log('30 seconds after (UTC-7):', thirtySecondsAfterUTC7);

        // console.log("Time test: ", thirtySecondsBefore, thirtySecondsAfter, "------------")

        const scheduledFlythroughs = await InvitationFlythrough.findAll({
            where: {
                schedule_send_date: {
                    [Op.gte]: thirtySecondsBeforeUTC7,
                    [Op.lte]: thirtySecondsAfterUTC7
                }
            },
            include: [
                {
                    model: Users,
                    as: 'invitation_flythrough_user',
                    include: [
                        { model: Roles }
                    ]
                },
                {
                    model: Users,
                    as: 'invitation_flythrough_backup_user',
                    include: [
                        { model: Roles }
                    ]
                },
                {
                    model: Clients
                },
            ]
        })

        console.log("------------", scheduledFlythroughs, "------------")


        if (!!scheduledFlythroughs && !!scheduledFlythroughs.length) {
            const schedArr = scheduledFlythroughs.map(invite => invite.get({ plain: true }));
            const test = await got.post(
                "https://platform.1clickdesign.com/api/invitation/scheduled_flythrough", {
                // "http://localhost:443/api/invitation/scheduled_flythrough", {
                headers: {
                    "Access-Control-Allow-Credentials": "true",
                    "Access-Control-Allow-Origin": "*",
                    "Access-Control-Allow-Methods": "POST",
                },
                json: {
                    invitations: schedArr
                }
            }
            )
        }

    } catch (error) {
        console.error('Error scheduling invitation', error);
    }
}

export default schedule_flythrough_invitation